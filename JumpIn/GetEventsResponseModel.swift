//
//  GetEventsResponseModel.swift
//  JumpIn
//
//  Created by Admin on 22.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetEventsResponseModel: ModelProtocol {
    
    var page: EventPageModel!
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        page <- map["events"]
    }
}


