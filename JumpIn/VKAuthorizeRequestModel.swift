//
//  VkAuthorizeRequestModels.swift
//  JumpIn
//
//  Created by Admin on 24.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class VKAuthorizeRequestModel: Mappable {
    
    var appId: String!
    var display = "page"
    var redirectLink: String!
    var permissions: [String]!
    var responseType = "token"
    var apiVersion = "5.52"
    
    init(appId: String, permissions: [String], redirectLink: String) {
        self.appId = appId
        self.permissions = permissions
        self.redirectLink = redirectLink
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        appId <- map["client_id"]
        display <- map["display"]
        redirectLink <- map["redirect_uri"]
        permissions <- map["scope"]
        responseType <- map["response_type"]
        apiVersion <- map["v"]
    }
}
