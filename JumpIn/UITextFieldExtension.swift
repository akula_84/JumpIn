//
//  UITextFieldExtension.swift
//  JumpIn
//
//  Created by IvanLazarev on 13/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit



fileprivate func getToolbarWithDoneButton(for view: UIView) -> UIToolbar {
    let toolBar = UIToolbar()
    toolBar.barStyle = .default
    toolBar.isTranslucent = true
    toolBar.tintColor = UIColor.black
    let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(view.resignFirstResponder))

    toolBar.setItems([space, doneButton], animated: false)
    toolBar.isUserInteractionEnabled = true
    toolBar.sizeToFit()
    return toolBar
}


extension UITextField {
    
    @discardableResult
    func addDoneButton() -> UIBarButtonItem {
        let toolbar = getToolbarWithDoneButton(for: self)
        inputAccessoryView = toolbar
        return toolbar.items!.last!
    }
}


extension UITextView {
    
    @discardableResult
    func addDoneButton() -> UIBarButtonItem {
        let toolbar = getToolbarWithDoneButton(for: self)
        inputAccessoryView = toolbar
        return toolbar.items!.last!
    }
}


extension UISearchBar {
    
    @discardableResult
    func addDoneButton() -> UIBarButtonItem {
        let toolbar = getToolbarWithDoneButton(for: self)
        inputAccessoryView = toolbar
        return toolbar.items!.last!
    }
}
