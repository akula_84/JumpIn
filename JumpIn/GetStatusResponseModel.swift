//
//  StatusResponseModel.swift
//  JumpIn
//
//  Created by Admin on 05.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetStatusResponseModel: ModelProtocol {
    
    enum UserAuthorizationStatus: String {
        case Authorized = "hello", NeedPhone = "phone", NeedInfo = "profile"
    }

    var status: UserAuthorizationStatus!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        //
        status <- map["view"]
    }
}
