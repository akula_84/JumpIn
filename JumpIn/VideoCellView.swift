//
//  VideoCellView.swift
//  JumpIn
//
//  Created by Admin on 22.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit
import AVKit


class VideoCellView: BaseCustomView {

    @IBOutlet private weak var thumbnailImageView: DownloadingImageView!
    @IBOutlet private weak var playIconImageView: UIImageView!
    //
    private var url: URL!
    
    
    func set(videoUrl: URL, thumbnail: UIImage? = nil, thumbnailLink link: String? = nil) {
        url = videoUrl
        if let image = thumbnail {
            thumbnailImageView.image = image
            return
        }
        thumbnailImageView.imageLink = link
        if link == nil { thumbnailImageView.image = nil }
    }
    
    func showVideo(in controller: UIViewController?, autoplay: Bool = true) {
        let player = AVPlayer(url: url)
        let playerVC = AVPlayerViewController()
        playerVC.player = player
        controller?.present(playerVC, animated: true, completion: {
            if autoplay { player.play() }
        })
    }
}
