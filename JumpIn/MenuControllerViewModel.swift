//
//  MenuControllerViewModel.swift
//  JumpIn
//
//  Created by IvanLazarev on 08/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper


class MenuControllerViewModel: BaseViewModel<GetProfileResponseModel> {

    var avatarLink: String?
    var nik: String!
    var status: String?
    var isPremium: Bool!

    required init(withModel model: GetProfileResponseModel) {
        avatarLink = model.avatar?.filepath
        nik = model.nickname
        status = model.status
        isPremium = model.premium
        super.init(withModel: model)
    }
    
    init(withModel model: GetUserInfoResponseModel) {
        avatarLink = model.userInfo.avatar?.filepath
        nik = model.userInfo.nickname
        status = model.userInfo.status
        isPremium = model.userInfo.isPremium!
        super.init()
    }
}
