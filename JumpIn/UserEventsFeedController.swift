//
//  UserEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit


class UserEventsFeedController: TabController {
    
    enum EventsType: Int {
        case own, last, participated
        
        init(feedType: ProfileController.EventsFeedType) {
            self.init(rawValue: feedType.rawValue)!
        }
    }

    init(userId: Int, eventsType: EventsType) {
        super.init()
        add(subcontrollers: [UserOwnEventsFeedController(userId: userId),
                             UserLastEventsFeedController(userId: userId),
                             UserParticipatedEventsFeedController(userId: userId)])
        title = NSLoc("user_events")
        setSelected(controllerIndex: eventsType.rawValue)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
