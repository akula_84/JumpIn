//
//  SlideshowPhotosControllerVM.swift
//  JumpIn
//
//  Created by IvanLazarev on 08/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper


class SlideshowPhotosControllerVM: BaseViewModel<PhotoWithLikesModelsPage> {
    
    var photos: [PhotoWithLikesViewModel] = []
    
    required init(withModel model: PhotoWithLikesModelsPage) {
        if let photoModels = model.models {
            photos = photoModels.map({ PhotoWithLikesViewModel(withModel: $0) })
        }
        super.init(withModel: model)
    }
    
    init(withPhotoViewModels photos: [PhotoWithLikesViewModel]) {
        self.photos = photos
        super.init()
    }
    
    convenience init(withPhotoModels photos: [PhotoWithLikesModel]) {
        self.init(withPhotoViewModels: photos.map({ PhotoWithLikesViewModel(withModel: $0) }))
    }
}
