//
//  BindSocialNetRequestModel.swift
//  JumpIn
//
//  Created by Admin on 01.09.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class BindSocialNetRequestModel {
    
    class TokenModel: Mappable {
        var accessToken: String!
        required init?(map: Map) { }
        init(accessToken: String) {
            self.accessToken = accessToken
        }
        func mapping(map: Map) { accessToken <- map["token"] }
    }
    
    var socialNetType: SocialNetType!
    var tokenModel: TokenModel
    init(accessToken: String, socialNet: SocialNetType) {
        self.socialNetType = socialNet
        self.tokenModel = TokenModel(accessToken: accessToken)
    }
}
