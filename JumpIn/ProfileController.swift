//
//  ProfileController.swift
//  JumpIn
//
//  Created by Admin on 18.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD
import UserNotifications
import StoreKit
import MessageUI


class ProfileController: BaseConfigurableController<ProfileControllerVM>, SKProductsRequestDelegate, SKPaymentTransactionObserver, UIScrollViewDelegate, MFMailComposeViewControllerDelegate {
    
    static let kProfile = "kProfile"
    //
    @IBOutlet fileprivate weak var userActionsView: UIView!
    @IBOutlet fileprivate weak var sendMessageButton: UIButton!
    @IBOutlet fileprivate weak var addToFriendsButton: UIButton!
    @IBOutlet fileprivate weak var deleteFromFriendsButton: UIButton!
    @IBOutlet fileprivate weak var photoCollectionView: UICollectionView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var getPremiumButton: UIButton!
    //
    @IBOutlet weak var nikLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var ownEventsCountLabel: UILabel!
    @IBOutlet weak var archivedEventsCountLabel: UILabel!
    @IBOutlet weak var participantEventsCountLabel: UILabel!
    @IBOutlet weak var avatarImageView: DownloadingImageView!
    @IBOutlet weak var socialNetsStackView: UIStackView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var acceptFriendshipRequestButton: UIButton!
    @IBOutlet weak var friendshipRequestDidSendedButton: UIButton!
    @IBOutlet weak var ownEventsButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var archivedEventsButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var userParticipatingEventsButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var deviceTokenLabel: UILabel!
    @IBOutlet weak var developersFeedbackButton: UIButton!
    @IBOutlet weak var complainButton: UIButton!
    //
    var dataLoader: DataLoadingManager<GetUserInfoRequestModel, ProfileController>!
    var photoCollectionViewManager: PhotoWithLikesCollectionViewManager!
    var transactionInProgress = false
    var userId: Int?
    
    enum EventsFeedType: Int {
        case own, last, participating
    }
    
    
    init(userId: Int? = nil) {
        self.userId = userId
        super.init(nibName: String(describing:type(of: self).self), bundle: nil)
        dataLoader = DataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getUserInfo as AnyObject)
        dataLoader.requestModel = GetUserInfoRequestModel(userId: userId)
        dataLoader.progressViewArea = .full
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        deviceTokenLabel.text = (UIApplication.shared.delegate as! AppDelegate).deviceToken
        automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 11, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        scrollView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        scrollView.contentOffset.y = -20
        acceptFriendshipRequestButton.titleLabel?.numberOfLines = 0
        acceptFriendshipRequestButton.titleLabel?.textAlignment = .center
        friendshipRequestDidSendedButton.titleLabel?.numberOfLines = 0
        friendshipRequestDidSendedButton.titleLabel?.textAlignment = .center
        statusButton.isEnabled = false
        setupPhotoCollectionManager()
        SKPaymentQueue.default().add(self)
        phoneButton.contentHorizontalAlignment = .left
        ownEventsButton.onClick = { [weak self] _ in self?.showEventsFeed(eventsType: .own) }
        archivedEventsButton.onClick = { [weak self] _ in self?.showEventsFeed(eventsType: .last) }
        userParticipatingEventsButton.onClick = { [weak self] _ in self?.showEventsFeed(eventsType: .participating) }
    }
    
    private func showEventsFeed(eventsType: EventsFeedType) {
        let targetVC: UIViewController
        if let id = userId {
            targetVC = UserEventsFeedController(userId: id, eventsType: UserEventsFeedController.EventsType(feedType: eventsType))
        }
        else {
            switch eventsType {
                case .own: targetVC = MyEventsFeedController()
                case .last: targetVC = MyLastEventsFeedController()
                case .participating:
                    let vc = MyEventsFeedController()
                    vc.setSelected(controllerIndex: 1)
                    targetVC = vc
            }
        }
        navigationController!.pushViewController(targetVC, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.setNavigationBarTransparent()
        navigationController!.navigationBar.applyDefaultTintColor()
        dataLoader.reloadData(handleError: false) { [weak self] (success, error) in
            guard !success else { return }
            self?.showError(error) { self?.backAction() }
        }
    }
    
    private func configureBottomButtons() {
        logoutButton.isHidden = viewModel.userType != .me
        developersFeedbackButton.isHidden = viewModel.userType != .me
        complainButton.isHidden = viewModel.userType == .me
    }
    
    override func refresh() {
        configureButtons()
        configureBottomButtons()
        nikLabel.text = viewModel.nick
        avatarImageView.image = #imageLiteral(resourceName: "user_no_image")
        avatarImageView.imageLink = viewModel.avatarLink
        statusButton.setTitle(viewModel.status, for: .normal)
        statusButton.isUserInteractionEnabled = viewModel.userType == .me
        if viewModel.userType == .me && viewModel.status.isEmpty {
            statusButton.setTitle(NSLoc("place_for_your_status"), for: .normal)
        }
        statusButton.titleLabel?.numberOfLines = 0
        firstNameLabel.text = viewModel.firstName
        firstNameLabel.superview!.isHidden = viewModel.firstName.isEmpty
        lastNameLabel.text = viewModel.lastName
        lastNameLabel.superview!.isHidden = viewModel.lastName.isEmpty
        sexLabel.text = viewModel.sex.localizedString
        birthdayLabel.text = viewModel.birthday
        birthdayLabel.superview!.isHidden = (viewModel.age.isEmpty || viewModel.birthday.isEmpty)
        ageLabel.text = viewModel.age
        ageLabel.superview!.isHidden = viewModel.age.isEmpty
        cityLabel.text = viewModel.city
        cityLabel.superview!.isHidden = viewModel.city.isEmpty
        phoneButton.setTitle(viewModel.phone, for: .normal)
        phoneButton.superview!.isHidden = viewModel.phone.isEmpty
        ownEventsCountLabel.text = String(describing: viewModel.ownEvents)
        archivedEventsCountLabel.text = String(describing: viewModel.archivedEvents)
        participantEventsCountLabel.text = String(describing: viewModel.participantEvents)
        reloadSocialNetViews(withSocialNets: viewModel.socialNets)
        photoCollectionView.superview!.isHidden = viewModel.photos.isEmpty
        photoCollectionViewManager.dataSource = viewModel.photos
        photoCollectionViewManager.reloadData()
        if viewModel.userType == .me {
            NotificationCenter.default.post(name: .profileUpdated, object: nil, userInfo: [type(of: self).kProfile: dataLoader.model])
        }
    }
    
    private func reloadSocialNetViews(withSocialNets socialNets: [SocialNetViewModel]) {
        clearSocialNetViews()
        socialNets.forEach {
            let subcontroller = SocialNetViewController(withViewModel: $0)
            self.addChildViewController(subcontroller)
            self.socialNetsStackView.addArrangedSubview(subcontroller.view)
            subcontroller.didMove(toParentViewController: self)
        }
        socialNetsStackView.layoutIfNeeded()
    }
    
    private func clearSocialNetViews() {
        childViewControllers.forEach {
            if let socialNetController = $0 as? SocialNetViewController {
                self.socialNetsStackView.removeArrangedSubview(socialNetController.view)
                socialNetController.view.removeFromSuperview()
                socialNetController.removeFromParentViewController()
            }
        }
    }
    
    private func configureButtons() {
        resetButtons()
        let type = self.viewModel.userType
        if type != .me {
            userActionsView.isHidden = false
            sendMessageButton.isHidden = false
        }
        switch type {
        case .me:
            getPremiumButton.superview!.isHidden = viewModel.isPremium!
            setupNavBarButtons()
            statusButton.isEnabled = true
            break
        case .friend:
            deleteFromFriendsButton.isHidden = false
            break
        case .stranger:
            addToFriendsButton.isHidden = false
            break
        case .iWantToBeFriendsWith:
            friendshipRequestDidSendedButton.isHidden = false
            break
        case .wantToBeFriends:
            acceptFriendshipRequestButton.isHidden = false
            break
//        case .myPremium:
//            break
        }
    }
    
    private func resetButtons() {
        navigationItem.rightBarButtonItem = nil
        statusButton.isEnabled = false
        userActionsView.isHidden = true
        sendMessageButton.isHidden = true
        getPremiumButton.superview!.isHidden = true
        deleteFromFriendsButton.isHidden = true
        addToFriendsButton.isHidden = true
        acceptFriendshipRequestButton.isHidden = true
        friendshipRequestDidSendedButton.isHidden = true
    }
    
    private func setupNavBarButtons() {
        let editBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_pen"), style: .plain, target: self, action: #selector(editAction))
        navigationItem.rightBarButtonItem = editBarButton
    }

    @objc private func editAction() {
        navigationController?.pushViewController(ProfileEditingController(), animated: true)
    }
    
    @IBAction func complainAction() {
        HUD.show(.progress)
        let model = ComplainRequestModel(withObjectType: .user, objectId: userId ?? UserInfoService.shared.userId)
        ServerApi.shared.complain(withModel: model, completion: { HUD.flash(.success) }, errorBlock: { [weak self] error, _ in
            self?.showError(withDescription: error?.description ?? NSLoc("unable_to_send_complaint"))
        })
    }
    
    @IBAction private func callAction() {
        let app = UIApplication.shared
        var phone = viewModel.phone.replacingOccurrences(of: " ", with: "")
        phone = phone.replacingOccurrences(of: "-", with: "")
        phone = phone.replacingOccurrences(of: "+", with: "")
        let url = "tel://\(phone)"
        if let phoneURL = URL(string: url), app.canOpenURL(phoneURL) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(phoneURL)
            } else {
                UIApplication.shared.openURL(phoneURL)
            }
        }
    }
    
    @IBAction private func addToFriendsAction(_ sender: Any) {
        let userId = dataLoader.requestModel?.userId
        HUD.show(.progress)
        ServerApi.shared.friendsAdd(withId: userId!, completion: { [weak self] in
            guard let `self` = self else { return }
            HUD.hide()
            self.dataLoader.reloadData(animated: true) { success, error in
                if success {
                    HUD.flash(.success)
                } else {
                    self.showError(error)
                }
            }
        }) { (error: MainURLEngine.Error?, _: HTTPURLResponse?) in
            self.showError(error)
        }
    }

    @IBAction private func deleteFromFriendsAction(_ sender: Any) {
        let userId = dataLoader.requestModel?.userId
        HUD.show(.progress)
        ServerApi.shared.friendsRemove(withId: userId!, completion: { [weak self] in
            guard let `self` = self else { return }
            HUD.hide()
            self.dataLoader.reloadData(animated: true) { success, error in
                if success {
                    HUD.flash(.success)
                } else {
                    self.showError(error)
                }
            }
        }) { (error: MainURLEngine.Error?, _: HTTPURLResponse?) in
            self.showError(error)
        }
    }
    
    @IBAction private func acceptFriendshipRequestAction() {
        AlertsPresentator.presentAlertAddUserToFriends(in: self, withUserName: viewModel.nick) { [weak self] yes in
            guard let `self` = self else { return }
            HUD.show(.progress)
            ServerApi.shared.acceptFriends(withId: self.dataLoader.model.userInfo.id, completion: {
                self.dataLoader.reloadData()
            }, errorBlock: { (error, _) in
                self.showError(error)
            })
        }
    }
    
    @IBAction private func editStatusAction(_ sender: Any) {
        let alertController = UIAlertController(title: NSLoc("enter_your_new_status"), message: nil, preferredStyle: UIAlertControllerStyle.alert)
        var statusTextField: UITextField!
        alertController.addTextField { (textField : UITextField) -> Void in
            textField.placeholder = NSLoc("your_status")
            textField.autocapitalizationType = .sentences
            statusTextField = textField
        }
        let cancelAction = UIAlertAction(title: NSLoc("cancel"), style: UIAlertActionStyle.cancel, handler: nil)
        let saveAction = UIAlertAction(title: NSLoc("save"), style: UIAlertActionStyle.default) { _ in
            let status = statusTextField.text ?? ""
            ServerApi.shared.updateProfile(status: status, completion: { [weak self] in
                HUD.hide()
                self?.dataLoader.reloadData()
            }) { [weak self] (error, _) in
                self?.showError(error)
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true)
    }

    @IBAction private func getPremiumAction() {
        let alert = UIAlertController(title: NSLoc("choose_a_payment_method"), message: nil, preferredStyle: .actionSheet)
        let inviteFriendsAction = UIAlertAction(title: NSLoc("invite_sto_friends"), style: .default) { _ in
            InviteFriendsToAppController.show(usingNavController: self.navigationController!)
        }
        let buyAction = UIAlertAction(title: NSLoc("buy"), style: UIAlertActionStyle.default) { (action) -> Void in
            let productIDs = ["com.swc.jumpin.buyuser"]
            
            let productIdentifiers = NSSet(array: productIDs)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
            
        }
        let cancelAction = UIAlertAction(title: NSLoc("cancel"), style: .cancel, handler: nil)
        alert.addAction(buyAction)
        alert.addAction(inviteFriendsAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }

    private func setupPhotoCollectionManager() {
        photoCollectionViewManager = PhotoWithLikesCollectionViewManager(with: photoCollectionView)
        photoCollectionViewManager.setup()
        photoCollectionViewManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let index = self.photoCollectionViewManager.indexOfDataSourceItem(forCellAt: indexPath)
            let photoId = self.dataLoader.model.userInfo.photos![index].id!
            let ownerId = self.dataLoader.model.userInfo.id!
            let controller = SlideshowPhotosController(selectedPhotoId: photoId, ownerType: .users, ownerId: ownerId)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction private func showAllPhotos() {
        let controller = AllPhotosController(ownerType: .users, ownerId: dataLoader.model.userInfo.id)
        navigationController!.pushViewController(controller, animated: true)
    }
    
    @IBAction private func logoutAction() {
        JUMAuthManager.logOut()
    }
    
    @IBAction private func connectWithDevelopersAction() {
        guard MFMailComposeViewController.canSendMail() else {
            AlertsPresentator.presentAlertBindMailToSendMessage(in: self)
            return
        }
        let mailController = MFMailComposeViewController()
        mailController.mailComposeDelegate = self
        mailController.setToRecipients(["director@atkterminal.ru"])
        present(mailController, animated: true)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction private func sendMessage() {
        let model = dataLoader.model!
        let userId = model.userInfo.id!
        let userAvatar = model.userInfo.avatar?.filepath
        navigationController!.pushViewController(DialogDetailedController(userId: userId, userAvatarLink: userAvatar), animated: true)
    }

    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count != 0 {
            self.showActions(response.products.first!)
        } else {
            print("There are no products.")
            STRouter.showError(withDescription: "There are no products.")
        }
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                ServerApi.shared.makeProfilePremium(completion: nil, errorBlock: nil)
            case SKPaymentTransactionState.failed:
                print("Transaction Failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                STRouter.showError(withDescription: "Transaction Failed")
                
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    func showActions(_ product: SKProduct) {
        if transactionInProgress {
            return
        }
        let actionSheetController = UIAlertController(title: product.localizedTitle, message:product.localizedDescription , preferredStyle: UIAlertControllerStyle.actionSheet)
        let buyAction = UIAlertAction(title: NSLoc("buy"), style: UIAlertActionStyle.default) { (action) -> Void in
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
            self.transactionInProgress = true
            
        }
        let cancelAction = UIAlertAction(title: NSLoc("cancel"), style: UIAlertActionStyle.cancel)
        actionSheetController.addAction(buyAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true)
    }
}
