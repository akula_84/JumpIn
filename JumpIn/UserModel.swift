//
//  GeneralShotUserModel.swift
//  JumpIn
//
//  Created by Admin on 15.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


class SmallUserModel: ModelProtocol {
    
    var id: Int!
    var nick: String?
    var avatar: PhotoWithLikesModel?
    var firstName: String!
    var lastName: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        nick <- map["nickname"]
        avatar <- map["avatar"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
    }
}


class ShortUserModel: SmallUserModel {

    var mainSocialNet: SocialNetModel?
    var isOnline: Bool?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        mainSocialNet <- map["first_social"]
        isOnline <- map["is_online"]
    }
}


class ShortUserModelsPage: PageModel<ShortUserModel> {}

