//
//  SearchController.swift
//  JumpIn
//
//  Created by Admin on 24.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class SearchController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet private weak var scopeControl: UISegmentedControl!
    @IBOutlet private var bottomSpacingLayout: NSLayoutConstraint!
    //
    var selectedScopeIndex: Int { return self.scopeControl.selectedSegmentIndex }
    private var scopes: [String]?
    var searchBarDelegate: UISearchBarDelegate?
    private var bottomSpacing: CGFloat
    private lazy var methodDescriptionsOfUISearchBarDelegate: [objc_method_description] = {
        //
        var count = UInt32()
        let pointer = protocol_copyMethodDescriptionList(UISearchBarDelegate.self, true, true, &count)
        let buff = UnsafeBufferPointer(start: pointer, count: Int(count))
        let array = Array(buff)
        free(pointer)
        return array
    }()
    
    
    init(scopes: [String]? = nil, bottomSpacing: CGFloat = 0) {
        //
        self.scopes = scopes
        self.bottomSpacing = bottomSpacing
        super.init(nibName: "SearchBar", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        //
        super.viewDidLoad()
        bottomSpacingLayout.constant = bottomSpacing
        if let scopes = scopes {
            //
            setScopes(withTitles: scopes, animated: false)
            scopeControl.addTarget(self, action: #selector(onChangeScope), for: .touchUpInside)
            scopeControl.selectedSegmentIndex = 0
        }
        else {
            scopeControl.isHidden = true
        }
    }
    
    func setScopes(withTitles titles: [String], animated: Bool) {
        //
        scopeControl.removeAllSegments()
        var i: Int = 0
        for title in titles {
            //
            scopeControl.insertSegment(withTitle: title, at: i, animated: animated)
            i += 1
        }
    }
    
    @objc private func onChangeScope() {
        searchBarDelegate?.searchBar?(searchBar, selectedScopeButtonIndexDidChange:scopeControl.selectedSegmentIndex)
    }
    
    
    //***** Forwarding
    
    override func responds(to aSelector: Selector!) -> Bool {
        //
        let (haveASelector, _) = forwardingInfo(for: aSelector)
        return haveASelector
    }
    
    override func forwardingTarget(for aSelector: Selector!) -> Any? {
        //
        let (_, target) = forwardingInfo(for: aSelector)
        return target
    }
    
    private func forwardingInfo(for aSelector: Selector!) -> (Bool, Any?) {
        //
        if methodDescriptionsOfUISearchBarDelegate.contains(where: {$0.name == aSelector}) {
            //
            if super.responds(to:aSelector) { return (true, self) }
            else {
                guard let delegate = searchBarDelegate else { return (false, nil) }
                return (delegate.responds(to:aSelector), delegate)
            }
        }
        else { return (super.responds(to:aSelector), self) }
    }
    
    //***** UISearchBarDelegate
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }
}
