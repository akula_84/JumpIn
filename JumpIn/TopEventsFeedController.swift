//
//  TopEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 25.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SideMenu


class TopEventsFeedController: EventsFeedController {
    
    let isSearching: Bool
    let withFilter: Bool
    
    init(isSearching: Bool = false, withFilter: Bool) {
        self.isSearching = isSearching
        self.withFilter = withFilter
        super.init(withEventsType: .generalCurrent)
        self.initConfig.shouldAddBackButton = isSearching
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isSearching ? title =  NSLoc("search") : setupTitleView()
        setupNavBarButtons()
        JUMPushManager.showPushIfNeed()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SideMenuManager.repairGestureRecognizers()
        updateCityLabel()
    }
    
    private func updateCityLabel() {
        let label = tableManager.tableInitConfig.customHeaderView as! UILabel
        let text = NSMutableAttributedString()
        let font = UIFont.systemFont(ofSize: 14)
        let str1 = NSAttributedString(string: NSLoc("your_city"), attributes: [.font : font, .foregroundColor : UIColor.gray])
        let str2 = NSAttributedString(string: UserInfoService.shared.city, attributes: [.font : font, .foregroundColor : UIColor.black])
        text.append(str1)
        text.append(str2)
        label.attributedText = text
        label.sizeToFit()
        tableManager.reloadHeaderViews()
    }
    
    override func installTableManager() {
        super.installTableManager()
        tableManager.tableInitConfig.customHeaderView = UILabel()
        tableManager.tableInitConfig.customHeaderViewInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
    }
    
    private func setupTitleView() {
        let titleImageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 66.5, height: 31.0)))
        titleImageView.contentMode = .scaleAspectFit
        titleImageView.image = #imageLiteral(resourceName: "small_logo")
        navigationItem.titleView = titleImageView
    }
    
    private func setupNavBarButtons() {
        var rightBarButtons: [UIBarButtonItem] = []
        if (!isSearching) {
            let menuBarBatton = UIBarButtonItem.createUsingUIButton(withImage: #imageLiteral(resourceName: "ic_menu"), target: self, selector: #selector(toggleMenu))
            navigationItem.leftBarButtonItem = menuBarBatton
            let categoryButton = UIBarButtonItem.createUsingUIButton(withImage: #imageLiteral(resourceName: "ic_categories"), target: self, selector: #selector(categoriesAction))
            let history = UIBarButtonItem.createUsingUIButton(withImage: #imageLiteral(resourceName: "ic_history"), target: self, selector: #selector(historyAction))
            rightBarButtons.append(history)
            rightBarButtons.append(categoryButton)
        }
        if withFilter {
            let filterBarButton = UIBarButtonItem.createUsingUIButton(withImage: #imageLiteral(resourceName: "ic_filter"), target: self, selector: #selector(filterAction))
            rightBarButtons.append(filterBarButton)
        }
        navigationItem.rightBarButtonItems = rightBarButtons
    }
    
    @objc private func historyAction() {
        navigationController?.pushViewController(LastTopEventsFeedController(), animated: true)
    }
    
    @objc private func categoriesAction() {
        navigationController?.pushViewController(CategoryListController(isCurrentEvents: true), animated: true)
    }
    
    @objc private func filterAction(sender: UIBarButtonItem) {
        let filter = EventsFilterController(withModel: loader.requestModel, filterButton: sender)
        navigationController?.pushViewController(filter, animated: true)
    }
}
