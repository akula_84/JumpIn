//
//  GetUserInfoResponseModel.swift
//  JumpIn
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


class GetUserInfoResponseModel: ModelProtocol {
    
    required init?(map: Map) {}
    
    class UserInfo: BaseProfileModel {

        //var nick: String!
        var city: String!
        var avatar: PhotoWithLikesModel?
        var phone: String?
        var photos: [PhotoWithLikesModel]?
        var socialNets: [SocialNetModel]?
        var status: String?
        var isPremium: Bool?
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            //nick <- map["nickname"]
            avatar <- map["avatar"]
            phone <- map["phone"]
            photos <- map["photos"]
            socialNets <- map["socials"]
            status <- map["status"]
            city <- map["city"]
            isPremium <- map["premium"]
        }
    }
    
    class AdditionalData: Mappable {
        
        class EventsCount: Mappable {
            required init?(map: Map) { }
            var own, archived, participant: Int!
            func mapping(map: Map) {
                own <- map["own"]
                archived <- map["archive"]
                participant <- map["participant"]
            }
        }
        
        enum UserType: String {
            case me, friend, not_friend, requested_by_user, requested_by_me
        }
        
        required init?(map: Map) { }
        
        var eventsCount: EventsCount!
        var age: String?
        var userType: UserType!
        
        func mapping(map: Map) {
            eventsCount <- map["events_count"]
            age <- map["age"]
            userType <- map["user_status"]
        }
    }
    
    var userInfo: UserInfo!
    var additionalData: AdditionalData!
    
    func mapping(map: Map) {
        userInfo <- map["user"]
        additionalData <- map["data"]
    }
}
