//
//  KeyboardMonitoring2.swift
//  JumpIn
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class KMScrollView: UIScrollView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        subscribeToKMService()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        subscribeToKMService()
    }
    
    private func subscribeToKMService() {
        KeyboardMonitoringService.shared.addToSubscribers(scrollView: self)
    }
}

class KMTableView: UITableView {
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        subscribeToKMService()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        subscribeToKMService()
    }
    
    private func subscribeToKMService() {
        KeyboardMonitoringService.shared.addToSubscribers(scrollView: self)
    }
}

class KMTextField: UITextField, KMTextViewProtocol {
    
    @IBInspectable var distanceFromKeyboard: CGFloat = 5
    
    override func becomeFirstResponder() -> Bool {
        NotificationCenter.default.post(name: KeyboardMonitoringService.kTextFieldWillBeginEditing, object: nil, userInfo: [KeyboardMonitoringService.kTextField: self])
        return super.becomeFirstResponder()
    }
}

class KMTextView: UITextView, KMTextViewProtocol {
    
    @IBInspectable var distanceFromKeyboard: CGFloat = 0
    
    override func becomeFirstResponder() -> Bool {
        NotificationCenter.default.post(name: KeyboardMonitoringService.kTextFieldWillBeginEditing, object: nil, userInfo: [KeyboardMonitoringService.kTextField: self])
        return super.becomeFirstResponder()
    }
}

protocol KMTextViewProtocol: class {
    var distanceFromKeyboard: CGFloat { get set }
}


class KeyboardMonitoringService {
    
    private class Subscriber {
        weak var scrollView: UIScrollView?
        var isEmpty: Bool { return scrollView == nil }
        private var oldInsets: UIEdgeInsets?
        
        init(scrollView: UIScrollView) {
            self.scrollView = scrollView
        }
        
        func storeInsets() {
            oldInsets = scrollView?.contentInset
        }
        
        func restoreInsets() {
            scrollView?.contentInset = oldInsets!
        }
    }
    
    static let kTextFieldWillBeginEditing = Notification.Name("kTextFieldWillBeginEditingNotification")
    static let kTextField = "kTextField"
    static let shared = KeyboardMonitoringService()
    //
    private var activeTextField: (KMTextViewProtocol & UIView)?
    private var activeSubscriber: Subscriber? {
        willSet {
            guard newValue !== activeSubscriber else { return }
            activeSubscriber?.restoreInsets()
        }
        didSet { activeSubscriber?.storeInsets() }
    }
    private var lastKeyboardFrame: CGRect!
    private var subscribers: [Subscriber] = []
    private var isKeyboardShowing = false
    
    private init() {
        let center = NotificationCenter.default
        let textFieldWillBeginEditing = type(of: self).kTextFieldWillBeginEditing
        center.addObserver(self, selector: #selector(self.textFieldWillBeginEditing), name: textFieldWillBeginEditing, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func textFieldWillBeginEditing(notification: Notification) {
        removeEmptySubscribers()
        let textField = notification.userInfo![type(of: self).kTextField] as! (KMTextViewProtocol & UIView)
        guard let scrollView = getSuperScrollView(ofTextView: textField), let subscriber = subscriber(forScrollView: scrollView) else {
            fatalError("TextField must be into subscribed scrollView")
        }
        activeTextField = textField
        activeSubscriber = subscriber
        if isKeyboardShowing {
            onChangeTextfieldFocus()
        }
    }
    
    private func getSuperScrollView(ofTextView textView: (UIView & KMTextViewProtocol)) -> UIScrollView? {
        var superview = textView.superview
        while superview != nil {
            if let scrollView = superview as? UIScrollView { return scrollView }
            superview = superview!.superview
        }
        return nil
    }
    
    private func subscriber(forScrollView scrollView: UIScrollView) -> Subscriber? {
        return subscribers.filter({ !$0.isEmpty && $0.scrollView === scrollView }).first
    }
    
    private func resetActiveControls() {
        activeTextField = nil
        activeSubscriber = nil
    }
    
    private func removeEmptySubscribers() {
        subscribers = subscribers.filter({ !$0.isEmpty })
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        lastKeyboardFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        onChangeTextfieldFocus()
        isKeyboardShowing = true
    }
    
    private func onChangeTextfieldFocus() {
        if let scrollView = activeSubscriber?.scrollView, let textField = activeTextField {
            let scrollViewMaxY = scrollView.convert(scrollView.bounds, to: nil).maxY
            let keyboardMinY = lastKeyboardFrame.minY
            if keyboardMinY < scrollViewMaxY, !isKeyboardShowing {
                scrollView.contentInset.bottom += scrollViewMaxY - keyboardMinY
                var textFieldBounds = textField.bounds
                textFieldBounds.size.height += textField.distanceFromKeyboard
                scrollView.scrollRectToVisible(textField.convert(textFieldBounds, to: scrollView), animated: true)
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        isKeyboardShowing = false
        activeSubscriber?.restoreInsets()
        resetActiveControls()
    }
    
    func addToSubscribers(scrollView: UIScrollView) {
        subscribers.append(Subscriber(scrollView: scrollView))
    }
}
