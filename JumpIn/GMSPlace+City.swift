//
//  GMSPlace+City.swift
//  JumpIn
//
//  Created by Admin on 29.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import GooglePlacePicker


extension GMSPlace {
    
    var city: String? {
        return addressComponents?.filter({ $0.type == "locality" }).first?.name
    }
}
