//
//  LastTopEventsFeedTableSubcontroller.swift
//  JumpIn
//
//  Created by Admin on 29.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class LastTopEventsFeedTableSubcontroller: EventsFeedController {
    
    let withSearchBar: Bool
    
    init(withSearchBar: Bool = false) {
        self.withSearchBar = withSearchBar
        super.init(withEventsType: .generalLast)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func installTableManager() {
        super.installTableManager()
        tableManager.tableInitConfig.shouldSetupSearchBar = withSearchBar
    }
}
