//
//  VideoTrimmerController.swift
//  JumpIn
//
//  Created by Admin on 30.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices
import PKHUD


class VideoTrimmerController: UIViewController {
    
    enum OwnError: Error {
        case noVideoSelected, tooSmallVideo
    }
    
    @IBOutlet private weak var playerView: UIView!
    @IBOutlet private weak var videoRangeSlider: ABVideoRangeSlider!
    @IBOutlet private weak var playButton: UIButton!
    //
    private var player: AVPlayer!
    private var timescaleOfLastSelectedVideo: CMTimeScale!
    private var error: OwnError?
    private var playbackTimeCheckerTimer: Timer?
    private var isPlaying = false {
        didSet {
            playButton.isSelected = isPlaying
            if isPlaying {
                guard videoUrl != nil else { return }
                player.play()
                startPlaybackTimeChecker()
            }
            else {
                player.pause()
                stopPlaybackTimeChecker()
            }
        }
    }
    private var videoUrl: URL! {
        didSet {
            if videoUrl != nil, isViewLoaded { onChangeVideo(url: videoUrl) }
        }
    }
    var onTrimVideo: ((URL)->Void)?
    
    
    class func presented(in controller: UIViewController?, withVideoUrl url: URL?) -> VideoTrimmerController {
        let trimmer = VideoTrimmerController(videoUrl: url)
        let navController = UINavigationController(rootViewController: trimmer)
        navController.navigationBar.barStyle = .blackOpaque
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
        controller?.present(navController, animated: true)
        return trimmer
    }
    
    private init(videoUrl: URL?) {
        self.videoUrl = videoUrl
        super.init(nibName: "VideoTrimmerController", bundle: nil)
        title = NSLoc("crop_video")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVideoRangeSlider()
        setupPlayButton()
        setupNavBarButtons()
        if videoUrl == nil {
            error = .noVideoSelected
        }
        else {
            onChangeVideo(url: videoUrl)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handleErrorsIfNeed()
    }
    
    private func handleErrorsIfNeed() {
        guard let error = self.error else { return }
        switch error {
        case .noVideoSelected:
            selectVideoTapped()
        case .tooSmallVideo:
            videoUrl = nil
            AlertsPresentator.presentAlertTooSmallVideo(in: self, completion: { [weak self] in self?.selectVideoTapped() })
        }
    }
    
    private func setupNavBarButtons() {
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        let cancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))
        navigationItem.leftBarButtonItem = cancel
        navigationItem.rightBarButtonItem = done
    }
    
    @objc private func doneTapped() {
        let range = CMTimeRange(start: cmtime(withSeconds: videoRangeSlider.startTime),
                                end: cmtime(withSeconds: videoRangeSlider.endTime))
        saveAndTrimVideo(timeRange: range) { [weak self] url in
            guard let url = url else {
                AlertsPresentator.presentAlertCantSaveVideo(in: self)
                return
            }
            self?.dismiss(animated: true, completion: { self?.onTrimVideo?(url) })
        }
    }
    
    @objc private func cancelTapped() {
        dismiss(animated: true)
    }
    
    @IBAction private func selectVideoTapped() {
        AlertsPresentator.presentVideoPickerWithPreChooseSource(in: self) { [weak self] in
            guard let picker = $0 else {
                if self?.videoUrl == nil { self?.dismiss(animated: true) }
                return
            }
            picker.delegate = self
        }
    }
    
    private func setupPlayButton() {
        let playIcon = #imageLiteral(resourceName: "ic_play_arrow_48pt").withRenderingMode(.alwaysTemplate)
        let pauseIcon = #imageLiteral(resourceName: "ic_pause_48pt").withRenderingMode(.alwaysTemplate)
        playButton.setBackgroundImage(playIcon, for: .normal)
        playButton.setBackgroundImage(pauseIcon, for: .selected)
    }
    
    private func setupVideoRangeSlider() {
        videoRangeSlider.delegate = self
        videoRangeSlider.startTimeView.backgroundView = generateStartEndTimeView()
        videoRangeSlider.startTimeView.marginLeft = 2.0
        videoRangeSlider.startTimeView.marginRight = 2.0
        videoRangeSlider.startTimeView.timeLabel.textColor = .white
        videoRangeSlider.endTimeView.backgroundView = generateStartEndTimeView()
        videoRangeSlider.endTimeView.marginLeft = 2.0
        videoRangeSlider.endTimeView.marginRight = 2.0
        videoRangeSlider.endTimeView.timeLabel.textColor = .white
        videoRangeSlider.startIndicator.isUserInteractionEnabled = false
        videoRangeSlider.endIndicator.isUserInteractionEnabled = false
    }
    
    private func generateStartEndTimeView() -> UIView {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        customView.backgroundColor = .black
        customView.alpha = 0.5
        customView.layer.borderColor = UIColor.black.cgColor
        customView.layer.borderWidth = 1.0
        customView.layer.cornerRadius = 8.0
        return customView
    }
    
    func onChangeVideo(url: URL) {
        let asset = AVAsset(url: url)
        guard asset.duration.seconds >= 4 else {
            error = .tooSmallVideo
            return
        }
        error = nil
        videoRangeSlider.setVideoURL(videoURL: url)
        videoRangeSlider.minSpace = 15
        videoRangeSlider.maxSpace = 15
        videoRangeSlider.setStartPosition(seconds: 0)
        videoRangeSlider.setEndPosition(seconds: 15)
        videoRangeSlider.updateProgressIndicator(seconds: 0)
        reloadVideoPlayer(with: url, playerView: playerView)
        timescaleOfLastSelectedVideo = asset.duration.timescale
    }
    
    private func reloadVideoPlayer(with url: URL, playerView: UIView) {
        let playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        
        NotificationCenter.default.addObserver(self, selector: #selector(itemDidFinishPlaying(_:)),
                                               name: .AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        player.seek(to: cmtime(withSeconds: videoRangeSlider.startTime))
        isPlaying = false
    }
    
    func startPlaybackTimeChecker() {
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector: #selector(onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }
    
    func stopPlaybackTimeChecker() {
        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
    @objc func onPlaybackTimeChecker() {
        let playbackTime = player.currentTime().seconds
        videoRangeSlider.updateProgressIndicator(seconds: playbackTime)
        if playbackTime >= videoRangeSlider.endTime {
            player.seek(to: cmtime(withSeconds: videoRangeSlider.startTime), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            isPlaying = false
        }
    }
    
    private func cmtime(withSeconds seconds: Double) -> CMTime {
        return CMTime(seconds: seconds, preferredTimescale: timescaleOfLastSelectedVideo)
    }
    
    private func saveAndTrimVideo(timeRange: CMTimeRange, completion: ((URL?)->Void)?)
    {
        let manager = FileManager.default
        guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask,
                                                       appropriateFor: nil, create: true) else { return }
        let url = videoUrl!
        let asset = AVAsset(url: url)
        var outputUrl = documentDirectory
        guard ((try? manager.createDirectory(at: outputUrl, withIntermediateDirectories: true, attributes: nil)) != nil) else {
            return
        }
        let df = DateFormatter()
        df.dateFormat = "ddMMyyyyHHmmss"
        let fileName = "\(df.string(from: Date())).mp4"
        outputUrl = outputUrl.appendingPathComponent(fileName)
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
        exportSession.outputURL = outputUrl
        exportSession.outputFileType = AVFileType.mp4
        exportSession.timeRange = timeRange
        HUD.show(.progress)
        exportSession.exportAsynchronously {
            var url: URL?
            switch exportSession.status {
            case .completed:
                url = outputUrl
            case .failed:
                print("failed \(String(describing: exportSession.error))")
            case .cancelled:
                print("cancelled \(String(describing: exportSession.error))")
            default: break
            }
            DispatchQueue.main.async {
                HUD.hide()
                completion?(url)
            }
        }
    }
    
    class func clearLocalCache() {
        let manager = FileManager.default
        guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask,
                                                       appropriateFor: nil, create: true) else { return }
        guard let urls = try? manager.contentsOfDirectory(at: documentDirectory, includingPropertiesForKeys: nil) else { return }
        urls.forEach { try? manager.removeItem(at: $0) }
        print("VideoTrimmerController: clear cache successfuly")
    }
    
    @IBAction func playPauseAction() {
        isPlaying = !isPlaying
    }
}


extension VideoTrimmerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
        videoUrl = nil
        error = .tooSmallVideo
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        videoUrl = info[UIImagePickerControllerMediaURL] as! URL
        picker.dismiss(animated: true)
    }
}


extension VideoTrimmerController: ABVideoRangeSliderDelegate {
    func didChangeValue(videoRangeSlider: ABVideoRangeSlider, startTime: Float64, endTime: Float64) {
        //
    }
    
    func indicatorDidChangePosition(videoRangeSlider: ABVideoRangeSlider, position: Float64) {
        isPlaying = false
        player.seek(to: cmtime(withSeconds: position), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
}


extension ABVideoRangeSlider {
    
    var startTime: Double {
        return Double(startPercentage/100) * Double(duration)
    }
    
    var endTime: Double {
        return Double(endPercentage/100) * Double(duration)
    }
}


