//
//  ComplainRequestModel.swift
//  JumpIn
//
//  Created by Ar4ibald Fox on 11.04.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import ObjectMapper


class ComplainRequestModel: Mappable {

    enum ObjectType: String {
        case event, user, message, photo, video
    }
    
    
    var objectType: ObjectType!
    var objectId: Int!
    var text = ""
    
    init(withObjectType type: ObjectType, objectId id: Int, text: String = "") {
        objectType = type
        objectId = id
        self.text = text
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        objectId <- map["object_id"]
        objectType <- map["type"]
        text <- map["text"]
    }
}
