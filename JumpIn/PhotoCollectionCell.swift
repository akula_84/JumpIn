//
//  PhotoCollectionCell.swift
//  JumpIn
//
//  Created by IvanLazarev on 04/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class ShowPhotosCollectionViewManager: CollectionViewManager<ShowPhotosCollectionCellManager> {}


class ShowPhotosCollectionCellManager: BaseViewManager<PhotoWithLikesViewModel, PhotoCollectionCell> {
    override func refresh() {
        view.imageView.image = #imageLiteral(resourceName: "no_image")
        view.imageView.imageLink = viewModel.link
    }
}


class PhotoCollectionCell: UICollectionViewCell, Configurable2 {

    var configurator: AnyObject?

    @IBOutlet fileprivate weak var imageView: DownloadingImageView!
}
