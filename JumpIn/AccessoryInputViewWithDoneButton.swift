//
//  AccessoryInputViewWithDoneButton.swift
//  JumpIn
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class AccessoryInputViewWithDoneButton: UIView {
    
    static let defaultDoneButtonRightPadding: CGFloat = 20.0
    static let defaultHeight: CGFloat = 40.0
    static let defaultTintColor = UIColor.gray
    //
    weak var doneButton: UIButton!
    private var onDone: ()->()
    private var doneButtonRightPadding: CGFloat
    
    
    convenience init(withOnDone onDone: @escaping ()->()) {
        //
        self.init(withHeight: AccessoryInputViewWithDoneButton.defaultHeight, onDone: onDone)
    }
    
    convenience init(withHeight height: CGFloat, onDone: @escaping ()->()) {
        //
        self.init(withHeight: height, onDone: onDone, doneButtonRightPadding: AccessoryInputViewWithDoneButton.defaultDoneButtonRightPadding)
    }
    
    init(withHeight height: CGFloat, onDone: @escaping ()->(), doneButtonRightPadding: CGFloat) {
        //
        self.onDone = onDone
        self.doneButtonRightPadding = doneButtonRightPadding
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height))
        backgroundColor = UIColor.white//UIColor(white: 0.97, alpha: 1.0)
        tintColor = AccessoryInputViewWithDoneButton.defaultTintColor
        setupDoneButton()
    }
    
    override init(frame: CGRect) {
        //
        fatalError("init(frame:) has not been implemented")
    }
    
    required init?(coder aDecoder: NSCoder) {
        //
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        //
        super.draw(rect)
        drawBottomLine(inRect: rect)
    }
    
    func drawBottomLine(inRect rect: CGRect) {
        //
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: rect.height))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height))
        path.close()
        path.lineWidth = 0.5
        tintColor.set()
        path.stroke()
    }

    func setupDoneButton() {
        let button = UIButton(type: .system)
        button.setTitle(NSLoc("done"), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15.0)
        button.setTitleColor(UIColor.darkText, for: .normal)//(red:110.0/255.0, green:116.0/255.0, blue:127.0/255.0, alpha:1.0), for: .normal)
        button.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        button.sizeToFit()
        button.frame.origin.x = bounds.width - button.bounds.width - doneButtonRightPadding
        button.frame.origin.y = bounds.height/2 - button.bounds.height/2
        button.autoresizingMask = .flexibleRightMargin
        addSubview(button)
        doneButton = button
    }
    
    @objc func doneAction() {
        onDone()
    }
}
