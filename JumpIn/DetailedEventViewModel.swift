//
//  DetailedEventViewModel.swift
//  JumpIn
//
//  Created by Admin on 22.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class EventOwnerViewModel: BaseViewModel<SmallUserModel> {

    var nick: String
    var avatarLink: String?

    required init(withModel model: SmallUserModel) {
        self.nick = model.nick ?? "\(model.firstName!) \(model.lastName!)"
        self.avatarLink = model.avatar?.filepath
        super.init(withModel: model)
    }
}


class DetailedEventViewModel: BaseViewModel<DetailedEventModel> {
    
    var name: String
    var category: String?
    var likes: String
    var dislikes: String
    var voteMe: Bool?
    var membersCount: String
    var owner: EventOwnerViewModel
    var members: BaseUserCellViewModelsPage
    var dateString: String
    var description: String
    var address: String
    var city: String
    var photos: [PhotoWithLikesViewModel]
    var withMe: Bool
    var meRequested: Bool
    var avatarLink: String?
    var isMy: Bool
    var isCurrent: Bool
    var whoWait: [UIImage] = [#imageLiteral(resourceName: "ic_boy"), #imageLiteral(resourceName: "ic_girl")]
    var isPremium: Bool
    var shareText: String?
    var isUserInvited: Bool
    
    
    required init(withModel model: DetailedEventModel) {
        let categoryInfo = CategoryInfoService.categories[model.category!]!
        name = model.name ?? ""
        category = categoryInfo.title.uppercased()
        likes = "\(model.likes!)"
        dislikes = "\(model.dislikes!)"
        voteMe = model.likedMe
        owner = EventOwnerViewModel(withModel: model.owner)
        members = BaseUserCellViewModelsPage(withModel: model.members)
        photos = model.photos.map({ PhotoWithLikesViewModel(withModel:$0) })
        withMe = model.withMe == true
        meRequested = model.meRequested == true
        membersCount = NSLoc("total") + ": \(model.members!.models!.count)"
        dateString = DateConverter.ForInterface.DetailedEvent.eventDateString(fromServerString: model.date)
        description = model.description!
        address = model.address
        city = model.owner.city!
        avatarLink = model.avatar?.filepath
        isMy = model.isMy
        isCurrent = model.timeFrame == .current
        if let sex = model.sex {
            switch sex {
            case .Man: whoWait = [#imageLiteral(resourceName: "ic_boy")]; break;
            case .Woman: whoWait = [#imageLiteral(resourceName: "ic_girl")]; break
            }
        }
        isPremium = model.isPremium
        shareText = model.shareText
        isUserInvited = model.isUserInvited
        super.init(withModel: model)
    }
}
