//
//  InviteFriendsToEventController.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


class InviteFriendsToEventController: FriendsSubController<TableViewManager<UITableView, InviteCellManager>> {
    
    let eventId: Int
    
    
    init(eventId: Int) {
        self.eventId = eventId
        super.init()
        title = NSLoc("invite_people")
        loader.requestModel = GetFriendsRequestModel(with: .all)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableManager()
        setupNavBarButtons()
    }

    @objc func inviteButtonAction() {
        HUD.show(.progress)
        guard let selectedIndexPaths = tableManager.tableView.indexPathsForSelectedRows else { return }
        let users = selectedIndexPaths.map({ self.loader.model.models![$0.row] })
        let failedUsers = NSMutableArray()
        inviteUsers(users, failedUsers: failedUsers, completion: { [weak self] in
            HUD.hide()
            let alert: UIAlertController
            if failedUsers.count > 0 {
                var usersString = ""
                for user in failedUsers {
                    let user = user as! ShortUserModel
                    usersString.append("\(user.nick ?? "\(user.firstName!) \(user.lastName!)"), ")
                }
                usersString = String(usersString[..<usersString.index(of: ",")!])
                alert = UIAlertController(title: NSLoc("done_with_errors"), message: NSLoc("Сthe_following_users_could_not_send_the_invitation") + ": \(usersString)", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(ok)
            }
            else {
                alert = UIAlertController(title: NSLoc("invites_sent"), message: "", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self?.navigationController!.popViewController(animated: true) })
                alert.addAction(ok)
            }
            self?.present(alert, animated: true)
        })
    }
    
    private func inviteUsers(_ users: [ShortUserModel], startAt index: Int = 0, failedUsers: NSMutableArray, completion: @escaping ()->Void) {
        if index < users.count {
            inviteUser(users[index], completion: { [weak self] (user, success) in
                if !success { failedUsers.add(user) }
                self?.inviteUsers(users, startAt: index + 1, failedUsers: failedUsers, completion: completion)
            })
        }
        else { completion() }
    }
    
    private func inviteUser(_ user: ShortUserModel, completion: ((ShortUserModel, Bool)->())?) {
        ServerApi.shared.inviteUser(withId: user.id, toEventWithId: eventId, completion: {
            completion?(user, true)
        }) { (error: MainURLEngine.Error?, _: HTTPURLResponse?) in
            completion?(user, false)
        }
    }

    private func configureTableManager() {
        tableManager.tableView.allowsMultipleSelection = true
        tableManager.didSearch = { [weak self] q in
            guard let `self` = self else { return }
            self.loader.requestModel?.searchString = q
            self.loader.reloadData(animated:  false)
        }
        tableManager.didSelectCellAtIndexPath = nil
    }

    private func setupNavBarButtons() {
        if let navController = navigationController, navController === parent {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_check_gray"), style: .plain, target: self, action: #selector(inviteButtonAction))
        }
    }
}
