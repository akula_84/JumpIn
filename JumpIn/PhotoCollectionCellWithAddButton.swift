//
//  MissingDataPhotoCollectionCell.swift
//  JumpIn
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class PhotoCollectionViewManager: CollectionViewManager<PhotoCollectionViewCellWithAddButtonManager> {
    
    var onClickAddPhoto: ((_ senderIndex: Int)->())?
    var onClickDeletePhoto:((_ senderIndex: Int)->())?
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! PhotoCollectionCellWithAddButton
        cell.onClickAddPhoto = { [weak self] _ in self?.onClickAddPhoto?(collectionView.indexPath(for: cell)!.row) }
        cell.onClickDeletePhoto = { [weak self] _ in self?.onClickDeletePhoto?(collectionView.indexPath(for: cell)!.row) }
        return cell
    }
}


class PhotoCollectionViewCellWithAddButtonManager: BaseViewManager<PhotoViewModel, PhotoCollectionCellWithAddButton> {
    override func refresh() {
        if let image = viewModel.image {
            view.photo = image
        }
        else {
            view.photoLink = viewModel.link
        }
    }
}

class PhotoCollectionCellWithAddButton: UICollectionViewCell, PhotoCellWithAddButton, Configurable2 {
    
    var configurator: AnyObject?

    var onClickAddPhoto: ((_ sender: PhotoCellWithAddButton)->())?
    var onClickDeletePhoto:((_ sender: PhotoCellWithAddButton)->())?
    var photo: UIImage? {
        didSet {
            if let imageView = self.photoImageView {
                imageView.image = photo ?? UIImage(named: "no_image")
            }
            if let addButton = self.addButton {
                addButton.isSelected = photo != nil
            }
        }
    }
    var photoLink: String? {
        didSet {
            if let imageView = self.photoImageView {
                if let photoLink = photoLink {
                    photoImageView.imageLink = photoLink
                }
                else {
                    imageView.image = UIImage(named: "no_image")
                }
            }
            if let addButton = self.addButton {
                addButton.isSelected = photoLink != nil
            }
        }
    }
    //
    @IBOutlet private weak var photoImageView: DownloadingImageView!
    @IBOutlet private weak var addButton: UIButton!
    
    
    @IBAction private func addAction() {
        if addButton.isSelected { onClickDeletePhoto?(self) }
        else { onClickAddPhoto?(self) }
    }
}
