//
//  VkApi.swift
//  JumpIn
//
//  Created by Admin on 24.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation

protocol VKOAuthModelProtocol: OAuthModelProtocol {
    var permissions: [String] { get }
    var kUserId: String { get }
}

struct VKOAuthModel: VKOAuthModelProtocol {
    
    let appId = "6028468"
    let permissions = ["photos", "email", "messages, friends", "offline"]
    let baseLink = "https://oauth.vk.com"
    let kAccessToken = "access_token"
    let kUserId = "user_id"
    let kError = "error_description"
    let authorizeMethodName = "authorize"
    let redirectLink = "https://oauth.vk.com/blank.html"
    //
    var requestModel: VKAuthorizeRequestModel {
        return VKAuthorizeRequestModel(appId: appId, permissions: permissions, redirectLink: redirectLink)
    }
    var displayName: String { return "LogInacrossVK" }
}

class VKApi: BaseOAuthHelper<VKOAuthModel> {
    
    static let shared = VKApi()
    //
    private(set) var userId: Int?
    override var token: String? { didSet { UserDefaults.standard.set(token, forKey: "vk_token") } }
    
    private override init() {
        super.init()
        token = UserDefaults.standard.string(forKey: "vk_token")
    }
    
    override func parseAuthorizationResult(link: String) -> Bool {
        let result = super.parseAuthorizationResult(link: link)
        if result {
            userId = Int(link[link.range(of: "\(oAuthModel.kUserId)=")!.upperBound...])
        }
        else {
            userId = nil
        }
        return result
    }
    
    override func logout() {
        super.logout()
        token = nil
        UserDefaults.standard.set(nil, forKey: "vk_token")
    }
}
