//
//  PhotoCellWithAddButton.swift
//  JumpIn
//
//  Created by Admin on 06.04.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


protocol PhotoCellWithAddButton: class {
    //
    var onClickAddPhoto: ((_ sender: PhotoCellWithAddButton)->())? { get set }
    var photoLink: String? { get set }
    var photo: UIImage? { get set }
}
