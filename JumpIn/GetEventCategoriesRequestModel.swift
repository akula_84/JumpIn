//
//  GetEventCategoriesRequestModel.swift
//  JumpIn
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetEventCategoriesRequestModel: Mappable {
    
    enum EventsType: String {
        case active = "current", inactive = "archive"
    }

    var type: EventsType!
    
    init(withType type: EventsType) {
        self.type = type
    }
    
    required init(map:Map) {}
    
    func mapping(map: Map) {
        type <- map["t"]
    }
}
