//
//  PullUpToRefreshTableFooterView.swift
//  JumpIn
//
//  Created by Admin on 09.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class PullUpToRefreshTableFooterView: BaseCustomView {

    @IBOutlet private weak var spinnerView: UIActivityIndicatorView!
    @IBOutlet private weak var arrowImageView: UIImageView!
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 64))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setup() {
        super.setup()
        arrowImageView.image = arrowImageView.image?.withRenderingMode(.alwaysTemplate)
    }
    
    func startRefreshing() {
        arrowImageView.isHighlighted = true
        spinnerView.startAnimating()
    }
    
    func stopRefreshing() {
        arrowImageView.isHighlighted = false
        spinnerView.stopAnimating()
    }
}
