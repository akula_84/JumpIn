//
//  GoogleGeocoderResponseModel.swift
//  JumpIn
//
//  Created by Admin on 20.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class MappableLocation: Mappable {
    
    var latitude: Double!
    var longitude: Double!
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        latitude <- map["lat"]
        longitude <- map["lng"]
    }
}

class GoogleGeocoderResultGeometryResponseModel: Mappable {
    
    var location: MappableLocation!
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        location <- map["location"]
    }
}

class GoogleGeocoderResultResponseModel: Mappable {
    
    var geometry: GoogleGeocoderResultGeometryResponseModel!
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        geometry <- map["geometry"]
    }
}

class GoogleGeocoderResponseModel: Mappable {
    
    var results: [GoogleGeocoderResultResponseModel]!
    var status: String!
    
    required init?(map: Map) { }
    func mapping(map: Map) {
        results <- map["results"]
        status <- map["status"]
    }
}
