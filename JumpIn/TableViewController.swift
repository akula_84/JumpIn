//
//  TableViewController.swift
//  JumpIn
//
//  Created by Admin on 21.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


protocol TableViewController: class {
    var tableView: UITableView! { get set }
}

extension TableViewController {
    
    func setupSearchController(for controller: UIViewController, withScopeButtonTitles titles: [String]? = nil, searchBarBottomSpacing: CGFloat = 0) -> SearchController {
        //
        let searchController = SearchController(scopes: titles, bottomSpacing: searchBarBottomSpacing)
        if let searchBarDelegate = controller as? UISearchBarDelegate {
            searchController.searchBarDelegate = searchBarDelegate
        }
        controller.addChildViewController(searchController)
        tableView.tableHeaderView = searchController.view
        searchController.didMove(toParentViewController: controller)
        return searchController
    }
}
