//
//  BaseUserSimpleCell.swift
//  JumpIn
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


protocol BaseUserCellViewModelProtocol: ViewModelProtocol {
    var nick: String { get set }
    var avatarLink: String? { get set }
    var mainSocialNetViewModel: SocialNetViewModel? { get set }
    var isOnline: Bool { get set }
    var state: BaseUserCell.State { get set }
}

class BaseUserCellViewModelsPage: PageViewModel<ShortUserModelsPage, BaseUserCellViewModel> { }

class BaseUserCellViewModel: BaseViewModel<ShortUserModel>, BaseUserCellViewModelProtocol {
    
    var nick: String
    var avatarLink: String?
    var mainSocialNetViewModel: SocialNetViewModel?
    var isOnline: Bool
    var state = BaseUserCell.State.default
    
    init(nick: String, isOnline: Bool = false) {
        self.nick = nick
        self.isOnline = isOnline
        super.init()
    }
    
    required init(withModel model: ShortUserModel) {
        self.nick = model.nick ?? "\(model.firstName!) \(model.lastName!)"
        self.avatarLink = model.avatar?.filepath
        if let socialNetModel = model.mainSocialNet {
            self.mainSocialNetViewModel = SocialNetViewModel(withModel: socialNetModel)
        }
        self.isOnline = model.isOnline == nil ? false : model.isOnline!
        super.init(withModel: model)
    }
}

class UsersTableViewManager: TableViewManager<UITableView, UserCellManager> {
    required init(with superView: UIView?) {
        super.init(with: superView)
        tableInitConfig.isCellHaveNib = false
    }
}

class UserCellManager: BaseUserCellManager<BaseUserCellViewModel, BaseUserCell> {}

class BaseUserCellManager<CellModelType: BaseUserCellViewModelProtocol, CellType: BaseUserCell>: BaseViewManager<CellModelType, CellType> {
    
    override func refresh() {
        if let socialNetViewModel = viewModel.mainSocialNetViewModel {
            view.socialNetImageView.image = socialNetViewModel.icon
            view.socialNetImageView.isHidden = false
        }
        else { view.socialNetImageView.isHidden = true }
        view.nameLabel.text = viewModel.nick
        view.photoImageView.image = #imageLiteral(resourceName: "user_no_image")
        view.photoImageView.imageLink = viewModel.avatarLink
        view.state = viewModel.state
        view.onlineView.isHidden = !viewModel.isOnline
    }
}


class BaseUserCell: UITableViewCell, Configurable2 {
    
    struct ButtonItem {
        let image: UIImage
        let action: ()->()
    }
    
    enum State {
        case `default`, loading, completed
    }
    
    var configurator: AnyObject?
    var state: State = .default { didSet { set(state: state, animated: false) } }
    
    @IBOutlet weak var socialNetImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var onlineView: UIView!
    @IBOutlet weak var photoImageView: DownloadingImageView!
    //
    var rightButtonItems: [ButtonItem] = [] { didSet { reloadRightStackView() } }
    private weak var rightStackView: UIStackView?
    private weak var rightStackViewForButtons: UIStackView?
    private weak var loadingIndicator: UIActivityIndicatorView!
    private weak var checkImageView: UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupContentView()
        setupRightStackView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupContentView() {
        let view = Bundle.main.loadNibNamed("BaseUserCellView", owner: self, options: nil)?.first as! UIView
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        view.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        view.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        view.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
    }
    
    private func reloadRightStackView() {
        rightStackView?.removeFromSuperview()
        setupRightStackView()
    }
    
    private func setupRightStackView() {
        let extraViews = setupExtraViewsToStackView()
        var views = [extraViews.checkImageView, extraViews.hud]
        if let substackView = setupRightStackViewForButtons() { views.insert(substackView, at: 0) }
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -17).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        rightStackView = stackView
    }
    
    private func setupRightStackViewForButtons()->UIStackView? {
        guard !rightButtonItems.isEmpty else { return nil }
        let stackView = UIStackView(arrangedSubviews: rightButtonItems.map({
            let button = UIButton(type: .custom)
            button.setImage($0.image, for: .normal)
            button.sizeToFit()
            button.addTarget(self, action: #selector(tapButtonAction(sender:)), for: .touchUpInside)
            return button
        }))
        stackView.axis = .horizontal
        stackView.spacing = 30
        rightStackViewForButtons = stackView
        return stackView
    }
    
    private func setupExtraViewsToStackView()->(hud: UIActivityIndicatorView, checkImageView: UIImageView) {
        let hud = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        hud.isHidden = true
        loadingIndicator = hud
        let checkImageView = UIImageView(image: #imageLiteral(resourceName: "ic_check_gray").withRenderingMode(.alwaysTemplate))
        checkImageView.isHidden = true
        self.checkImageView = checkImageView
        return (hud, checkImageView)
    }
    
    
    func set(state: State, animated: Bool) {
        switch state {
        case .default:
            setLoadingIndicator(hidden: true, animated: animated, completion: { [weak self] in
                self?.setCheckMarkImage(hidden: true, animated: animated, completion: {
                    self?.setRightButtons(hidden: false, animated: animated, completion: nil)
                })
            })
            break
        case .loading:
            setCheckMarkImage(hidden: true, animated: animated, completion: { [weak self] in
                self?.setRightButtons(hidden: true, animated: animated, completion: {
                    self?.setLoadingIndicator(hidden: false, animated: animated, completion: nil)
                })
            })
            break
        case .completed:
            setLoadingIndicator(hidden: true, animated: animated, completion: { [weak self] in
                self?.setRightButtons(hidden: true, animated: animated, completion: {
                    self?.setCheckMarkImage(hidden: false, animated: animated, completion: nil)
                })
            })
            break
        }
    }
    
    private func setLoadingIndicator(hidden: Bool, animated: Bool, completion: (()->Void)?) {
        guard hidden != loadingIndicator.isHidden else { completion?(); return }
        UIView.animate(withDuration: 0.3, animations: {
            let hud = self.loadingIndicator!
            hud.isHidden = hidden
            if hidden { hud.stopAnimating() } else { hud.startAnimating() }
        }) { if $0 { completion?() } }
    }
    
    private func setCheckMarkImage(hidden: Bool, animated: Bool, completion: (()->Void)?) {
        guard hidden != checkImageView.isHidden else { completion?(); return }
        UIView.animate(withDuration: 0.3, animations: {
            self.checkImageView.isHidden = hidden
        }) { if $0 { completion?() } }
    }
    
    private func setRightButtons(hidden: Bool, animated: Bool, completion: (()->Void)?) {
        guard let stackView = rightStackViewForButtons, hidden != stackView.isHidden else { completion?(); return }
        UIView.animate(withDuration: 0.3, animations: {
            stackView.isHidden = hidden
        }) { if $0 { completion?() } }
    }
    
    @objc private func tapButtonAction(sender: UIButton) {
        let index = rightStackViewForButtons!.arrangedSubviews.index(of: sender)!
        rightButtonItems[index].action()
    }
}
