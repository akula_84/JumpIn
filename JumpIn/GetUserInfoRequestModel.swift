//
//  GetUserInfoRequestModel.swift
//  JumpIn
//
//  Created by Admin on 16.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation

class GetUserInfoRequestModel {
    var userId: Int?
    init(userId: Int?) {
        self.userId = userId
    }
}
