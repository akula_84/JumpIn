//
//  MessageSending.swift
//  JumpIn
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


//MARK: Sending Message Request\Response Models


class JsonRpcSendMessageRequestPayloadModel: JsonRpcRequestPayloadModel {
    
    var userId: Int!
    var text: String?
    var encodedPhoto: String?
    var photo: UIImage?
    
    init(targetUserId id: Int, photo: UIImage?, text: String?) {
        self.userId = id
        self.photo = photo
        self.text = text
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userId <- map["user_id"]
        text <- map["message"]
        encodedPhoto <- map["photo"]
    }
    
    func prepareToSend(withComletion completion: (()->Void)?) {
        if let photo = photo {
            DispatchQueue.global(qos: .utility).async { [weak self] in
                self?.encodedPhoto = UIImageJPEGRepresentation(photo, 0)!.base64EncodedString()
                completion?()
            }
            if text == nil {
                text = "Фотография"
            }
        }
        
        else  {
            completion?()
            return
        }
    }
}


class JsonRpcSendMessageRequestModel: JsonRpcRequestModel<JsonRpcSendMessageRequestPayloadModel> {
    
    override var method: String! { get { return "send_message" } set {} }
    
    init(targetUserId id: Int, photo: UIImage?, text: String?) {
        super.init(payloadModel: JsonRpcSendMessageRequestPayloadModel(targetUserId: id, photo: photo, text: text))
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    func prepareToSend(withComletion completion: (()->Void)?) {
        payloadModel!.prepareToSend(withComletion: completion)
    }
}

class JsonRpcSendMessageResponsePayloadModel: Mappable {
    
    var messageId: Int!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        messageId <- map["message_id"]
    }
}

class JsonRpcSendMessageResponseModel: JsonRpcResponseModel<JsonRpcSendMessageResponsePayloadModel> {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        result <- map["result"]
    }
}


//MARK: Delete Message Models


class JsonRpcDeleteMessageRequestPayloadModel: JsonRpcRequestPayloadModel {
    
    var messageId: Int!
    
    init(messageId: Int) {
        self.messageId = messageId
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        messageId <- map["message_id"]
    }
}

class JsonRpcDeleteMessageRequestModel: JsonRpcRequestModel<JsonRpcDeleteMessageRequestPayloadModel> {
    
    override var method: String! { get { return "delete_message" } set {} }
    
    init(messageId: Int) {
        super.init(payloadModel: JsonRpcDeleteMessageRequestPayloadModel(messageId: messageId))
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
}

class JsonRpcDeleteMessageResponse: JsonRpcResponseModel<Int> { }


//MARK: Message Events Models


class JsonRpcMessageEventResultModel: Mappable {
    
    enum EventType: String {
        case newMessage = "new_message", deletedMessage = "deleted_message"
    }
    
    required init?(map: Map) { }
    
    var senderId: Int!
    var type: EventType!
    
    func mapping(map: Map) {
        type <- map["type"]
        let kSenderId: String
        switch type! {
            case .newMessage: kSenderId = "from"; break
            case .deletedMessage: kSenderId = "user_id"
        }
        senderId <- map[kSenderId]
    }
}

class JsonRpcNewMessageEventResultModel: JsonRpcMessageEventResultModel {
    
    var id: Int!
    var message: String!
    var photoLink: String?
    var senderAvatarLink: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        message <- map["message"]
        photoLink <- map["photo_path"]
        id <- map["message_id"]
        senderAvatarLink <- map["from_avatar"]
    }
}

extension DialogMessageModel {
    convenience init(model: JsonRpcNewMessageEventResultModel) {
        self.init()
        id = model.id
        text = model.message
        photoLink = model.photoLink
        timestampDate = Date()
        senderId = model.senderId
        senderAvatarLink = model.senderAvatarLink
        recipientId = UserInfoService.shared.userId
    }
}

class JsonRpcDeletedMessageEventResultModel: JsonRpcMessageEventResultModel {
    
    var messageId: Int!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        messageId <- map["message_id"]
    }
}

class JsonRpcNewMessageEventModel: JsonRpcEventModel <JsonRpcNewMessageEventResultModel> {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        result <- map["result"]
    }
}

class JsonRpcDeletedMessageEventModel: JsonRpcEventModel <JsonRpcDeletedMessageEventResultModel> {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        result <- map["result"]
    }
}
