//
//  DetailedCategoryForLastEventsController.swift
//  JumpIn
//
//  Created by Admin on 21.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class DetailedCategoryForLastEventsController: EventsFeedController {
    
    init(category: CategoryInfoService.CategoryInfo.Name?) {
        super.init(withEventsType: .generalLast, category: category, enableSearching: false)
        loader.requestModel.isPremium = category == nil ? true : nil
        title = NSLoc("pastEvents")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
    }
    
    private func setupNavBarButtons() {
        let filterBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_filter"), style: .plain, target: self, action: #selector(filterAction))
        navigationItem.rightBarButtonItem = filterBarButton
    }
    
    @objc private func filterAction(sender: UIBarButtonItem) {
        let controller = EventsFilterController(withModel: loader.requestModel!, filterButton: sender)
        navigationController?.pushViewController(controller, animated: true)
    }
}
