//
//  UserRequestCell.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class UserRequestCellManager: BaseUserCellManager<BaseUserCellViewModel, UserRequestCell> {}

class UserRequestCell: BaseUserCell {
    
    var onTapButtonWithAnswer: ((Bool)->Void)?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        rightButtonItems = [ButtonItem(image: #imageLiteral(resourceName: "ic_blue_user_with_plus")) { [weak self] in self?.onTapButtonWithAnswer?(true) },
                            ButtonItem(image: #imageLiteral(resourceName: "ic_little_red_user_with_cross")) { [weak self] in self?.onTapButtonWithAnswer?(false) }]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
