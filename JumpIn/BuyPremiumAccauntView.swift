//
//  BuyPremiumAccauntView.swift
//  JumpIn
//
//  Created by Admin on 29.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class BuyPremiumAccauntView: BaseCustomView {
    
    private let gradient = CAGradientLayer()
    var onBuy: (()->Void)?

    override func setup() {
        super.setup()
        gradient.colors = [UIColor.white.cgColor, #colorLiteral(red: 1, green: 0.9764705882, blue: 0.8941176471, alpha: 1).cgColor]
        gradient.frame = bounds
        backgroundColor = .clear
        layer.insertSublayer(gradient, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = bounds
    }
    
    @IBAction private func BuyAction() {
        onBuy?()
    }
}
