//
//  Loading.swift
//  JumpIn
//
//  Created by Admin on 11.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper


protocol LoadingData: class {
    
    associatedtype ResponseModelType: Mappable
    associatedtype RequestModelType
    var model: ResponseModelType! { get set }
    var reloadDataFunc: ((RequestModelType?, ((ResponseModelType)->Void)?, RequestErrorBlockType?)->Void) { get }
    func reloadData()
    func requestModelToLoadData()->RequestModelType?
}


protocol ConfigurableWithLoadingData: Configurable, LoadingData {
    func generateViewModel(withModel model: ResponseModelType) -> ViewModelType
}
