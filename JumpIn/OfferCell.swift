//
//  OfferCellTableViewCell.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class OfferCellManager: BaseUserCellManager<BaseUserCellViewModel, OfferCell> {}

class OfferCell: BaseUserCell {
    
    var sendFriendRequestAction: (()->Void)?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        rightButtonItems = [(ButtonItem(image: #imageLiteral(resourceName: "ic_blue_user_with_plus"), action: { [weak self] in
            self?.sendFriendRequestAction?() }))]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
