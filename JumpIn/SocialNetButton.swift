//
//  SocialNetButton.swift
//  JumpIn
//
//  Created by Admin on 12.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class SocialNetButton: BaseCustomButton {
    
    @IBOutlet private weak var leftViewHorizontalOffsetLayout: NSLayoutConstraint!
    @IBOutlet private weak var leftViewVerticalOffsetLayout: NSLayoutConstraint!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var leftLabel: UILabel!
    @IBOutlet private weak var leftImageView: UIImageView!
    //
    var cornerRadius: CGFloat = 4.0 { didSet { refreshView() } }
    @IBInspectable var title: String = "" { didSet { refreshView() } }
    var leftLabelText: String = "" { didSet { refreshView() } }
    var leftViewVerticalOffset: CGFloat = 0 { didSet { refreshView() } }
    var leftViewHorizontalOffset: CGFloat = 0 { didSet { refreshView() } }
    var leftIcon: UIImage? { didSet { refreshView() } }
    
    func refreshView() {
        layer.cornerRadius = cornerRadius
        titleLabel.text = NSLoc(title)
        leftLabel.text = NSLoc(leftLabelText)
        leftViewVerticalOffsetLayout.constant = leftViewVerticalOffset
        leftViewHorizontalOffsetLayout.constant += leftViewHorizontalOffset
        leftImageView.image = leftIcon?.withRenderingMode(.alwaysTemplate)
        leftLabel.isHidden = leftIcon != nil
    }
    
    override func setup() {
        super.setup()
        refreshView()
    }
    
    override var xibName: String {
        return "SocialNetButton"
    }
}
