//
//  BaseCustomButton.swift
//  JumpIn
//
//  Created by Admin on 12.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class BaseCustomButton: BaseCustomView {
    
    private var highlighted = false
    var isEnabled = true
    var onClick: ((_ sender: BaseCustomButton)->())?
    var highlightedAlpha = CGFloat(0.2)

    private func animateButton(withAlpha alpha: CGFloat) {
        //
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            //
            if let internalView = self.internalView {
                internalView.alpha = alpha
            }
            else {
                self.alpha = alpha
            }
        }, completion: nil);
    }
    
    private func onClickAction() {
        if highlighted {
            if let _onClick = onClick, isEnabled {
                _onClick(self)
            }
        }
        highlighted = false
    }
    
    // Touches Handling:
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard isEnabled else { return }
        animateButton(withAlpha: highlightedAlpha)
        highlighted = true;
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        guard isEnabled else { return }
        animateButton(withAlpha: 1.0)
        onClickAction()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        guard isEnabled else { return }
        animateButton(withAlpha: 1.0)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard isEnabled else { return }
        if !self.bounds.contains(touches.first!.location(in: self)) {
            animateButton(withAlpha: 1.0)
            onClickAction()
        }
    }
}
