//
//  STRouter+MenuNav.swift
//  RNIS
//
//  Created by Артем Кулагин on 10.01.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation
import UIKit

/**
 Расширение для работы с меню
 */
extension STRouter {
    
    static var rootMain: BaseNavigationController? {
        return window??.rootViewController as? BaseNavigationController
    }
    
    static var window: UIWindow?? {
        return UIApplication.shared.delegate?.window
    }
    static func prepareRootMain(_ viewController: UIViewController?) {
        window??.rootViewController = viewController
    }
    
    static func pushMain(_ viewController: UIViewController?, animated: Bool = true, completion: EmptyBlock? = nil) {
        rootMain?.push(viewController, animated: animated, completion:completion)
    }
    
    static func popMain(animated: Bool = true, completion: EmptyBlock? = nil) {
        rootMain?.pop(animated: animated, completion: completion)
    }
    
    static func popToRootMain(_ animated: Bool = true, completion:EmptyBlock? = nil) {
        rootMain?.popToRoot(animated: animated, completion: completion)
    }
    
    
    static func present(_ viewControllerToPresent:UIViewController?, animated: Bool = true, completion: EmptyBlock? = nil) {
        guard let viewControllerToPresent = viewControllerToPresent,
            var vc:UIViewController = rootMain else {
                return
        }
        
        while let presented = vc.presentedViewController {
            vc = presented
        }
        viewControllerToPresent.modalPresentationStyle = .overCurrentContext
        vc.present(viewControllerToPresent, animated: animated, completion: completion)
    }
    
    static func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        /*
        if let SSASide = base as? SSASideMenu {
            if let nav = SSASide.contentViewController as? UINavigationController{
                return topViewController(nav.visibleViewController)
            }
        }
         */
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(top)
            } else if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        
        return base
    }
    
    static var topView: UIView? {
        return topViewController()?.view
    }
}
