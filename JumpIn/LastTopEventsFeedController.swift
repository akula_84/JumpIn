//
//  LastTopEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 29.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class LastTopEventsFeedController: TabController {

    let isSearching: Bool
    
    init(isSearching: Bool = false) {
        //
        self.isSearching = isSearching
        super.init()
        self.title = isSearching ?  NSLoc("search") : NSLoc("pastEvents")
        self.add(subcontroller: LastTopEventsFeedTableSubcontroller(withSearchBar: isSearching))
        self.showScopeControl = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
