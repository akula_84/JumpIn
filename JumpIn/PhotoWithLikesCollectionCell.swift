//
//  EventPhotoCollectionCell.swift
//  JumpIn
//
//  Created by Admin on 17.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


protocol PhotoWithLikesCollectionViewManagerDelegate: class {
    func photoCollectionManager(_ manager: PhotoWithLikesCollectionViewManager, didChangeVoteForVideoAt: IndexPath)
    func photoCollectionManager(_ manager: PhotoWithLikesCollectionViewManager, failedToChangeVoteAt: IndexPath, error: MainURLEngine.Error?)
}


class PhotoWithLikesCollectionViewManager: CollectionViewManager<PhotoWithLikesCollectionCellManager>, PhotoWithLikesCollectionCellManagerDelegate {
    
    weak var delegate: PhotoWithLikesCollectionViewManagerDelegate?
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        guard let videoCell = cell as? PhotoWithLikesCollectionCell,
            let cellManager = videoCell.configurator as? PhotoWithLikesCollectionCellManager else { return cell }
        cellManager.delegate = self
        return cell
    }
    
    func cellManager(_ manager: PhotoWithLikesCollectionCellManager, didChangeVote vote: Bool?) {
        let indexPath = collectionView.indexPath(for: manager.view)!
        let item = dataSource[indexOfDataSourceItem(forCellAt: indexPath)]
        let model = VoteRequestModel(type: .photo, id: item.id, rating: vote)
        ServerApi.shared.vote(withModel: model, completion: { [weak self] in
            guard let `self` = self else { return }
            self.delegate?.photoCollectionManager(self, didChangeVoteForVideoAt: indexPath)
        }) { [weak self] (error, _) in
            guard let `self` = self else { return }
            self.delegate?.photoCollectionManager(self, failedToChangeVoteAt: indexPath, error: error)
        }
    }
}


protocol PhotoWithLikesCollectionCellManagerDelegate: class {
    func cellManager(_ manager: PhotoWithLikesCollectionCellManager, didChangeVote: Bool?)
}


class PhotoWithLikesCollectionCellManager: BaseViewManager<PhotoWithLikesViewModel, PhotoWithLikesCollectionCell> {
    
    weak var delegate: PhotoWithLikesCollectionCellManagerDelegate?
    
    override func refresh() {
        view.imageView.image = #imageLiteral(resourceName: "no_image")
        view.imageView.imageLink = viewModel.link
        view.likeButton.tintColor = UIColor(white: 0.8, alpha: 1)
        view.dislikeButton.tintColor = UIColor(white: 0.8, alpha: 1)
        if let vote = viewModel.voteMe {
            if vote {
                view.likeButton.tintColor = #colorLiteral(red: 0.2784313725, green: 0.5333333333, blue: 0.9529411765, alpha: 1)
            }
            else {
                view.dislikeButton.tintColor = #colorLiteral(red: 0.2784313725, green: 0.5333333333, blue: 0.9529411765, alpha: 1)
            }
        }
        view.likeButton.setTitle("\(viewModel.likes)", for: .normal)
        view.dislikeButton.setTitle("\(viewModel.dislikes)", for: .normal)
        view.likeButton.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
        view.dislikeButton.addTarget(self, action: #selector(dislikeAction), for: .touchUpInside)
        //view.likeButton.superview?.isHidden = viewModel.isLikesPanelHidden
    }
    
    @objc private func likeAction() {
        var vote: Bool?
        let oldVote = viewModel.voteMe
        if oldVote == nil || oldVote == false { vote = true }
        delegate?.cellManager(self, didChangeVote: vote) 
        viewModel.voteMe = vote
        if vote != nil {
            if vote! {
                viewModel.likes += 1
                if let old = oldVote, !old {
                    viewModel.dislikes -= 1
                }
            }
        }
        else {
            viewModel.likes -= 1
        }
        refresh()
    }
    
    @objc private func dislikeAction() {
        var vote: Bool?
        let oldVote = viewModel.voteMe
        if oldVote == nil || oldVote == true { vote = false }
        delegate?.cellManager(self, didChangeVote: vote)
        viewModel.voteMe = vote
        if vote != nil {
            if !vote! {
                viewModel.dislikes += 1
                if let old = oldVote, old {
                    viewModel.likes -= 1
                }
            }
        }
        else {
            viewModel.dislikes -= 1
        }
        refresh()
    }
}

class PhotoWithLikesCollectionCell: UICollectionViewCell, Configurable2 {

    var configurator: AnyObject?
    
    @IBOutlet fileprivate weak var imageView: DownloadingImageView!
    @IBOutlet fileprivate weak var likeButton: UIButton!
    @IBOutlet fileprivate weak var dislikeButton: UIButton!
    
}
