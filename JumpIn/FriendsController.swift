//
//  FriendsController.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class FriendsController: TabController {

    override init() {
        super.init()
        title = NSLoc("drawer_friend")
        let allFriendsVC = FriendsListController()
        let onlineFriendsVC = FriendsListController(showOnlineFriendsOnly: true)
        add(subcontrollers: [allFriendsVC, onlineFriendsVC, RequestsListController(), OffersListController()])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self,
                                                            action: #selector(addAction))
    }
    
    @objc private func addAction() {
        InviteFriendsToAppController.show(usingNavController: navigationController!)
    }

    deinit {
        print("Deinit \(self)")
    }
}
