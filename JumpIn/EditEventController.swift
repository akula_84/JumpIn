//
//  EditEventController.swift
//  JumpIn
//
//  Created by Admin on 20.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD
import AVKit
import GooglePlacePicker
import ObjectMapper


class EditEventViewModel: DetailedEventViewModel {
    
    required init(withModel model: DetailedEventModel) {
        super.init(withModel: model)
        dateString = DateConverter.ForInterface.EventEditing.eventDateString(fromServerString: model.date)
    }
}


class EditEventPhotoCollectionManager: CollectionViewManager<PhotoWithLikesCollectionCellManager> {}


class EditEventVideoCollectionManager: VideoCollectionViewManager {
    
    private let addVideoCellId = "AddVideoCollectionCell"
    var onTapAddVideo: (()->Void)?
    
    
    override func indexOfDataSourceItem(forCellAt indexPath: IndexPath, useBlockIfExist: Bool = true) -> Int {
        return indexPath.item - 1
    }
    
    override func indexPathOfCell(forDataSourceItemAt index: Int, useBlockIfExist: Bool = true) -> IndexPath {
        return IndexPath(item: index + 1, section: 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return super.collectionView(collectionView, numberOfItemsInSection: section) + 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.row > 0 else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addVideoCellId, for: indexPath)
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row > 0 else {
            onTapAddVideo?()
            return
        }
        return super.collectionView(collectionView, didSelectItemAt: indexPath)
    }
    
    override func setup() {
        let nib = UINib(nibName: addVideoCellId, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: addVideoCellId)
        super.setup()
    }
}


class EditEventController: BaseConfigurableController<EditEventViewModel>, UITextFieldDelegate, UITextViewDelegate, VideoCollectionViewManagerDelegate, GMSPlacePickerViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var hideForFriendsFromSocialNets: UISwitch!
    @IBOutlet weak var whoWaitLabel: UILabel!
    @IBOutlet weak var markOnMapButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var addButtonContainerView: UIView!
    @IBOutlet weak var eventNameTextField: UITextField!
    @IBOutlet weak var descriptionPlaceHolderLabel: UILabel!
    @IBOutlet weak var videoCollectionView: UICollectionView!
    //
    @IBOutlet weak var chooseSexButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var nameEventButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var descriptionButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var locationButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var categoryButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var dateButton: BaseCustomButtonWithoutXib!
    //
    private var isAddition: Bool { return model == nil }
    private let photoCollectionCellNibName = String(describing: PhotoCollectionCellWithAddButton.self)
    private var sex: SexModel? {
        didSet {
            whoWaitLabel.text = WhoWaitViewModel(withModel: sex).localizedString
            isChanged = true
        } }
    private var category: CategoryInfoService.CategoryInfo.Name! {
        didSet {
            categoryLabel.text = CategoryInfoService.categories[category]!.title
            isChanged = true
        } }
    private var model: DetailedEventModel!
    private var date: Date? { didSet { dateTextField.text = DateConverter.ForInterface.EventEditing.eventDateString(from: date!) } }
    private let avatarWithPhotosController: AvatarWithPhotosCollectionController
    private var isChanged = false
    private var isFirstRefresh = true
    private var city: String?
    private var videoCollectionManager: EditEventVideoCollectionManager!
    private var videoModels: [VideoModel] = []
    private var videoViewModels: [VideoCellViewModel] { return videoModels.map({ VideoCellViewModel(withModel: $0) }) }
    private var lastLoadedVideoPage: VideoPageModel? { didSet { refreshVideoCollection() } }
    private var videoInfoToUpload: [(videoUrl: URL, thumbnail: UIImage)] = [] {
        didSet {
            isChanged = true
            refreshVideoCollection()
        }
    }
    private var eventId: Int?
    private weak var buyView: BuyPremiumAccauntView?
    
    
    init(model: DetailedEventModel? = nil) {
        avatarWithPhotosController = AvatarWithPhotosCollectionController(withType: .events, ownerId: model?.id)
        super.init(nibName: String(describing:type(of: self).self), bundle: nil)
        self.model = model
        if let model = model { viewModel = EditEventViewModel(withModel: model) }
        self.addChildViewController(avatarWithPhotosController)
        avatarWithPhotosController.didMove(toParentViewController: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLoc(isAddition ? "add_drawer_event" : "edit")
        setupNavBarButtons()
        descriptionTextView.addDoneButton()
        setupButtons()
        setupTextFields()
        stackView.insertArrangedSubview(avatarWithPhotosController.view, at: 0)
        refresh()
        setupVideoCollectionManager()
    }
    
    private func addBuyView() {
        if buyView == nil {
            let buyView = BuyPremiumAccauntView()
            buyView.onBuy = { [weak self] in self?.showAlertAboutOpenningAppStoreIfNeed(cancelBlock: nil) }
            stackView.addArrangedSubview(buyView)
            self.buyView = buyView
        }
    }
    
    private func removeBuyView() {
        if let buyView = self.buyView {
            stackView.removeArrangedSubview(buyView)
            buyView.removeFromSuperview()
        }
    }
    
    private func setupTextFields() {
        let attributes: [NSAttributedStringKey : Any] = [.foregroundColor : #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1), .font : dateTextField.font!]
        setupDateTextField(attributes: attributes)
        eventNameTextField.attributedPlaceholder = NSAttributedString(string: eventNameTextField.placeholder!, attributes: attributes)
    }
    
    private func setupButtons() {
        chooseSexButton.onClick = { [weak self] _ in
            AlertsPresentator.presentAlertChooseGender(in: self) { [weak self] in self?.sex = $0 }
        }
        categoryButton.onClick = { [weak self] _ in
            self?.view.endEditing(true)
            AlertsPresentator.presentAlertChooseCategory(in: self) { [weak self] in self?.category = $0 }
        }
        descriptionButton.highlightedAlpha = 1
        descriptionButton.onClick = { [weak self] _ in self?.descriptionTextView.becomeFirstResponder() }
        nameEventButton.highlightedAlpha = 1
        nameEventButton.onClick = { [weak self] _ in self?.eventNameTextField.becomeFirstResponder() }
        locationButton.highlightedAlpha = 1
        locationButton.onClick = { [weak self] _ in self?.goToSetAddress() }
    }
    
    private func setupNavBarButtons() {
        if !isAddition {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_check_gray"), style: .plain, target: self, action: #selector(editionDoneAction))
            addButtonContainerView.isHidden = true
        }
    }
    
    private func setupDateTextField(attributes: [NSAttributedStringKey : Any]) {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.backgroundColor = .white
        datePicker.datePickerMode = .dateAndTime
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerChangedAction), for: .valueChanged)
        dateTextField.inputView = datePicker
        dateTextField.attributedPlaceholder = NSAttributedString(string: dateTextField.placeholder!, attributes: attributes)
        dateTextField.addDoneButton()
    }
    
    private func setupVideoCollectionManager() {
        videoCollectionManager = EditEventVideoCollectionManager(with: videoCollectionView)
        videoCollectionManager.setup()
        videoCollectionManager.dataSource = videoViewModels
        videoCollectionManager.delegate = self
        videoCollectionManager.onTapAddVideo = { [weak self] in
            guard let `self` = self else { return }
            AlertsPresentator.presentVideoPickerWithPreChooseSource(in: self) { $0?.delegate = self }
        }
    }
    
    func videoCollectionManager(_ manager: VideoCollectionViewManager, controllerToPresentSelectedVideoAt index: Int) -> UIViewController? {
        return self
    }
    
    func videoCollectionManager(_ manager: VideoCollectionViewManager, didChangeVoteForVideoAt: IndexPath) {
        reloadVideos()
    }
    
    func videoCollectionManager(_ manager: VideoCollectionViewManager, failedToChangeVoteAt: IndexPath, error: MainURLEngine.Error?) {
        showError(error)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadVideos()
    }
    
    private func reloadVideos() {
        guard let model = model else { return }
        videoModels.removeAll()
        ServerApi.shared.getEventVideos(with: GetEventVideosRequestModel(eventId: model.id, numberOfVideosOnSinglePage: 20), completion: { [weak self] in self?.lastLoadedVideoPage = $0 }, errorBlock: nil)
    }
    
    private func refreshVideoCollection() {
        videoModels.removeAll()
        let localVideosModels = videoInfoToUpload.map({ VideoModel(videoUrl: $0.videoUrl, thumbnail: $0.thumbnail) })
        if let page = lastLoadedVideoPage {
            videoModels = page.models
        }
        videoModels += localVideosModels
        videoModels.forEach({ $0.isLikesPanelHidden = true })
        videoCollectionManager.dataSource = videoViewModels
        videoCollectionManager.reloadData()
    }
    
    func makeUserPremium(errorBlock: (()->Void)?) {
        HUD.show(.progress)
        ServerApi.shared.makeProfilePremium(completion: { [weak self] in
            HUD.hide()
            self?.refresh()
            
        }) { [weak self] (error, _) in
            self?.showError(error)
            errorBlock?()
        }
    }
    
    func showAlertAboutOpenningAppStoreIfNeed(cancelBlock: (()->Void)?) {
        if !UserInfoService.shared.isPremium {
            AlertsPresentator.makeEventTopForThreeDays(in: self, completion: { [weak self] yes in
                if yes { self?.makeUserPremium(errorBlock: cancelBlock) }
                else { cancelBlock?() }
            })
        }
    }
    
    override func refresh() {
        let isUserPremium = UserInfoService.shared.isPremium
        hideForFriendsFromSocialNets.isEnabled = isUserPremium
        if !isUserPremium {
            hideForFriendsFromSocialNets.isOn = false
            addBuyView()
        }
        else {
            removeBuyView()
        }
        guard viewModel != nil else { return }
        category = model.category
        descriptionTextView.text = viewModel.description
        descriptionPlaceHolderLabel.isHidden = !viewModel.description.isEmpty
        dateTextField.text = viewModel.dateString
        addressLabel.text = viewModel.address
        //inviteButton.isEnabled = false
        sex = model.sex
        date = DateConverter.ForServer.Event.eventDate(from: model.date)
        eventNameTextField.text = viewModel.name
        avatarWithPhotosController.set(photos: model.photos.compactMap({ PhotoViewModel(withPhoto: $0) }))
        avatarWithPhotosController.set(avatar: PhotoViewModel(withPhoto: model.avatar))
        if isFirstRefresh {
            isFirstRefresh = false
            isChanged = false
        }
    }
    
    @objc private func datePickerChangedAction(sender: UIDatePicker) {
        date = sender.date
    }

    @IBAction func markOnMapAction(_ sender: Any) {
        let controller = MapAddressController()
        controller.didChangeAddress = { [weak self] address in
            guard let `self` = self else { return }
            self.isChanged = true
            self.addressLabel.text = address
            self.city = controller.city
        }
        navigationController?.pushViewController(controller, animated: true)
    }

    private func goToSetAddress() {
        let placePickerController = GMSPlacePickerViewController(config: GMSPlacePickerConfig(viewport: nil))
        placePickerController.delegate = self
        present(placePickerController, animated: true)
//        let controller = SearchPlacesController(citiesOnly: false)
//        controller.didChangeAddress = { [weak self] address in
//            guard let `self` = self else { return }
//            self.addressLabel.text = address
//            self.city = controller.city
//            if let city = controller.city { self.addressLabel.text?.append(", \(city)") }
//            self.isChanged = true
//        }
//        controller.presentIntoNavController(by: self)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        addressLabel.text = place.name
        city = place.city
        if let city = self.city { addressLabel.text?.append(", \(city)") }
        isChanged = true
        viewController.dismiss(animated: true)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true)
    }

    private func showAlertAboutNoValideField(withName name: String) {
        let alert = UIAlertController(title: NSLoc("error_form"), message: NSLoc("error_form1") + NSLoc(name), preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }

    private func validateFields() -> Bool {
        if date == nil {
            showAlertAboutNoValideField(withName: "date")
            return false
        }
        let name = eventNameTextField.text
        if name == nil || name!.isEmpty {
            showAlertAboutNoValideField(withName: "name")
            return false
        }
        if category == nil {
            showAlertAboutNoValideField(withName: "category")
            return false
        }
        let descr = descriptionTextView.text
        if descr == nil {
            showAlertAboutNoValideField(withName: "description")
            return false
        }
        guard avatarWithPhotosController.isAvatarExists else {
            showAlertAboutNoValideField(withName: "main_photo")
            return false
        }
        return true
    }

    @IBAction private func editionDoneAction() {
        guard validateFields() else { return }
        let model = StoreEventRequestModel()
        model.name = eventNameTextField.text
        model.category = category
        model.date = DateConverter.ForServer.Event.eventDateString(from: date!)
        model.description = descriptionTextView.text
        if let incomingModel = self.model, let id = incomingModel.id { model.id = id }
        model.location = addressLabel.text
        model.sex = sex
        HUD.show(.progress)
        ServerApi.shared.storeEvent(with: model, completion: { [weak self] id in
            guard let `self` = self else { return }
            self.eventId = id
            self.avatarWithPhotosController.ownerId = id
            self.avatarWithPhotosController.applyChanges(withCompletion: {
                self.uploadVideoToEvent(withId: id)
            })
        }) { [weak self] (error, _) in
            self?.showError(error)
        }
    }
    
    private func uploadVideoToEvent(withId id: Int? = nil) {
        let eventId = id ?? model.id
        UploadEventVideoRequestModel.createAsync(withEventId: eventId!, inputs: videoInfoToUpload, completion: { [weak self] model in
            guard let `self` = self else { return }
            guard let _model = model else {
                HUD.hide()
                AlertsPresentator.presentAlertErrorWhileProcessingVideo(in: self)
                return
            }
            ServerApi.shared.uploadEventVideo(with: _model, completion: {
                self.onDoneSuccessEdition()
            }, errorBlock: { [weak self] (error, _) in
                self?.showError(error)
            })
        })
    }
    
    private func onDoneSuccessEdition() {
        HUD.flash(.success, onView: self.view, delay: 0.3) { [weak self] _ in
            guard let `self` = self, let navController = self.navigationController else { return }
            let completion: ()->Void = {
                if self.isAddition, let id = self.eventId {
                    var controllers = navController.viewControllers
                    controllers.removeLast()
                    controllers.append(EventDetailsController(eventId: id))
                    navController.setViewControllers(controllers, animated: true)
                }
                else {
                    navController.popViewController(animated: true)
                }
            }
            if let city = self.city, city != UserInfoService.shared.city.components(separatedBy: ", ")[0] {
                AlertsPresentator.presentAlertChangeCityToSeeEventInFeed(in: self, withAddress: self.addressLabel.text!, completion: completion)
            }
            else { completion() }
        }
    }
    
    override func backAction() {
        if isChanged || avatarWithPhotosController.isChanged {
            AlertsPresentator.presentAlertExitWithoutSaving(in: self, withCompletion: { exit in
                if exit { super.backAction() }
            })
        }
        else {
            super.backAction()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        isChanged = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        isChanged = true
        descriptionPlaceHolderLabel.isHidden = true
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        descriptionPlaceHolderLabel.isHidden = textView.text != nil && !textView.text!.isEmpty
    }
    
    @IBAction func hideEventForFriendsSwitchDidChangeAction() {
        isChanged = true
    }
    
    deinit {
        VideoTrimmerController.clearLocalCache()
    }
    
    
    //MARK: UIImagePickerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let url = info[UIImagePickerControllerMediaURL] as? URL else {
            print("NO URL RECEIVED FROM PICKER")
            return
        }
        picker.dismiss(animated: true) {
            let trimmer = VideoTrimmerController.presented(in: self, withVideoUrl: url)
            trimmer.onTrimVideo = { url in
                self.videoInfoToUpload.append((url, generateThumbnail(forVideoWithUrl: url)))
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}
