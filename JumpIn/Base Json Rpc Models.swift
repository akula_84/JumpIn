//
//  BaseJsonRpcModel.swift
//  JumpIn
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


protocol JsonRpcBaseModelProtocol: class, Mappable {
    var jsonrpc: String { get }
}

protocol JsonRpcRequestResponseModelProtocol: JsonRpcBaseModelProtocol {
    var id: Int! { get set }
}

protocol JsonRpcRequestPayloadModelProtocol: class, Mappable {}

protocol JsonRpcRequestModelProtocol: JsonRpcRequestResponseModelProtocol {
    associatedtype PayloadModelT: JsonRpcRequestPayloadModelProtocol
    var method: String! { get }
    var payloadModel: PayloadModelT? { get set }
}

protocol JsonRpcResponseComplexResultModelProtocol: class, Mappable { }

protocol JsonRpcResponseModelProtocol: JsonRpcRequestResponseModelProtocol {
    associatedtype ResultT
    var result: ResultT! { get }
}

protocol JsonRpcResponseModelWithComplexResultProtocol {
    associatedtype ComplexResultT: JsonRpcResponseComplexResultModelProtocol
    var result: ComplexResultT! { get }
}

protocol JsonRpcEventModelProtocol: JsonRpcBaseModelProtocol {
    associatedtype ResultT
    var result: ResultT! { get }
}

class JsonRpcRequestPayloadModel: JsonRpcRequestPayloadModelProtocol {
    init() {}
    required init?(map: Map) {}
    func mapping(map: Map) {}
}

class JsonRpcBaseModel: JsonRpcBaseModelProtocol {
    
    var jsonrpc = "2.0"
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        jsonrpc <- map["jsonrpc"]
    }
}

class JsonRpcErrorModel: JsonRpcBaseModel {
    
    struct Error: Mappable {
        
        var code: Int!
        var message: String!
        
        init?(map: Map) {}
        
        mutating func mapping(map: Map) {
            code <- map["code"]
            message <- map["message"]
        }
    }
    
    var error: Error!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        error <- map["error"]
    }
}

class JsonRpcRequestResponseModel: JsonRpcBaseModel, JsonRpcRequestResponseModelProtocol {
    
    var id: Int!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
    }
}

class JsonRpcRequestModel<PayloadModel: JsonRpcRequestPayloadModelProtocol>: JsonRpcRequestResponseModel, JsonRpcRequestModelProtocol {
    
    var method: String!
    var payloadModel: PayloadModel?
    
    init(payloadModel: PayloadModel?) {
        self.payloadModel = payloadModel
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        method <- map["method"]
        payloadModel <- map["params"]
    }
}

class JsonRpcResponseComplexResultModel: JsonRpcResponseComplexResultModelProtocol {
    required init?(map: Map) {}
    func mapping(map: Map) {}
}

class JsonRpcResponseModelWithComplexResult<ComplexResult: JsonRpcResponseComplexResultModelProtocol>: JsonRpcResponseModel<ComplexResult>, JsonRpcResponseModelWithComplexResultProtocol { }

class JsonRpcResponseModel<Result>: JsonRpcRequestResponseModel, JsonRpcResponseModelProtocol {
    
    var result: Result!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        result <- map["result"]
    }
}

class JsonRpcEventModel<Result>: JsonRpcBaseModel, JsonRpcEventModelProtocol {
    
    var result: Result!
}
