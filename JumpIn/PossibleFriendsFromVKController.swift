//
//  InviteFriendsInAppController.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


extension BaseUserCellViewModel {
    
    convenience init(withModel model: GetFriendsFromSocNetResponseModelItem) {
        self.init(nick: model.name)
        avatarLink = model.avatarLink
    }
}

class PossibleFriendsFromVKControllerViewModel: PageViewModelProtocol {
    
    var viewModels: [BaseUserCellViewModel]
    
    required init(withModel model: GetFriendsFromSocNetResponseModel) {
        viewModels = model.models!.map({ BaseUserCellViewModel(withModel: $0) })
    }
}


class PossibleFriendsFromVKController: BaseConfigurableListController<PossibleFriendsFromVKControllerViewModel> {
    
    var tableManager: TableViewManager<UITableView, InviteCellManager>!
    var loader: PageDataLoadingManager<GetFriendsFromSocNetRequestModel, PossibleFriendsFromVKController>!
    var inviteRequestModels: [SendInviteToAppRequestModelItem]? {
        guard let selectedIndexPaths = tableManager.tableView.indexPathsForSelectedRows, !selectedIndexPaths.isEmpty else {
            return nil
        }
        let invites = selectedIndexPaths.map({ indexPath in
            SendInviteToAppRequestModelItem(socNetUserId: loader.model.models![indexPath.row].socNetUserId!, provider: .vk)
        })
        return invites
    }
    var loaded = false
    
    init() {
        super.init(nibName: nil, bundle: nil)
        title = NSLoc("not_invited")
        setupLoader()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init() instead")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableManager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.reloadData()
    }
    
    private func setupLoader() {
        loader = PageDataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getFriendsFromSocNet as AnyObject)
        loader.requestModel = GetFriendsFromSocNetRequestModel(filter: .withoutJumpinFriends)
        loader.requestModel.socNetProvider = .vk
        loader.requestModel.socNetToken = VKApi.shared.token
    }
    
    private func setupTableManager() {
        tableManager = TableViewManager(with: view)
        tableManager.didSearch = { [weak self] in
            self?.loader.requestModel.searchString = $0
            self?.loader.reloadData()
        }
        tableManager.tableInitConfig.isCellHaveNib = false
        tableManager.tableInitConfig.shouldSetupRefreshControl = true
        tableManager.tableInitConfig.onRefresh = { [weak self] in
            self?.loader.reloadData()
        }
        tableManager.setup()
        tableManager.tableView.allowsMultipleSelection = true
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            guard let `self` = self else { return }
            if self.loaded { self.loader.loadNextPage(animated: false) }
        }
    }
    
    override func refresh() {
        loaded = true
        tableManager.dataSource = viewModel.viewModels
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .bottom : .none
        tableManager.reloadData()
    }
    
//    private func prepareBinder() -> SocNetBinder {
//        if binder == nil {
//            binder = SocNetBinder(delegate: self)
//        }
//        return binder!
//    }
}

