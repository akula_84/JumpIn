//
//  CategoryListController.swift
//  JumpIn
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD
import SideMenu


class CategoryListControllerViewModel: BaseViewModel<GetEventsCategoriesResponseModel> {
    
    var categories: [CategoryCellViewModel]
    
    required init(withModel model: GetEventsCategoriesResponseModel) {
        categories = model.categories!.map({ CategoryCellViewModel(withModel: $0) })
        super.init(withModel: model)
    }
}

class CategoryListController: BaseConfigurableController<CategoryListControllerViewModel> {
    
    struct EventsTypeInfo {
        let title: String
        let requestModelEventsType: GetEventCategoriesRequestModel.EventsType
        let eventsFeedControllerEventsType: EventsFeedController.EventsType
        let searchController: UIViewController
        let shouldSetupHistoryButton: Bool
        
        init(isCurrentEvents: Bool) {
            if isCurrentEvents {
                title = NSLoc("categories")
                requestModelEventsType = .active
                eventsFeedControllerEventsType = .generalCurrent
                searchController = TopEventsFeedController(isSearching: true, withFilter: false)
                shouldSetupHistoryButton = true
            }
            else {
                title = NSLoc("pastEvents")
                requestModelEventsType = .inactive
                eventsFeedControllerEventsType = .generalLast
                searchController = LastTopEventsFeedController(isSearching: true)
                shouldSetupHistoryButton = false
            }
        }
    }
    
    private let eventsTypeInfo: EventsTypeInfo
    private var loader: DataLoadingManager<GetEventCategoriesRequestModel, CategoryListController>!
    private var tableManager: TableViewManager<UITableView, CategoryCellManager>!
    private let isCurrentEvents: Bool
    
    
    init(isCurrentEvents: Bool) {
        self.isCurrentEvents = isCurrentEvents
        eventsTypeInfo = EventsTypeInfo(isCurrentEvents: isCurrentEvents)
        super.init(nibName: nil, bundle: nil)
        self.title = eventsTypeInfo.title
        loader = DataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getEventCategoryList as AnyObject)
        loader.requestModel = GetEventCategoriesRequestModel(withType: eventsTypeInfo.requestModelEventsType)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableManager = TableViewManager(with: view)
        tableManager.tableInitConfig.cellHeight = 175.0
        tableManager.tableInitConfig.shouldSetupSearchBar = false
        tableManager.setup()
        tableManager.locationOfCellWithLoadingIndicator = .none
        tableManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let category = self.loader.model.categories[self.tableManager.indexOfDataSourceItem(forCellAt: indexPath)].category
            let eventsType = self.eventsTypeInfo.eventsFeedControllerEventsType
            let targetController: UIViewController
            if self.isCurrentEvents {
                targetController = EventCategoryDetailsController(category: category, eventsType: eventsType, isCurrentEvents: self.isCurrentEvents)
            }
            else { targetController = DetailedCategoryForLastEventsController(category: category) }
            self.navigationController!.pushViewController(targetController, animated: true)
        }
        setupNavBarButtons()
        loader.reloadData()
        if isCurrentEvents { SideMenuManager.removeAllPanGestureRecognizers() }
    }
    
    override func refresh() {
        tableManager.dataSource = viewModel.categories
        tableManager.reloadData()
    }
    
    private func setupNavBarButtons() {
        let searchBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_search_white"), style: .plain, target: self, action: #selector(searchAction))
        var barButtons = [searchBarButton]
        if eventsTypeInfo.shouldSetupHistoryButton {
            let historyButton = UIButton(type: .custom)
            historyButton.setImage(#imageLiteral(resourceName: "ic_history"), for: .normal)
            historyButton.addTarget(self, action: #selector(historyAction), for: .touchUpInside)
            historyButton.sizeToFit()
            let historyBarButton = UIBarButtonItem(customView: historyButton)
            barButtons.append(historyBarButton)
        }
        navigationItem.rightBarButtonItems = barButtons
    }
    
    @objc private func searchAction() {
        navigationController?.pushViewController(eventsTypeInfo.searchController, animated: true)
    }
    
    @objc private func historyAction() {
        navigationController?.pushViewController(CategoryListController(isCurrentEvents: false), animated: true)
    }
}
