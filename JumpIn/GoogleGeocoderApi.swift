//
//  GoogleGeocoderApi.swift
//  JumpIn
//
//  Created by Admin on 20.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper
import CoreLocation

class GoogleGeocoderApi {
    
    static let shared = GoogleGeocoderApi()
    private let urlEngine = MainURLEngine(with: "https://maps.googleapis.com/maps/api/geocode")
    
    private init() {}
    
    private func send(request model:Mappable?, parser:JsonParser? = nil, completionForEmptyResponse:(()->Void)? = nil, shouldUseHttpMethodPost:Bool = false, errorBlock:RequestErrorBlockType?) {
        urlEngine.send(request: model, method: "json", parser: parser, completionForEmptyResponse: completionForEmptyResponse, shouldUseHttpMethodPost: shouldUseHttpMethodPost, errorBlock: errorBlock)
    }
    
    func geocodeAddress(with model: GoogleGeocoderRequestModel, completion: ((CLLocationCoordinate2D)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser() { (model: GoogleGeocoderResponseModel) in
            if model.status == "OK", !model.results.isEmpty {
                let coordinate: CLLocationCoordinate2D
                let mappableCoordinate = model.results.first!.geometry.location!
                coordinate = CLLocationCoordinate2D(latitude: mappableCoordinate.latitude, longitude: mappableCoordinate.longitude)
                completion?(coordinate)
            }
            else {
                errorBlock?(nil, nil)
            }
        }
        send(request: model, parser: parser, errorBlock: errorBlock)
    }

}
