//
//  ProfileEditingController.swift
//  JumpIn
//
//  Created by Admin on 20.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


extension ProfileEditingController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentAlertMissedCity() {
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let alert = UIAlertController(title: "", message: NSLoc("error_city"), preferredStyle: .alert)
        alert.addAction(ok)
        present(alert, animated: true)
    }
    
    func presentSexAlert() {
        let alert = UIAlertController(title: NSLoc("error_sex"), message: nil, preferredStyle: .actionSheet)
        let man = UIAlertAction(title: NSLoc("Men"), style: .default, handler: { _ in self.sex = .Man })
        let woman = UIAlertAction(title: NSLoc("Ladies"), style: .default, handler: { _ in self.sex = .Woman })
        alert.addAction(man)
        alert.addAction(woman)
        present(alert, animated: true, completion: nil)
    }
}
