//
//  JUMPlaceholderController.swift
//  JumpIn
//
//  Created by Артем Кулагин on 23.04.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit

class JUMPlaceholderController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        JUMAuthManager.checkStatusInit()
    }
}
