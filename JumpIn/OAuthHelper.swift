//
//  OAuthHelper.swift
//  JumpIn
//
//  Created by Admin on 26.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


protocol OAuthModelProtocol {
    
    associatedtype RequestModel: Mappable
    
    var appId: String { get }
    var kAccessToken: String { get }
    var kError: String { get }
    var authorizeMethodName: String { get }
    var baseLink: String { get }
    var requestModel: RequestModel { get }
    var displayName: String { get }
    var redirectLink: String { get }
    //
    init()
}

protocol OAuthHelperProtocol {
    
    associatedtype OAuthModelT: OAuthModelProtocol
    
    var token: String? { get }
    var lastError: String? { get }
    var oAuthModel: OAuthModelT { get }
    //
    func authorizationRequest() -> URLRequest
    func isValidAuthorizationResult(link: String) -> Bool
    func parseAuthorizationResult(link: String) -> Bool
    func clearCookieAndCache()
    func logout()
}

class BaseOAuthHelper<OAuthModel: OAuthModelProtocol>: OAuthHelperProtocol {
    
    var token: String?
    private(set) var lastError: String?
    //
    let oAuthModel = OAuthModel()
    private let urlEngine: MainURLEngine
    
    init() {
        urlEngine = MainURLEngine(with: oAuthModel.baseLink)
    }
    
    func authorizationRequest() -> URLRequest {
        return urlEngine.request(with: oAuthModel.requestModel.toJSON(), method: oAuthModel.authorizeMethodName)
    }
    
    func isValidAuthorizationResult(link: String) -> Bool {
        guard let range = link.range(of: oAuthModel.redirectLink), range.lowerBound == link.startIndex else {
            return false
        }
        return true
    }
    
    func parseAuthorizationResult(link: String) -> Bool {
        guard let range = link.range(of: "\(oAuthModel.kAccessToken)=") else {
            guard let range = link.range(of: "\(oAuthModel.kError)=") else { fatalError("Cant parse link") }
            lastError = link[range.upperBound...].replacingOccurrences(of: "+", with: " ")
            token = nil
            clearCookieAndCache()
            return false
        }
        lastError = nil
        token = String(link[range.upperBound...])
        if let stopCharacterIndex = token!.index(of: "&") {
            token = String(token![..<stopCharacterIndex])
        }
        return true
    }
    
    func clearCookieAndCache() {
        URLCache.shared.removeAllCachedResponses()
        HTTPCookieStorage.shared.removeCookies(since: Date(timeIntervalSince1970: 0))
    }
    
    func logout() {
        clearCookieAndCache()
    }
}
