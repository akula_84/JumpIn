//
//  ServerPhotoModel.swift
//  JumpIn
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


class PhotoWithLikesModel: ModelProtocol {
    
    var id: Int!
    var filepath: String!
    var likes: Int!
    var dislikes: Int!
    var likedMe: Bool?
    
    required init?(map: Map) {}
    init() {}
    
    func mapping(map: Map) {
        id <- map["id"]
        filepath <- map["absolute_path"]
        likes <- map["likes"]
        dislikes <- map["dislikes"]
        likedMe <- map["liked_me"]
    }
}

class PhotoWithLikesViewModel: BaseViewModel<PhotoWithLikesModel> {

    var id: Int
    var likes: Int
    var dislikes: Int
    var voteMe: Bool?
    var link: String?
    
    required init(withModel model: PhotoWithLikesModel) {
        id = model.id
        likes = model.likes
        dislikes = model.dislikes
        voteMe = model.likedMe
        link = model.filepath
        super.init(withModel: model)
    }
}

class PhotoWithLikesModelsPage: PageModel<PhotoWithLikesModel> {}
class PhotoWithLikesViewModelsPage: PageViewModel<PhotoWithLikesModelsPage, PhotoWithLikesViewModel> {}
