//
//  LogoutRequestModel.swift
//  JumpIn
//
//  Created by Ar4ibald Fox on 11.04.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import ObjectMapper


class LogoutRequestModel: Mappable {
    
    var deviceToken: String!
    
    init(withDeviceToken token: String) {
        deviceToken = token
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        deviceToken <- map["token"]
    }

}
