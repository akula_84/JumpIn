//
//  ObjectHolder.swift
//  JumpIn
//
//  Created by Admin on 19.04.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation

class ObjectHolder {
    static var sharedInstance = ObjectHolder()
    private var holdingObjects: [Int:AnyObject] = [:]
    private var counter: Int = 0
    
    
    private init() {}
    
    func upCount() {
        //
        counter = counter + 1
    }
    
    func hold(object: AnyObject) -> Int {
        //
        upCount()
        holdingObjects[counter] = object
        return counter
    }
    
    func release(objectWithIndex index: Int) {
        //
        holdingObjects.removeValue(forKey: index)
    }
}
