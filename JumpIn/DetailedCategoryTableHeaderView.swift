//
//  DetailedCategoryTableHeaderView.swift
//  JumpIn
//
//  Created by Admin on 20.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class DetailedCategoryTableHeaderView: BaseCustomView {
    
    @IBOutlet private weak var categoryMainImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var numberOfEventsLabel: UILabel!
    //
    var categoryImage: UIImage? { didSet { refresh() } }
    var titleText: String? { didSet { refresh() } }
    var numberOfEvents: Int? { didSet { refresh() } }
    
    private func refresh() {
        guard internalView != nil else { return }
        categoryMainImageView.image = categoryImage
        titleLabel.text = titleText
        numberOfEventsLabel.text = numberOfEvents != nil ? String(describing: numberOfEvents) : ""
    }
    
    override func setup() {
        super.setup()
        refresh()
    }
}
