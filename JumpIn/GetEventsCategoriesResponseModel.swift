//
//  GetEventsCategoriesResponseModel.swift
//  JumpIn
//
//  Created by Admin on 28.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class GetEventsCategoriesResponseModel: ModelProtocol {

    class CategoryModel: ModelProtocol {
        
        var category: CategoryInfoService.CategoryInfo.Name?
        var eventsCount: Int!
        
        required init?(map: Map) {}
        
        func mapping(map: Map) {
            category <- map["category"]
            eventsCount <- map["count"]
        }
    }
    
    var categories: [CategoryModel]!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        categories <- map["categories"]
    }
}
