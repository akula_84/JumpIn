//
//  MemberCell.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class ConfirmedMemberCellManager: BaseUserCellManager<BaseUserCellViewModel, ConfirmedMemberCell> {}

class ConfirmedMemberCell: BaseUserCell {
    
    var onTapRemoveButton: (()->Void)?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        rightButtonItems = [(ButtonItem(image: #imageLiteral(resourceName: "ic_little_red_user_with_cross"), action: { [weak self] in self?.onTapRemoveButton?() }))]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
