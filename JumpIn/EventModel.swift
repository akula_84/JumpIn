//
//  BaseEventModel.swift
//  JumpIn
//
//  Created by Admin on 22.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class EventPageModel: PageModel<EventModel> {}

class EventModel: ModelProtocol {
    
    enum TimeFrame: String {
    case current, past = "archive"
}
    
    var name: String!
    var id: Int!
    var category: CategoryInfoService.CategoryInfo.Name?
    var isMy: Bool!
    var timeFrame: TimeFrame!
    var avatar: PhotoWithLikesModel?
    var membersCount: Int!
    var likes: Int!
    var dislikes: Int!
    var likedMe: Bool?
    var withMe: Bool!
    var meRequested: Bool!
    var isPremium: Bool!
    var location: String?
    
    var locationCity: String {
        return location?.components(separatedBy: ", ").first ?? ""
    }

    required init?(map: Map) {}
    
    func mapping(map: Map) {
        print("EventModel",map.JSON)
        name <- map["name"]
        id <- map["id"]
        category <- map["category"]
        isMy <- map["own_me"]
        timeFrame <- map["time_frame"]
        avatar <- map["avatar"]
        membersCount <- map["users_count"]
        likes <- map["likes"]
        dislikes <- map["dislikes"]
        likedMe <- map["liked_me"]
        withMe <- map["is_participant"]
        meRequested <- map["is_requested_me"]
        isPremium <- map["premium"]
        location <- map["location"]
    }
}
