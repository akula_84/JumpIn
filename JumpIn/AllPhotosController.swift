//
//  AllPhotosController.swift
//  JumpIn
//
//  Created by IvanLazarev on 04/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class AllPhotosController: BaseConfigurableController<AllPhotosControllerViewModel> {
    
    enum OwnerType: String {
        case users, events
    }

    var loader: DataLoadingManager<GetPhotosRequestModel, AllPhotosController>!
    var photosCollectionViewManager: ShowPhotosCollectionViewManager!

    @IBOutlet weak var photoCollectionView: UICollectionView!

    init(ownerType: OwnerType, ownerId: Int) {
        //self.ownerType = ownerType
        //self.ownerId = ownerId
        super.init(nibName: String(describing: type(of: self).self), bundle: nil)
        self.loader = DataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getPhotos as AnyObject)
        self.loader.requestModel = GetPhotosRequestModel(type: GetPhotosRequestModel.OwnerType(rawValue: ownerType.rawValue)!, id: ownerId)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLoc("photos")
        setupPhotoCollectionManager()
        loader.reloadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let layout = photoCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 3
        let width = (photoCollectionView.bounds.width - 2 * layout.minimumInteritemSpacing)/3
        layout.itemSize = CGSize(width: width, height: width)
    }

    private func setupPhotoCollectionManager() {
        photosCollectionViewManager = ShowPhotosCollectionViewManager(with: photoCollectionView)
        photosCollectionViewManager.setup()
        photosCollectionViewManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let type = SlideshowPhotosController.OwnerType(rawValue: self.loader.requestModel!.type.rawValue)!
            let id = self.loader.requestModel!.id
            let photos = self.viewModel.photos
            let index = self.photosCollectionViewManager.indexOfDataSourceItem(forCellAt: indexPath)
            let controller = SlideshowPhotosController(photoViewModels: photos, index: index, ownerType: type, ownerId: id)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    override func refresh() {
        photosCollectionViewManager.dataSource = viewModel.photos
        photosCollectionViewManager.reloadData()
    }
}
