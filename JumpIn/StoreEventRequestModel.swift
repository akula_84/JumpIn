//
//  StoreEventRequestModel.swift
//  JumpIn
//
//  Created by Admin on 18.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


//enum StoreEventSexModel: Int {
//    case Woman = 1, Man, Both
//
//    init(sexModel: SexModel?) {
//        guard let model = sexModel else { self = .Both; return }
//        self = StoreEventSexModel(rawValue: model.rawValue)!
//    }
//}


class StoreEventRequestModel: Mappable {
    
    var name: String!
    var category: CategoryInfoService.CategoryInfo.Name!
    var description: String!
    var date: String! //Y-m-d H:i:s
    var location: String!
    var sex: SexModel?
    var id: Int?
    var isPremium: Bool?
    
    required init?(map: Map) {}
    init() {}
    
    func mapping(map: Map) {
        name <- map["name"]
        category <- map["category"]
        date <- map["date_time"]
        location <- map["location"]
        sex <- map["gender"]
        id <- map["id"]
        description <- map["description"]
        isPremium <- map["premium"]
    }
}
