//
//  SlideshowPhotosController.swift
//  JumpIn
//
//  Created by IvanLazarev on 04/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage
import PKHUD


class SlideshowPhotosController: BaseConfigurableController<SlideshowPhotosControllerVM> {
    
    enum OwnerType: String {
        case users, events
    }
    
    var loader: DataLoadingManager<GetPhotosRequestModel, SlideshowPhotosController>!
    var photosCollectionViewManager: ShowPhotosCollectionViewManager!
    var initialSelectedPhotoId: Int!
    var index: Int!
    private var currentPhoto: PhotoWithLikesViewModel {
        return viewModel.photos[index]
    }

    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var dislikesLabel: UILabel!
    @IBOutlet weak var likeButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var dislikeButton: BaseCustomButtonWithoutXib!
    
    private init(ownerType: OwnerType, ownerId: Int) {
        super.init(nibName: String(describing: type(of: self).self), bundle: nil)
        self.loader = DataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getPhotos as AnyObject)
        self.loader.requestModel = GetPhotosRequestModel(type: GetPhotosRequestModel.OwnerType(rawValue: ownerType.rawValue)!, id: ownerId)
    }
    
    convenience init(selectedPhotoId: Int, ownerType: OwnerType, ownerId: Int) {
        self.init(ownerType: ownerType, ownerId: ownerId)
        self.initialSelectedPhotoId = selectedPhotoId
    }
    
    convenience init(photoViewModels photos: [PhotoWithLikesViewModel], index: Int, ownerType: OwnerType, ownerId: Int) {
        self.init(ownerType: ownerType, ownerId: ownerId)
        self.index = index
        self.viewModel = SlideshowPhotosControllerVM(withPhotoViewModels: photos)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if viewModel == nil { reloadData(withSpecialAnimationAfterSuccessLoading: false) } else { refresh() }
        setupLikesButtons()
        setupSlideshow()
        if let index = index { slideshow.setCurrentPage(index, animated: false) }
        setupNavBarButtons()
    }
    
    private func setupNavBarButtons() {
        let more = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_complain"), style: .plain, target: self, action: #selector(moreAction))
        navigationItem.rightBarButtonItem = more
    }
    
    @objc private func moreAction() {
        AlertsPresentator.presentActionsForPhoto(in: self) { [weak self] action in
            guard let action = action, let `self` = self else { return }
            switch action {
            case .complain: self.complain(photoId: self.viewModel.photos[self.index].id)
            }
        }
    }
    
    private func complain(photoId: Int) {
        HUD.show(.progress)
        let model = ComplainRequestModel(withObjectType: .photo, objectId: photoId)
        ServerApi.shared.complain(withModel: model, completion: { HUD.flash(.success) }, errorBlock: { [weak self] error, _ in
            self?.showError(error)
        })
    }

    private func setupLikesButtons() {
        likeButton.onClick = { [weak self] _ in
            guard let `self` = self else { return }
            let rating = self.currentPhoto.voteMe == true ? nil : true
            self.voteAction(rating: rating)
        }
        dislikeButton.onClick = { [weak self] _ in
            guard let `self` = self else { return }
            let rating = self.currentPhoto.voteMe == false ? nil : false
            self.voteAction(rating: rating)
        }
    }

    override func refresh() {
        let inputs = viewModel.photos.compactMap {
            $0.link != nil ? SDWebImageSource(url: URL(string: $0.link!)!) : nil
        }
        slideshow.setImageInputs(inputs)
        if index == nil {
            index = viewModel.photos.index(where: { $0.id == self.initialSelectedPhotoId })!
            slideshow.setCurrentPage(index, animated: false)
        }
        likesLabel.text = "\(currentPhoto.likes)"
        dislikesLabel.text = "\(currentPhoto.dislikes)"
        navigationItem.title = "\(slideshow.currentPage + 1) из \(slideshow.images.count)"
    }

    func setupSlideshow() {
        slideshow.zoomEnabled = true
        slideshow.pageControlPosition = .hidden
        slideshow.currentPageChanged = { [weak self] index in
            self?.index = index
            self?.refresh()
        }
    }

    func reloadData(withSpecialAnimationAfterSuccessLoading useSpecialAnimation: Bool) {
        if useSpecialAnimation {
            loader.reloadData(animated: false) { [weak self]  success, error in
                if success { HUD.flash(.success) } else { self?.showError(error) }
            }
        }
        else { loader.reloadData() }
    }

    func voteAction(rating: Bool?) {
        let model = VoteRequestModel(type: .photo, id: currentPhoto.id, rating: rating)
        HUD.show(.progress)
        ServerApi.shared.vote(withModel: model, completion: { [weak self] in
            self?.reloadData(withSpecialAnimationAfterSuccessLoading: true)
        }) { [weak self] (error: MainURLEngine.Error?, _: HTTPURLResponse?) in
            self?.showError(error)
        }
    }
}
