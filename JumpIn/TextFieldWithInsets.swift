//
//  TextFieldWithInsets.swift
//  JumpIn
//
//  Created by Admin on 03.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

@IBDesignable
class TextFieldWithInsets: UITextField {

    @IBInspectable var leftPadding: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = bounds
        rect.origin.x += leftPadding
        return rect
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        var rect = bounds
        rect.origin.x += leftPadding
        return rect
    }
}
