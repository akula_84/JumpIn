//
//  GetFriendsFromSocialNetworksRequestModel.swift
//  JumpIn
//
//  Created by Admin on 23.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetFriendsFromSocNetRequestModel: BaseRequestModelWithPagination {
    
    enum Filter: String {
        case no = "all", withoutJumpinFriends = "without_jumpin_friends", referredUsers = "referred_users"
    }
    
    enum SocNetProvider: String {
        case vk = "vkontakte"
    }

    var filter: Filter!
    var socNetToken: String?
    var socNetProvider: SocNetProvider?
    var searchString: String?
    
    init(filter: Filter) {
        self.filter = filter
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        filter <- map["filter"]
        socNetToken <- map["token"]
        socNetProvider <- map["provider"]
        searchString <- map["search"]
    }
}
