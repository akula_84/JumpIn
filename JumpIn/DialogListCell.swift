//
//  DialogListCell.swift
//  JumpIn
//
//  Created by Admin on 01.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SwipeCellKit


class DialogListCellManager: BaseViewManager<DialogListCellViewModel, DialogListCell> {
    override func refresh() {
        view.userNameLabel.text = viewModel.user.name
        view.userAvatarImageView.image = #imageLiteral(resourceName: "user_no_image")
        view.userAvatarImageView.imageLink = viewModel.user.avatarLink
        view.lastMessageLabel.text = viewModel.lastMessage.text
        view.lastMessageTimeLabel.text = viewModel.lastMessage.timestamp
        view.numberOfNewMessagesLabel.text = viewModel.numberOfNewMessages
        view.numberOfNewMessagesLabel.superview!.isHidden = viewModel.numberOfNewMessages == nil
    }
}

class DialogListCell: SwipeTableViewCell, Configurable2 {
    @IBOutlet weak var userAvatarImageView: DownloadingImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var lastMessageTimeLabel: UILabel!
    @IBOutlet weak var numberOfNewMessagesLabel: UILabel!
    //
    var configurator: AnyObject?
}
