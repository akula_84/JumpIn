//
//  SideMenuManagerExtention.swift
//  JumpIn
//
//  Created by Admin on 19.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import SideMenu


extension SideMenuManager {

    static var presentingNavController: UINavigationController {
        return UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController
    }

    class func setup() {
        self.configureSideMenu(withLeftMenuController: MenuController())
    }
    
    class func removeAllPanGestureRecognizers() {
        presentingNavController.navigationBar.removeAllPanGestureRecognizers()
        presentingNavController.view.removeAllPanGestureRecognizers()
    }
    
    class func repairGestureRecognizers() {
        SideMenuManager.default.menuAddPanGestureToPresent(toView: presentingNavController.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: presentingNavController.view, forMenu: .left)
    }

    class func configureSideMenu(withLeftMenuController controller: UIViewController) {
        let menuNavController = UISideMenuNavigationController(rootViewController: controller)
        menuNavController.leftSide = true
        SideMenuManager.default.menuLeftNavigationController = menuNavController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: presentingNavController.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: presentingNavController.view, forMenu: .left)
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - 57
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAllowPushOfSameClassTwice = false
    }
}


extension UIViewController {

    var menu: UISideMenuNavigationController? {
        return SideMenuManager.default.menuLeftNavigationController
    }

    @objc func toggleMenu() {
        guard let menu = menu else { return }
        if menu.isOpened { menu.close() }
        else { menu.open() }
    }
}


extension UISideMenuNavigationController {

    var isOpened: Bool {
        return presentingViewController != nil
    }

    func open() {
        SideMenuManager.presentingNavController.present(self, animated: true)
    }

    func close() {
        dismiss(animated: true)
    }
}

extension UIView {
    
    func removeAllPanGestureRecognizers() {
        gestureRecognizers?.forEach({
            if $0 is UIPanGestureRecognizer {
                $0.view!.removeGestureRecognizer($0)
            }
        })
    }
}

