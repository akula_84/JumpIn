//
//  BaseRequestModelWithPagination.swift
//  JumpIn
//
//  Created by Admin on 15.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


protocol RequestModelWithPageProtocol: class {
    var page: Int { get set }
}


class BaseRequestModelWithPagination: RequestModelWithPageProtocol, Mappable {
    
    var page: Int = 1
    var pageMapKey: String { return "page" }
    
    init() { }

    required init?(map: Map) { }
    
    func mapping(map: Map) {
        page <- map[pageMapKey]
    }
}
