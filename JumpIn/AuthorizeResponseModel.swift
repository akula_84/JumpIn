//
//  JWTokenResponse.swift
//  JumpIn
//
//  Created by Admin on 27.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class AuthorizeResponseModel: ModelProtocol {
//
    var jwtoken: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        //
        jwtoken <- map["token"]
    }
}
