//
//  CategoryService.swift
//  JumpIn
//
//  Created by Admin on 16.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class CategoryInfoService {
    
    struct CategoryInfo {
        
        enum Name: String {
            case sporty, residence, light, hitchhike, hard, cultural, oneToOne = "1to1"
        }
        
        let name: Name
        let title: String
        let description: String
        let bgImage: UIImage
        
        init(name: Name) {
            self.name = name
            let _self = type(of: self)
            title = _self.title(for: name)
            description = _self.description(for: name)
            bgImage = UIImage(named: name.rawValue)!
        }
        
        private static func title(for category: CategoryInfo.Name)->String {
            switch category {
                case .cultural: return NSLoc("cultural")
                case .hard: return NSLoc("hard")
                case .hitchhike: return NSLoc("hitchhike")
                case .light: return NSLoc("light")
                case .oneToOne: return NSLoc("oneto1")
                case .residence: return NSLoc("residence")
                case .sporty: return NSLoc("sporty")
            }
        }
        
        private static func description(for category: CategoryInfo.Name)->String {
            switch category {
                case .cultural: return NSLoc("Cultural_events")
                case .hard: return NSLoc("Hard_parties")
                case .hitchhike: return NSLoc("Fellow_travellers")
                case .light: return NSLoc("Light_parties")
                case .oneToOne: return NSLoc("Face_to_face")
                case .residence: return NSLoc("Accomodation")
                case .sporty: return NSLoc("Active_leisure")
            }
        }
    }
    
    static let categories: [CategoryInfo.Name : CategoryInfo] = [
        .sporty: CategoryInfo(name: .sporty),
        .residence: CategoryInfo(name: .residence),
        .light: CategoryInfo(name: .light),
        .hitchhike: CategoryInfo(name: .hitchhike),
        .hard: CategoryInfo(name: .hard),
        .cultural: CategoryInfo(name: .cultural),
        .oneToOne: CategoryInfo(name: .oneToOne)
    ]
}
