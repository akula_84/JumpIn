//
//  Utils.swift
//  Spytricks
//
//  Created by Артем Кулагин on 09.02.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation
import UIKit

typealias EmptyBlock = () -> Void

func NSLoc(_ key: String?) -> String {
    guard let key = key else {
        return ""
    }
    let value = NSLocalizedString(key, comment: "")
    if Utils.isRu {
        return value
    }
    // Fall back to en
    guard
        let path = Bundle.main.path(forResource: "en", ofType: "lproj"),
        let bundle = Bundle(path: path)
        else { return value }
    return NSLocalizedString(key, bundle: bundle, comment: "")
}

class Utils {
    
    static var isRu: Bool {
        return NSLocale.preferredLanguages.first?.hasPrefix("ru") ?? false
    }
    
    static var isIPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static var isIphoneX: Bool {
        return UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436
    }
    
    static func stringFromSwiftClass(_ swiftClass: AnyClass) -> String {
        return NSStringFromClass(swiftClass).components(separatedBy: ".").last!
    }
    static func dictToJson(_ name: String?) -> Any? {
        if let path = Bundle.main.url(forResource: name, withExtension: "json") {
            do {
                let data = try Data(contentsOf: path, options: .alwaysMapped)
                do {
                    return try JSONSerialization.jsonObject(with: data, options: [])
                } catch {
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func mainQueue(_ handler: @escaping EmptyBlock) {
        DispatchQueue.main.async(execute: handler)
    }
    
    static func queueUserInitiated(handler: EmptyBlock?) {
        queueGlobal(.userInitiated, handler: handler)
    }
    
    static func queueBackground(handler: EmptyBlock?) {
        queueGlobal(.background, handler: handler)
    }
    
    static func queueUtility(handler: EmptyBlock?) {
        queueGlobal(.utility, handler: handler)
    }
    
    static func queueGlobal(_ qos: DispatchQoS.QoSClass = DispatchQoS.QoSClass.default, handler: EmptyBlock?) {
        DispatchQueue.global(qos: qos).async {
            handler?()
        }
    }
    
    static func delay(_ delay:Double, closure: EmptyBlock?) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: {
            closure?()
        })
    }
}
