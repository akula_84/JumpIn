//
//  MembersManagingController.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class MembersManagingController: TabController {

    init(eventId: Int) {
        super.init()
        title = NSLoc("managing_participants")
        add(subcontrollers: [ConfirmedMembersController(eventId: eventId), UnconfirmedMembersController(eventId: eventId)])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
