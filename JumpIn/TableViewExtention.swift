//
//  TableViewExtention.swift
//  JumpIn
//
//  Created by Admin on 28.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


extension UITableView {
    
    enum ExceptingSide {
        case none, top, bottom, both
    }
    
    func setupRefreshControl() -> UIRefreshControl {
        let control = UIRefreshControl()
        addSubview(control)
        return control
    }
    
    func setupSearchBar(delegate: UISearchBarDelegate?, insets: UIEdgeInsets, withCustomView customView: UIView? = nil, customViewLocation: TableInitConfiguration.CustomHeaderViewLocation? = nil, customViewInsets: UIEdgeInsets? = nil) -> UISearchBar {
        let contentView = UIView()
        let searchBar = UISearchBar()
        searchBar.placeholder = NSLoc("search")
        searchBar.delegate = delegate
        searchBar.sizeToFit()
        searchBar.autoresizingMask = [.flexibleWidth]
        searchBar.frame.origin.x = insets.left
        let contentWidth = searchBar.bounds.width + insets.left + insets.right
        let contentHeight = searchBar.bounds.height + insets.top + insets.bottom
        let contentFrame = CGRect(origin: .zero, size: CGSize(width: contentWidth, height: contentHeight))
        contentView.frame = contentFrame
        if let view = customView, let viewInsets = customViewInsets, let location = customViewLocation {
            contentView.frame.size.height += view.bounds.height + viewInsets.top + viewInsets.bottom
            view.frame.origin.x = viewInsets.left
            if location == .beforeSearchBar {
                view.frame.origin.y = viewInsets.top
                contentView.addSubview(view)
                searchBar.frame.origin.y = view.frame.maxY + viewInsets.bottom + insets.top
            }
            else if location == .afterSearchBar {
                searchBar.frame.origin.y = insets.top
                view.frame.origin.y = searchBar.frame.maxY + insets.bottom + viewInsets.top
                contentView.addSubview(view)
            }
        }
        contentView.addSubview(searchBar)
        contentView.clipsToBounds = true
        tableHeaderView = contentView
        return searchBar
    }
    
    private func pinToAllSides(view: UIView, usingInsets insets: UIEdgeInsets, excepting: ExceptingSide = .none) {
        var constraints: [NSLayoutConstraint] = []
        constraints.append(contentsOf: [view.leftAnchor.constraint(equalTo: view.superview!.leftAnchor, constant: insets.left),
                                        view.rightAnchor.constraint(equalTo: view.superview!.rightAnchor, constant: -insets.right)])
        if excepting != .both, excepting != .top {
            constraints.append(view.topAnchor.constraint(equalTo: view.superview!.topAnchor, constant: insets.top))
        }
        if excepting != .both, excepting != .bottom {
            view.bottomAnchor.constraint(equalTo: view.superview!.bottomAnchor, constant: -insets.bottom)
        }
        //setupConstraints(constraints)
    }
    
//    private func setupConstraints(_ constraints: [NSLayoutConstraint]) {
//        constraints.forEach( {
//            $0.isActive = true
//            //$0.priority = UILayoutPriority(rawValue: 999)
//        } )
//    }
}
