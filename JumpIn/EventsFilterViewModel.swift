//
//  EventsFilterViewModel.swift
//  JumpIn
//
//  Created by IvanLazarev on 11/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper


class EventsFilterViewModel: BaseViewModel<GetEventsRequestModel> {

    var sortedType: GetEventsRequestModel.PopularityType?
    var startDate: String?
    var endDate: String?
    
    override init() {
        super.init()
    }

    required init(withModel model: GetEventsRequestModel) {
        sortedType = model.popularityType
        startDate = model.dateStart
        endDate = model.dateEnd

        super.init(withModel: model)
    }
}
