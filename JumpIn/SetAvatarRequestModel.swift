//
//  SetAvatarRequestModel.swift
//  JumpIn
//
//  Created by Admin on 29.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class SetAvatarRequestModel: Mappable {

    var targetType: UploadTargetType!
    var photoId: Int!
    var eventId: Int?
    
    required init?(map: Map) {}
    init(targetType type: UploadTargetType, photoId: Int, eventId: Int?) {
        self.targetType = type
        self.photoId = photoId
        self.eventId = eventId
    }
    
    func mapping(map: Map) {
        targetType <- map["model_type"]
        eventId <- map["model_id"]
        photoId <- map["photo_id"]
    }
}
