//
//  EventCategoryDetailsController.swift
//  JumpIn
//
//  Created by Admin on 15.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class HorizontalEventsFeedController: BaseConfigurableController<EventPageViewModel> {
    
    class EventsCollectionViewManager: CollectionViewManager<EventCellManager<EventCollectionCell>> {
        
        private var autoScrollTimer: Timer?
        private var isAutoScrollStopped = false
        
        required init(with collectionView: UICollectionView) {
            super.init(with: collectionView)
            collectionInitConfig.isCellHaveXib = false
        }
        
        func startAutoScroll() {
            isAutoScrollStopped = false
            autoScrollTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(autoScrollTimerAction), userInfo: nil, repeats: false)
        }
        
        @objc private func autoScrollTimerAction() {
            let indexPathsForVisibleItems = collectionView.indexPathsForVisibleItems
            guard indexPathsForVisibleItems.count > 1 else { return }
            let targetIndexPath = collectionView.indexPathsForVisibleItems.sorted { $0.row < $1.row }[1]
            collectionView.scrollToItem(at: targetIndexPath, at: .left, animated: true)
        }
        
        func stopAutoScroll() {
            isAutoScrollStopped = true
            autoScrollTimer?.invalidate()
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if scrollView === collectionView {
                guard !isAutoScrollStopped else { return }
                autoScrollTimer?.invalidate()
                startAutoScroll()
            }
        }
    }
    
    var onRefreshed: (()->Void)?
    let collectionManager: EventsCollectionViewManager
    var loader: DataLoadingManager<GetEventsRequestModel, HorizontalEventsFeedController>!
    var autoHideSliderIfNoElements = true
    override var navigationController: UINavigationController? { return nil }
    
    
    init(collectionView: UICollectionView, eventsType: EventsFeedController.EventsType) {
        self.collectionManager = EventsCollectionViewManager(with: collectionView)
        self.collectionManager.setup()
        super.init(nibName: nil, bundle: nil)
        loader = DataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getEvents as AnyObject)
        loader.requestModel = GetEventsRequestModel(eventsType: eventsType)
        if autoHideSliderIfNoElements { collectionManager.collectionView.isHidden = true }
        view = collectionView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionManager.startAutoScroll()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        collectionManager.stopAutoScroll()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureCollectionManager() {
        collectionManager.dataSource = viewModel.viewModels
        collectionManager.didSelectCellAtIndexPath = { [weak self] in
            guard let `self` = self else { return }
            let event = self.loader.model.models![self.collectionManager.indexOfDataSourceItem(forCellAt: $0)]
            self.parent!.navigationController!.pushViewController(EventDetailsController(eventId: event.id), animated: true)
        }
    }
    
    override func refresh() {
        configureCollectionManager()
        collectionManager.reloadData()
        if autoHideSliderIfNoElements { collectionManager.collectionView.isHidden = collectionManager.dataSource.isEmpty }
        onRefreshed?()
    }
}


class EventCategoryDetailsController: BaseViewController {
    
    @IBOutlet weak var generalStackView: UIStackView!
    @IBOutlet weak var tableStackView: UIStackView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var topEventsCollectionView: UICollectionView!
    @IBOutlet fileprivate weak var categoryMainImage: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var numberOfEventsLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    fileprivate var category: CategoryInfoService.CategoryInfo.Name?
    private let tableSubcontroller: EventsFeedController
    private var horizontalSliderSubcontroller: HorizontalEventsFeedController?
    private let eventsType: EventsFeedController.EventsType
    private var tableRefreshed = false { didSet { resfreshNumberOfEventsLabel() } }
    private var sliderRefreshed = false { didSet { resfreshNumberOfEventsLabel() } }
    private let isPremium: Bool
    private let isCurrentEvents: Bool
    
    
    init(category: CategoryInfoService.CategoryInfo.Name?, eventsType: EventsFeedController.EventsType, isCurrentEvents: Bool) {
        self.isCurrentEvents = isCurrentEvents
        isPremium = category == nil
        self.category = category
        self.eventsType = eventsType
        self.tableSubcontroller = EventsFeedController(withEventsType: eventsType, enableSearching: false)
        super.init(nibName: "EventCategoryDetailsController", bundle: nil)
        addChildViewController(tableSubcontroller)
        tableSubcontroller.didMove(toParentViewController: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        setupNavBarButtons()
        automaticallyAdjustsScrollViewInsets = false
        setupTableSubcontroller()
        setupHorizontalSliderSubcontroller()
        if let c = category {
            let info = CategoryInfoService.categories[c]!
            categoryMainImage.image = info.bgImage
            titleLabel.text = info.title
        }
        else {
            categoryMainImage.image = #imageLiteral(resourceName: "premium")
            titleLabel.text = NSLoc("top_events")
        }
    }
    
    private func setupTableSubcontroller() {
        tableSubcontroller.showCityTitle = true
        tableSubcontroller.loader.requestModel!.category = category
        tableSubcontroller.loader.requestModel!.isPremium = isPremium ? true : nil//isCurrentEvents ? isPremium : nil
        let view = tableSubcontroller.view!
        tableStackView.addArrangedSubview(view)
        tableSubcontroller.tableManager.autoUpdateTableViewHeightToFit = true
        tableSubcontroller.onRefreshed = { [weak self] in
            self?.tableRefreshed = true
            //self?.tableSubcontroller.tableManager.locationOfCellWithLoadingIndicator = .none
        }
        tableSubcontroller.tableManager.locationOfCellWithLoadingIndicator = .none
        tableSubcontroller.tableManager.onShowCellWithLoadingIndicator = nil
    }
    
    private func setupHorizontalSliderSubcontroller() {
        topEventsCollectionView.isHidden = true/*
        guard let isPremium = tableSubcontroller.loader.requestModel!.isPremium, !isPremium, isCurrentEvents else {
            topEventsCollectionView.isHidden = true
            return
        }
        let horizontalSliderSubcontroller = HorizontalEventsFeedController(collectionView: topEventsCollectionView, eventsType: eventsType)
        horizontalSliderSubcontroller.loader.requestModel.isPremium = true
        horizontalSliderSubcontroller.loader.requestModel.category = category
        addChildViewController(horizontalSliderSubcontroller)
        horizontalSliderSubcontroller.didMove(toParentViewController: self)
        horizontalSliderSubcontroller.onRefreshed = { [weak self] in self?.sliderRefreshed = true }
        self.horizontalSliderSubcontroller = horizontalSliderSubcontroller
         */
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableSubcontroller.tableManager.setTableViewHeightToFit()
    }
    
    private func printHeights() {
        print("TableView Height: \(tableSubcontroller.tableManager.tableView.bounds.height)")
        print("TableStackView Height: \(tableStackView.bounds.height)")
        print("GeneralStackView Height: \(generalStackView.bounds.height)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cityLabel.text = UserInfoService.shared.city
        navigationController!.setNavigationBarTransparent()
        tableRefreshed = false
        sliderRefreshed = false
        tableSubcontroller.loader.reloadData()
    }
    
    private func setupNavBarButtons() {
        var barButtons: [UIBarButtonItem] = []
        if eventsType == .generalCurrent {
            let historyButton = UIButton(type: .custom)
            historyButton.setImage(#imageLiteral(resourceName: "ic_history"), for: .normal)
            historyButton.addTarget(self, action: #selector(historyAction), for: .touchUpInside)
            historyButton.sizeToFit()
            barButtons.append(UIBarButtonItem(customView: historyButton))
        }
        let filterBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_filter"), style: .plain, target: self, action: #selector(filterAction))
        barButtons.append(filterBarButton)
        navigationItem.rightBarButtonItems = barButtons
    }
    
    private func resfreshNumberOfEventsLabel() {
        if tableRefreshed && (isPremium || sliderRefreshed) {
            var numberOfEvents: Int = 0
            numberOfEvents += tableSubcontroller.tableManager.dataSource.count
            if let horizontalSliderSubcontroller = self.horizontalSliderSubcontroller {
                numberOfEvents += horizontalSliderSubcontroller.collectionManager.dataSource.count
            }
            var events_word: String!
            if numberOfEvents > 10, numberOfEvents < 20 {
                events_word = NSLoc("of_events")
            }
            else {
                switch numberOfEvents % 10 {
                case 1: events_word = NSLoc("event")
                case 2...4: events_word = NSLoc("oe_events")
                default: events_word = NSLoc("of_events")
                }
            }
            numberOfEventsLabel.text = numberOfEvents > 0 ? "\(numberOfEvents) \(events_word!)" : NSLoc("no_events")
        }
    }
    
    @objc private func historyAction() {
        navigationController?.pushViewController(DetailedCategoryForLastEventsController(category: category), animated: true)
    }
    
    @objc private func filterAction(sender: UIBarButtonItem) {
        let controller = EventsFilterController(withModel: tableSubcontroller.loader.requestModel!, filterButton: sender)
        controller.didApply = { [unowned controller, weak self] in
            if let horizontalSliderSubcontroller = self?.horizontalSliderSubcontroller {
                controller.applyFilter(to: horizontalSliderSubcontroller.loader.requestModel!)
            }
        }
        navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func searchAction() {
        let controller = EventsFeedController(withEventsType: eventsType, category: category, enableSearching: true)
        navigationController?.pushViewController(controller, animated: true)
    }
}
