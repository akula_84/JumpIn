//
//  UpdateProfileRequestModel.swift
//  JumpIn
//
//  Created by Admin on 10.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class UpdateProfileRequestModel: ProfileModel {    
    
    var city: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        city <- map["city"]
    }
}
