//
//  SocialNetFbButton.swift
//  JumpIn
//
//  Created by Admin on 12.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class SocialNetFbButton: SocialNetButton {

    override func setup() {
        super.setup()
        backgroundColor = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
        leftLabelText = "f"
    }
}
