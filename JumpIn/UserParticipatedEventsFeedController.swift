//
//  UserParticipatedEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit

class UserParticipatedEventsFeedController: EventsFeedController {

    init(userId: Int) {
        super.init(withEventsType: .userParticipated)
        self.title = NSLoc("participates")
        loader.requestModel.userId = userId
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
