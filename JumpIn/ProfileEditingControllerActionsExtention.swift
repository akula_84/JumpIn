//
//  ProfileEditingControllerActionsExtention.swift
//  JumpIn
//
//  Created by Admin on 20.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


extension ProfileEditingController { //Switches Actions
    
    func makeUserPremium(errorBlock: (()->Void)?) {
        HUD.show(.progress)
        ServerApi.shared.makeProfilePremium(completion: { [weak self] in
            HUD.hide()
            self?.reloadData()
        }) { [weak self] (error, _) in
            self?.showError(error)
            errorBlock?()
        }
    }
    
    func showAlertAboutOpenningAppStoreIfNeed(cancelBlock: (()->Void)?) {
        if !viewModel.premium {
            AlertsPresentator.presentAlertHereWillOpenAppStore(in: self, completion: { [weak self] yes in
                if yes { self?.makeUserPremium(errorBlock: cancelBlock) }
                else { cancelBlock?() }
            })
        }
    }
    
    @IBAction private func onChange(`switch`: UISwitch) {
        isChanged = true
        if `switch` === showPhoneSwitch {
            viewModel.showPhone = `switch`.isOn
            showAlertAboutOpenningAppStoreIfNeed(cancelBlock: { [weak self] in
                `switch`.isOn = !`switch`.isOn
                self?.viewModel.showPhone = `switch`.isOn
                self?.refresh()
            })
        }
        else if `switch` === dontShowAgeSwitch {
            viewModel.dontShowAge = `switch`.isOn
            showAlertAboutOpenningAppStoreIfNeed(cancelBlock: { [weak self] in
                `switch`.isOn = !`switch`.isOn
                self?.viewModel.dontShowAge = `switch`.isOn
                self?.refresh()
            })
        }
        else if `switch` === showMySocNetProfileSwitch {
            viewModel.showMyProfile = `switch`.isOn
            showAlertAboutOpenningAppStoreIfNeed(cancelBlock: { [weak self] in
                `switch`.isOn = !`switch`.isOn
                self?.viewModel.showMyProfile = `switch`.isOn
                self?.refresh()
            })
        }
        refresh()
    }
}



extension ProfileEditingController { //Buttons Actions
    
    @IBAction func addVkReferenceAction() {
        isChanged = true
        binder.bindVk()
    }
    
    @IBAction func addFbReferenceAction() {
        isChanged = true
        binder.bindFb()
    }
    
    @IBAction func addInstagramReferenceAction() {
        isChanged = true
        binder.bindInstagram()
    }
    
    @IBAction func continueAction() {
        sendChanges()
    }
}
