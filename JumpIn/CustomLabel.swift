//
//  CustomLabel.swift
//  JumpIn
//
//  Created by Admin on 02.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class CustomLabel: UILabel {
    
    enum Orientation: Int {
        case left, right
    }
    
    
    @IBInspectable var triangleHeight: CGFloat = 10
    @IBInspectable var triangleBottomWidth: CGFloat = 20
    var orientation: Orientation = .left { didSet {
        if orientation == .right {
            backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            layer.borderUIColor = #colorLiteral(red: 0.1921568662, green: 0.007843137719, blue: 0.09019608051, alpha: 1)
            layer.borderWidth = 1
        }
        else {
            backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            layer.borderWidth = 0
        }
        setNeedsDisplay()
        } }
    @IBInspectable var textInsets: UIEdgeInsets = UIEdgeInsets(top: 12, left: 15, bottom: 12, right: 15)
    private var drawingInsets: UIEdgeInsets {
        var insets = UIEdgeInsets.zero
        if orientation == .left { insets.left += triangleHeight }
        else { insets.right += triangleHeight }
        return insets
    }
    override var text: String? {
        didSet {
            let type: NSTextCheckingResult.CheckingType = .link
            linksMatches.removeAll()
            guard let attrText = self.attributedText, let detector = try? NSDataDetector(types: type.rawValue) else { return }
            linksMatches = detector.matches(in: attrText.string, options: .reportProgress, range: NSRange(location: 0, length: attrText.string.count))
            let linkColor = UIColor(red: 71/255.0, green: 136/255.0, blue: 243/255.0, alpha: 1)
            let _attrText = NSMutableAttributedString(attributedString: attrText)
            linksMatches.forEach({
                _attrText.addAttribute(.foregroundColor, value: linkColor, range: $0.range)
                _attrText.addAttribute(.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: $0.range)
            })
            self.attributedText = _attrText
        }
    }
    private var linksMatches: [NSTextCheckingResult] = []
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        clearsContextBeforeDrawing = true
        isUserInteractionEnabled = true
    }
    
    @objc func onTapAction(gesture: UITapGestureRecognizer) {
        linksMatches.forEach({
            if gesture.didTapAttributedTextInLabel(label: self, inRange: $0.range) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open($0.url!)
                }
                else {
                    UIApplication.shared.openURL($0.url!)
                }
                return
            }
        })
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        //
        var newBounds = bounds
        newBounds.size.width -= textInsets.left + textInsets.right + drawingInsets.left + drawingInsets.right
        var textRect = super.textRect(forBounds: newBounds, limitedToNumberOfLines: numberOfLines)
        textRect.size.height += textInsets.bottom + textInsets.top + drawingInsets.bottom + drawingInsets.top
        textRect.size.width += textInsets.right + textInsets.left + drawingInsets.right + drawingInsets.left
        return textRect
    }

    override func draw(_ rect: CGRect) {
        //
        let originalRect = rect
        let rect = UIEdgeInsetsInsetRect(rect, drawingInsets)
        let x = rect.origin.x
        let y = rect.origin.y
        let height = rect.height
        let width = rect.width
        let xMax = x + width
        let yMax = y + height
        let rad = layer.cornerRadius
        let path = UIBezierPath()
        //
        path.move(to: CGPoint(x:x, y:y))
        path.addArc(withCenter: CGPoint(x: x + rad, y: y + rad), radius: rad, startAngle: .pi, endAngle: 3 * .pi/2, clockwise: true)
        path.addLine(to: CGPoint(x: xMax - rad, y: y))
        path.addArc(withCenter: CGPoint(x: xMax - rad, y: y + rad), radius: rad, startAngle: 3 * .pi/2, endAngle: 0, clockwise: true)
        if orientation == .right {
            path.addLine(to: CGPoint(x: xMax, y: y + height/2 - triangleBottomWidth/2))
            path.addLine(to: CGPoint(x: xMax+triangleHeight, y: y+height/2))
            path.addLine(to: CGPoint(x: xMax, y: y + height/2 + triangleBottomWidth/2))
        }
        path.addLine(to: CGPoint(x: xMax, y: yMax - rad))
        path.addArc(withCenter: CGPoint(x: xMax - rad, y: yMax - rad), radius: rad, startAngle: 0, endAngle: .pi/2, clockwise: true)
        path.addLine(to: CGPoint(x: x + rad, y: yMax))
        path.addArc(withCenter: CGPoint(x: x + rad, y: yMax - rad), radius: rad, startAngle: .pi/2, endAngle: .pi, clockwise: true)
        if orientation == .left {
            path.addLine(to: CGPoint(x: x, y: y + height/2 + triangleBottomWidth/2))
            path.addLine(to: CGPoint(x: x - triangleHeight, y: y + height/2))
            path.addLine(to: CGPoint(x: x, y: y + height/2 - triangleBottomWidth/2))
        }
        path.addLine(to: CGPoint(x: x, y: y + rad))
        path.close()
        //
        let shape = CAShapeLayer()
        shape.frame = originalRect
        shape.fillColor = backgroundColor?.cgColor
        shape.path = path.cgPath
        layer.mask = shape
        //
        layer.sublayers?.first?.removeFromSuperlayer()
        //
        let strokeLayer = CAShapeLayer()
        strokeLayer.frame = originalRect
        strokeLayer.strokeColor = layer.borderColor
        strokeLayer.path = path.cgPath
        strokeLayer.fillColor = UIColor.clear.cgColor
        strokeLayer.lineWidth = layer.borderWidth
        layer.addSublayer(strokeLayer)
        //
        let rectToDrawText = UIEdgeInsetsInsetRect(rect, textInsets)
        drawText(in: rectToDrawText)
//        if let attrText = attributedText {
//            attrText.draw(in: rectToDrawText)
//        }
//        else {
//            drawText(in: rectToDrawText)
//        }
    }
}


extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        guard let attrText = label.attributedText else { return false }
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: attrText)
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        textContainer.lineFragmentPadding = 0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        let locationOfTouchInLabel = location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width)/2 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height)/2 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
