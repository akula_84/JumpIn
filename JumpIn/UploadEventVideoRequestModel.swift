//
//  UploadEventVideoRequestModel.swift
//  JumpIn
//
//  Created by Admin on 31.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import ObjectMapper


class UploadEventVideoRequestModel {
    
    class VideoDataModel: Mappable {
        
        var video: String! //base64 encoded
        var thumbnail: String! //base64 encoded
        
        required init?(map: Map) { }
        
        init(video: String, thumbnail: String) {
            self.video = video
            self.thumbnail = thumbnail
        }
        
        class func createAsync(withVideoUrl videoUrl: URL, thumbnail: UIImage, completion: ((VideoDataModel?)->Void)?) {
            let _completion = { (model: VideoDataModel?)->Void in
                DispatchQueue.main.async {
                    completion?(model)
                }
            }
            DispatchQueue.global(qos: .utility).async {
                guard let videoData = try? Data(contentsOf: videoUrl) else {
                    _completion(nil)
                    return
                }
                let video = videoData.base64EncodedString()
                guard let image = UIImageJPEGRepresentation(thumbnail, 0)?.base64EncodedString() else {
                    _completion(nil)
                    return
                }
                let model = VideoDataModel(video: video, thumbnail: image)
                _completion(model)
            }
        }
        
        func mapping(map: Map) {
            video <- map["video"]
            thumbnail <- map["thumbnail"]
        }
    }
    
    
    var eventId: Int
    var videoDataModels: [VideoDataModel]
    
    init(eventId: Int, videoDataModels: [VideoDataModel]) {
        self.eventId = eventId
        self.videoDataModels = videoDataModels
    }
    
    private class func convert(inputs: [(videoUrl: URL, thumbnail: UIImage)], completion: ((NSArray)->Void)?, convertedModels: NSMutableArray = NSMutableArray()) {
        var inputs = inputs
        guard let input = inputs.popLast() else {
            completion?(convertedModels)
            return
        }
        VideoDataModel.createAsync(withVideoUrl: input.videoUrl, thumbnail: input.thumbnail) { model in
            if let model = model { convertedModels.add(model) }
            convert(inputs: inputs, completion: completion, convertedModels: convertedModels)
        }
    }
    
    class func createAsync(withEventId id: Int, inputs: [(videoUrl: URL, thumbnail: UIImage)], completion: ((UploadEventVideoRequestModel?)->Void)?) {
        convert(inputs: inputs, completion: { array in
            let videoModels: [VideoDataModel] = array.map({ $0 as! VideoDataModel })
            completion?(UploadEventVideoRequestModel(eventId: id, videoDataModels: videoModels))
        })
    }
}
