//
//  MyConfirmedEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 28.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SwipeCellKit


class MyConfirmedEventsFeedController: EventsFeedController {

    init() {
        super.init(withEventsType: .myConfirmedCurrent)
        self.title = NSLoc("confirmed")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
//        //
//        guard orientation == .right else { return nil }
//        let archiveAction = SwipeAction(style: .default, title: "Покинуть событие", handler: nil)
//        archiveAction.backgroundColor = UIColor(red: 255.0/255.0, green: 96.0/255.0, blue: 116.0/255.0, alpha: 1.0)
//        archiveAction.image = #imageLiteral(resourceName: "ic_remove")
//        return [archiveAction]
//    }

}
