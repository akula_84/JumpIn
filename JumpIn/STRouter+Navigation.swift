//
//  STRouter+Navigation.swift
//  RNIS
//
//  Created by Артем Кулагин on 21.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
import SideMenu

/**
 Расширение для работы с навигацией
 */
extension STRouter {
    static func gotoNext(withAuthorizationStatus status: GetStatusResponseModel.UserAuthorizationStatus) {
        switch status {
        case .NeedPhone:
            pushMain(EnterPhoneController())
            break
        case .NeedInfo:
            pushMain(ProfileEditingController(isEnterMissingData:true))
            break
        case .Authorized:
          //  ServerApi.destroySocials()
            SideMenuManager.setup()
            pushMain(TopEventsFeedController(withFilter: true))
            break
        }
    }
    
    static func showLicense() {
         LicenseAgreementController.initialController?.pushMain(false)
    }
}
