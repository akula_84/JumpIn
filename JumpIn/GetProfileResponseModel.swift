//
//  ProfileResponseModel.swift
//  JumpIn
//
//  Created by Admin on 10.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetProfileResponseModel: ProfileModel {
    
    var filled: Bool!
    var photos: [PhotoWithLikesModel]!
    var phone: String?
    var email: String?
    var socialNets: [SocialNetModel]!
    var premium: Bool!
    var city: String!
    var status: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        filled <- map["filled"]
        photos <- map["photos"]
        phone <- map["phone"]
        email <- map["email"]
        socialNets <- map["socials"]
        premium <- map["premium"]
        city <- map["city"]
        status <- map["status"]
    }
}
