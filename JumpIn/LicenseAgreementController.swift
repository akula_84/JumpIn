//
//  LicenseAgreementController.swift
//  JumpIn
//
//  Created by Admin on 03.04.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit


class LicenseAgreementController: BaseViewController {
    
    var isUserAgree: Bool = false {
        didSet {
            agreementPinkChekerButton.isSelected = !isUserAgree
            enterButton.backgroundColor = isUserAgree ? #colorLiteral(red: 0.3647058824, green: 0.6784313725, blue: 0.8862745098, alpha: 1) : .lightGray
            enterButton.isEnabled = isUserAgree
        }
    }
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var agreementPinkChekerButton: UIButton!
    
    
    @IBAction func tapAgreementButtonLabelAction(_ sender: Any) {
        isUserAgree = !isUserAgree
    }
    
    @IBAction func tapAgreementButtonAction(_ sender: Any) {
        isUserAgree = !isUserAgree
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let pathEnglish = Utils.isRu ? "RussianAgreement" : "EnglishAgreement"
        if let filepath = Bundle.main.path(forResource: pathEnglish, ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                textView.text = contents
                textView.contentOffset = CGPoint(x: 0, y: 0);
           } catch {}
        }
    }
}
