//
//  ProfileEditingControllerTextFieldExtention.swift
//  JumpIn
//
//  Created by Admin on 20.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


extension ProfileEditingController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        isChanged = true
        if textField === sexTextField {
            presentSexAlert()
            return false
        }
        return true
    }
    
}
