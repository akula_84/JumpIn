//
//  DialogCell.swift
//  JumpIn
//
//  Created by Admin on 02.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


enum DialogMessageModelPendingState {
    case noPending, pendingToSend, pendingToDelete
}


class DialogMessageModel: ModelProtocol {
    
    enum MediaType: String {
        case noMedia = "", photo = "photograf"
        var localizedString: String {
            return NSLoc(rawValue)
        }
    }
    
    var id: Int!
    var senderId: Int!
    var recipientId: Int!
    var text: String!
    var senderAvatarLink: String?
    var encodedPhoto: String?
    var photo: UIImage?
    var photoLink: String?
    var timestamp: String?
    var pendingState = DialogMessageModelPendingState.noPending
    var timestampDate: Date?
    var mediaType = MediaType.noMedia
    
    required init?(map: Map) { }
    
    init() { }
    
    init(senderId: Int, recipientId: Int, text: String, senderAvatarLink: String?) {
        self.senderId = senderId
        self.recipientId = recipientId
        self.text = text
        self.senderAvatarLink = senderAvatarLink
    }

    init(senderId: Int, recipientId: Int, photo: UIImage, label: String? = nil, senderAvatarLink: String?) {
        self.senderId = senderId
        self.recipientId = recipientId
        self.text = label ?? MediaType.photo.localizedString
        self.senderAvatarLink = senderAvatarLink
        self.photo = photo
        mediaType = .photo
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        senderId <- map["sender_user_id"]
        recipientId <- map["recipient_user_id"]
        text <- map["text"]
        senderAvatarLink <- map["sender_avatar"]
        timestamp <- map["created_at"]
        photoLink <- map["photo_path"]
        //
        if photoLink != nil || photo != nil {
            mediaType = .photo
        }
    }
}


class DialogCellViewModel: BaseViewModel<DialogMessageModel> {
    
    var senderAvatarLink: String?
    var text: String?
    var orientation: DialogCell.Orientation
    var pendingState: DialogMessageModelPendingState
    var timestamp: String
    var photo: UIImage?
    var photoLink: String?
    var isSendedByCurrentUser: Bool
    var mediaType: DialogMessageModel.MediaType
    
    required init(withModel model: DialogMessageModel) {
        senderAvatarLink = model.senderAvatarLink
        text = model.text
        isSendedByCurrentUser = model.senderId == UserInfoService.shared.userId
        orientation = isSendedByCurrentUser ? .right : .left
        pendingState = model.pendingState
        switch pendingState {
            case .noPending:
                if let timestamp = model.timestamp {
                    self.timestamp = DateConverter.ForInterface.Dialogs.messageTimestamp(from: timestamp)
                }
                else {
                    self.timestamp = DateConverter.ForInterface.Dialogs.messageTimestamp(from: model.timestampDate!)
                }
                break
                //
            case .pendingToSend:
                timestamp = NSLoc("sending")
                break
                //
            case .pendingToDelete:
                timestamp = NSLoc("deleting")
                break
        }
        
        photo = model.photo
        photoLink = model.photoLink
        mediaType = model.mediaType
        
        if mediaType == .photo, text == mediaType.localizedString {
            text = nil
        }
        super.init(withModel: model)
    }
}


protocol DialogCellManagerDelegate: class {
    func onTapAvatar(manager: DialogCellManager)
    func onLongPress(manager: DialogCellManager)
    func onTapPhoto(manager: DialogCellManager, photo: UIImage?, photoLink: String?)
}

class DialogCellManager: BaseViewManager<DialogCellViewModel, DialogCell> {
    
    weak var delegate: DialogCellManagerDelegate?
    
    required init(view: UIView) {
        super.init(view: view)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        tap.cancelsTouchesInView = false
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction))
        longTap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.view.addGestureRecognizer(longTap)
    }
    
    override func refresh() {
        view.timeLabel.text = viewModel.timestamp
        view.orientation = viewModel.orientation
        if let link = viewModel.senderAvatarLink { view.avatarImageView.imageLink = link }
        else { view.avatarImageView.image = #imageLiteral(resourceName: "user_no_image") }
        view.alpha = viewModel.pendingState != .noPending ? 0.5 : 1
        view.photoImageView.isHidden = false
        if let photo = viewModel.photo {
            view.photoImageView.image = photo
        }
        else if let photoLink = viewModel.photoLink {
            view.photoImageView.image = #imageLiteral(resourceName: "no_image")
            view.photoImageView.imageLink = photoLink
        }
        else {
            view.photoImageView.isHidden = true
        }
        view.messageLabel.isHidden = viewModel.text == nil
        view.messageLabel.text = viewModel.text
    }
    
    @objc private func longPressAction() {
        if viewModel.pendingState == .noPending {
            delegate?.onLongPress(manager: self)
        }
    }
    
    @objc private func tapAction(tap: UITapGestureRecognizer) {
        if view.avatarImageView.bounds.contains(tap.location(in: view.avatarImageView)) {
            if viewModel.pendingState == .noPending {
                delegate?.onTapAvatar(manager: self)
            }
        }
        else if viewModel.photo != nil || viewModel.photoLink != nil {
            if view.photoImageView.bounds.contains(tap.location(in: view.photoImageView)) {
                delegate?.onTapPhoto(manager: self, photo: viewModel.photo, photoLink: viewModel.photoLink)
            }
        }
        if view.messageLabel.bounds.contains(tap.location(in: view.messageLabel)) { view.messageLabel.onTapAction(gesture: tap) }
    }
}

class DialogCell: UITableViewCell, Configurable2 {
    
    enum Orientation: Int {
        case left, right
    }
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageLabel: CustomLabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var messageStackView: UIStackView!
    @IBOutlet weak var avatarImageView: DownloadingImageView!
    @IBOutlet weak var photoImageView: DownloadingImageView!
    //
    var configurator: AnyObject?
    var orientation: Orientation = .left {
        didSet {
            if oldValue != orientation {
                let subviews = stackView.arrangedSubviews
                subviews.forEach( { self.stackView.removeArrangedSubview($0) } )
                subviews.reversed().forEach( { self.stackView.addArrangedSubview($0) } )
                if orientation == .left {
                    timeLabel.textAlignment = .left
                    messageStackView.alignment = .leading
                }
                else {
                    timeLabel.textAlignment = .right
                    messageStackView.alignment = .trailing
                }
                messageLabel.orientation = CustomLabel.Orientation(rawValue: orientation.rawValue)!
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        messageLabel.setNeedsDisplay()
    }
}
