//
//  GetDialogMessagesRequestModel.swift
//  JumpIn
//
//  Created by Admin on 15.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation


class GetDialogMessagesRequestModel: RequestModelWithPageProtocol {

    let userId: Int
    var page: Int
    
    init(userId: Int, page: Int? = nil) {
        self.userId = userId
        self.page = page ?? 1
    }
}
