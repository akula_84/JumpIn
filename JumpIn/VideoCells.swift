//
//  VideoCollectionCell.swift
//  JumpIn
//
//  Created by Admin on 19.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit
import AVFoundation
import ObjectMapper
import AVKit


class VideoModel: ModelProtocol {
    
    var id: Int?
    var filePath: String!
    var thumbnailLink: String?
    var thumbnail: UIImage?
    var url: URL?
    var likes: Int!
    var dislikes: Int!
    var vote: Bool?
    var isLikesPanelHidden = false
    
    init(videoUrl: URL, thumbnail: UIImage?) {
        url = videoUrl
        self.thumbnail = thumbnail
    }
    required init?(map: Map) { }
    func mapping(map: Map) {
        filePath <- map["absolute_path"]
        thumbnailLink <- map["thumbnail"]
        id <- map["id"]
        likes <- map["likes"]
        dislikes <- map["dislikes"]
        vote <- map["liked_me"]
    }
}


class VideoCellViewModel: BaseViewModel<VideoModel> {
    
    let id: Int?
    let url: URL
    var thumbnailLink: String?
    var thumbnail: UIImage?
    var likes: Int = 0
    var dislikes: Int = 0
    var vote: Bool?
    var isLikesPanelHidden: Bool
    
    
    required init(withModel model: VideoModel) {
        id = model.id
        if let url = model.url {
            self.url = url
        }
        else {
            url = URL(string: model.filePath)!
        }
        thumbnailLink = model.thumbnailLink
        thumbnail = model.thumbnail
        likes = model.likes ?? 0
        dislikes = model.dislikes ?? 0
        vote = model.vote
        isLikesPanelHidden = model.isLikesPanelHidden
        super.init(withModel: model)
    }
}

protocol VideoCellProtocol: Configurable2 where Self: UIView {
    var videoView: VideoCellView! { get set }
}

class VideoCellManager<VideoCell: VideoCellProtocol, VideoCellVM: VideoCellViewModel>: BaseViewManager<VideoCellVM, VideoCell> {
    override func refresh() {
        view.videoView.set(videoUrl: viewModel.url, thumbnail: viewModel.thumbnail, thumbnailLink: viewModel.thumbnailLink)
    }
}


//MARK: Collection Cell

protocol VideoCollectioCellManagerDelegate: class {
    func cellManager(_ manager: VideoCollectioCellManager, didChangeVote: Bool?)
}

class VideoCollectioCellManager: VideoCellManager<VideoCollectionCell, VideoCellViewModel> {
    
    weak var delegate: VideoCollectioCellManagerDelegate?
    
    override func refresh() {
        super.refresh()
        view.likeButton.tintColor = UIColor(white: 0.8, alpha: 1)
        view.dislikeButton.tintColor = UIColor(white: 0.8, alpha: 1)
        if let vote = viewModel.vote {
            if vote {
                view.likeButton.tintColor = #colorLiteral(red: 0.2784313725, green: 0.5333333333, blue: 0.9529411765, alpha: 1)
            }
            else {
                view.dislikeButton.tintColor = #colorLiteral(red: 0.2784313725, green: 0.5333333333, blue: 0.9529411765, alpha: 1)
            }
        }
        view.likeButton.setTitle("\(viewModel.likes)", for: .normal)
        view.dislikeButton.setTitle("\(viewModel.dislikes)", for: .normal)
        view.likeButton.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
        view.dislikeButton.addTarget(self, action: #selector(dislikeAction), for: .touchUpInside)
        view.likeButton.superview?.isHidden = viewModel.isLikesPanelHidden
    }
    
    @objc private func likeAction() {
        var vote: Bool?
        let oldVote = viewModel.vote
        if oldVote == nil || oldVote == false { vote = true }
        delegate?.cellManager(self, didChangeVote: vote)
        viewModel.vote = vote
        if vote != nil {
            if vote! {
                viewModel.likes += 1
                if let old = oldVote, !old {
                    viewModel.dislikes -= 1
                }
            }
        }
        else {
            viewModel.likes -= 1
        }
        refresh()
    }
    
    @objc private func dislikeAction() {
        var vote: Bool?
        let oldVote = viewModel.vote
        if oldVote == nil || oldVote == true { vote = false }
        delegate?.cellManager(self, didChangeVote: vote)
        viewModel.vote = vote
        if vote != nil {
            if !vote! {
                viewModel.dislikes += 1
                if let old = oldVote, old {
                    viewModel.likes -= 1
                }
            }
        }
        else {
            viewModel.dislikes -= 1
        }
        refresh()
    }
}


protocol VideoCollectionViewManagerDelegate: class {
    func videoCollectionManager(_ manager: VideoCollectionViewManager, controllerToPresentSelectedVideoAt index: Int) -> UIViewController?
    func videoCollectionManager(_ manager: VideoCollectionViewManager, didChangeVoteForVideoAt: IndexPath)
    func videoCollectionManager(_ manager: VideoCollectionViewManager, failedToChangeVoteAt: IndexPath, error: MainURLEngine.Error?)
}


class VideoCollectionViewManager: CollectionViewManager<VideoCollectioCellManager>, VideoCollectioCellManagerDelegate {
    
    weak var delegate: VideoCollectionViewManagerDelegate?
    var autoplaySelectedVideo = true
    
    required init(with collectionView: UICollectionView) {
        super.init(with: collectionView)
        didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let i = self.indexOfDataSourceItem(forCellAt: indexPath)
            guard let cell = collectionView.cellForItem(at: indexPath) as? VideoCollectionCell else { return }
            cell.videoView.showVideo(in: self.delegate?.videoCollectionManager(self, controllerToPresentSelectedVideoAt: i))
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        guard let videoCell = cell as? VideoCollectionCell,
              let cellManager = videoCell.configurator as? VideoCollectioCellManager else { return cell }
        cellManager.delegate = self
        return cell
    }
    
    func cellManager(_ manager: VideoCollectioCellManager, didChangeVote vote: Bool?) {
        let indexPath = collectionView.indexPath(for: manager.view)!
        let item = dataSource[indexOfDataSourceItem(forCellAt: indexPath)]
        let model = VoteRequestModel(type: .video, id: item.id!, rating: vote)
        ServerApi.shared.vote(withModel: model, completion: { [weak self] in
            guard let `self` = self else { return }
            self.delegate?.videoCollectionManager(self, didChangeVoteForVideoAt: indexPath)
        }) { [weak self] (error, _) in
            guard let `self` = self else { return }
            self.delegate?.videoCollectionManager(self, failedToChangeVoteAt: indexPath, error: error)
        }
    }
}


class VideoCollectionCell: UICollectionViewCell, VideoCellProtocol {
    
    var configurator: AnyObject?
    @IBOutlet weak var videoView: VideoCellView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
}


//MARK: Table Cell


protocol VideoTableViewManagerDelegate: class {
    func videoTableManager(_ manager: VideoTableViewManager, controllerToPresentSelectedVideoAt index: Int) -> UIViewController?
    func videoTableManager(_ manager: VideoTableViewManager, didSelectToDelete: Bool, cellAt: IndexPath)
}


class VideoTableViewManager: TableViewManager<UITableView, VideoTableCellManager>, VideoTableCellManagerDelegate {
    
    weak var delegate: VideoTableViewManagerDelegate?
    var autoplaySelectedVideo = true
    private var isSelectButtonsHidden = true
    private var isSelectButtonsAnimationEnabled = true
    
    
    required init(with superView: UIView?) { 
        super.init(with: superView)
        didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            if self.isSelectButtonsHidden {
                let i = self.indexOfDataSourceItem(forCellAt: indexPath)
                guard let cell = self.tableView.cellForRow(at: indexPath) as? VideoTableCell else { return }
                let presentingController = self.delegate?.videoTableManager(self, controllerToPresentSelectedVideoAt: i)
                cell.videoView.showVideo(in: presentingController)
            }
            else if let videoCell = self.tableView.cellForRow(at: indexPath) as? VideoTableCell, let cellManager = videoCell.configurator as? VideoTableCellManager {
                cellManager.selectButtonTapped()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if let videoCell = cell as? VideoTableCell, let cellManager = videoCell.configurator as? VideoTableCellManager {
            cellManager.delegate = self
            cellManager.setSelectButton(hidden: isSelectButtonsHidden, animated: isSelectButtonsAnimationEnabled)
        }
        return cell
    }
    
    func setSelectButtons(hidden: Bool, animated: Bool) {
        isSelectButtonsHidden = hidden
        isSelectButtonsAnimationEnabled = animated
        reloadData()
    }
    
    func cellManager(_ manager: VideoTableCellManager, didSelectCellToDelete selected: Bool) {
        guard let indexPath = tableView.indexPath(for: manager.view) else { return }
        delegate?.videoTableManager(self, didSelectToDelete: selected, cellAt: indexPath)
    }
}


class VideoTableCellViewModel: VideoCellViewModel {
    var isSelectedToDelete = false
}


protocol VideoTableCellManagerDelegate: class {
    func cellManager(_ manager: VideoTableCellManager, didSelectCellToDelete: Bool)
}


class VideoTableCellManager: VideoCellManager<VideoTableCell, VideoTableCellViewModel> {
    
    private enum VideoViewLeadingPosition: CGFloat {
        case right = 60, normal = 0
    }
    
    weak var delegate: VideoTableCellManagerDelegate?
    
    required init(view: UIView) {
        super.init(view: view)
        self.view.selectButton.addTarget(self, action: #selector(selectButtonTapped), for: .touchUpInside)
    }
    
    @objc func selectButtonTapped() {
        viewModel.isSelectedToDelete = !viewModel.isSelectedToDelete
        refresh()
        delegate?.cellManager(self, didSelectCellToDelete: viewModel.isSelectedToDelete)
    }
    
    override func refresh() {
        super.refresh()
        view.selectButton.isSelected = viewModel.isSelectedToDelete
    }
    
    func setSelectButton(hidden: Bool, animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                self.setSelectButton(hidden: hidden)
                self.view.layoutIfNeeded()
            })
        }
        else {
            setSelectButton(hidden: hidden)
        }
    }
    
    private func setSelectButton(hidden: Bool) {
        view.videoViewLeading.constant = hidden ? VideoViewLeadingPosition.normal.rawValue : VideoViewLeadingPosition.right.rawValue
    }
}


class VideoTableCell: UITableViewCell, VideoCellProtocol {
    
    var configurator: AnyObject?
    @IBOutlet weak var videoView: VideoCellView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var videoViewLeading: NSLayoutConstraint!
}


func generateThumbnail(forVideoWithUrl url: URL) -> UIImage {
    let asset = AVURLAsset(url: url)
    let timeScale = asset.duration.timescale
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.appliesPreferredTrackTransform = true
    if let cgimage = try? imageGenerator.copyCGImage(at: CMTime(seconds: 3, preferredTimescale: timeScale), actualTime: nil) {
        return UIImage(cgImage: cgimage)
    }
    return UIImage(color: .lightGray)
}
