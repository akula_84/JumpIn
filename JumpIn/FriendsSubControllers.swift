//
//  AllFriendsController.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD
import ObjectMapper


protocol FriendsTableManagerDataSource: class {
    func userId(at index: Int) -> Int?
}

protocol FriendsTableManagerProtocol {
    var usersDataSource: FriendsTableManagerDataSource? { get set }
}


class FriendsSubController<TableViewManagerT: TableViewManaging>: BaseConfigurableListController<BaseUserCellViewModelsPage>, FriendsTableManagerDataSource {
    
    typealias ResponseModelT = ShortUserModelsPage
    typealias ViewModelT = BaseUserCellViewModelsPage

    var loader: PageDataLoadingManager<GetFriendsRequestModel, FriendsSubController>!
    var tableManager: TableViewManagerT!
    var loaded = false
    var didSelectUser: ((ShortUserModel, Int)->Void)?
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
        loader = PageDataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getFriends as AnyObject)
        self.didSelectUser = { [weak self] model, _ in
            self?.navigationController!.pushViewController(ProfileController(userId: model.id), animated: true)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableManager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.reloadData()
    }
    
    override func refresh() {
        loaded = true
        tableManager.dataSource = viewModel.viewModels as! [TableViewManagerT.DataSourceItem]
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .bottom : .none
        tableManager.reloadData()
    }

    func createTableManager() {
        tableManager = TableViewManagerT(with: self.view)
    }

    func setupTableManager() {
        createTableManager()
        if var tableManager = tableManager as? FriendsTableManagerProtocol {
            tableManager.usersDataSource = self
        }
        tableManager.tableInitConfig.isCellHaveNib = false
        tableManager.tableInitConfig.shouldSetupRefreshControl = true
        tableManager.tableInitConfig.onRefresh = { [weak self] in self?.loader.reloadData() }
        tableManager.setup()
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            guard let `self` = self else { return }
            if self.loaded { self.loader.loadNextPage(animated: false) }
        }
        tableManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let i = self.tableManager.indexOfDataSourceItem(forCellAt: indexPath, useBlockIfExist: true)
            self.didSelectUser?(self.loader.model.models[i], i)
        }
        tableManager.didSearch = { [weak self] text in
            self?.loader.requestModel.searchString = text
            self?.loader.reloadData()
        }
    }

    func userId(at index: Int) -> Int? {
        let user = loader.model.models?[index]
        return user?.id
    }
}


class FriendsListController: FriendsSubController<TableViewManager<UITableView, UserCellManager>> {
    
    let onlineOnly: Bool
    
    
    init(showOnlineFriendsOnly: Bool = false) {
        onlineOnly = showOnlineFriendsOnly
        super.init()
        title = NSLoc(showOnlineFriendsOnly ? "online" : "allo")
        loader.requestModel = GetFriendsRequestModel(with: showOnlineFriendsOnly ? .online : .all)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupTableManager() {
        super.setupTableManager()
        tableManager.tableInitConfig.emptyListLabelText = NSLoc(onlineOnly ? "no_friends_online" : "the_list_is_empty")
    }
    
    deinit {
        print("Deinit \(self)")
    }
}


class RequestsListController: FriendsSubController<FriendsRequestsListTableManager> {
    
    override init() {
        super.init()
        title = NSLoc("inquiries")
        loader.requestModel = GetFriendsRequestModel(with: .requests)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func createTableManager() {
        tableManager = FriendsRequestsListTableManager(viewController: self)
        tableManager.tableInitConfig.emptyListLabelText = NSLoc("no_friend_requests")
    }
}

class OffersListController: FriendsSubController<FriendsOffersListTableManager> {
    
    override init() {
        super.init()
        title = NSLoc("proposed")
        loader.requestModel = GetFriendsRequestModel(with: .suggests)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func createTableManager() {
        tableManager = FriendsOffersListTableManager(viewController: self)
        tableManager.tableInitConfig.emptyListLabelText = NSLoc("No friends offered")
        tableManager.tableInitConfig.shouldSetupSearchBar = false
    }
}


class FriendsRequestsListTableManager: TableViewManager<UITableView, UserRequestCellManager>, FriendsTableManagerProtocol {
    weak var usersDataSource: FriendsTableManagerDataSource?
    weak var viewController: BaseViewController?

    init(viewController: BaseViewController) {
        self.viewController = viewController
        super.init(with: viewController.view)
    }

    required init(with superView: UIView?) {
        fatalError("init(with:) has not been implemented")
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _cell = super.tableView(tableView, cellForRowAt: indexPath)
        guard let cell = _cell as? UserRequestCell else { return _cell }
        cell.onTapButtonWithAnswer = { [weak self] accept in
            guard let `self` = self else { return }
            guard let userId = self.usersDataSource?.userId(at: indexPath.row) else { return }
            guard let indexPath = self.tableView.indexPath(for: cell) else { return }
            self.dataSource[indexPath.row].state = .loading
            let successCompletion: () -> Void = { [weak self] in
                guard let `self` = self else { return }
                HUD.flash(.success)
                self.dataSource.remove(at: indexPath.row)
                if let indexPaths = tableView.indexPathsForVisibleRows, indexPaths.contains(indexPath) {
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
            let errorCompletion: (MainURLEngine.Error?, HTTPURLResponse?) -> Void = { [weak self] (error, _) in
                guard let `self` = self else { return }
                self.dataSource[indexPath.row].state = .loading
                self.viewController?.showError(error)
            }
            if accept {
                HUD.show(.progress)
                ServerApi.shared.acceptFriends(withId: userId, completion: successCompletion, errorBlock: errorCompletion)
            } else {
                HUD.show(.progress)
                ServerApi.shared.denyFriends(withId: userId, completion: successCompletion, errorBlock: errorCompletion)
            }
        }
        return cell
    }
}


class FriendsOffersListTableManager: TableViewManager<UITableView, OfferCellManager>, FriendsTableManagerProtocol {
    weak var usersDataSource: FriendsTableManagerDataSource?
    weak var viewController: BaseViewController?

    init(viewController: BaseViewController) {
        self.viewController = viewController
        super.init(with: viewController.view)
    }

    required init(with superView: UIView?) {
        fatalError("init(with:) has not been implemented")
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _cell = super.tableView(tableView, cellForRowAt: indexPath)
        guard let cell = _cell as? OfferCell else { return _cell }
        cell.sendFriendRequestAction = { [weak self] in
            guard let `self` = self else { return }
            guard let userId = self.usersDataSource?.userId(at: indexPath.row) else { return }
            guard let indexPath = self.tableView.indexPath(for: cell) else { return }
            self.dataSource[indexPath.row].state = .loading
            let successCompletion: () -> Void = { [weak self] in
                guard let `self` = self else { return }
                HUD.flash(.success)
                self.dataSource.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            let errorCompletion: (MainURLEngine.Error?, HTTPURLResponse?) -> Void = { [weak self] (error, _) in
                guard let `self` = self else { return }
                self.dataSource[indexPath.row].state = .loading
                self.viewController?.showError(error)
            }
            HUD.show(.progress)
            ServerApi.shared.friendsAdd(withId: userId, completion: successCompletion, errorBlock: errorCompletion)
        }
        return cell
    }
}

