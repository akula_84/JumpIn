//
//  CALayerExtention.swift
//  JumpIn
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

extension CALayer {
    
    var borderUIColor: UIColor? {
        get { return borderColor != nil ? UIColor(cgColor: borderColor!) : nil }
        set { borderColor = newValue?.cgColor }
    }
}
