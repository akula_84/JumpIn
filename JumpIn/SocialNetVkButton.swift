//
//  SocialNetVkButton.swift
//  JumpIn
//
//  Created by Admin on 12.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class SocialNetVkButton: SocialNetButton {

    override func setup() {
        super.setup()
        backgroundColor = #colorLiteral(red: 0.3019607843, green: 0.4784313725, blue: 0.6941176471, alpha: 1)
        leftLabelText = "в"
        leftViewVerticalOffset = -3
        leftViewHorizontalOffset = -2
    }

}
