//
//  DialogListController.swift
//  JumpIn
//
//  Created by Admin on 01.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SwipeCellKit
import PKHUD


class DialogListController: BaseConfigurableListController<DialogListCellViewModelPage>, SwipeTableViewManagerDelegate {
    
    var loader: PageDataLoadingManager<GetDialogListRequestModel, DialogListController>!
    var tableManager: SwipeTableViewManager<UITableView, DialogListCellManager>!
    var loaded = false
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
        title = NSLoc("drawer_mail")
        loader = PageDataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getDialogList as AnyObject)
        loader.requestModel = GetDialogListRequestModel()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
        tableManager = SwipeTableViewManager(with: self.view)
        tableManager.swipeDelegate = self
        tableManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let i = self.tableManager.indexOfDataSourceItem(forCellAt: indexPath)
            let model = self.loader.model.models![i]
            self.navigationController!.pushViewController(DialogDetailedController(userId: model.userId, userAvatarLink: model.userAvatarLink), animated: true)
        }
        tableManager.didSearch = { [weak self] searchString in
            self?.loader.requestModel.searchString = searchString
            self?.loader.reloadData()
        }
        tableManager.tableInitConfig.shouldSetupRefreshControl = true
        tableManager.tableInitConfig.onRefresh = { [weak self] in self?.loader.reloadData() }
        tableManager.setup()
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            guard let `self` = self else { return }
            if self.loaded { self.loader.loadNextPage(animated: false) }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.reloadData()
    }

    private func setupNavBarButtons() {
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAction))
        navigationItem.rightBarButtonItem = addBarButton
    }

    @objc private func addAction() {
        let vc = FriendsListController()
        vc.title = NSLoc("add_dialog")
        vc.didSelectUser = { [weak self] model, _ in
            self?.navigationController?.pushViewController(DialogDetailedController(userId: model.id, userAvatarLink: model.avatar?.filepath), animated: true)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func refresh() {
        loaded = true
        tableManager.dataSource = viewModel.viewModels
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .bottom : .none
        tableManager.reloadData()
    }
    
    func tableViewManager(_ manager: AnyObject, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] _, _ in
            guard let `self` = self else { return }
            let index = self.tableManager.indexOfDataSourceItem(forCellAt: indexPath)
            AlertsPresentator.presentAlertDeletingConversation(in: self, userName: self.viewModel.viewModels[index].user.name, completion: { [weak self] yes in
                guard yes else { return }
                self?.removeConversation(at: indexPath)
            })
        }
        deleteAction.image = #imageLiteral(resourceName: "ic_garbage")
        deleteAction.backgroundColor = #colorLiteral(red: 1, green: 0.3803921569, blue: 0.4549019608, alpha: 1)
        return [deleteAction]
    }
    
    func removeConversation(at indexPath: IndexPath) {
        HUD.show(.progress)
        let index = tableManager.indexOfDataSourceItem(forCellAt: indexPath)
        ServerApi.shared.removeConversation(with: loader.model.models[index].userId, completion: { [weak self] in
            HUD.hide()
            guard let `self` = self else { return }
            self.loader.model.models.remove(at: index)
            self.loader.updateControllerViewModel()
            self.tableManager.dataSource = self.viewModel.viewModels
            self.tableManager.deleteRow(at: indexPath)
        }, errorBlock: { [weak self] (error, _) in
            self?.showError(error)
        })
    }
}


