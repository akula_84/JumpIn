//
//  MyLastEventsFeedTableSubcontroller.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class MyLastEventsFeedTableSubcontroller: EventsFeedController {
    
    let _eventsType: _EventsType
    
    enum _EventsType: String {
        case my = "mys", confirmed = "confirmed"
        var localizedString: String {
            return NSLoc(rawValue)
        }
    }

    init(eventsType: _EventsType) {
        self._eventsType = eventsType
        super.init(withEventsType: eventsType == .my ? .myOwnLast : .myConfirmedLast)
        title = eventsType.localizedString
        loader.requestModel.timeFrame = .past
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
