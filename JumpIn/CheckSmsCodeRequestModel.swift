//
//  CheckSmsCodeRequestModel.swift
//  JumpIn
//
//  Created by Admin on 07.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class CheckSmsCodeRequestModel: Mappable {

    var code: String!
    
    init(withCode code:String) {
        //
        self.code = code
    }
    
    required init(map:Map) {}
    
    func mapping(map: Map) {
        //
        code <- map["sms_code"]
    }
}
