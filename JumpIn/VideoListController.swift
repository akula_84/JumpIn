//
//  VideoListController.swift
//  JumpIn
//
//  Created by Admin on 23.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit
import PKHUD


class EventsVideoLoader<Controller: ConfigurableListControllerProtocol>: PageDataLoadingManager<GetEventVideosRequestModel, Controller> where Controller: UIViewController {
    
    convenience init(controller: Controller, eventId: Int, numberOfVideosOnSinglePage: Int) {
        self.init(withController: controller, reloadDataFunc: ServerApi.shared.getEventVideos as AnyObject)
        requestModel = GetEventVideosRequestModel(eventId: eventId, numberOfVideosOnSinglePage: numberOfVideosOnSinglePage)
    }
    
    required init(withController controller: UIViewController, reloadDataFunc func: AnyObject) {
        super.init(withController: controller, reloadDataFunc: `func`)
    }
}

class VideoPageModel: PageModel<VideoModel> {}
class VideoListControllerViewModel: PageViewModel<VideoPageModel, VideoTableCellViewModel> {}


class VideoListController: BaseConfigurableController<VideoListControllerViewModel>, VideoTableViewManagerDelegate, ConfigurableListControllerProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    private var tableManager: VideoTableViewManager!
    private var loader: EventsVideoLoader<VideoListController>!
    let allowEditing: Bool
    let eventId: Int
    private var videoIdsToDelete: Set<Int> = []
    private var deletingModeEnabled: Bool = false {
        didSet {
            let color = deletingModeEnabled ? UIColor(red: 1, green: 96/255.0, blue: 116/255.0, alpha: 1) : UIColor.black
            trashBarButton.tintColor = color
            tableManager.setSelectButtons(hidden: !deletingModeEnabled, animated: true)
        }
    }
    private weak var trashBarButton: UIBarButtonItem!

    
    init(eventId: Int, allowEditing: Bool) {
        self.eventId = eventId
        self.allowEditing = allowEditing
        super.init(nibName: nil, bundle: nil)
        title = NSLoc("video")
        loader = EventsVideoLoader(controller: self, eventId: eventId, numberOfVideosOnSinglePage: 12)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupBgImageView()
        setupTableManager()
        setupNavBarButtons()
    }
    
    private func setupTableManager() {
        tableManager = VideoTableViewManager(with: view)
        tableManager.tableInitConfig.cellHeight = 208
        tableManager.tableInitConfig.shouldSetupSearchBar = false
        tableManager.setup()
        tableManager.delegate = self
        tableManager.locationOfCellWithLoadingIndicator = .bottom
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            self?.loader.loadNextPage()
        }
        tableManager.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "ic_videoListBg"))
    }
    
    func videoTableManager(_ manager: VideoTableViewManager, controllerToPresentSelectedVideoAt index: Int) -> UIViewController? {
        return self
    }
    
    private func setupNavBarButtons() {
        guard allowEditing else { return }
        let trash = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(trashTapped))
        trashBarButton = trash
        let plus = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(plusTapped))
        navigationItem.rightBarButtonItems = [plus, trash]
    }
    
    @objc private func plusTapped() {
        AlertsPresentator.presentVideoPickerWithPreChooseSource(in: self) { [weak self] in $0?.delegate = self }
    }
    
    @objc private func trashTapped() {
        deletingModeEnabled = !deletingModeEnabled
        if !deletingModeEnabled {
            removeSelectedVideos()
        }
    }
    
    private func removeSelectedVideos() {
        HUD.show(.progress)
        removeVideosRecursively(completion: { [weak self] isClear in
            guard let `self` = self else { return }
            if isClear {
                var indexPaths = [IndexPath]()
                self.tableManager.dataSource.enumerated().forEach({ index, element in
                    if element.isSelectedToDelete {
                        indexPaths.append(self.tableManager.indexPathOfCell(forDataSourceItemAt: index))
                    }
                })
                self.loader.reloadData(animated: false, refreshController: false, completion: { success, error in
                    guard success else {
                        self.showError(error)
                        return
                    }
                    self.updateTableManagerDSAndLI()
                    self.tableManager.deleteRows(at: indexPaths)
                    HUD.flash(.success)
                })
            }
            else {
                self.loader.reloadData()
                AlertsPresentator.presentAlertErrorWhileDeletingVideos(in: self)
            }
        })
    }
    
    private func removeVideosRecursively(completion: ((Bool)->Void)?, isClear: Bool = true) {
        guard let id = videoIdsToDelete.popFirst() else {
            completion?(isClear)
            return
        }
        ServerApi.shared.removeVideo(withId: id, completion: { [weak self] in
            self?.removeVideosRecursively(completion: completion, isClear: isClear)
        }) { [weak self] (_, _) in
            self?.removeVideosRecursively(completion: completion, isClear: false)
        }
    }
    
    private func uploadVideo(url: URL) {
        HUD.show(.progress)
        let inputs = [(videoUrl: url, thumbnail: generateThumbnail(forVideoWithUrl: url))]
        UploadEventVideoRequestModel.createAsync(withEventId: self.eventId, inputs: inputs, completion: { [weak self] model in
            guard let model = model else {
                HUD.hide()
                AlertsPresentator.presentAlertErrorWhileProcessingVideo(in: self)
                return
            }
            ServerApi.shared.uploadEventVideo(with: model, completion: {
                self?.loader.reloadData(animated: false, completion: { success, error in
                    if success {
                        HUD.flash(.success)
                        self?.tableManager.scrollTableViewToBottom(animated: true)
                    }
                    else {
                        HUD.hide()
                        self?.showError(error)
                    }
                    VideoTrimmerController.clearLocalCache()
                })
            }, errorBlock: { [weak self] (error, _) in
                self?.showError(error)
            })
        })
    }
    
    private func updateTableManagerDSAndLI() {
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .bottom : .none
        tableManager.dataSource = viewModel.viewModels
    }
    
    override func refresh() {
        updateTableManagerDSAndLI()
        tableManager.reloadData()
    }
    
    func videoTableManager(_ manager: VideoTableViewManager, didSelectToDelete selected: Bool, cellAt indexPath: IndexPath) {
        let id = loader.model.models[tableManager.indexOfDataSourceItem(forCellAt: indexPath)].id!
        if selected {
            videoIdsToDelete.insert(id)
        }
        else {
            videoIdsToDelete.remove(id)
        }
    }
    
    
    //MARK: UIImagePickerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let url = info[UIImagePickerControllerMediaURL] as? URL else {
            print("NO URL RECEIVED FROM PICKER")
            return
        }
        picker.dismiss(animated: true) {
            let trimmer = VideoTrimmerController.presented(in: self, withVideoUrl: url)
            trimmer.onTrimVideo = { [weak self] url in
                self?.uploadVideo(url: url)
            }
        }
    }
}
