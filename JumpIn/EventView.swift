//
//  EventCellView.swift
//  JumpIn
//
//  Created by Admin on 16.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


class EventViewManager: BaseViewManager<EventViewModel, EventView> {
    override func refresh() {
        view.bgImageView.image = nil
        view.bgImageView.imageLink = viewModel.bgImageLink
        view.leftBgImageView.image = viewModel.leftBgImage
        view.subtitleLabel.text = viewModel.category
        view.titleLabel.text = viewModel.name
        view.likesLabel.text = viewModel.likes
        view.membersLabel.text = viewModel.membersCount
        view.dislikesLabel.text = viewModel.dislikes
        view.greenView.isHidden = viewModel.greenLabelHidden
        view.greenLabel.text = viewModel.greenLabelText
        view.orangeView.isHidden = viewModel.orangeLabelHidden
        view.orangeLabel.text = viewModel.orangeLabelText
        view.locationLabel.text = UserInfoService.city
    }
}


class EventPageViewModel: PageViewModel<EventPageModel, EventViewModel> {}

class EventViewModel: BaseViewModel<EventModel> {
    
    var name: String
    var category: String?
    var likes: String
    var dislikes: String
    var membersCount: String
    var bgImageLink: String?
    var greenLabelHidden: Bool
    var greenLabelText: String?
    var leftBgImage: UIImage?
    var orangeLabelHidden: Bool
    var orangeLabelText: String
    var location: String?
    
    required init(withModel model: EventModel) {
        let categoryInfo = CategoryInfoService.categories[model.category!]!
        self.bgImageLink = model.avatar?.filepath ?? nil
        self.greenLabelHidden = !model.withMe && !model.isMy
        if model.isPremium {
            self.leftBgImage = #imageLiteral(resourceName: "premium-fill")
        }
        else {
            self.leftBgImage = UIImage(named: "\(model.category!.rawValue)-fill")!
        }
        if model.isMy { self.greenLabelText = NSLoc("my") }
        else if model.withMe { self.greenLabelText = NSLoc("self_active") }
        self.orangeLabelHidden = model.timeFrame == .current
        self.orangeLabelText = NSLoc("event_label_archive")
        self.name = model.name ?? ""
        self.category = categoryInfo.title.uppercased()
        self.likes = "\(model.likes!)"
        self.dislikes = "\(model.dislikes!)"
        self.membersCount = "\(model.membersCount!)"
        self.location = model.locationCity
        super.init(withModel: model)
    }
}


class EventView: BaseCustomView, Configurable2 {
    
    @IBOutlet weak var bgImageView: DownloadingImageView!
    @IBOutlet weak var leftBgImageView: UIImageView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var membersLabel: UILabel!
    @IBOutlet weak var dislikesLabel: UILabel!
    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var orangeView: UIView!
    @IBOutlet weak var orangeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!

    var configurator: AnyObject?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.generalInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.generalInit()
    }
    
    private func generalInit() {
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        backgroundColor = .clear
        layer.cornerRadius = 5
        clipsToBounds = true
    }
}
