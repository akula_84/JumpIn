//
//  GetTopEventsList.swift
//  JumpIn
//
//  Created by Admin on 28.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetEventsRequestModel: BaseRequestModelWithPagination, ModelProtocol {
    
    enum UserScope: String {
        case his, past = "archive", participated = "participant"
    }
    
    enum SelfScope: String {
        case own = "my", confirmed = "approved", requested, invited
    }
    
    enum `Type`: String {
        case all, my = "self", user
    }
    
    enum PopularityType: String {
        case new, popular, unpopular, old
    }
    
    enum TimeFrame: String {
        case current, past = "archive"
    }
    
    
    var type: Type!
    var userScope: UserScope?
    var selfScope: SelfScope?
    var category: CategoryInfoService.CategoryInfo.Name?
    var userId: Int?
    var popularityType: PopularityType?
    var timeFrame: TimeFrame?
    var dateStart: String?
    var dateEnd: String?
    var searchString: String?
    var isPremium: Bool?
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        type <- map["t"]
        userScope <- map["user_scope"]
        selfScope <- map["self_scope"]
        category <- map["c"]
        userId <- map["user_id"]
        popularityType <- map["f"]
        timeFrame <- map["time_frame"]
        dateStart <- map["date_start"]
        dateEnd <- map["date_end"]
        searchString <- map["q"]
        isPremium <- map["premium"]
    }
}
