//
//  BaseNavigationController.swift
//  JumpIn
//
//  Created by Admin on 17.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class BaseNavigationController: UINavigationController, UINavigationControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        applyDefaultSettings()
    }
    
    var completion: EmptyBlock? = nil
    
    func push(_ viewController:UIViewController?, animated: Bool = false,
              completion:EmptyBlock? = nil) {
        guard let viewController = viewController else {
            return
        }
        self.completion = completion
        self.delegate = self
        pushViewController(viewController, animated: animated)
        
        interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func pop(animated: Bool = false, completion:EmptyBlock? = nil) {
        self.completion = completion
        self.delegate = self
        popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool = false, completion:EmptyBlock? = nil) {
        self.completion = completion
        self.delegate = self
        popToRootViewController(animated: animated)
    }
    
    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.delegate = nil
        self.completion?()
    }
}


extension UINavigationController {
    
    func setNavigationBarTransparent() {
        navigationBar.isTranslucent = true
        navigationBar.tintColor = .white
    }
    
    func applyDefaultSettings() {
        let navBar = navigationBar
        navBar.barTintColor = UIColor.white
        navBar.isTranslucent = false
        navBar.applyDefaultTintColor()
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.titleTextAttributes = [.foregroundColor : UINavigationBar.defaultTintColor, .font : UIFont.systemFont(ofSize: 18.0)]
        setNavigationBarHidden(false, animated: true)
    }
}


extension UINavigationBar {
    
    static var defaultTintColor: UIColor { return  UIColor(red: 110.0/255.0, green: 116.0/255.0, blue: 127.0/255.0, alpha: 1.0)}
    
    func applyDefaultTintColor() {
        tintColor = type(of:self).defaultTintColor
    }
}
