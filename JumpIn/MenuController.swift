//
//  MenuController.swift
//  JumpIn
//
//  Created by Admin on 18.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


class MenuController: BaseConfigurableController<MenuControllerViewModel> {

    var dataLoader: DataLoadingManager<EmptyModel, MenuController>!
    //
    @IBOutlet weak var myEventsButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var allEventsButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var friendsButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var messagesButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var profileInfoView: UIView!
    @IBOutlet weak var usersAvatar: DownloadingImageView!
    @IBOutlet weak var usersNickname: UILabel!
    @IBOutlet weak var usersStatus: UILabel!
    @IBOutlet weak var numberOfUnreadMessagesLabel: UILabel!
    @IBOutlet weak var premiumStatusImageView: UIImageView!
    
    
    init() {
        super.init(nibName: "MenuController", bundle: nil)
        dataLoader = DataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getProfile(with:completion:errorBlock:) as AnyObject)
        dataLoader.requestModel = EmptyModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(onProfileUpdated), name: .profileUpdated, object: nil)
        automaticallyAdjustsScrollViewInsets = false
        clearUI()
        configureButtons()
        addGesture()
        dataLoader.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.setNavigationBarHidden(true, animated: animated)
        dataLoader.reloadData(animated: false, completion: nil)
        ServerApi.shared.getUnreadMessagesCount(completion: { [weak self] in
            let number = $0.numberOfUnreadMessages!
            self?.numberOfUnreadMessagesLabel.text = String(describing: number)
            self?.numberOfUnreadMessagesLabel.superview!.isHidden = number <= 0
            }, errorBlock: nil)
    }
    
    private func addGesture() {
        profileInfoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileAction)))
    }
    
    private func configureButtons() {
        myEventsButton.onClick = { [weak self] _ in self?.switchViewController(MyEventsFeedController())}
        allEventsButton.onClick = { [weak self] _ in self?.switchViewController(TopEventsFeedController(withFilter: true))}
        friendsButton.onClick = { [weak self] _ in self?.switchViewController(FriendsController()) }
        messagesButton.onClick = { [weak self] _ in self?.switchViewController(DialogListController()) }
    }

    private func clearUI() {
        usersNickname.text = ""
        usersStatus.text = ""
        premiumStatusImageView.isHidden = true
    }

    @objc func onProfileUpdated(_ notification: Notification) {
        let profile = notification.userInfo![ProfileController.kProfile] as! GetUserInfoResponseModel
        self.viewModel = MenuControllerViewModel(withModel: profile)
        self.refresh()
    }

    override func refresh() {
        usersNickname.text = viewModel.nik
        usersStatus.text = viewModel.status
        if let link = viewModel.avatarLink { usersAvatar.imageLink = link }
        else { usersAvatar.image = #imageLiteral(resourceName: "user_no_image") }
        premiumStatusImageView.isHidden = !viewModel.isPremium
    }
    
    private func switchViewController(_ controller: UIViewController) {
        navigationController!.pushViewController(controller, animated: true)
    }
    
    @objc private func profileAction() {
        switchViewController(ProfileController())
    }

    @IBAction func addEventAction() {
        switchViewController(EditEventController())
    }
}
