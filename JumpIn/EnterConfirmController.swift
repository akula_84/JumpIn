//
//  EnterConfirmController.swift
//  JumpIn
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


class EnterConfirmController: BaseViewController {
    
    @IBOutlet private weak var codeTextField: UnderLineTextField!
    @IBOutlet fileprivate weak var confirmButton: UIButton!
    //
    private var _accessoryInputView: AccessoryInputViewWithDoneButton?
    
    
    init() {
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
        codeTextField.correctionEnabled = false
        codeTextField.delegate = self
        codeTextField.keyboardType = .numberPad
        confirmButton.setBackgroundImage(UIImage(color: UIColor(red: 93.0/255.0, green: 173.0/255.0, blue: 226.0/255.0, alpha: 1.0)), for: .normal)
        confirmButton.setBackgroundImage(UIImage(color:UIColor.lightGray), for: .disabled)
    }
    
    override var inputAccessoryView: UIView? {
        if _accessoryInputView == nil {
            _accessoryInputView = AccessoryInputViewWithDoneButton(withOnDone: { [weak self] in
                _ = self?.codeTextField.resignFirstResponder()
            })
        }
        return _accessoryInputView
    }
    
    @IBAction func confirmAction() {
        HUD.show(.progress)
        ServerApi.shared.check(smsCode: codeTextField.text!, completion: { [weak self] in
            HUD.hide()
            self?.navigationController?.pushViewController(ProfileEditingController(isEnterMissingData:true), animated: true)
        }) { (_, _) in
            HUD.flash(.error, delay: 0.5)
        }
        
    }

    @IBAction func sendCodeAgainAction() {
        guard let phone = UserInfoService.shared.phone else {
            return
        }
        HUD.show(.progress)
        ServerApi.shared.sendSmsCode(to: phone, completion: {
            HUD.flash(.success, delay: 0.5)
        }) { (_, _) in
            HUD.flash(.error, delay: 0.5)
        }
    }
    
}


extension EnterConfirmController: UnderLineTextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let limit = 4
        let newText = textField.text != nil ? NSString(string: textField.text!).replacingCharacters(in: range, with: string) : string
        confirmButton.isEnabled = newText.count >= limit
        return newText.count <= limit
    }
}
