//
//  ServerSocialNetModel.swift
//  JumpIn
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


class SocialNetModel: ModelProtocol {
    
    var id: Int!
    var userId: Int!
    var socialNetName: String!
    var hidden: Bool?
    var userNameDisplayingInSocialNet: String!
    var pageId: Int!
    var userLinkInSocialNet: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        userId <- map["user_id"]
        socialNetName <- map["provider"]
        hidden <- map["hide"]
        userNameDisplayingInSocialNet <- map["social_name"]
        pageId <- map["page_id"]
        userLinkInSocialNet <- map["url"]
    }
}
