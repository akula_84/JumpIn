//
//  MenuManager.swift
//  JumpIn
//
//  Created by Admin on 14.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

//import UIKit
//import SideMenu
//
//
//protocol MenuManagerProtocol {
//    associatedtype LeftController: UIViewController
//    associatedtype MenuContainerController: UIViewController
//    //
//    static var shared: Self { get }
//    var menuContainerController: MenuContainerController { get }
//    var isMenuOpened: Bool { get }
//    var frontController: UIViewController { get set }
//    var leftController: LeftController { get }
//    //
//    func openMenu()
//    func closeMenu()
//    func toggleMenu()
//}
//
//final class MenuManager: MenuManagerProtocol {
//
//    static let shared = MenuManager()
//    //
//    var frontController: UIViewController {
//        get { return menuContainerController.rootViewController! }
//        set { menuContainerController.rootViewController = frontController }
//    }
//    var leftController: MenuController {
//        return menuContainerController.leftViewController as! MenuController
//    }
//    private(set) var menuContainerController: LGSideMenuController
//    var isMenuOpened: Bool {
//        return menuContainerController.isLeftViewShowing
//    }
//
//    private init() {
//        let frontVc = BaseNavigationController(rootViewController: TopEventsFeedController())
//        menuContainerController = LGSideMenuController(rootViewController: frontVc)
//        menuContainerController.leftViewController = MenuController()
//        menuContainerController.swipeGestureArea = .full
//    }
//
//    func openMenu() {
//        menuContainerController.showLeftViewAnimated()
//    }
//
//    func closeMenu() {
//        menuContainerController.hideLeftViewAnimated()
//    }
//
//    @objc func toggleMenu() {
//        menuContainerController.toggleLeftViewAnimated()
//    }
//
//    func switchTo(_ viewController: UIViewController, embeddedInNavController: Bool) {
//        var vc = viewController
//        if embeddedInNavController {
//            vc = BaseNavigationController(rootViewController: vc)
//        }
//        frontController = vc
//        closeMenu()
//    }
//}

