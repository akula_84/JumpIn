//
//  STRouter+Loader.swift
//  Spytricks
//
//  Created by Артем Кулагин on 06.04.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import UIKit
import PKHUD

/**
 Расширение для работы с лоадером
 */
extension STRouter {
    /// Функция появления лоадера
    static func showLoader() {
        HUD.show(.progress)
    }
    
    /// Убрать индикацию загрузки
    static func removeLoader() {
        HUD.hide()
    }
    
    static func showError(_ error: MainURLEngine.Error?, action: (()->Void)? = nil) {
        showError(withDescription: error?.description, action: action)
    }
    
    static func showError(withDescription text: String?, action: (()->Void)? = nil) {
        print("showError",text)
        guard let view = topView else {
            return
        }
        HUD.flash(.error, onView: view, delay: 0.3, completion: { _ in
            guard let text = text else { return }
            let alert = UIAlertController(title: "Ошибка", message: text, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel) { _ in action?() }
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        })
    }
}
