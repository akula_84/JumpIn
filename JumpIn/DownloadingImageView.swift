//
//  DownloadingImageView.swift
//  JumpIn
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class DownloadingImageView: UIImageView {

    @IBInspectable var imageLink: String? { didSet { downloadImage() } }
    @IBInspectable var useCache: Bool = true
    @IBInspectable var placeholder: UIImage?
    //
    private(set) var isDownloading = false
    private var downloadTask: URLSessionDataTask?
    
    
    private func downloadImage() {
        guard imageLink != nil else { return }
        //
        let url = URL(string: imageLink!)
        guard url != nil else { return }
        //
        cancelDownloading()
        if trySetImageFromCache() { return }
        //
        isDownloading = true
        if let placeholder = self.placeholder { image = placeholder }
        //
        let request = URLRequest(url: url!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0)
        downloadTask = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] data, responce, error in
            guard let `self` = self else { return }
            self.isDownloading = false
            guard data != nil else { return }
            if let image = UIImage(data: data!) {
                DispatchQueue.main.async { [weak self] in
                    self?.image = image
                    self?.trySaveImageIntoCache(imageDataLength: data!.count)
                }
            }
        })
        downloadTask?.resume()
    }
    
    func cancelDownloading() {
        if let downloadTask = self.downloadTask { downloadTask.cancel() }
        isDownloading = false
    }
    
    private func trySetImageFromCache() -> Bool {
        if useCache {
            if let image = ImageCacheService.shared.image(forLink: imageLink!) {
                self.image = image
                return true
            }
        }
        return false
    }
    
    private func trySaveImageIntoCache(imageDataLength: Int) {
        if self.useCache && image != nil && imageLink != nil {
            ImageCacheService.shared.set(image: image!, forLink: self.imageLink!, dataLength: imageDataLength)
        }
    }
}
