//
//  MainEventsFeedCell.swift
//  JumpIn
//
//  Created by Admin on 28.04.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SwipeCellKit


protocol EventCell: Configurable2 {
    var eventView: EventView! { get set }
}


class EventCellManager<EventCellType: UIView>: BaseViewManager<EventViewModel, EventCellType> where EventCellType: EventCell {
    
    required init(view: UIView) {
        super.init(view: view)
        self.view.eventView.configurator = EventViewManager(view: self.view.eventView)
    }
    
    override func refresh() {
        let manager = view.eventView.configurator as! EventViewManager
        manager.viewModel = viewModel
        manager.refresh()
    }
}


class EventTableCell: SwipeTableViewCell, EventCell {
    
    weak var eventView: EventView!
    var configurator: AnyObject?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupEventView()
    }
    
    private func setupEventView() {
        let view = EventView(frame: bounds)
        addSubview(view)
        eventView = view
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class EventCollectionCell: UICollectionViewCell, EventCell {
    
    weak var eventView: EventView!
    var configurator: AnyObject?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupEventView()
    }
    
    private func setupEventView() {
        let view = EventView(frame: bounds)
        addSubview(view)
        eventView = view
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
