//
//  EnterPhoneController.swift
//  JumpIn
//
//  Created by Admin on 12.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


enum SocialNetType {
    case Vk, Fb, Instagram
}


class EnterPhoneController: BaseViewController {
    
    @IBOutlet private weak var phoneTextField: UnderLineTextField!
    @IBOutlet fileprivate weak var continueButton: UIButton!
    //
    private var _accessoryInputView: AccessoryInputViewWithDoneButton?
    fileprivate var phone: String!
    let maxNumberOfCharactersInPhone: Int = 12
    let maxNumberOfDigitsInPhone: Int = 11
    
    init() {
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
        phoneTextField.keyboardType = .numberPad
        phoneTextField.delegate = self
        
        continueButton.setBackgroundImage(UIImage(color: UIColor(red: 93.0/255.0, green: 173.0/255.0, blue: 226.0/255.0, alpha: 1.0)), for: .normal)
        continueButton.setBackgroundImage(UIImage(color:UIColor.lightGray), for: .disabled)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.navigationBar.isTranslucent = true
    }
    
    
    override var inputAccessoryView: UIView? {
        if _accessoryInputView == nil {
            _accessoryInputView = AccessoryInputViewWithDoneButton(withOnDone: { [weak self] in
                _ = self?.phoneTextField.resignFirstResponder()
            })
        }
        return _accessoryInputView
    }
    
    
    @IBAction func continueAction() {
        HUD.show(.progress)
        UserInfoService.shared.phone = phone
        ServerApi.shared.sendSmsCode(to: phone, completion: { [weak self] in
            HUD.hide()
            self?.navigationController?.pushViewController(EnterConfirmController(), animated: true)
        }) { [weak self] (error, _) in
            self?.showError(error)
        }
    }
}


extension EnterPhoneController: UnderLineTextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let text = textField.text
        if text == nil || text!.isEmpty {
            textField.text = "+7"
        }
        else if phone != nil {
            textField.text = "+\(phone!)"
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let textLength = textField.text!.count
        if textLength == 2 {
            textField.text = ""
        }
        else if textLength == maxNumberOfCharactersInPhone {
            var text = textField.text!
            phone = String(text[text.index(text.startIndex, offsetBy: 1)...])
            var index = text.index(text.startIndex, offsetBy: 2)
            text.insert(contentsOf: " (", at: index)
            index = text.index(index, offsetBy: 5)
            text.insert(contentsOf: ") ", at: index)
            index = text.index(index, offsetBy: 5)
            text.insert(contentsOf: "-", at: index)
            index = text.index(index, offsetBy: 3)
            text.insert(contentsOf: "-", at: index)
            textField.text = text
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = textField.text != nil ? NSString(string: textField.text!).replacingCharacters(in: range, with: string) : string
        let newTextLength = newText.count
        return newTextLength <= maxNumberOfCharactersInPhone && newTextLength >= 2
    }
    
    func textField(_ textField: UnderLineTextField, didChangeText newText: String?) {
        let textLength = textField.text?.count
        continueButton.isEnabled = textLength == maxNumberOfCharactersInPhone
        if textLength == maxNumberOfCharactersInPhone {
            _ = textField.resignFirstResponder()
        }
    }
}
