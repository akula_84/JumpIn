	//
//  AppDelegate.swift
//  JumpIn
//
//  Created by Admin on 10.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import FBSDKCoreKit
import GoogleMaps
import GooglePlaces
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var deviceToken: String? {
        didSet {
            UserDefaults.standard.set(deviceToken, forKey: "deviceToken")
        }
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //
        deviceToken = UserDefaults.standard.string(forKey: "deviceToken")
        //
        Crashlytics().debugMode = true
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey("AIzaSyCUcB9INGO0_A52D4fkAiq0ugKL8zWqvvA")
        GMSPlacesClient.provideAPIKey("AIzaSyD5xEOmEjrPzCQvnuT0esS3YXx3_CW27Bs")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //
//        let vc = TopEventsFeedController()
//        let window = UIWindow(frame: UIScreen.main.bounds)
//        window.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        window.rootViewController = BaseNavigationController(rootViewController: vc)
//        self.window = window
//        window.makeKeyAndVisible()
        //
        /*
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
     */
        registerForPushNotifications()
        //
      //  JUMAuthManager.checkStatusInit()
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        sendInviterIfExist(using: url)
        return true
    }
    
    private func sendInviterIfExist(using url: URL) {
        if let query = url.query, let range = query.range(of: "deep_link?referred_by_user_id=") {
            var inviterId = String(query[range.upperBound...])
            if let separatorIndex = inviterId.index(of: "&") {
                inviterId = String(inviterId[..<separatorIndex])
            }
            var provider = String(query[query.range(of: "referred_by_provider=")!.upperBound...])
            if let separatorIndex = provider.index(of: "&") {
                provider = String(provider[..<separatorIndex])
            }
            let _provider = GetFriendsFromSocNetRequestModel.SocNetProvider(rawValue: provider)!
            let model = SetInviterToAppRequestModel(inviterId: Int(inviterId)!, socNetProvider: _provider)
            ServerApi.shared.setInviterToApp(with: model, completion: {
                print("inviter id sended to server succesfull!")
            }, errorBlock: nil)
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    
    //MARK: PUSH NOTIFICATIONS
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        completionHandler()
        JUMPushManager.check(response.notification.request.content.userInfo)
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    private func registerForPushNotifications() {
        let app = UIApplication.shared
        if #available(iOS 10, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .badge, .sound])
            { (granted, error) in
                guard error == nil else {
                    print("ERROR while authorization for PUSH NOTIFICATIONS: \(error!)")
                    return
                }
                if granted {
                    DispatchQueue.main.async {
                        app.registerForRemoteNotifications()
                    }
                }
                else {
                    //Handle user denying permissions..
                    print("NO PERMISSIONS GRANTED for PUSH NOTIFICATIONS")
                }
            }
            
            //Register for remote notifications.. If permission above is NOT granted, all notifications are delivered silently to AppDelegate.
            //app.registerForRemoteNotifications()
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            app.registerUserNotificationSettings(settings)
            app.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("SUCCESS register for PUSH NOTIFICATIONS with settings: \(notificationSettings)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR while register for PUSH NOTIFICATIONS: \(error)")
        deviceToken = nil
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        self.deviceToken = token
        ServerApi.shared.sendPushToken(token)
        print("SUCCESS register for PUSH NOTIFICATIONS with TOKEN: \(token)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("DID RECEIVE PUSH NOTIFICATION: \(userInfo)")
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("didReceiveRemoteNotification",userInfo)
        JUMPushManager.check(userInfo)

    }
}

