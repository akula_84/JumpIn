//
//  GetFriendsRequestModel.swift
//  JumpIn
//
//  Created by Admin on 16.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class GetFriendsRequestModel: BaseRequestModelWithPagination {
    
    enum `Type`: String {
        case all, online, requests, suggests
    }
    
    var type: Type!
    var searchString: String?
    
    
    init(with type: Type) {
        super.init()
        self.type = type
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        type <- map["t"]
        searchString <- map["q"]
    }
}
