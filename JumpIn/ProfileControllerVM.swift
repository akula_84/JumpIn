//
//  ProfileControllerVM.swift
//  JumpIn
//
//  Created by Admin on 12.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper


class ProfileControllerVM: BaseViewModel<GetUserInfoResponseModel> {
    
    enum UserType: String {
        case me, friend, stranger = "not_friend", wantToBeFriends = "requested_by_user", iWantToBeFriendsWith = "requested_by_me"
        init(model: GetUserInfoResponseModel.AdditionalData.UserType) {
            self = UserType(rawValue: model.rawValue)!
        }
    }
    
    var avatarLink: String?
    var nick: String
    var firstName: String
    var lastName: String
    var sex: SexViewModel
    var city: String
    var phone: String
    var photos: [PhotoWithLikesViewModel]
    var socialNets: [SocialNetViewModel]
    var ownEvents: Int
    var archivedEvents: Int
    var participantEvents: Int
    var age: String
    var status: String
    var birthday: String
    var userType: UserType
    var isPremium: Bool?
    
    required init(withModel model: GetUserInfoResponseModel) {
        userType = UserType(model: model.additionalData.userType)
        isPremium = model.userInfo.isPremium
        nick = model.userInfo.nickname
        firstName = model.userInfo.first_name
        lastName = model.userInfo.last_name
        sex = SexViewModel(withModel: model.userInfo.sex)
        city = model.userInfo.city
        phone = model.userInfo.phone ?? ""
        avatarLink = model.userInfo.avatar?.filepath
        if let links = model.userInfo.photos {
            photos = links.map({ return PhotoWithLikesViewModel(withModel: $0) })
        }
        else {
            photos = []
        }
        if let nets = model.userInfo.socialNets {
            socialNets = nets.map({ return SocialNetViewModel(withModel: $0) })
        }
        else {
            socialNets = []
        }
        let events = model.additionalData.eventsCount!
        ownEvents = events.own
        archivedEvents = events.archived
        participantEvents = events.participant
        age = model.additionalData.age ?? ""
        status = model.userInfo.status ?? ""
        if let bdateStr = model.userInfo.birthday {
            birthday = DateConverter.ForInterface.Profile.birthday(fromServerBirthday: bdateStr)
        }
        else { birthday = "" }
        super.init(withModel: model)
    }
}
