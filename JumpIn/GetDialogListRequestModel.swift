//
//  GetDialogListRequestModel.swift
//  JumpIn
//
//  Created by Admin on 17.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class GetDialogListRequestModel: BaseRequestModelWithPagination {
    
    var searchString: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        searchString <- map["search"]
    }
}
