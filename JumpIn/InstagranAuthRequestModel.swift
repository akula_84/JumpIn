//
//  InstAuthorizeRequestModel.swift
//  JumpIn
//
//  Created by Admin on 26.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class InstagranAuthRequestModel: Mappable {
    
    var appId: String!
    var redirectLink: String!
    var responseType = "token"
    
    init(appId: String, redirectLink: String) {
        self.appId = appId
        self.redirectLink = redirectLink
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        appId <- map["client_id"]
        redirectLink <- map["redirect_uri"]
        responseType <- map["response_type"]
    }
}
