//
//  AvatarWithPhotosCollectionController.swift
//  JumpIn
//
//  Created by Admin on 24.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper
import PKHUD


class PhotoViewModel: BaseViewModel<EmptyModel> {
    var link: String?
    var image: UIImage?
    var indexInCollection: Int?
    var id: Int?
    
    required init(withModel model: EmptyModel) {
        super.init(withModel: model)
    }
    
    override init() {
        super.init()
    }

    init?(withPhoto photo: PhotoWithLikesModel?) {
        guard let photo = photo else { return nil }
        self.link = photo.filepath
        self.id = photo.id
        super.init()
    }
    init(withImage image: UIImage?, index: Int? = nil) {
        self.image = image
        self.indexInCollection = index
        super.init()
    }
}

class AvatarWithPhotosCollectionController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    enum OwnerType: String {
        case events, users
        
        var photoUploadType: UploadTargetType {
            switch self {
                case .events: return .event
                case .users: return .user
            }
        }
    }

    @IBOutlet weak var avatarImageView: DownloadingImageView!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var goToAllPhotosButton: UIButton!
    
    private var photos:[PhotoViewModel] = []
    private var avatar: PhotoViewModel?
    private(set) var isAvatarExists = false
    var maxNumberOfPhotos: Int {
        switch type { case .events: return 11; case .users: return 4 }
    }
    private var type: OwnerType
    var ownerId: Int? {
        didSet {
            if isViewLoaded {
                goToAllPhotosButton.isHidden = ownerId == nil
            }
        }
    }
    private var indexOfCellThatRequestChanging: Int?
    private var newPhotosToUpload: [PhotoViewModel] = []
    private var existingPhotosIdToDelete: [Int] = []
    private var newAvatarIndex: Int?
    private var collectionManager: PhotoCollectionViewManager!
    private var noImage: UIImage {
        switch type {
            case .users: return #imageLiteral(resourceName: "user_no_image")
            case .events: return #imageLiteral(resourceName: "no_image")
        }
    }
    var isChanged: Bool { return !existingPhotosIdToDelete.isEmpty || !newPhotosToUpload.isEmpty }
    
    init(withType type: OwnerType, ownerId: Int?) {
        self.type = type
        self.ownerId = ownerId
        super.init(nibName: "\(Swift.type(of: self).self)", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(photos: [PhotoViewModel]) {
        self.photos = photos
        if isViewLoaded { refreshPhotoCollection() }
    }
    
    func set(avatar: PhotoViewModel?) {
        self.avatar = avatar
        if isViewLoaded { refreshAvatar() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionManager = PhotoCollectionViewManager(with: photoCollectionView)
        collectionManager.setup()
        collectionManager.onClickAddPhoto = { [weak self] in
            self?.indexOfCellThatRequestChanging = $0
            self?.presentImagePickerSourceAlert()
        }
        collectionManager.onClickDeletePhoto = { [weak self] in
            self?.indexOfCellThatRequestChanging = $0
            self?.presentDeleteImageAlert()
        }
        refreshPhotoCollection()
        refreshAvatar()
        goToAllPhotosButton.isHidden = ownerId == nil
    }
    
    private func presentDeleteImageAlert() {
        let alert = UIAlertController(title: NSLoc("remove_photo"), message: nil, preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: NSLoc("remove"), style: .destructive, handler: { _ in
            self.newAvatarIndex = nil
            if let i = self.indexOfCellThatRequestChanging {
                if let _i = self.newPhotosToUpload.index(where: { $0.indexInCollection != nil && i == $0.indexInCollection! }) {
                    self.newPhotosToUpload.remove(at: _i)
                }
                else {
                    self.existingPhotosIdToDelete.append(self.collectionManager.dataSource[i].id!)
                }
                self.collectionManager.dataSource[i].image = nil
                self.collectionManager.dataSource[i].link = nil
                self.collectionManager.reloadData()
            }
            else {
                self.isAvatarExists = false
                self.plusButton.isSelected = false
                self.avatarImageView.image = self.noImage
                if let i = self.newPhotosToUpload.index(where: { $0.indexInCollection == nil}) {
                    self.newPhotosToUpload.remove(at: i)
                }
                else {
                    self.existingPhotosIdToDelete.append(self.avatar!.id!)
                }
            }
        })
        let cancel = UIAlertAction(title: NSLoc("cancel"), style: .cancel, handler: nil)
        alert.addAction(delete)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    private func presentImagePickerSourceAlert() {
        AlertsPresentator.presentAlertChoosePhotoSource(in: self) { photoSource in
            guard UIImagePickerController.isSourceTypeAvailable(photoSource) else { return }
            self.presentImagePicker(withSourceType: photoSource)
        }
    }
    
    private func presentImagePicker(withSourceType type: UIImagePickerControllerSourceType) {
        let picker = UIImagePickerController()
        picker.sourceType = type
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    private func refreshAvatar() {
        isAvatarExists = avatar != nil
        if let avatar = avatar {
            avatarImageView.imageLink = avatar.link
            plusButton.isSelected = true
        }
        else { avatarImageView.image = noImage }
    }
    
    private func refreshPhotoCollection() {
        collectionManager.dataSource.removeAll()
        for i in 0...maxNumberOfPhotos - 1 {
            let model = i <= photos.count - 1 ? photos[i] : PhotoViewModel()
            collectionManager.dataSource.append(model)
        }
        collectionManager.reloadData()
    }

    fileprivate func showError(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

    func applyChanges(withCompletion completion: (()->Void)?) {
        deletePhotos { [weak self] (success, error) in
            guard let `self` = self else { return }
            if !success {
                HUD.flash(.error, onView: self.view, delay: 0.5, completion: { _ in
                    var message = NSLoc("could_not_delete_photo")
                    if let error = error { message.append(" \(error)") }
                    self.showError(title:  NSLoc("error"), message: message)
                })
            }
            else {
                self.uploadPhotos(withCompletion: { (success, error, ids) in
                    if !success {
                        HUD.flash(.error, onView: self.view, delay: 0.5, completion: { _ in
                            var message = NSLoc("could_not_load_photo")
                            if let error = error { message.append(" \(error)") }
                            self.showError(title: NSLoc("error"), message: message)
                        })
                    }
                    else if let i = self.newAvatarIndex {
                        self.setAvatar(withPhotoId: ids![i], targetType: self.type.photoUploadType, eventId: self.ownerId, completion: { (success, error) in
                            if !success {
                                HUD.flash(.error, onView: self.view, delay: 0.5, completion: { _ in
                                    var message = NSLoc("failed_to_set_the_avatar")
                                    if let error = error { message.append(" \(error)") }
                                    self.showError(title:  NSLoc("error"), message: message)
                                })
                            }
                            else {
                                completion?()
                            }
                        })
                    }
                    else {
                        completion?()
                    }
                })
            }
        }
    }
    
    private func setAvatar(withPhotoId id: Int, targetType: UploadTargetType, eventId: Int?, completion: ((_ success: Bool, _ error: String?)->Void)?) {
        let model = SetAvatarRequestModel(targetType: targetType, photoId: id, eventId: eventId)
        ServerApi.shared.setAvatar(withModel: model, completion: { 
            completion?(true, nil)
        }) { (error, _) in
            completion?(false, error?.description)
        }
    }
    
    private func uploadPhotos(withCompletion completion: ((_ success: Bool, _ error: String?, _ responseArray: [Int]?)->Void)?) {
        guard newPhotosToUpload.count > 0 else {
            completion?(true, nil, nil)
            return
        }
        encodeToBase64(photos: newPhotosToUpload.map({ $0.image! })) { [weak self] encodedPhotos in
            guard let `self` = self else { return }
            let requestModel = PhotoUploadRequestModel(withType: self.type.photoUploadType, id: self.ownerId, photos: encodedPhotos)
            ServerApi.shared.uploadPhotos(withModel: requestModel, completion: {
                completion?(true, nil, $0)
            }, errorBlock: { (error, _) in
                completion?(false, error?.description, nil)
            })
        }
    }
    
    private func encodeToBase64(photos: [UIImage], completion: (([String])->Void)?) {
        DispatchQueue.global(qos: .utility).async {
            let base64Photos: [String] = photos.map({ UIImageJPEGRepresentation($0, 0)!.base64EncodedString() })
            DispatchQueue.main.async {
                completion?(base64Photos)
            }
        }
    }
    
    private func deletePhotos(withCompletion completion: ((_ success: Bool, _ error: String?)->Void)?) {
        if let id = self.existingPhotosIdToDelete.popLast() {
            ServerApi.shared.deletePhoto(withId: id, completion: { [weak self] in
                self?.deletePhotos(withCompletion: completion)
                }, errorBlock: { (error, _) in
                    completion?(false, error?.description)
            })
        }
        else {
            completion?(true, nil)
        }
    }
    
    @IBAction private func showAllPhotosAction() {
        guard let ownerId = ownerId else { return }
        let type = AllPhotosController.OwnerType(rawValue: self.type.rawValue)!
        navigationController!.pushViewController(AllPhotosController(ownerType: type, ownerId: ownerId), animated: true)
    }
    
    @IBAction private func avatarPlusButtonAction(_ sender: UIButton) {
        indexOfCellThatRequestChanging = nil
        if sender.isSelected {
            presentDeleteImageAlert()
        }
        else {
            presentImagePickerSourceAlert()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        if let i = indexOfCellThatRequestChanging {
            collectionManager.dataSource[i].image = image
            newPhotosToUpload.append(PhotoViewModel(withImage: image, index: i))
            collectionManager.reloadData()
        }
        else {
            isAvatarExists = true
            avatarImageView.image = image
            newPhotosToUpload.append(PhotoViewModel(withImage: image))
            newAvatarIndex = newPhotosToUpload.count - 1
            plusButton.isSelected = true
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
