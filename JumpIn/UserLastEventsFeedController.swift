//
//  UserLastEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit

class UserLastEventsFeedController: EventsFeedController {

    init(userId: Int) {
        super.init(withEventsType: .userOwn)
        self.title = NSLoc("completed")
        loader.requestModel.userId = userId
        loader.requestModel.timeFrame = .past
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
