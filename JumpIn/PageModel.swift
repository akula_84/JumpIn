//
//  PageModel.swift
//  JumpIn
//
//  Created by Admin on 16.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper


//protocol ModelsContainer: ModelProtocol {
//    associatedtype ModelElementType: ModelProtocol
//    var models: [ModelElementType]! { get set }
//}


protocol PageModelProtocolNonSpecialized: class {
    var currentPage: Int! { get set }
    var models: [AnyObject]! { get set }
}

protocol PageModelProtocol: ModelProtocol {
    associatedtype ModelElementType: ModelProtocol
    var currentPage: Int! { get set }
    var models: [ModelElementType]! { get set }
    var nextPageLink: String? { get set }
}

class PageModel<Element: ModelProtocol>: PageModelProtocol {
    
    var currentPage: Int!
    var models: [Element]!
    var nextPageLink: String?
    
    required init?(map: Map) {}
    
    init() { }
    
    func mapping(map: Map) {
        currentPage <- map["current_page"]
        models <- map["data"]
        nextPageLink <- map["next_page_url"]
    }
}


protocol PageViewModelProtocol: ViewModelProtocol where ModelType: PageModelProtocol {
    associatedtype ElementViewModelType: ViewModelProtocol
    var viewModels: [ElementViewModelType] { get set }
}

//protocol ListViewModelProtocol: _ListViewModelProtocol where ModelType.ModelElementType == ElementViewModelType.ModelType { }

class PageViewModel<Model: PageModelProtocol, ElementViewModel: ViewModelProtocol>: BaseViewModel<Model>, PageViewModelProtocol where Model.ModelElementType == ElementViewModel.ModelType {
    var viewModels: [ElementViewModel] = []
    
    init(viewModels: [ElementViewModel]) {
        self.viewModels = viewModels
        super.init()
    }
    
    required init(withModel model: Model) {
        viewModels = model.models.map({ ElementViewModel(withModel: $0) })
        super.init(withModel: model)
    }
}

//class PageViewModel<Element: ViewModelProtocol>: PageViewModelProtocol {
//
//    var currentPage: Int
//    var viewModels: [Element]
//
//    required init<T: PageModelProtocol>(withModel model: T) where T.ElementType: Element. {
//        //
//    }
//
//    init(currentPage: Int, viewModels: [ViewModelType]) {
//        self.currentPage = currentPage
//        self.viewModels = viewModels
//        super.init()
//    }
//
//    required init(withModel model: ModelType) {
//        currentPage = model.currentPage
//        viewModels = model.models?.map({ return ViewModelType(withModel: $0) }) ?? []
//        super.init(withModel: model)
//    }
//}

