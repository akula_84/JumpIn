//
//  SendDeepLinkRequestModelItem.swift
//  JumpIn
//
//  Created by Admin on 23.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

 
class SendInviteToAppRequestModelItem: Mappable {

    var socNetUserId: Int!
    var socNetProvider: GetFriendsFromSocNetRequestModel.SocNetProvider!
    
    init(socNetUserId id: Int, provider: GetFriendsFromSocNetRequestModel.SocNetProvider) {
        self.socNetUserId = id
        self.socNetProvider = provider
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        socNetUserId <- map["id"]
        socNetProvider <- map["provider"]
    }
}
