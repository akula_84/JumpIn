//
//  MainURLEngine.swift
//  JumpIn
//
//  Created by Admin on 20.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


typealias RequestErrorBlockType = (_ error:MainURLEngine.Error?, _ response:HTTPURLResponse?)->()

protocol JsonParser: class {
    var holderIndex: Int! { get set }
    func parse(_: Any, parseEnd:(()->Void)?)
}

extension JsonParser {
    func hold() { holderIndex = ObjectHolder.sharedInstance.hold(object: self) }
    func dispose() { ObjectHolder.sharedInstance.release(objectWithIndex: holderIndex) }
}

class JsonObjectParser<T:Mappable>: JsonParser {
    
    typealias CompletionType = (_ object:T)->Void
    
    var completion: CompletionType
    var holderIndex: Int!
    
    
    init(withCompletion completion: @escaping CompletionType) {
        //
        self.completion = completion
    }
    
    func parse(_ json: Any, parseEnd:(()->Void)?) {
        //
        if let jsonDict = json as? [String:Any], let responseModel = T(JSON:jsonDict) {
            //
            DispatchQueue.main.async { [weak self] in
                //
                self?.completion(responseModel)
                if let parseEnd = parseEnd { parseEnd() }
            }
        }
        else {
            fatalError("Received json object is not a valid dictionary or there are some errors while parsing it content")
        }
    }
}


class JsonArrayParser<T:Mappable>: JsonParser {
    
    typealias CompletionType = (_ object:[T])->Void
    var holderIndex: Int!
    
    var completion: CompletionType
    
    
    init(withCompletion completion: @escaping CompletionType) {
        //
        self.completion = completion
    }
    
    func parse(_ json: Any, parseEnd:(()->Void)?) {
        //
        if let jsonArray = json as? [[String:Any]] {
            //
            DispatchQueue.main.async { [weak self] in
                //
                let responseArray = Array<T>(JSONArray:jsonArray)
                self?.completion(responseArray)
                if let parseEnd = parseEnd { parseEnd() }
            }
        }
        else {
            fatalError("Received json object is not a valid array or there are some errors while parsing it content")
        }
    }
}

protocol MainURLEngineDelegate: class {
    func process(request: URLRequest, method: String)->URLRequest
}

class MainURLEngine {
    
    struct Error {
        var description: String?
        var code: Int?
    }
    
    let baseLink: String
    weak var delegate: MainURLEngineDelegate?
    
    init(with baseLink: String, delegate: MainURLEngineDelegate? = nil) {
        self.baseLink = baseLink
        self.delegate = delegate
    }
    
    func send<T: Mappable>(request model: [T]?, method: String, parser: JsonParser? = nil, completionForRawJson: ((Any)->Void)? = nil, completionForEmptyResponse: (()->Void)? = nil, shouldUseHttpMethodPost: Bool = false, errorBlock: RequestErrorBlockType?) {
        send(request: model?.toJSON(), method: method, parser: parser, completionForRawJson: completionForRawJson, completionForEmptyResponse: completionForEmptyResponse, shouldUseHttpMethodPost: shouldUseHttpMethodPost, errorBlock: errorBlock)
    }
    
    func send(request model: Mappable?, method: String, parser: JsonParser? = nil, completionForRawJson: ((Any)->Void)? = nil, completionForEmptyResponse: (()->Void)? = nil, shouldUseHttpMethodPost: Bool = false, errorBlock: RequestErrorBlockType?) {
        send(request: model?.toJSON(), method: method, parser: parser, completionForRawJson: completionForRawJson, completionForEmptyResponse: completionForEmptyResponse, shouldUseHttpMethodPost: shouldUseHttpMethodPost, errorBlock: errorBlock)
    }
    
    func request(with model: Any?, method: String, shouldUseHttpMethodPost: Bool = false) -> URLRequest {
        var link = "\(baseLink)/\(method)"
        if !shouldUseHttpMethodPost {
            if let paramsDict = model as? [String : Any] {
                link.append("?\(paramsDict.stringFromHttpParameters())")
            }
        }
        var request = URLRequest(url: URL(string: link)!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.timeoutInterval = 40
        print("SEND: \(link)")
        if shouldUseHttpMethodPost {
            request.httpMethod = "POST"
            if model is [String : Any] || model is [Any] {
                request.httpBody = try! JSONSerialization.data(withJSONObject: model!, options: .prettyPrinted)
                print("POST MODEL: \(String(describing: model!))")
            }
        }
        return request
    }
    
    private func send(request model: Any?, method: String, parser: JsonParser? = nil, completionForRawJson: ((Any)->Void)? = nil, completionForEmptyResponse: (()->Void)? = nil, shouldUseHttpMethodPost: Bool = false, errorBlock: RequestErrorBlockType?) {
        parser?.hold()
        var request = self.request(with: model, method: method, shouldUseHttpMethodPost: shouldUseHttpMethodPost)
        if let delegate = delegate {
            request = delegate.process(request: request, method: method)
        }
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 { //no errors
                    print("Response status - OK")
                    if let completionForEmptyResponse = completionForEmptyResponse {
                        DispatchQueue.main.async {
                            completionForEmptyResponse()
                        }
                    }
                    else {
                        if data != nil && data!.count > 0 {
                            if let json = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) {
                                print(json)
                                if let completionForRawJson = completionForRawJson {
                                    DispatchQueue.main.async {
                                        completionForRawJson(json)
                                    }
                                }
                                else if let parser = parser {
                                    parser.parse(json) { parser.dispose() }
                                }
                            }
                            else {
                                print("FATAL: не удалось распарсить ответ сервера")
                            }
                        }
                        else {
                            print("WARNING: RESPONSE DATA is EMPTY and NO HANDLER")
                        }
                    }
                }
                else {
                    print("response: \(httpResponse)")
                    if let errorBlock = errorBlock {
                        parser?.dispose()
                        DispatchQueue.main.async {
                            if data != nil && data!.count > 0 {
                                if let json = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) {
                                    self.parseError(json, httpResponse: httpResponse, errorBlock: errorBlock)
                                }
                                else {
                                    print("WARNING: не удалось распарсить данные, которые прислал сервер")
                                    errorBlock(nil, httpResponse)
                                }
                            }
                            else {
                                print("WARNING: There is ERROR but RESPONSE DATA is EMPTY")
                            }
                        }
                    }
                    else {
                        print("WARNING: There is ERROR but NO HANDLER")
                    }
                }
            }
            else {
                if response == nil {
                    print("WARNING: There is NO RESPONSE")
                }
                else {
                    print("WARNING: BAD RESPONSE")
                    print(response!.debugDescription)
                }
            }
            if let networkError = error {
                var _error = Error()
                _error.description = networkError.localizedDescription
                DispatchQueue.main.async { errorBlock?(_error, nil) }
                print("NETWORK ERROR: \(_error.description ?? "nil")")
            }
        }
        
        task.resume()
    }
    
    func parseError(_ json: Any, httpResponse: HTTPURLResponse?, errorBlock: RequestErrorBlockType?) {
        let jsonDict = json as? [String:Any]
        if let errorDescription = jsonDict?["error"] as? String {
            var _error = Error()
            _error.description = errorDescription
            errorBlock?(_error, httpResponse)
            print("error: \(_error.description ?? "nil")")
        } else if let errorDescription = jsonDict?["phone"] as? String {
            var _error = Error()
            _error.description = errorDescription
            errorBlock?(_error, httpResponse)
            print("phone: \(_error.description ?? "nil")")
        }  else {
            print("WARNING : не удалось распарсить ошибку, которую прислал сервер.")
            print(json)
            errorBlock?(nil, httpResponse)
        }
    }
}
