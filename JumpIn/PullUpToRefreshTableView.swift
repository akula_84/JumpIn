//
//  PullUpToRefreshTableView.swift
//  JumpIn
//
//  Created by Admin on 09.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class PullUpToRefreshTableView: UITableView {

    private weak var footerView: PullUpToRefreshTableFooterView!
    private var _contentInset = UIEdgeInsets.zero
    private var extraTopInset: CGFloat = 0 { didSet { super.contentInset = contentInset } }
    private var extraBottomInset: CGFloat = 0 { didSet { super.contentInset = contentInset } }
    override var contentInset: UIEdgeInsets {
        get {
            var insets = _contentInset
            insets.top += extraTopInset
            insets.bottom += extraBottomInset
            return insets
        }
        set {
            _contentInset = newValue
            super.contentInset = contentInset
        }
    }
//    override var contentOffset: CGPoint {
//        didSet {
//            print("Offset: \(contentOffset.y), Size: \(contentSize.height)")
//            if contentOffset.y + bounds.height >= contentSize.height {
//                startRefreshing() } }
//    }
    private(set) var isRefreshing = false
    
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        generalInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        generalInit()
    }
    
    private func generalInit() {
        setupFooter()
        extraBottomInset = -footerView.bounds.height
        clipsToBounds = true
    }
    
    private func setupFooter() {
        let footer = PullUpToRefreshTableFooterView()
        footerView = footer
        tableFooterView = footerView
    }
    
    override func reloadData() {
        super.reloadData()
        var dif = bounds.height - contentSize.height
        if dif < 0 { dif = 0 }
        extraTopInset = dif
    }
    
    func startRefreshing() {
        guard !isRefreshing else { return }
        isRefreshing = true
        extraBottomInset = 0
        footerView.startRefreshing()
    }
    
    func stopRefreshing() {
        extraBottomInset = -footerView.bounds.height
        footerView.stopRefreshing()
        isRefreshing = false
    }
}
