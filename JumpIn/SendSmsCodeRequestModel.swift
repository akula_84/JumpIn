//
//  SendSmsCodeRequestModel.swift
//  JumpIn
//
//  Created by Admin on 06.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class SendSmsCodeRequestModel: Mappable {

    var phone: String!
    
    init(withPhone phone:String) {
        //
        self.phone = phone
    }
    
    required init(map:Map) {}
    
    func mapping(map: Map) {
        //
        phone <- map["phone"]
    }
}
