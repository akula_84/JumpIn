//
//  DialogsSocketManager.swift
//  JumpIn
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Starscream
import ObjectMapper


class ThreadSafeType<T> {
    
    private let queue = DispatchQueue(label: String(describing: ThreadSafeType<T>.self))
    private var internalValue: T
    //
    var value: T {
        get {
            var val: T! = nil
            queue.sync { val = self.internalValue }
            return val
        }
        set { queue.sync { self.internalValue = newValue } }
    }
    
    init(value: T) {
        internalValue = value
    }
}


class SocketManager: WebSocketDelegate {
    
    static let shared = SocketManager()
    //
    private var socket: WebSocket!
    private var isSocketConnected = ThreadSafeType(value: false)
    private var isSocketAuthorized = ThreadSafeType(value: false)
    fileprivate var isSocketReady: Bool { return isSocketAuthorized.value }
    private let notificationCenter = NotificationCenter.default
    private let _self = SocketManager.self
    private var messageId = ThreadSafeType(value: 0 as Int)
    private var isSendingQueueWorking = ThreadSafeType(value: false)
    private var isAppInBackground = ThreadSafeType(value: false)
    private var isUsing: Bool { return numberOfUsers.value > 0 }
    private var numberOfUsers = ThreadSafeType(value: 0 as Int)
    private var numberOfAutoReconnects = ThreadSafeType(value: 0 as Int)
    private let maxNumberOfAutoReconnects = 3
    private var reconnectTimeout: DispatchTime { return .now() + 3 }
    private var authMessageId: Int = 0
    private var numberOfErrors = ThreadSafeType(value: 0 as Int)
    private let maxNumberOfErrors: Int = 20
    private var bgTaskId = ThreadSafeType(value: UIBackgroundTaskInvalid)
    private var sendingQueue: JsonRpcSocketMessageSendingQueue { return .shared }
    private var receiver: JsonRpcSocketMessageReceiver { return .shared }
    private let myCloseCode: UInt16 = 55555
    
    
    private init() {
        DispatchQueue.global(qos: .background).sync {
            self.socket = self.createSocket()
            self.startPingServer()
            self.notificationCenter.addObserver(self, selector: #selector(self.appDidBecomeActive),
                                           name: .UIApplicationDidBecomeActive, object: nil)
            self.notificationCenter.addObserver(self, selector: #selector(self.appDidEnterBackground),
                                                name: .UIApplicationDidEnterBackground, object: nil)
        }
    }
    
    private func recreateBgTaskId() {
        bgTaskId.value = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBgTask()
        })
    }
    
    private func endBgTask() {
        UIApplication.shared.endBackgroundTask(bgTaskId.value)
        bgTaskId.value = UIBackgroundTaskInvalid
        print("Background task ended.")
    }
    
    @objc fileprivate func sendingQueueDidChangeWorkingStatus(isWorking: Bool) {
        isSendingQueueWorking.value = isWorking
        if !isSendingQueueWorking.value && (isAppInBackground.value || !isUsing) {
            disconnectSocket()
        }
    }
    
    @objc private func appDidBecomeActive() {
        isAppInBackground.value = false
        if isUsing { connectSocketIfNeed() }
    }
    
    @objc private func appDidEnterBackground() {
        recreateBgTaskId()
        isAppInBackground.value = true
        if !isSendingQueueWorking.value { disconnectSocket() }
    }
    
    private func createSocket() -> WebSocket {
        let socket = WebSocket(url: URL(string: "ws://jump.softwarecenter.ru:8082")!)
        socket.delegate = self
        socket.callbackQueue = DispatchQueue(label: "com.jumpin.SocketManager.socketCallbackQueue", qos: .background)
        return socket
    }
    
    private func startPingServer() {
        if isUsing {
            print("Socket Ping")
            socket.write(ping: "Hello, Server".data(using: .utf8)!)
        }
        DispatchQueue.global(qos: .background).asyncAfter(deadline: DispatchTime.now() + 5) {
            self.startPingServer()
        }
    }
    
    fileprivate func send(requestModel: JsonRpcRequestResponseModelProtocol) {
        guard isSocketReady || (requestModel is JsonRpcAuthorizationRequestModel && isSocketConnected.value) else {
            fatalError("Socket is not ready now!") }
        let message = requestModel.toJSONString()!
        print("Socket Sending Message: \(message)")
        socket.write(string: message)
    }
    
    private func authorize() {
        guard !isSocketAuthorized.value else { return }
        let model = JsonRpcAuthorizationRequestModel(payloadModel: JsonRpcAuthorizationRequestTokenPayloadModel(token: UserInfoService.shared.token))
        model.id = nextMessageId()
        authMessageId = model.id
        send(requestModel: model)
    }
    
    private func reloadSocket() {
        socket.delegate = nil
        socket = self.createSocket()
        print("Socket was Reloaded")
    }
    
    private func connectSocketIfNeed() {
        if !socket.isConnected {
            if (try? socket.connect()) == nil {
                print("Error while Reconnecting Socket")
                reloadSocket()
                socket.connect()
            }
        }
    }
    
    private func disconnectSocket() {
        if socket.isConnected {
            isSocketConnected.value = false
            socket.disconnect(forceTimeout: 20, closeCode: myCloseCode)
        }
    }
    
    //Public funcs
    
    func nextMessageId() -> Int {
        let id = messageId.value
        messageId.value += 1
        return id
    }
    
    func beginUsing() {
        DispatchQueue.global(qos: .background).async {
            self.numberOfUsers.value += 1
            self.connectSocketIfNeed()
        }
    }
    
    func endUsing() {
        DispatchQueue.global(qos: .background).async {
            self.numberOfUsers.value -= 1
            if !self.isUsing, !self.isSendingQueueWorking.value { self.disconnectSocket() }
        }
    }
    
    //Socket Delegate
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("Socket Connected")
        numberOfErrors.value = 0
        numberOfAutoReconnects.value = 0
        isSocketConnected.value = true
        authorize()
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("Socket Disconnected with error: \(error?.localizedDescription ?? "null")")
        isSocketConnected.value = false
        isSocketAuthorized.value = false
        numberOfErrors.value += 1
        sendingQueue.onSocketReadyStatusChanged(isReady: false)
        
        if isAppInBackground.value, !isSendingQueueWorking.value {
            endBgTask()
        }
        
        guard isUsing else { return }
        
        if error != nil && (error! as NSError).code == myCloseCode {
            print("Normal Disconnect")
            return
        }
        
        if numberOfAutoReconnects.value >= maxNumberOfAutoReconnects {
            DispatchQueue.global(qos: .background).asyncAfter(deadline: reconnectTimeout, execute: {
                self.connectSocketIfNeed()
            })
        }
        else {
            numberOfAutoReconnects.value += 1
            connectSocketIfNeed()
        }
        
        print("Number of Errors: \(numberOfErrors.value)")
        if numberOfErrors.value >= maxNumberOfErrors {
            reloadSocket()
            numberOfErrors.value = 0
            connectSocketIfNeed()
        }
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("Socket receive message: \(text)")
        guard let dict = try? JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: []) as? [String : Any],
            let id = dict?["id"] as? Int, id == authMessageId else {
                receiver.onMessage(message: text)
                return
        }
        print("Socket Authorized")
        isSocketAuthorized.value = true
        sendingQueue.onSocketReadyStatusChanged(isReady: true)
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) { }
}


protocol JsonRpcSocketMessageReceiverObserver: class {
    func onReceive(rawResponse: String)
}

class JsonRpcSocketMessageReceiver {
    
    struct ObserverContainer {
        let observer: JsonRpcSocketMessageReceiverObserver
        let messageId: Int?
    }
    
    static let shared = JsonRpcSocketMessageReceiver()
    private var observers = ThreadSafeType(value: [ObserverContainer]())
    
    private init() { }
    
    @objc fileprivate func onMessage(message: String) {
        if let messageId = JsonRpcRequestResponseModel(JSONString: message)?.id {
            observers.value.forEach({
                if let id = $0.messageId {
                    if id == messageId { $0.observer.onReceive(rawResponse: message) }
                }
                else { $0.observer.onReceive(rawResponse: message) }
            })
        }
        else {
            observers.value.forEach({
                if $0.messageId == nil {
                    $0.observer.onReceive(rawResponse: message)
                }
            })
        }
    }
    
    func add<T: JsonRpcSocketMessageReceiverObserver>(observer: T, forMessageWithId id: Int? = nil) {
        observers.value.append(ObserverContainer(observer: observer, messageId: id))
    }
    
    func remove(observer: AnyObject) {
        observers.value = observers.value.filter({ $0.observer !== observer })
    }
}


class DialogMessageEventsObserver: JsonRpcSocketMessageReceiverObserver {
    
    var onReceiveNewMessage: ((JsonRpcNewMessageEventResultModel)->Void)?
    var onDeleteMessage: ((JsonRpcDeletedMessageEventResultModel)->Void)?
    
    init() {
        JsonRpcSocketMessageReceiver.shared.add(observer: self)
    }
    
    func onReceive(rawResponse: String) {
        guard let dict = try? JSONSerialization.jsonObject(with: rawResponse.data(using: .utf8)!, options: []) else { return }
        guard let _dict = dict as? [String : Any] else { return }
        guard let result = _dict["result"] as? [String : Any] else { return }
        guard let type = result["type"] as? String else { return }
        if type == "new_message" {
            let newMessageEventModel = JsonRpcNewMessageEventModel(JSONString: rawResponse)!
            DispatchQueue.main.async {
                self.onReceiveNewMessage?(newMessageEventModel.result)
            }
        }
        else if type == "deleted_message" {
            let deleteMessageEventModel = JsonRpcDeletedMessageEventModel(JSONString: rawResponse)!
            DispatchQueue.main.async {
                self.onDeleteMessage?(deleteMessageEventModel.result)
            }
        }
    }
    
    func dispose() {
        JsonRpcSocketMessageReceiver.shared.remove(observer: self)
    }
}


class JsonRpcSocketMessageSendingQueue: JsonRpcSocketMessageReceiverObserver {
    
    static let shared = JsonRpcSocketMessageSendingQueue()
    //
    private var queue = ThreadSafeType(value: [JsonRpcRequestResponseModelProtocol]())
    private var working = ThreadSafeType(value: false)
    private var isSocketReady: Bool { return socketManager.isSocketReady }
    private var socketManager: SocketManager { return .shared }
    private let notificationCenter = NotificationCenter.default
    private let _self = JsonRpcSocketMessageSendingQueue.self
    
    
    private init() {
        JsonRpcSocketMessageReceiver.shared.add(observer: self)
    }
    
    func add(model: JsonRpcRequestResponseModelProtocol) {
        DispatchQueue.global(qos: .background).async {
            self.queue.value.append(model)
            self.wakeUp()
        }
    }
    
    @objc fileprivate func onSocketReadyStatusChanged(isReady: Bool) {
        if isReady { sendFirst() }
    }
    
    func onReceive(rawResponse: String) {
        let response = JsonRpcRequestResponseModel(JSONString: rawResponse)!
        guard let currentModel = queue.value.first, currentModel.id == response.id else { return }
        queue.value.removeFirst()
        if let currentModel = queue.value.first { send(currentModel) }
        else {
            working.value = false
            notifyQueueWorkingStatusChanged()
        }
    }
    
    private func send(_ model: JsonRpcRequestResponseModelProtocol) {
        if isSocketReady { socketManager.send(requestModel: model) }
    }
    
    private func sendFirst() {
        guard let model = queue.value.first else { return }
        send(model)
    }
    
    private func wakeUp() {
        guard !working.value else { return }
        sendFirst()
        working.value = true
        notifyQueueWorkingStatusChanged()
    }
    
    private func notifyQueueWorkingStatusChanged() {
        socketManager.sendingQueueDidChangeWorkingStatus(isWorking: working.value)
    }
}

