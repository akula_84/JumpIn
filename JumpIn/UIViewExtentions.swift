//
//  UIViewExtentions.swift
//  JumpIn
//
//  Created by Admin on 13.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

extension UIView {
    
    func installTapGestureRecognizerToHideKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        tap.cancelsTouchesInView = false
        addGestureRecognizer(tap)
    }
    
    @objc func tapAction(sender: UITapGestureRecognizer) {
        sender.view?.endEditing(true)
    }
}
