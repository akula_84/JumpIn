//
//  GeneralSexViewModel.swift
//  JumpIn
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation

enum SexModel: Int {
    case Woman = 1, Man
}

enum SexViewModel: String {
    case Man = "men_short", Woman = "women_short", Unknown = "unknown"
    init(withModel model: SexModel?) {
        guard let model = model else { self = .Unknown; return }
        switch model {
        case .Woman: self = .Woman; break
        case .Man: self = .Man; break
        }
    }
    var localizedString: String {
        return NSLoc(rawValue)
    }
}

enum SexFilterViewModel: String {
    case Man = "men_short", Woman = "women_short", `Any` = "any"
    init(withModel model: SexModel?) {
        guard let model = model else { self = .`Any`; return }
        switch model {
        case .Woman: self = .Woman; break
        case .Man: self = .Man; break
        }
    }
    var localizedString: String {
        return NSLoc(rawValue)
    }
}

enum WhoWaitViewModel: String {
    case Man = "guys", Woman = "girls", `Any` = "all"
    init(withModel model: SexModel?) {
        guard let model = model else { self = .`Any`; return }
        switch model {
        case .Woman: self = .Woman; break
        case .Man: self = .Man; break
        }
    }
    var localizedString: String {
        return NSLoc(rawValue)
    }
}
