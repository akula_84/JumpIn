//
//  UserEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit

class UserOwnEventsFeedController: EventsFeedController {

    init(userId: Int) {
        super.init(withEventsType: .userOwn)
        self.title = NSLoc("self")
        loader.requestModel.userId = userId
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
