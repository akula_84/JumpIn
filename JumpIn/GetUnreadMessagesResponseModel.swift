//
//  GetUnreadMessagesResponseModel.swift
//  JumpIn
//
//  Created by Admin on 22.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetUnreadMessagesResponseModel: Mappable {
    
    var numberOfUnreadMessages: Int!

    required init?(map: Map) { }
    
    func mapping(map: Map) {
        numberOfUnreadMessages <- map["unread_messages_count"]
    }
}
