//
//  NotificationCenter + Extensions.swift
//  JumpIn
//
//  Created by IvanLazarev on 07/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation


extension Notification.Name {
    static let profileUpdated = Notification.Name("profileUpdated")
}
