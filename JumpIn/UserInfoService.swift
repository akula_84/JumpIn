//
//  EnterInfoService.swift
//  JumpIn
//
//  Created by Admin on 18.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class UserInfoService {
    
    static let shared = UserInfoService()
    //
    var token: String! { didSet { save() } }
    var tokenFromLastSession: String! { didSet { save() } }
    var phone: String! { didSet { save() } }
    var userId: Int! { didSet { save() } }
    var city: String! { didSet { save() } }
    var avatarLink: String? { didSet { save() } }
    var isPremium: Bool { didSet { save() } }
    
    private init () {
        let defaults = UserDefaults.standard
        token = defaults.string(forKey: "token")
        phone = defaults.string(forKey: "phone")
        userId = defaults.integer(forKey: "userId")
        city = defaults.string(forKey: "city")
        tokenFromLastSession = defaults.string(forKey: "tokenFromLastSession")
        avatarLink = defaults.string(forKey: "avatarLink")
        isPremium = defaults.bool(forKey: "isPremium")
    }
    
    static var isHaveToken: Bool {
        return shared.token != nil
    }
    
    static var city: String {
        return shared.city
    }
    
    private func save() {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "token")
        defaults.set(phone, forKey: "phone")
        defaults.set(userId, forKey: "userId")
        defaults.set(city, forKey: "city")
        defaults.set(tokenFromLastSession, forKey: "tokenFromLastSession")
        defaults.set(avatarLink, forKey: "avatarLink")
        defaults.set(isPremium, forKey: "isPremium")
        defaults.synchronize()
    }
    
    func clear(saveToken: Bool) {
        if saveToken { tokenFromLastSession = token }
        token = nil
        phone = nil
        userId = nil
        city = nil
        avatarLink = nil
    }
    
    func restoreTokenFromLastSession() -> Bool {
        if let lastToken = tokenFromLastSession {
            token = lastToken
            return true
        }
        return false
    }
}
