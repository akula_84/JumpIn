//
//  SetInviterUserRequestModel.swift
//  JumpIn
//
//  Created by Admin on 23.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class SetInviterToAppRequestModel: Mappable {
    
    var id: Int!
    var socNetProvider: GetFriendsFromSocNetRequestModel.SocNetProvider!
    
    init(inviterId: Int, socNetProvider: GetFriendsFromSocNetRequestModel.SocNetProvider) {
        self.id = inviterId
        self.socNetProvider = socNetProvider
    }
    
    required init?(map: Map) { } 
    
    func mapping(map: Map) {
        id <- map["referred_by_user_id"]
        socNetProvider <- map["referred_by_provider"]
    }
}

