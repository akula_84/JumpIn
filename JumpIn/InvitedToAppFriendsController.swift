//
//  InvitedToAppFriendsController.swift
//  JumpIn
//
//  Created by Admin on 27.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class InvitedToAppFriendsTableManager: TableViewManager<UITableView, InviteCellManager> {
    
    required init(with superView: UIView?) {
        super.init(with: superView)
        tableInitConfig.isCellHaveNib = false
    }
}
class InvitedToAppFriendsLoader: PageDataLoadingManager<GetFriendsFromSocNetRequestModel, InvitedToAppFriendsController> { }
class InvitedToAppFriendsControllerViewModel: PageViewModelProtocol {
    var viewModels: [BaseUserCellViewModel]
    required init(withModel model: GetFriendsFromSocNetResponseModel) {
        viewModels = model.models.map({ BaseUserCellViewModel(withModel: $0) })
    }
}


class InvitedToAppFriendsController: BaseConfigurableListController<InvitedToAppFriendsControllerViewModel> {

    private var tableManager: InvitedToAppFriendsTableManager!
    private var loader: InvitedToAppFriendsLoader!
    var loaded = false
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setupLoader()
        title = NSLoc("accepted")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableManager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.reloadData()
    }
    
    private func setupLoader() {
        loader = InvitedToAppFriendsLoader(withController: self, reloadDataFunc: ServerApi.shared.getFriendsFromSocNet as AnyObject)
        loader.requestModel = GetFriendsFromSocNetRequestModel(filter: .referredUsers)
    }

    private func setupTableManager() {
        tableManager = InvitedToAppFriendsTableManager(with: view)
        tableManager.didSearch = { [weak self] in
            self?.loader.requestModel.searchString = $0
            self?.loader.reloadData()
        }
        tableManager.tableInitConfig.shouldSetupRefreshControl = true
        tableManager.tableInitConfig.onRefresh = { [weak self] in
            self?.loader.reloadData()
        }
        tableManager.setup()
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            guard let `self` = self else { return }
            if self.loaded { self.loader.loadNextPage(animated: false) }
        }
    }
    
    override func refresh() {
        loaded = true
        tableManager.dataSource = viewModel.viewModels
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .bottom : .none
        tableManager.reloadData()
    }
}

