//
//  MyEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 25.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class MyEventsFeedController: TabController {
    var myOwnController: MyOwnEventsFeedController!
    var myConfirmeController: MyConfirmedEventsFeedController!
    var myRequestController: MyRequestedEventsFeedController!
    var myInvitedController: MyInvitedEventsFeedController!

    override init() {
        //
        super.init()
        self.title = NSLoc("personal_event")
        myOwnController = MyOwnEventsFeedController()
        myConfirmeController = MyConfirmedEventsFeedController()
        myRequestController = MyRequestedEventsFeedController()
        myInvitedController = MyInvitedEventsFeedController()
        self.add(subcontrollers: [myOwnController, myConfirmeController, myRequestController, myInvitedController])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
    }
    
    private func setupNavBarButtons() {
        let historyButton = UIButton(type: .custom)
        historyButton.setImage(#imageLiteral(resourceName: "ic_history"), for: .normal)
        historyButton.addTarget(self, action: #selector(historyAction), for: .touchUpInside)
        historyButton.sizeToFit()
        let historyBarButton = UIBarButtonItem(customView: historyButton)
        let filterBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_filter"), style: .plain, target: self, action: #selector(filterAction))
        navigationItem.rightBarButtonItems = [historyBarButton, filterBarButton]
    }
    
    @objc private func historyAction() {
        navigationController?.pushViewController(MyLastEventsFeedController(), animated: true)
    }
    
    @objc private func filterAction(sender: UIBarButtonItem) {
        var model: GetEventsRequestModel!
        if selectedController == myOwnController {
            model = myOwnController.loader.requestModel
        } else if selectedController == myConfirmeController {
            model = myConfirmeController.loader.requestModel
        } else if selectedController == myRequestController {
            model = myRequestController.loader.requestModel
        } else {
            model = myInvitedController.loader.requestModel
        }
        navigationController?.pushViewController(EventsFilterController(withModel: model, filterButton: sender), animated: true)
    }
}
