//
//  CategoryCell.swift
//  JumpIn
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper

class CategoryCellViewModel: BaseViewModel<GetEventsCategoriesResponseModel.CategoryModel> {
    
    var title: String
    var subtitle: String
    var numberOfEvents: Int
    var bgImage: UIImage
    
    required init(withModel model: GetEventsCategoriesResponseModel.CategoryModel) {
        if let c = model.category {
            let info = CategoryInfoService.categories[c]!
            title = info.title
            subtitle = info.description
            bgImage = info.bgImage
        }
        else {
            title = NSLoc("top_events")
            subtitle = ""
            bgImage = #imageLiteral(resourceName: "premium")
        }
        self.numberOfEvents = model.eventsCount
        super.init(withModel: model)
    }
}

class CategoryCellManager: BaseViewManager<CategoryCellViewModel, CategoryCell> {
    
    override func refresh() {
        view.bgImageView.image = viewModel.bgImage
        view.titleLabel.text = viewModel.title
        view.subtitleLabel.text = viewModel.subtitle
        view.eventsLabel.text = viewModel.numberOfEvents.eventText
    }
}

class CategoryCell: UITableViewCell, Configurable2 {
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var eventsLabel: UILabel!
    //
    var configurator: AnyObject?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
