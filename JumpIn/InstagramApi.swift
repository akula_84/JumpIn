//
//  InstagramApi.swift
//  JumpIn
//
//  Created by Admin on 25.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


struct InstagramOAuthModel: OAuthModelProtocol {
    
    let appId = "b171c0b1965f48bda23be913decf0dea"
    let baseLink = "https://api.instagram.com/oauth"
    let kAccessToken = "access_token"
    let kUserId = "user_id"
    let kError = "error_description"
    let authorizeMethodName = "authorize"
    let redirectLink = "http://jump.softwarecenter.ru"
    //
    var requestModel: InstagranAuthRequestModel {
        return InstagranAuthRequestModel(appId: appId, redirectLink: redirectLink)
    }
    var displayName: String { return "instagramnAcrossLogIn" }
}

class InstagramApi: BaseOAuthHelper<InstagramOAuthModel> {
    
    static let shared = InstagramApi()
    
    private override init() {
        super.init()
    }
}
