//
//  UIBarButtonItemExtension.swift
//  JumpIn
//
//  Created by Admin on 13.03.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit


extension UIBarButtonItem {
    
    class func createUsingUIButton(withImage image: UIImage, target: Any, selector: Selector, increasingWidthBy inc: CGFloat = 5) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.addTarget(target, action: selector, for: .touchUpInside)
        button.sizeToFit()
        button.frame.size.width += inc
        let item = UIBarButtonItem(customView: button)
        return item
    }
}
