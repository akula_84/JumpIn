//
//  PhotoUploadRequestModel.swift
//  JumpIn
//
//  Created by Admin on 28.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

enum UploadTargetType: String {
    case event, user
}

class PhotoUploadRequestModel: Mappable {
    
    var type: UploadTargetType!
    var id: Int?
    var photos: [String]!

    required init?(map: Map) {}
    
    init(withType type: UploadTargetType, id: Int?, photos: [String]!) {
        self.type = type
        self.id = id
        self.photos = photos
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        id <- map["id"]
        photos <- map["photos"]
    }
}
