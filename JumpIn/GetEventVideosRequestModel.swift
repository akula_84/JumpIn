//
//  GetEventVideosRequestModel.swift
//  JumpIn
//
//  Created by Admin on 29.01.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit

class GetEventVideosRequestModel: RequestModelWithPageProtocol {
    var page: Int = 0
    var eventId: Int
    var numberOfVideosOnSinglePage: Int
    
    init(eventId: Int, numberOfVideosOnSinglePage: Int) {
        self.eventId = eventId
        self.numberOfVideosOnSinglePage = numberOfVideosOnSinglePage
    }
}
