//
//  MyInvitedEventsFeedController.swift
//  JumpIn
//
//  Created by IvanLazarev on 15/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SwipeCellKit


class MyInvitedEventsFeedController: EventsFeedController {

    init() {
        super.init(withEventsType: .myInvitation)
        self.title = NSLoc("Invitations")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

