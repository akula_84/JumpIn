//
//  BaseCustomView.swift
//  JumpIn
//
//  Created by Admin on 11.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class BaseCustomView: UIView {
    
    var internalView: UIView?
    var xibName: String { return String(describing: type(of: self)) }
    
    
    func setup() {
        guard xibName.count > 0 else {
            return
        }
        internalView = (Bundle.main.loadNibNamed(xibName, owner: self, options: nil)!.first as! UIView)
        internalView!.frame = bounds
        internalView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        backgroundColor = internalView!.backgroundColor
        internalView!.backgroundColor = UIColor.clear
        addSubview(internalView!)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}
