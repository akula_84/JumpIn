//
//  DialogInputTextView.swift
//  JumpIn
//
//  Created by Admin on 04.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class DialogInputTextViewController: UIViewController, UITextViewDelegate {

    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var plusButton: UIButton!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var placeholderLabel: UILabel!
    //
    var onSendMessage: ((_ message: String)->())?
    var onTapPlus: (()->())?
    var text: String? {
        get { return textView.text }
        set { textView?.text = newValue; sendButton?.isEnabled = text != nil }
    }
    var isSendButtonEnabled = true {
        didSet { if isViewLoaded { sendButton.isEnabled = isSendButtonEnabled } }
    }
    
    init() {
        super.init(nibName: "DialogInputTextViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.contentInset.top += 4
        textView.contentInset.left += 5
        sendButton.setBackgroundImage(UIImage(color: #colorLiteral(red: 0.4297555387, green: 0.7359806895, blue: 0.9094614387, alpha: 1)), for: .normal)
        sendButton.setBackgroundImage(UIImage(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)), for: .disabled)
        sendButton.isEnabled = isSendButtonEnabled
    }
    
    @IBAction private func plusButtonAction() {
        onTapPlus?()
    }
    
    @IBAction private func sendButtonAction() {
        textView.text = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if !textView.text.isEmpty {
            onSendMessage?(textView.text)
            textView.text = ""
        }
        textViewDidChange(textView)
    }

    //MARK: - UITextViewDelegate 

    public func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
