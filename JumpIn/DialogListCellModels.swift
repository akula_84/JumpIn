//
//  DialogListCellModels.swift
//  JumpIn
//
//  Created by Admin on 11.10.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper


class DialogListItemModel: ModelProtocol {
    var userId: Int!
    var userName: String!
    var userAvatarLink: String?
    var lastMessage: String?
    var lastMessageTimestamp: String?
    var numberOfNewMessages: Int!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        userId <- map["user_id"]
        userName <- map["name"]
        userAvatarLink <- map["avatar"]
        lastMessage <- map["text"]
        lastMessageTimestamp <- map["created_at"]
        numberOfNewMessages <- map["unread_messages_count"]
    }
}

class DialogListItemModelPage: PageModel<DialogListItemModel> {}

class DialogListCellViewModel: BaseViewModel<DialogListItemModel> {
    
    struct User {
        let name: String
        let avatarLink: String?
    }
    struct Message {
        let text: String
        let timestamp: String
        init(text: String?, timestamp: String?) {
            self.text = text ?? ""
            self.timestamp = timestamp ?? ""
        }
    }
    
    let user: User
    let lastMessage: Message
    let numberOfNewMessages: String?
    
    required init(withModel model: DialogListItemModel) {
        user = User(name: model.userName, avatarLink: model.userAvatarLink)
        var formattedTimestamp: String? = nil
        if let timestamp = model.lastMessageTimestamp {
            formattedTimestamp = DateConverter.ForInterface.Dialogs.messageTimestamp(from: timestamp)
        }
        lastMessage = Message(text: model.lastMessage, timestamp: formattedTimestamp)
        numberOfNewMessages = model.numberOfNewMessages > 0 ? "\(model.numberOfNewMessages!)" : nil
        super.init(withModel: model)
    }
}

class DialogListCellViewModelPage: PageViewModel<DialogListItemModelPage, DialogListCellViewModel> {}
