//
//  UnderLineTextField.swift
//  JumpIn
//
//  Created by Admin on 13.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


@objc protocol UnderLineTextFieldDelegate: UITextFieldDelegate {
    @objc optional func textField(_ textField: UnderLineTextField, didChangeText newText: String?)
}


class UnderLineTextField: BaseCustomView {

    @IBOutlet private weak var textFieldImageSpacing: NSLayoutConstraint!
    //
    @IBOutlet private weak var leftImageView: UIImageView!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var underLine: UIView!
    
    var text: String? {
        get { return textField.text }
        set { textField.text = text }
    }
    @IBInspectable var leftImage: UIImage? { didSet { refresh() } }
    @IBInspectable var textColor: UIColor = UIColor(red:110.0/255.0, green:116.0/255.0, blue:127.0/255.0, alpha:1.0) { didSet { refresh() } }
    @IBInspectable var placeholder: String? { didSet { refresh() } }
    @IBInspectable var noLeftImageModeEnabled: Bool = false { didSet { refresh() } }
    @IBInspectable var fontSize: CGFloat = 15.0 { didSet { refresh() } }
    var correctionEnabled: Bool? { didSet { refresh() } }
    var keyboardType: UIKeyboardType? { didSet { refresh() } }
    weak var delegate: UnderLineTextFieldDelegate? { didSet { textField.delegate = delegate } }
    
    
    func refresh() {
        let font = UIFont.systemFont(ofSize: fontSize)
        if let image = leftImage {
            leftImageView.image = image.withRenderingMode(.alwaysTemplate)
        }
        if let keyboardType = self.keyboardType {
            textField.keyboardType = keyboardType
        }
        if let placeholder = self.placeholder {
            let attributes: [NSAttributedStringKey : Any] = [.foregroundColor : textColor, .font : font]
            textField.attributedPlaceholder = NSAttributedString(string: NSLoc(placeholder), attributes: attributes)
        }
        if let correctionEnabled = self.correctionEnabled {
            textField.autocorrectionType = correctionEnabled ? .yes : .no
        }
        leftImageView.tintColor = textColor
        textField.textColor = textColor
        underLine.backgroundColor = textColor
        if let textFieldImageSpacing = self.textFieldImageSpacing {
            textFieldImageSpacing.isActive = !noLeftImageModeEnabled
        }
        textField.font = font
    }
    
    override func setup() {
        super.setup()
        textField.addTarget(self, action: #selector(textFieldTextDidChange), for: .editingChanged)
        refresh()
    }
    
    @objc private func textFieldTextDidChange() {
        delegate?.textField?(self, didChangeText: text)
    }
    
    override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        return textField.resignFirstResponder()
    }
}
