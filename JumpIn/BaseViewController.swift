//
//  BaseViewController.swift
//  JumpIn
//
//  Created by Admin on 11.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


class BaseViewController: UIViewController {
    
    
    class InitConfiguration {
        var backButtonImage: UIImage = #imageLiteral(resourceName: "back_arrow")
        var shouldAddBackButton = true
    }
    
    
    private let leftArrowTag: Int = 100
    final var initConfig = InitConfiguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.hidesBackButton = true
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.count > 1 {
                addBackButton()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navController = navigationController, parent === navController {
            navController.applyDefaultSettings()
        }
    }
    
    func addBackButton() {
        if initConfig.shouldAddBackButton {
            let leftArrowBarButton = UIBarButtonItem(image: initConfig.backButtonImage, style: .plain, target: self, action: #selector(backAction))
            leftArrowBarButton.tag = leftArrowTag
            if var items = navigationItem.leftBarButtonItems {
                items.insert(leftArrowBarButton, at: 0)
                navigationItem.leftBarButtonItems = items
            }
            else {
                navigationItem.leftBarButtonItem = leftArrowBarButton
            }
        }
    }
    
    
    @objc func backAction() {
        _ = navigationController?.popViewController(animated: true)
    }

    func showError(_ error: MainURLEngine.Error?, action: (()->Void)? = nil) {
        showError(withDescription: error?.description, action: action)
    }
    
    func showError(withDescription text: String?, action: (()->Void)? = nil) {
        HUD.flash(.error, onView: view, delay: 0.3, completion: { [weak self] _ in
            guard let text = text else { return }
            let alert = UIAlertController(title: NSLoc("error"), message: text, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel) { _ in action?() }
            alert.addAction(ok)
            self?.present(alert, animated: true, completion: nil)
        })
    }
}
