//
//  SocialNetViewController.swift
//  JumpIn
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper


class SocialNetViewModel: BaseViewModel<SocialNetModel> {
    
    enum `Type`: String {
        case vk = "vkontakte", fb = "facebook", instagram
        var icon: UIImage {
            switch self {
                case .fb: return #imageLiteral(resourceName: "ic_fb")
                case .vk: return #imageLiteral(resourceName: "ic_vk")
                case .instagram: return #imageLiteral(resourceName: "ic_instagram")
            }
        }
    }
    
    var icon: UIImage
    var url: String?
    var userName: String
    var type: Type
    
    required init(withModel model: SocialNetModel) {
        type = Type(rawValue: model.socialNetName)!
        icon = type.icon
        userName = model.userNameDisplayingInSocialNet
        url = model.userLinkInSocialNet
        super.init(withModel: model)
    }
}


class SocialNetViewController: UIViewController {

    var viewModel: SocialNetViewModel { didSet { refresh() } }
    //
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var userLinkLabel: UILabel!
    
    init(withViewModel viewModel: SocialNetViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing:type(of: self).self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapAction)))
        refresh()
    }
    
    private func refresh() {
        if isViewLoaded {
            iconImageView.image = viewModel.icon
            userLinkLabel.text = viewModel.url
            userNameLabel.text = viewModel.userName
        }
    }
    
    @objc private func tapAction() {
        let app = UIApplication.shared
        guard let link = viewModel.url, let url = URL(string: link), app.canOpenURL(url) else { return }
        if #available(iOS 10, *) { app.open(url) }
        else { app.openURL(url) }
    }
}
