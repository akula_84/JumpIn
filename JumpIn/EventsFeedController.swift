//
//  EventsFeedTableSubcontroller.swift
//  JumpIn
//
//  Created by Admin on 27.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SwipeCellKit



class EventsFeedController: BaseConfigurableListController<EventPageViewModel>, SwipeTableViewManagerDelegate {
    
    enum EventsType: Int {
        case generalCurrent, generalLast, myOwnCurrent, myOwnLast, myRequested, myConfirmedCurrent, myConfirmedLast, userOwn, userLast, userParticipated, myInvitation
    }
    
    var loader: PageDataLoadingManager<GetEventsRequestModel, EventsFeedController>!
    var tableManager: SwipeTableViewManager<UITableView, EventCellManager<EventTableCell>>!
    let eventsType: EventsType
    let category: CategoryInfoService.CategoryInfo.Name?
    let searchingEnabled: Bool
    var onRefreshed: (()->Void)?
    var loaded = false
    var showCityTitle = false

    
    init(withEventsType eventsType: EventsType, category: CategoryInfoService.CategoryInfo.Name? = nil, enableSearching: Bool = true) {
        self.eventsType = eventsType
        self.category = category
        self.searchingEnabled = enableSearching
        super.init(nibName: nil, bundle: nil)
        loader = PageDataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getEvents as AnyObject)
        loader.requestModel = GetEventsRequestModel(eventsType: eventsType)
        loader.requestModel?.category = category
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installTableManager() {
        tableManager = SwipeTableViewManager(with: self.view)
        tableManager.swipeDelegate = self
        let config = tableManager.tableInitConfig
        tableManager.tableInitConfig.isCellHaveNib = false
        tableManager.tableInitConfig.tableViewInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        tableManager.tableInitConfig.cellHeight = 125
        tableManager.tableInitConfig.separatorHeight = 9
        tableManager.tableInitConfig.shouldSetupSearchBar = searchingEnabled
        tableManager.tableInitConfig.shouldSetupRefreshControl = true
        config.showCityTitle = showCityTitle
        
        tableManager.tableInitConfig.onRefresh = { [weak self] in self?.loader.reloadData(animated: false, completion: nil) }
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            guard let `self` = self else { return }
            if self.loaded { self.loader.loadNextPage(animated: false) }
        }
        tableManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let i = self.tableManager.indexOfDataSourceItem(forCellAt: indexPath)
            let event = self.loader.model.models![i]
            self.navigationController?.pushViewController(EventDetailsController(eventId: event.id), animated: true)
        }
        tableManager.didSearch = { [weak self] q in
            guard let `self` = self else { return }
            self.loader.requestModel?.searchString = q
            self.loader.reloadData(animated:  false)
        }
    }
    
    func updateTableManager() {
        tableManager.dataSource = viewModel.viewModels
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        installTableManager()
        tableManager.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.reloadData()
    }
    
    override func refresh() {
        loaded = true
        updateTableManager()
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .bottom : .none
        tableManager.reloadData()
        onRefreshed?()
    }
    
    func tableViewManager(_ manager: AnyObject, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.maximumButtonWidth = 100
        options.minimumButtonWidth = 100
        return options
    }
    
    func tableViewManager(_ manager: AnyObject, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return nil
    }
}


extension GetEventsRequestModel {
    
    convenience init(eventsType type: EventsFeedController.EventsType) {
        self.init()
        switch type {
        case .generalCurrent:
            self.type = .all
            self.timeFrame = .current
            break
        case .generalLast:
            self.type = .all
            self.timeFrame = .past
            break
        case .myConfirmedCurrent:
            self.type = .my
            self.selfScope = .confirmed
            self.timeFrame = .current
            break
        case .myConfirmedLast:
            self.type = .my
            self.selfScope = .confirmed
            timeFrame = .past
            break
        case .myOwnCurrent:
            self.type = .my
            self.selfScope = .own
            self.timeFrame = .current
            break
        case .myOwnLast:
            self.type = .my
            self.selfScope = .own
            self.timeFrame = .past
            break
        case .myRequested:
            self.type = .my
            self.selfScope = .requested
            break
        case .userOwn:
            self.type = .user
            self.userScope = .his
            self.timeFrame = .current
            break
        case .userLast:
            self.type = .user
            self.userScope = .past
            self.timeFrame = .past
            break
        case .userParticipated:
            self.type = .user
            self.userScope = .participated
            break
        case .myInvitation:
            self.type = .my
            self.selfScope = .invited
            break
        }
    }
}
