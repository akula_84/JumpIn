//
//  EnterController.swift
//  JumpIn
//
//  Created by Admin on 11.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD
import FBSDKCoreKit
import FBSDKLoginKit
import SideMenu


class EnterController: BaseViewController {

    @IBOutlet weak var fbButton: SocialNetFbButton!
    @IBOutlet weak var vkButton: SocialNetVkButton!
    @IBOutlet weak var instagramButton: SocialNetInstagramButton!
    //
    let toEnterPhoneSegueId = "fromEnterToEnterPhone"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fbButton.onClick = { [weak self] _ in
            self?.fbAuthorize()
        }
        vkButton.onClick = { [weak self] _ in
            self?.vkAuthorize()
        }
        instagramButton.onClick = { [weak self] _ in
            self?.instagramAuthorize()
        }
       // self.checkStatus()
    }
    /*
    private func checkStatus() {
        guard let _ = UserInfoService.shared.token else { return }
        HUD.show(.progress)
        ServerApi.shared.getStatus(completion: { [weak self] status in
            ServerApi.shared.getProfile(completion: { _ in
                HUD.hide()
                self?.gotoNext(withAuthorizationStatus: status)
            }, errorBlock: { [weak self] error, _ in
                self?.showError(error)
            })
        }, errorBlock: { [weak self] (error, _) in
            if error != nil && error!.description != nil && error!.description! == "Профиль не заполнен." {
                self?.gotoNext(withAuthorizationStatus: .NeedInfo)
            }
            else {
                self?.showError(error)
            }
        })
    }
    */
    private func fbAuthorize() {
        let manager = FBSDKLoginManager()
        manager.logIn(withReadPermissions: ["user_birthday", "email", "user_location"], from: self, handler: { [weak self] (result, error) in
            if result != nil && result!.token != nil {
                self?.authorize(withSocialNetToken: result!.token.tokenString, ofType: .Fb)
            }
            else if error != nil {
                self?.showError(nil)
                print(error!)
            }
        })
    }
    
    private func instagramAuthorize() {
        let vc = InstagramOAuthController.createIntoNavController(withCompletion: { [weak self] token in
            self?.authorize(withSocialNetToken: token, ofType: .Instagram)
        })
        self.present(vc, animated: true, completion: nil)
    }
    
    private func vkAuthorize() {
        let vc = VKOAuthController.createIntoNavController { [weak self] token in
            self?.authorize(withSocialNetToken: token, ofType: .Vk)
        }
        present(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.setNavigationBarHidden(true, animated: true)
    }
    /*
    fileprivate func gotoNext(withAuthorizationStatus status: GetStatusResponseModel.UserAuthorizationStatus) {
        let navController = navigationController!
        switch status {
        case .NeedPhone:
            navController.pushViewController(EnterPhoneController(), animated: true)
            break
        case .NeedInfo:
            navController.pushViewController(ProfileEditingController(isEnterMissingData:true), animated: true)
            break
        case .Authorized:
            SideMenuManager.setup()
            navController.pushViewController(TopEventsFeedController(withFilter: true), animated: true)
            break
        }
    }
    */
    fileprivate func authorize(withSocialNetToken token: String, ofType type: SocialNetType) {
        HUD.show(.progress)
        ServerApi.shared.authorize(withSocialNetToken: token, type: type, completion: { jwt in
            HUD.hide()
            JUMAuthManager.checkStatus()
        }) { [weak self] (error, response) in
            self?.showError(error)
        }
    }
}
