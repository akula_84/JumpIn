//
//  EnterMissingDataController.swift
//  JumpIn
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD
import GooglePlaces
import SideMenu


class ProfileEditingController: BaseViewController, SocNetBinderDelegate {
    
    var viewModel: ViewModel!
    var isEnterMissingData: Bool
    //
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var nikTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var sexTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityRightArrow: UIImageView!
    @IBOutlet weak var cityButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var showPhoneSwitch: UISwitch!
    //@IBOutlet weak var allowAddVideoContentInMyEventsSwitch: UISwitch!
    @IBOutlet weak var dontShowAgeSwitch: UISwitch!
    @IBOutlet weak var showMySocNetProfileSwitch: UISwitch!
    @IBOutlet private weak var socialNetNameLabel: UILabel!
    @IBOutlet private weak var socialNetLabel: UILabel!
    @IBOutlet private weak var showMySocNetProfileLabel: UILabel!
    @IBOutlet private weak var socialNetIcon:UIImageView!
    @IBOutlet private weak var continueButton: UIButton!
    @IBOutlet private weak var bindVkButton: UIButton!
    @IBOutlet private weak var bindFbButton: UIButton!
    @IBOutlet fileprivate weak var bindInstagramButton: UIButton!
    @IBOutlet weak var showPhoneLabel: UILabel!
    @IBOutlet weak var hideAgeLabel: UILabel!
    @IBOutlet var rightTextFields: [KMTextField]!
    @IBOutlet var socialNetsStackView: UIStackView!
    //
    var sex: SexModel! { didSet { sexTextField.text = SexViewModel(withModel: sex).localizedString } }
    private var model: GetProfileResponseModel!
    private var birthDate: Date? { didSet {
        dateTextField.text = DateConverter.ForInterface.Profile.birthday(from: birthDate!)
        let picker = dateTextField.inputView as! UIDatePicker
        picker.date = birthDate!
        }
    }
    private let avatarWithPhotosController = AvatarWithPhotosCollectionController(withType: .users, ownerId: UserInfoService.shared.userId)
    var binder: SocNetBinder!
    var isChanged = false
    private weak var buyView: BuyPremiumAccauntView?
    
    
    init(isEnterMissingData: Bool = false) {
        self.isEnterMissingData = isEnterMissingData
        super.init(nibName: "ProfileEditingController", bundle: nil)
        self.addChildViewController(avatarWithPhotosController)
        avatarWithPhotosController.didMove(toParentViewController: self)
        self.binder = SocNetBinder(delegate: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func binder(_ binder: SocNetBinder, didBindWithResult result: SocNetBinder.Result) {
        if result.success {
            HUD.show(.progress, onView: view)
            reloadData(showHUD: false, completion: { [weak self] success in
                if success {
                    HUD.flash(.success, delay: 0.5)
                }
                else {
                    self?.showError(nil)
                }
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setRightTextFieldsAttributes()
        cityButton.onClick = { [weak self] _ in self?.pushChooseCityController() }
        configureDateTextField()
        //if viewModel == nil { viewModel = ViewModel() }
        stackView.insertArrangedSubview(avatarWithPhotosController.view, at: 0)
        if !isEnterMissingData {
            title = NSLoc("edit")
            setupNavBarButtons()
            continueButton.superview?.isHidden = true
        }
        else {
            avatarWithPhotosController.goToAllPhotosButton.isHidden = true
        }
        reloadData()
        //if !UserInfoService.shared.isPremium { addBuyView() }
    }
    
    private func setRightTextFieldsAttributes() {
        rightTextFields.forEach({
            let attr: [NSAttributedStringKey : Any] = [.foregroundColor : $0.textColor!, .font : $0.font!]
            let attrPlaceholder = NSAttributedString(string: $0.placeholder ?? "", attributes: attr)
            $0.attributedPlaceholder = attrPlaceholder
        })
    }
    
    private func reloadSocialNetViews(withSocialNets socialNets: [SocialNetViewModel]) {
        clearSocialNetViews()
        socialNets.forEach {
            let subcontroller = SocialNetViewController(withViewModel: $0)
            self.addChildViewController(subcontroller)
            self.socialNetsStackView.addArrangedSubview(subcontroller.view)
            subcontroller.didMove(toParentViewController: self)
        }
        socialNetsStackView.layoutIfNeeded()
    }
    
    private func clearSocialNetViews() {
        childViewControllers.forEach {
            if let socialNetController = $0 as? SocialNetViewController {
                self.socialNetsStackView.removeArrangedSubview(socialNetController.view)
                socialNetController.view.removeFromSuperview()
                socialNetController.removeFromParentViewController()
            }
        }
    }
    
    private func configureDateTextField() {
        let datePicker = UIDatePicker()
        datePicker.backgroundColor = .white
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date(timeInterval: -(24*60*60), since: Date())
        datePicker.addTarget(self, action: #selector(datePickerDidChangedAction(sender:)), for: .valueChanged)
        dateTextField.inputView = datePicker
        if let date = birthDate { datePicker.date = date }
        dateTextField.addDoneButton()
    }

    @objc private func datePickerDidChangedAction(sender: UIDatePicker) {
        birthDate = sender.date
    }
    
    private func setupNavBarButtons() {
        let acceptChangesBarButton = UIBarButtonItem(image: UIImage(named:"ic_check_gray"), style: .plain, target: self, action: #selector(sendChanges))
        navigationItem.rightBarButtonItem = acceptChangesBarButton
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isEnterMissingData {
            navigationController?.setNavigationBarHidden(true, animated: animated)
        }
        //refresh()
    }
    
    
    func reloadData(showHUD: Bool = true, completion:((_ success: Bool)->Void)? = nil) {
        if showHUD { HUD.show(.progress) }
        ServerApi.shared.getProfile(completion: { [weak self] model in
            if showHUD { HUD.hide() }
            guard let `self` = self else { return }
            self.model = model
            self.viewModel = ViewModel(withResponseModel: model)
            self.refresh()
            completion?(true)
        }) { [weak self] (error, _) in
            self?.showError(error) { self?.backAction() }
            completion?(false)
        }
    }
    
    
    @objc func sendChanges() {
        guard let city = cityTextField.text, !city.isEmpty else {
            presentAlertMissedCity()
            return
        }
        if !model.premium, let city =  UserInfoService.shared.city, !city.isEmpty,
            let first = city.components(separatedBy: ", ").first, city != first {
            AlertsPresentator.presentAlertChangeCityCanOnlyPremiumUser(in: self)
            return
        }
        HUD.show(.progress)
        ServerApi.shared.updateProfile(with: generateUpdateProfileRequestModel(), completion: { [weak self] in
            guard let `self` = self else { return }
            self.avatarWithPhotosController.applyChanges(withCompletion: {
                HUD.hide()
                if self.isEnterMissingData {
                    SideMenuManager.setup()
                    self.navigationController!.pushViewController(TopEventsFeedController(withFilter: true), animated: true)
                }
                else {
                    self.navigationController!.popViewController(animated: true)
                }
            })
        }) { [weak self] (error, _) in
            self?.showError(error)
        }
    }
    
    
    func refresh() {
        nikTextField.text = viewModel.nik
        firstNameTextField.text = viewModel.firstName
        lastNameTextField.text = viewModel.lastName
        let firstSocNet = viewModel.socialNets.first!
        socialNetNameLabel.text = firstSocNet.userName
        socialNetLabel.text = firstSocNet.url
        socialNetIcon.image = firstSocNet.icon
        reloadSocialNetViews(withSocialNets: viewModel.socialNets.filter({ $0 !== firstSocNet }))
        if let date = viewModel.birthDate { birthDate = date }
        sex = model.sex
        cityTextField.text = viewModel.city
        cityRightArrow.isHidden = viewModel.city != nil
        phoneTextField.text = viewModel.phone
        configureSwitchsAndTheirLabels()
        avatarWithPhotosController.set(photos: model.photos.compactMap({ PhotoViewModel(withPhoto: $0) }))
        avatarWithPhotosController.set(avatar: PhotoViewModel(withPhoto: model.avatar))
        configureSocialNetButtons()
        if !viewModel.premium { addBuyView() } else { removeBuyView() }
    }
    
    private func addBuyView() {
        if buyView == nil {
            let buyView = BuyPremiumAccauntView()
            buyView.onBuy = { [weak self] in self?.showAlertAboutOpenningAppStoreIfNeed(cancelBlock: nil) }
            stackView.addArrangedSubview(buyView)
            self.buyView = buyView
        }
    }
    
    private func removeBuyView() {
        if let buyView = self.buyView {
            stackView.removeArrangedSubview(buyView)
            buyView.removeFromSuperview()
        }
    }
    
    private func configureSocialNetButtons() {
        bindVkButton.isHidden = false
        bindFbButton.isHidden = false
        bindInstagramButton.isHidden = false
        let socialNets = viewModel.socialNets!
        if socialNets.contains(where: { $0.type == .vk }) {
            bindVkButton.isHidden = true
        }
        if socialNets.contains(where: { $0.type == .fb }) {
            bindFbButton.isHidden = true
        }
        if socialNets.contains(where: { $0.type == .instagram }) {
            bindInstagramButton.isHidden = true
        }
    }
    
    
    private func pushChooseCityController() {
        let vc = SearchPlacesController(citiesOnly: true)
        vc.didChangeAddress = { [weak self] in
            self?.cityTextField.text = $0
            self?.isChanged = true
        }
        vc.presentIntoNavController(by: self)
    }
    
    func generateUpdateProfileRequestModel() -> UpdateProfileRequestModel {
        let model = UpdateProfileRequestModel()
        model.nickname = nikTextField.text
        model.first_name = firstNameTextField.text
        model.last_name = lastNameTextField.text
        model.sex = sex
        model.birthday = birthDate != nil ? DateConverter.ForServer.Profile.birthday(from: birthDate!) : nil
        if self.model.premium {
            model.city = cityTextField.text
            model.show_phone = showPhoneSwitch.isOn
            model.hide_age = dontShowAgeSwitch.isOn
        }
        else if isEnterMissingData {
            model.city = cityTextField.text
        }
        return model
    }
    
    private func configureSwitchsAndTheirLabels() {
        showPhoneLabel.textColor = viewModel.showPhone ? #colorLiteral(red: 0.6642268896, green: 0.6642268896, blue: 0.6642268896, alpha: 1) : #colorLiteral(red: 0.6642268896, green: 0.6642268896, blue: 0.6642268896, alpha: 0.39)
        hideAgeLabel.textColor = viewModel.dontShowAge ? #colorLiteral(red: 0.6642268896, green: 0.6642268896, blue: 0.6642268896, alpha: 1) : #colorLiteral(red: 0.6642268896, green: 0.6642268896, blue: 0.6642268896, alpha: 0.39)
        showMySocNetProfileLabel.textColor = viewModel.showMyProfile ? #colorLiteral(red: 0.6642268896, green: 0.6642268896, blue: 0.6642268896, alpha: 1) : #colorLiteral(red: 0.6642268896, green: 0.6642268896, blue: 0.6642268896, alpha: 0.39)
        dontShowAgeSwitch.isOn = viewModel.dontShowAge
        showPhoneSwitch.isOn = viewModel.showPhone
        showMySocNetProfileSwitch.isOn = viewModel.showMyProfile
    }
    
    override func backAction() {
        if isEnterMissingData {
            super.backAction()
        }
        else if isChanged || avatarWithPhotosController.isChanged {
            AlertsPresentator.presentAlertExitWithoutSaving(in: self, withCompletion: { exit in
                if exit { super.backAction() }
            })
        }
        else {
            super.backAction()
        }
    }
    
    
    class ViewModel {
        
        var photoLinks: [String] = []
        var avatarLink: String?
        var nik: String!
        var firstName: String!
        var lastName: String!
        var birthDate: Date!
        var sex: SexViewModel!
        var city: String!
        var phone: String?
        var showPhone = false
        //var allowAddVideoContentInMyEvents = false
        var dontShowAge = false
        var showMyProfile = true
        let socialNets: [SocialNetViewModel]!
        var isFilled = false
        var premium = false
        
        
        //init() {}
        
        init(withResponseModel model: GetProfileResponseModel) {
            nik = model.nickname
            firstName = model.first_name
            lastName = model.last_name
            if let bdate = model.birthday {
                birthDate = DateConverter.ForServer.Profile.birthdate(from: bdate)
            }
            sex = SexViewModel(withModel:model.sex)
            city = model.city
            phone = model.phone
            //allowAddVideoContentInMyEvents = model.videos_events ?? false
            showPhone = model.show_phone ?? false
            dontShowAge = model.hide_age ?? false
            isFilled = model.filled
            socialNets = model.socialNets.map({ SocialNetViewModel(withModel: $0) })
            premium = model.premium
        }
    }

}
