//
//  InviteCell.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

//class InviteCellViewModel: BaseUserCellViewModel { }

class InviteCellManager: BaseUserCellManager<BaseUserCellViewModel, InviteCell> {}

class InviteCell: BaseUserCell {
    
    lazy var animateView: UIView! = {
        let view = UIView(frame: self.bounds)
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        view.isUserInteractionEnabled = false
        view.alpha = 0
        self.addSubview(view)
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? .checkmark : .none
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if highlighted { animateView.alpha = 0.5 }
        else { UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.animateView.alpha = 0 }) }
    }
    
    private func setupAnimateView() {
        
    }
}
