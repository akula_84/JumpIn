//
//  EventDetailsController+Store.swift
//  JumpIn
//
//  Created by Артем Кулагин on 20.07.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import Foundation
import StoreKit
import PKHUD
import UIKit

extension EventDetailsController {
    
    func bayTopEvent() {
        let productIDs = ["com.swc.jumpin.event.top"]
        let productIdentifiers = NSSet(array: productIDs)
        let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
        
        productRequest.delegate = self
        productRequest.start()
        HUD.show(.progress)
        /*
        let alert = UIAlertController(title: "Добавить евент в топ?", message: nil, preferredStyle: .alert)
        let buyAction = UIAlertAction(title: "Купить", style: UIAlertActionStyle.default) { (action) -> Void in
            let productIDs = ["com.swc.jumpin.event.top"]
            let productIdentifiers = NSSet(array: productIDs)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
            
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(buyAction)
        alert.addAction(cancelAction)
        STRouter.present(alert)
        */
    }
 
    func showActions(_ product: SKProduct) {
        /*
         if transactionInProgress {
         return
         }
         */
        HUD.hide()
        let actionSheetController = UIAlertController(title: product.localizedTitle, message:"\(product.localizedPrice ?? "")", preferredStyle: UIAlertControllerStyle.actionSheet)
        let buyAction = UIAlertAction(title: NSLoc("buy"), style: UIAlertActionStyle.default) { (action) -> Void in
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
            HUD.show(.progress)
            // self.transactionInProgress = true
            
        }
        let cancelAction = UIAlertAction(title: NSLoc("cancel"), style: UIAlertActionStyle.cancel)
        actionSheetController.addAction(buyAction)
        actionSheetController.addAction(cancelAction)
        STRouter.present(actionSheetController)
    }
    
    func confirmTopAction() {
        HUD.show(.progress)
        ServerApi.shared.makeEventPremium(withId: eventId, completion: { [weak self] in
            self?.reloadData(showSuccessAfterLoading: true)
        }) { [weak self] (error, _) in
            self?.showError(error)
        }
    }
}
