//
//  JUMPushManager.swift
//  JumpIn
//
//  Created by Артем Кулагин on 23.04.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import Foundation
import  UIKit

class JUMPushManager {
    
    static var eventId: Int?
    static var userId: Int?
    static var userAvatarLink: String?
    
    static var uiLoaded: Bool = false
    
    static func check(_ userInfo: [AnyHashable : Any]) {
        print("JUMPushManager check", userInfo)
        guard let data = userInfo["data"] as? [String : Any],
            let type = data["type"] as? String else {
                return
        }
        if type.contains("event") {
            openEvent(data)
        } else if type.contains("chat") {
            openDialog(data)
        } else {
            openDialog(data)
        }
        /*
        switch type {
        case "event_invitation",
             "event_join_request",
             "event_join_request_accept":
            openEvent(data)
        case "chat_message_received":
            openDialog(data)
        default:
            openDialog(data)
            return
        }
        */
    }

    static func showPushIfNeed() {
        uiLoaded = true
        showEventDetailsIfNeed()
        showDialogIfNeed()
    }
    
    static func openEvent(_ data: [String : Any]) {
        guard let eventId = data["event_id"] as? Int else {
            return
        }
        self.eventId = eventId
        if uiLoaded {
            showEventDetailsIfNeed()
        }
    }
    
    static func showEventDetailsIfNeed() {
        if let eventId = eventId {
            EventDetailsController(eventId: eventId).pushMain()
            self.eventId = nil
        }
    }
    
    static func openDialog(_ data: [String : Any]) {
        guard let userId = data["sender_id"] as? Int else {
            return
        }
        self.userId = userId
        self.userAvatarLink = data["sender_avatar"] as? String
        if uiLoaded {
            showDialogIfNeed()
        }
    }
    
    static func showDialogIfNeed() {
        if let userId = userId {
            DialogDetailedController(userId: userId, userAvatarLink: userAvatarLink).pushMain()
            self.userId = nil
            self.userAvatarLink = nil
        }
    }
}


