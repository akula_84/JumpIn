//
//  SearchAddressController.swift
//  JumpIn
//
//  Created by Admin on 21.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker


class SearchPlacesController: BaseViewController {
    
    fileprivate var resultsViewController: GMSAutocompleteResultsViewController!
    fileprivate var searchController: UISearchController!
    var didChangeAddress: ((String) -> ())?//place, city
    private var filter: GMSAutocompleteFilter?
    fileprivate var dismissing = false
    fileprivate var citiesOnly = false
    var city: String?
    
    
    func presentIntoNavController(by controller: UIViewController) {
        let navController = UINavigationController(rootViewController: self)
        controller.present(navController, animated: true)
    }
    
    init(citiesOnly: Bool) {
        self.citiesOnly = citiesOnly
        if citiesOnly {
            filter = GMSAutocompleteFilter()
            filter!.type = .city
        }
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController.delegate = self
        resultsViewController.autocompleteFilter = filter
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController.searchResultsUpdater = resultsViewController
        searchController.searchBar.sizeToFit()
        searchController.searchBar.delegate = self
        searchController.searchBar.showsCancelButton = true
        navigationItem.titleView = searchController?.searchBar
        definesPresentationContext = true
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.searchController.searchBar.becomeFirstResponder()
        }
    }
    
    func dismiss() {
        dismissing = true
        searchController.isActive = false
    }
}


extension SearchPlacesController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismiss(animated: true)
    }
}


extension SearchPlacesController: UISearchControllerDelegate {
    
    func didDismissSearchController(_ searchController: UISearchController) {
        if dismissing { dismiss(animated: true) }
    }
}


extension SearchPlacesController: GMSAutocompleteResultsViewControllerDelegate {
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        print(place)
        city = place.addressComponents?.filter({ $0.type == "locality" }).first?.name
        didChangeAddress?(place.name)
        dismiss()
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }

    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
