//
//  EventDetailsController.swift
//  JumpIn
//
//  Created by Admin on 04.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import GoogleMaps
import ObjectMapper
import PKHUD
import QBImagePickerController
import SideMenu
import AVKit
import StoreKit


class EventDetailsController: BaseConfigurableController<DetailedEventViewModel>, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, QBImagePickerControllerDelegate, VideoCollectionViewManagerDelegate, PhotoWithLikesCollectionViewManagerDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    var loader: DataLoadingManager<GetDetailedEventRequestModel, EventDetailsController>!
    var usersTableViewManager: UsersTableViewManager!
    var photosCollectionViewManager: PhotoWithLikesCollectionViewManager!
    var videoCollectionViewManager: VideoCollectionViewManager!
    //
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var photosStackView: UIStackView!
    @IBOutlet weak var membersStackView: UIStackView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!
    @IBOutlet fileprivate weak var mapView: GMSMapView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarImageView: DownloadingImageView!
    @IBOutlet weak var participateButton: UIButton!
    @IBOutlet weak var likeButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var dislikeButton: BaseCustomButtonWithoutXib!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var dislikesLabel: UILabel!
    @IBOutlet weak var ownerUserNickButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var membersCountLabel: UILabel!
    @IBOutlet weak var ownerAvatarImageView: DownloadingImageView!
    @IBOutlet weak var showAllDescriptionButton: UIButton!
    @IBOutlet weak var whoWaitStackView: UIStackView!
    @IBOutlet weak var addPhotoView: UIView!
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var videoCollectionView: UICollectionView!
    @IBOutlet weak var mapViewCoveringView: UIView!
    @IBOutlet weak var complainButton: UIButton!
    //
    private var videoModels: [VideoModel] = []
    private var videoViewModels: [VideoCellViewModel] { return videoModels.map({ VideoCellViewModel(withModel: $0) }) }
    private var photoModels: [PhotoWithLikesModel] = []
    private var photoViewModels: [PhotoWithLikesViewModel] { return photoModels.map({ PhotoWithLikesViewModel(withModel: $0) }) }
    let eventId: Int
    private var needUpdate: Bool
    private var mapPinLocation: CLLocationCoordinate2D!
    
    
    init(eventId: Int) {
        self.needUpdate = true
        self.eventId = eventId
        super.init(nibName: String(describing: type(of: self).self), bundle: nil)
        self.initConfig.backButtonImage = #imageLiteral(resourceName: "ic_back_in_round").withRenderingMode(.alwaysOriginal)
        self.loader = DataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getDetailedEvent as AnyObject)
        loader.requestModel = GetDetailedEventRequestModel(withEventId: eventId)
        loader.progressViewArea = .full
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        clearUI()
        setupLikesButtons()
        automaticallyAdjustsScrollViewInsets = false
        setupUsersTableManager()
        setupPhotoCollectionManager()
        setupVideoCollectionManager()
        ownerAvatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapOwnerAction)))
        mapView.isUserInteractionEnabled = false
        mapViewCoveringView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapOnMapAction)))
        SideMenuManager.removeAllPanGestureRecognizers()
        SKPaymentQueue.default().add(self)
        prepareImageTopButton()
    }
    
    func prepareImageTopButton() {
        topButton.setImage( Utils.isRu ? #imageLiteral(resourceName: "ic_topButton") : #imageLiteral(resourceName: "ic_topButtonEng"), for: UIControlState())
    }
    
    @objc private func tapOnMapAction() {
        let app = UIApplication.shared
        if app.canOpenURL(URL(string: "comgooglemaps://")!) {
            app.openURL(URL(string: "comgooglemaps://?center=\(mapPinLocation.latitude),\(mapPinLocation.longitude)&zoom=14&views=traffic")!)
        }
        else {
            app.openURL(URL(string: "itms://itunes.apple.com/us/app/google-maps-gps-navigation/id585027354?mt=8")!)
        }
    }
    
    private func reloadVideos() {
        videoModels.removeAll()
        ServerApi.shared.getEventVideos(with: GetEventVideosRequestModel(eventId: eventId, numberOfVideosOnSinglePage: 20), completion: { [weak self] page in
            guard let `self` = self else { return }
            self.videoModels = page.models
            self.videoCollectionViewManager.dataSource = self.videoViewModels
            self.videoCollectionView.superview!.isHidden = page.models.isEmpty
            self.videoCollectionViewManager.reloadData()
        }, errorBlock: nil)
    }
    
    private func reloadPhotos() {
        photoModels.removeAll()
        let model = GetPhotosRequestModel(type: .events, id: eventId)
        ServerApi.shared.getPhotos(withModel: model, completion: { [weak self] page in
            guard let `self` = self else { return }
            self.photoModels = page.models
            self.photosCollectionViewManager.dataSource = self.photoViewModels
            self.photoCollectionView.superview!.isHidden = page.models.isEmpty
            self.photosCollectionViewManager.reloadData()
        }, errorBlock: nil)
    }
    
    @objc private func onTapOwnerAction() {
        let profileController = ProfileController(userId: loader.model.owner.id)
        navigationController!.pushViewController(profileController, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let currentNumberOfLines = Int(descriptionLabel.bounds.height / descriptionLabel.font.lineHeight)
        showAllDescriptionButton.isHidden = currentNumberOfLines <= 5
        usersTableViewManager.setTableViewHeightToFit()
    }

    private func setupLikesButtons() {
        likeButton.onClick = { [weak self] _ in
            let rating = self?.viewModel.voteMe == true ? nil : true
            self?.voteAction(rating: rating)
        }
        dislikeButton.onClick = { [weak self] _ in
            let rating = self?.viewModel.voteMe == false ? nil : false
            self?.voteAction(rating: rating)
        }
    }
    
    private func setupVideoCollectionManager() {
        videoCollectionViewManager = VideoCollectionViewManager(with: videoCollectionView)
        videoCollectionViewManager.setup()
        videoCollectionViewManager.dataSource = videoViewModels
        videoCollectionViewManager.delegate = self
    }
    
    func videoCollectionManager(_ manager: VideoCollectionViewManager, controllerToPresentSelectedVideoAt index: Int) -> UIViewController? {
        return self
    }
    
    func videoCollectionManager(_ manager: VideoCollectionViewManager, didChangeVoteForVideoAt: IndexPath) {
        reloadVideos()
    }
    
    func videoCollectionManager(_ manager: VideoCollectionViewManager, failedToChangeVoteAt: IndexPath, error: MainURLEngine.Error?) {
        showError(error)
    }

    private func setupPhotoCollectionManager() {
        photosCollectionViewManager = PhotoWithLikesCollectionViewManager(with: photoCollectionView)
        photosCollectionViewManager.setup()
        photosCollectionViewManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let model = self.loader.model!
            let photoId = self.photoModels[self.photosCollectionViewManager.indexOfDataSourceItem(forCellAt: indexPath)].id!
            let controller = SlideshowPhotosController(selectedPhotoId: photoId, ownerType: .events, ownerId: model.id!)
            self.navigationController?.pushViewController(controller, animated: true)
            self.needUpdate = true
        }
        photosCollectionViewManager.delegate = self
    }
    
    func photoCollectionManager(_ manager: PhotoWithLikesCollectionViewManager, didChangeVoteForVideoAt: IndexPath) {
        reloadPhotos()
    }
    
    func photoCollectionManager(_ manager: PhotoWithLikesCollectionViewManager, failedToChangeVoteAt: IndexPath, error: MainURLEngine.Error?) {
        showError(error)
    }
    
    private func setupUsersTableManager() {
        usersTableViewManager = UsersTableViewManager(with: nil)
        let tableView = usersTableViewManager.setup()
        membersStackView.addArrangedSubview(tableView)
        tableView.tableHeaderView = nil
        usersTableViewManager.locationOfCellWithLoadingIndicator = .none
        usersTableViewManager.autoUpdateTableViewHeightToFit = true
        usersTableViewManager.didSelectCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let member = self.loader.model.members.models![self.usersTableViewManager.indexOfDataSourceItem(forCellAt: indexPath)]
            let vc = ProfileController(userId: member.id)
            self.navigationController!.pushViewController(vc, animated: true)
        }
    }
    
    private func configureParticipateButton() {
        var title: String
        participateButton.backgroundColor = #colorLiteral(red: 0.3647058824, green: 0.6784313725, blue: 0.8862745098, alpha: 1)
        if !viewModel.isCurrent {
            title = NSLoc("past")
            participateButton.backgroundColor = #colorLiteral(red: 0.8862745098, green: 0.7176470588, blue: 0.2470588235, alpha: 1)
        } else if viewModel.isMy {
            title = NSLoc("invite_people")
        } else if viewModel.withMe {
            title = NSLoc("leave_event")
            participateButton.backgroundColor = #colorLiteral(red: 1, green: 0.2509803922, blue: 0.3058823529, alpha: 1)
        } else if viewModel.meRequested {
            title = NSLoc("request_has_been_sent")
            participateButton.backgroundColor = #colorLiteral(red: 0.8666666667, green: 0.8666666667, blue: 0.8666666667, alpha: 1)
        } else if viewModel.isUserInvited {
            title = NSLoc("accept_invitation")
        } else {
            title = NSLoc("participate")
        }
        participateButton.setTitle(title, for: .normal)
        participateButton.isEnabled = viewModel.isCurrent && !viewModel.meRequested
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.setNavigationBarTransparent()
        if needUpdate {
            loader.reloadData(handleError: false) { [weak self] (success, error) in
                guard !success else { return }
                self?.showError(error) { self?.backAction() }
            }
            reloadVideos()
            reloadPhotos()
            needUpdate = false
        }
    }
    
    private func configureNavBarButtons() {
        let share = barButtonItem(withImage: #imageLiteral(resourceName: "ic_share_in_round").withRenderingMode(.alwaysOriginal), action: #selector(shareAction))
        var rightBarButtons = [share]
        if viewModel.isMy {
            let membersManaging = barButtonItem(withImage: #imageLiteral(resourceName: "ic_checked_user_in_round").withRenderingMode(.alwaysOriginal),
                                                action: #selector(membersManagingAction))
            rightBarButtons.append(membersManaging)
            if viewModel.isCurrent {
                let edit = barButtonItem(withImage: #imageLiteral(resourceName: "ic_pen_in_round").withRenderingMode(.alwaysOriginal), action: #selector(editAction))
                rightBarButtons.append(edit)
            }
        } else {
            if !viewModel.isCurrent {
                let camera = barButtonItem(withImage: #imageLiteral(resourceName: "ic_camera").withRenderingMode(.alwaysOriginal), action: #selector(addPhoto))
                rightBarButtons.append(camera)
                addPhotoView.isHidden = false
            }
        }
        navigationItem.setRightBarButtonItems(rightBarButtons, animated: true)
    }
    
    private func barButtonItem(withImage image: UIImage, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem.createUsingUIButton(withImage: image, target: self, selector: action)
    }

    private func clearUI() {
        scrollView.isHidden = true
    }
    
    override func refresh() {
        complainButton.isHidden = viewModel.isMy
        scrollView.isHidden = false
        configureNavBarButtons()
        titleLabel.text = viewModel.name
        likesLabel.text = viewModel.likes
        dislikesLabel.text = viewModel.dislikes
        avatarImageView.image = #imageLiteral(resourceName: "no_image")
        avatarImageView.imageLink = viewModel.avatarLink
        ownerUserNickButton.setTitle(viewModel.owner.nick, for: .normal)
        ownerAvatarImageView.image = #imageLiteral(resourceName: "user_no_image")
        ownerAvatarImageView.imageLink = viewModel.owner.avatarLink
        dateLabel.text = viewModel.dateString
        descriptionLabel.text = viewModel.description
        locationLabel.text = viewModel.address
        membersCountLabel.text = viewModel.membersCount
        usersTableViewManager.dataSource = viewModel.members.viewModels
        membersStackView.isHidden = usersTableViewManager.dataSource.count == 0
        usersTableViewManager.reloadData()
        //photosCollectionViewManager.dataSource = viewModel.photos
        //photosStackView.isHidden = photosCollectionViewManager.dataSource.count == 0
        //photosCollectionViewManager.reloadData()
        configureParticipateButton()
        updateMap(with: viewModel.address)
        refreshWhoWaitStackView()
        
        topButton.isHidden = !viewModel.isMy || !viewModel.isCurrent || viewModel.isPremium
        //topButton.isHidden = false
    }
    
    private func refreshWhoWaitStackView() {
        whoWaitStackView.arrangedSubviews.forEach({
            if $0 is UIImageView {
                self.whoWaitStackView.removeArrangedSubview($0)
                $0.removeFromSuperview()
            }
        })
        viewModel.whoWait.map( { UIImageView(image: $0) } ).forEach( { self.whoWaitStackView.addArrangedSubview($0) } )
    }
    
    private func updateMap(with address: String) {
        mapViewCoveringView.isUserInteractionEnabled = false
        let model = GoogleGeocoderRequestModel(with: address)
        let hud = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        hud.color = .gray
        hud.startAnimating()
        mapViewCoveringView.addSubview(hud)
        hud.center = CGPoint(x: mapViewCoveringView.bounds.width/2, y: mapViewCoveringView.bounds.height/2)
        GoogleGeocoderApi.shared.geocodeAddress(with: model, completion: { [weak self] location in
            guard let `self` = self else { return }
            self.mapView.camera = GMSCameraPosition.camera(withTarget: location, zoom: 16)
            self.mapView.clear()
            let marker = GMSMarker(position: location)
            marker.icon = #imageLiteral(resourceName: "ic_orange_pin")
            marker.map = self.mapView
            hud.removeFromSuperview()
            self.mapViewCoveringView.isUserInteractionEnabled = true
            self.mapPinLocation = location
            }, errorBlock: { _, _ in hud.removeFromSuperview() })
    }

    @objc private func membersManagingAction() {
        needUpdate = true
        navigationController?.pushViewController(MembersManagingController(eventId: loader.model.id), animated: true)
    }
    
    @objc private func editAction() {
        needUpdate = true
        navigationController?.pushViewController(EditEventController(model:loader.model), animated: true)
    }
    
    @objc private func shareAction() {
        guard let shareText = viewModel.shareText else { return }
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        present(vc, animated: true, completion: nil)
    }
    
    func voteAction(rating: Bool?) {
        let model = VoteRequestModel(type: .event, id: loader.model.id, rating: rating)
        HUD.show(.progress)
        ServerApi.shared.vote(withModel: model, completion: { [weak self] in
            self?.reloadData(showSuccessAfterLoading: true)
        }) { [weak self] (error: MainURLEngine.Error?, _: HTTPURLResponse?) in
            self?.showError(error)
        }
    }

    func reloadData(showSuccessAfterLoading show: Bool = false) {
        self.loader.reloadData(animated: false) { [weak self] success, error in
            if success, show { HUD.flash(.success) }
            if !success {
                self?.showError(error)
            }
        }
    }
    
    @IBAction func complainAction() {
        HUD.show(.progress)
        let model = ComplainRequestModel(withObjectType: .event, objectId: eventId)
        ServerApi.shared.complain(withModel: model, completion: { HUD.flash(.success) }, errorBlock: { [weak self] error, _ in
            self?.showError(error)
        })
    }
    
    @IBAction private func ownerNickTapAction() {
        onTapOwnerAction()
    }
    
    @IBAction func makeTopAction() {
        
        bayTopEvent()
        /*
        HUD.show(.progress)
        ServerApi.shared.makeEventPremium(withId: eventId, completion: { [weak self] in
            self?.reloadData(showSuccessAfterLoading: true)
        }) { [weak self] (error, _) in
            self?.showError(error)
        }
        */
    }

    @IBAction func addPhoto(_ sender: UIButton) {
        AlertsPresentator.presentAlertChoosePhotoSource(in: self) { [weak self] photoSource in
            guard UIImagePickerController.isSourceTypeAvailable(photoSource) else { return }
            if photoSource == .photoLibrary {
                let vc = QBImagePickerController()
                vc.mediaType = .image
                vc.allowsMultipleSelection = true;
                vc.maximumNumberOfSelection = 10;
                vc.showsNumberOfSelectedAssets = true;
                vc.delegate = self;
                self?.present(vc, animated: true)
            }
            else if photoSource == .camera {
                self?.presentImagePicker(withSourceType: .camera)
            }
        }        
    }
    
    private func presentImagePicker(withSourceType type: UIImagePickerControllerSourceType) {
        let picker = UIImagePickerController()
        picker.sourceType = type
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        HUD.show(.progress)
        self.uploadPhotos(withCompletion: { (succes, error, ids) in
            if succes {
                self.loader.reloadData()
            }
            
            self.needUpdate = true
        }, [image])
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func participateAction() {
        if viewModel.isMy {
            let controller = InviteFriendsToEventController(eventId: eventId)
            navigationController!.pushViewController(controller, animated: true)
        }
        else {
            let successCompletion = { [weak self] ()->Void in
                self?.reloadData(showSuccessAfterLoading: true)
            }
            let errorCompletion: RequestErrorBlockType = { [weak self] (error: MainURLEngine.Error?, _: HTTPURLResponse?) in
                self?.showError(error)
            }
            if viewModel.withMe {
                HUD.show(.progress)
                ServerApi.shared.leaveEvent(withId: loader.model.id, completion: successCompletion, errorBlock: errorCompletion)
            }
            else {
                HUD.show(.progress)
                ServerApi.shared.joinToEvent(withId: loader.model.id, completion: successCompletion, errorBlock: errorCompletion)
            }
        }
    }
    
    @IBAction private func showAllDescription(_ sender: UIButton) {
        if descriptionLabel.numberOfLines > 0 {
            sender.setTitle(NSLoc("show_shortly"), for: .normal)
            descriptionLabel.numberOfLines = 0
        }
        else {
            sender.setTitle(NSLoc("show_full"), for: .normal)
            descriptionLabel.numberOfLines = 5
        }
    }
    
    @IBAction private func showAllPhotos() {
        let controller = AllPhotosController(ownerType: .events, ownerId: loader.model.id)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction private func showAllVideosAction() {
        needUpdate = true
        navigationController!.pushViewController(VideoListController(eventId: eventId, allowEditing: viewModel.isMy), animated: true)
    }
    
     func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        imagePickerController.dismiss(animated: true, completion: nil)
        needUpdate = true;
        print("cancel")
    }
    
    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        imagePickerController.dismiss(animated: true, completion: nil)
        let requestOptions = PHImageRequestOptions()
        requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
        requestOptions.isSynchronous = true
        var yourArray = [UIImage]()
        
        for asset in assets {
            PHImageManager.default().requestImage(for: asset as! PHAsset, targetSize: CGSize(width: 800.0, height: 800.0), contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (pickedImage, info) in
                    yourArray.append(pickedImage!)
            })
        }
        HUD.show(.progress)
        self.uploadPhotos(withCompletion: { [weak self] (succes, error, ids) in
            if succes {
                self?.loader.reloadData()
            }
            self?.needUpdate = true
        }, yourArray)
    }
    private func uploadPhotos(withCompletion completion: ((_ success: Bool, _ error: String?, _ responseArray: [Int]?)->Void)?,_ photos : Array <UIImage>) {
        guard photos.count > 0 else {
            completion?(true, nil, nil)
            return
        }
        encodeToBase64(photos: photos) { [weak self] encodedPhotos in
            guard let `self` = self else { return }
            let requestModel = PhotoUploadRequestModel(withType: .event, id: self.eventId, photos: encodedPhotos)
            ServerApi.shared.uploadPhotos(withModel: requestModel, completion: {
                completion?(true, nil, $0)
            }, errorBlock: { (error, _) in
                completion?(false, error?.description, nil)
            })
        }
    }
    
    private func encodeToBase64(photos: [UIImage], completion: (([String])->Void)?) {
        DispatchQueue.global(qos: .utility).async {
            let base64Photos: [String] = photos.map({ UIImageJPEGRepresentation($0, 0)!.base64EncodedString() })
            DispatchQueue.main.async {
                completion?(base64Photos)
            }
        }
    }
    
    
    @IBOutlet private weak var topButtonWidth: NSLayoutConstraint!
    @IBOutlet private weak var topButtonHeight: NSLayoutConstraint!
    private var lastScrollOffsetY: CGFloat = 0
    let topButtonDefaultSideWidth: CGFloat = 116
    //
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == self.scrollView else { return }
        let offsetY = scrollView.contentOffset.y
        let maxOffsetY = scrollView.contentSize.height - scrollView.bounds.height
        setTopButton(hidden: lastScrollOffsetY - offsetY < 0 && offsetY > 0 || offsetY >= maxOffsetY)
        lastScrollOffsetY = offsetY
    }
    
    private func setTopButton(hidden: Bool) {
        view.layoutIfNeeded()
        self.topButtonWidth.constant = hidden ? 0 : self.topButtonDefaultSideWidth
        self.topButtonHeight.constant = hidden ? 0 : self.topButtonDefaultSideWidth
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.topButton.alpha = hidden ? 0 : 1
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        HUD.hide()
        print("paymentQueue didReceive")
        if response.products.count != 0 {
            self.showActions(response.products.first!)
        } else {
            print("There are no products.")
            STRouter.showError(withDescription: "There are no products.")
        }
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]){
        HUD.hide()
        print("paymentQueue updatedTransactions")
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                //transactionInProgress = false
                confirmTopAction()
                //ServerApi.shared.makeProfilePremium(completion: nil, errorBlock: nil)
            case SKPaymentTransactionState.failed:
                print("Transaction Failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                //transactionInProgress = false
                STRouter.showError(withDescription: "Transaction Failed")
                
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeStore()
    }
    
    deinit {
        removeStore()
    }
    
    func removeStore() {
        SKPaymentQueue.default().remove(self)
    }
}
