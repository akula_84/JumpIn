//
//  DateConverter.swift
//  JumpIn
//
//  Created by Admin on 15.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation


extension DateFormatter {
    
    convenience init(withDateFormatString string: String) {
        self.init()
        self.dateFormat = string
    }
}

private extension String {
    
    func withTime(_ timeFormat: String) -> String {
        return "\(self) \(timeFormat)"
    }
}

class UTCDateFormatter: DateFormatter {
    
    override init() {
        super.init()
        timeZone = TimeZone(secondsFromGMT: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


class DateConverter {
    
    struct DateFormat {
        
        struct Time {
            static let HHmm = "HH:mm"
            static let HHmmss = "HH:mm:ss"
            static let hhmm = "hh:mm"
            static let hhmmss = "hh:mm:ss"
        }
        
        struct Date {
            static let ddMMyyyy = "dd.MM.yyyy"
            static let dMMyyyy = "d.MM.yyyy"
            static let ddMM = "dd.MM"
            static let dMM = "d.MM"
            static let dMMM = "d MMM"
            static let dMMMM = "d MMMM"
        }
        
        struct ForServer {
            static let date = "yyyy-MM-dd"
            static let dateWithTime = "yyyy-MM-dd HH:mm:ss"
        }
        
        struct ForDetailedEvent {
            static let eventDate = NSLoc("start") + "  d.MM.yyyy в HH:mm"
        }
    }
    
    
    class ForInterface {
        
        class Dialogs {
            
            class func messageTimestamp(from date: Date) -> String {
                let now = Date()
                let dateFormat: String
                let day: TimeInterval = 24*60*60
                if (now.timeIntervalSince(date) < day) {
                    dateFormat = DateFormat.Time.HHmm
                }
                else if (now.timeIntervalSince(date) < day*7) {
                    dateFormat = "EEE HH:mm"
                }
                else {
                    dateFormat = "d MMM HH:mm"
                }
                let formattedTimestamp = DateFormatter(withDateFormatString: dateFormat).string(from: date)
                return formattedTimestamp
            }
            
            class func messageTimestamp(from serverTimestamp: String) -> String {
                let date = UTCDateFormatter(withDateFormatString: DateFormat.ForServer.dateWithTime).date(from: serverTimestamp)!
                let formattedTimestamp = self.messageTimestamp(from: date)
                return formattedTimestamp
            }
        }
        
        class Profile {
            
            class func birthday(from date: Date) -> String {
                return DateFormatter(withDateFormatString: DateFormat.Date.ddMMyyyy).string(from: date)
            }
            
            class func birthdate(from string: String) -> Date {
                return DateFormatter(withDateFormatString: DateFormat.ForServer.date).date(from: string)!
            }
            
            class func birthday(fromServerBirthday string: String) -> String {
                return self.birthday(from: ForServer.Profile.birthdate(from: string))
            }
        }
        
        class DetailedEvent {
            
            class func eventDateString(from date: Date) -> String {
                return DateFormatter(withDateFormatString: DateFormat.ForDetailedEvent.eventDate).string(from: date)
            }
            
            class func eventDate(from string: String) -> Date {
                return DateFormatter(withDateFormatString: DateFormat.ForDetailedEvent.eventDate).date(from: string)!
            }
            
            class func eventDateString(fromServerString string: String) -> String {
                return self.eventDateString(from: ForServer.Event.eventDate(from: string))
            }
        }
        
        class EventEditing {
            
            class func eventDateString(from date: Date) -> String {
                return DateFormatter(withDateFormatString: DateFormat.Date.ddMMyyyy.withTime(DateFormat.Time.HHmm)).string(from: date)
            }
            
            class func eventDate(from string: String) -> Date {
                return DateFormatter(withDateFormatString: DateFormat.Date.ddMMyyyy.withTime(DateFormat.Time.HHmm)).date(from: string)!
            }
            
            class func eventDateString(fromServerString string: String) -> String {
                return self.eventDateString(from: ForServer.Event.eventDate(from: string))
            }
        }
        
        class EventsFilter {
            
            class func date(fromServerDateString string: String) -> Date {
                return UTCDateFormatter(withDateFormatString: DateFormat.ForServer.date).date(from: string)!
            }
        }
    }
    
    class ForServer {
        
        class EventsFilter {
            
            class func string(from date: Date) -> String {
                return DateFormatter(withDateFormatString: DateFormat.ForServer.date).string(from: date)
            }
        }
        
        class Profile {
            
            class func birthdate(from string: String) -> Date {
                return UTCDateFormatter(withDateFormatString: DateFormat.ForServer.date).date(from: string)!
            }
            
            class func birthday(from date: Date) -> String {
                return DateFormatter(withDateFormatString: DateFormat.ForServer.date).string(from: date)
            }
            
            class func birthday(fromInterfaceBirthday string: String) -> String {
                return self.birthday(from: ForInterface.Profile.birthdate(from: string))
            }
        }
        
        class Event {
            
            class func eventDate(from string: String) -> Date {
                return UTCDateFormatter(withDateFormatString: DateFormat.ForServer.dateWithTime).date(from: string)!
            }
            
            class func eventDateString(from date: Date) -> String {
                return UTCDateFormatter(withDateFormatString: DateFormat.ForServer.dateWithTime).string(from: date)
            }
            
            class func eventDateString(fromInterfaceString string: String) -> String {
                return self.eventDateString(from: ForInterface.DetailedEvent.eventDate(from: string))
            }
        }
    }
}
