//
//  InviteFriendsToAppController.swift
//  JumpIn
//
//  Created by Admin on 13.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD


class InviteFriendsToAppController: TabController {
    
    let possibleFriendsController = PossibleFriendsFromVKController()
    let invitedFriendsController = InvitedToAppFriendsController()
    
    
    class func show(usingNavController navController: UINavigationController) {
        if VKApi.shared.token == nil {
            navController.pushViewController(PetitionEnterInVKController(), animated: true)
        }
        else { navController.pushViewController(InviteFriendsToAppController(), animated: true) }
    }

    override init() {
        super.init()
        title = NSLoc("invite")
        add(subcontrollers: [possibleFriendsController, invitedFriendsController])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
    }
    
    private func setupNavBarButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_check_gray"), style: .plain, target: self, action: #selector(sendInvitesAction))
    }
    
    @objc private func sendInvitesAction() {
        guard let invites = possibleFriendsController.inviteRequestModels else { return }
        HUD.show(.progress)
        ServerApi.shared.sendInviteToApp(with: invites, completion: {
            HUD.flash(.success)
        }) { [weak self] (error, _) in
            HUD.hide()
            VKApi.shared.logout()
            let viewControllers = self?.navigationController?.viewControllers
            guard var _viewControllers = viewControllers else { return }
            _viewControllers.removeLast()
            _viewControllers.append(PetitionEnterInVKController())
            self?.navigationController?.setViewControllers(_viewControllers, animated: true)
        }
    }
}
