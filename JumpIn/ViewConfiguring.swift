//
//  TableViewManager.swift
//  JumpIn
//
//  Created by Admin on 14.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import SwipeCellKit


protocol HavingXib: class {
    static func xibName() -> String?
}


extension UIView: HavingXib {
    class func xibName() -> String? { return nil }
}


protocol ViewConfigurator: class {
    associatedtype ViewType: UIView, Configurable2
    associatedtype ViewModelType: ViewModelProtocol
    //
    var viewModel: ViewModelType! { get }
    var view: ViewType { get }
    //
    init(view: UIView)
    func set(viewModel: AnyObject)
    func refresh()
}


class BaseViewManager<ViewModelT: ViewModelProtocol, ViewT: UIView>: ViewConfigurator where ViewT: Configurable2 {
    
    var viewModel: ViewModelT!
    unowned var view: ViewT
    
    required init(view: UIView) {
        self.view = view as! ViewT
    }
    
    func set(viewModel: AnyObject) {
        self.viewModel = viewModel as! ViewModelT
    }
    
    func refresh() {
        fatalError("method not implemented")
    }
}


protocol Configurable2: class {
    var configurator: AnyObject? { get set }
}

extension Configurable2 where Self: UIView {
    func configure<ViewModelType: ViewModelProtocol, CellManager: ViewConfigurator>(with viewModel: ViewModelType)->CellManager {
        if configurator == nil {
            configurator = CellManager(view: self)
        }
        let cellManager = configurator as! CellManager
        cellManager.set(viewModel: viewModel)
        cellManager.refresh()
        return cellManager
    }
}


protocol CellViewsManaging {
    associatedtype DataSourceItem
    associatedtype Cell: UIView, Configurable2
    //
    var dataSource: [DataSourceItem] { get set }
    var didSelectCellAtIndexPath: ((IndexPath)->())? { get set }
    var indexPathOfCellForDataSourceItemAtIndex: ((Int)->(IndexPath))? { get set }
    var indexOfDataSourceItemForCellAtIndexPath: ((IndexPath)->(Int))? { get set }
    func reloadData()
    func indexOfDataSourceItem(forCellAt indexPath: IndexPath, useBlockIfExist: Bool) -> Int
    func indexPathOfCell(forDataSourceItemAt index: Int, useBlockIfExist: Bool) -> IndexPath
}

protocol TableViewManaging: CellViewsManaging {
    var tableView: UITableView! { get }
    var tableInitConfig: TableInitConfiguration { get }
    var autoUpdateTableViewHeightToFit: Bool { get set }
    init(with superView: UIView?)
    func setTableViewHeightToFit()
    func scrollTableViewToBottom(animated: Bool)
    @discardableResult func setup()->UITableView
    var didSearch: ((String?) -> ())? { get set }
    var locationOfCellWithLoadingIndicator: LocationOfCellWithLoadingIndicatorInTableView { get set }
    var onShowCellWithLoadingIndicator: (()->Void)? { get set }
}

protocol CollectionViewManaging: CellViewsManaging {
    var collectionView: UICollectionView { get }
    init(with collectionView: UICollectionView)
    var collectionInitConfig: CollectionViewInitConfiguration { get }
    func setup()
}


final class CollectionViewInitConfiguration {
    var cellId: String!
    var isCellHaveXib: Bool = true
    var tableViewInsets: UIEdgeInsets = .zero
    var installTopConstraint = true
    var installBottomConstraint = true
    var installLeadingConstraint = true
    var installTrailingConstraint = true
    var cellSize: CGSize?
    var minimumLineSpacing: CGFloat?
    var minimumInteritemSpacing: CGFloat?
}


class CollectionViewManager<CellManagerType: ViewConfigurator>: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, CollectionViewManaging {
    
    typealias Cell = CellManagerType.ViewType
    typealias CellViewModelType = CellManagerType.ViewModelType
    
    var dataSource: [CellViewModelType] = []
    var didSelectCellAtIndexPath: ((IndexPath)->Void)?
    var indexOfDataSourceItemForCellAtIndexPath: ((IndexPath) -> (Int))?
    var indexPathOfCellForDataSourceItemAtIndex: ((Int) -> (IndexPath))?
    unowned let collectionView: UICollectionView
    final var collectionInitConfig = CollectionViewInitConfiguration()
    private weak var superView: UIView?
    private var collectionViewLayout: UICollectionViewFlowLayout {
        return collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    }
    
    required init(with collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init()
        collectionInitConfig.cellId = String(describing: Cell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    convenience init(withParentView parentView: UIView) {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        self.init(with: collectionView)
        self.superView = parentView
    }
    
    func setup() {
        registerCells()
        if let size = collectionInitConfig.cellSize {
            collectionViewLayout.itemSize = size
        }
        if let minInterItemSpacing = collectionInitConfig.minimumInteritemSpacing {
            collectionViewLayout.minimumInteritemSpacing = minInterItemSpacing
        }
        if let minLineSpacing = collectionInitConfig.minimumLineSpacing {
            collectionViewLayout.minimumLineSpacing = minLineSpacing
        }
        if superView != nil { affixCollectionView(into: superView!) }
    }
    
    private func registerCells() {
        if collectionInitConfig.isCellHaveXib {
            let nib = UINib(nibName: collectionInitConfig.cellId, bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: collectionInitConfig.cellId)
        }
        else {
            collectionView.register(Cell.self, forCellWithReuseIdentifier: collectionInitConfig.cellId)
        }
    }
    
    private func affixCollectionView(into superView: UIView) {
        superView.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        let insets = collectionInitConfig.tableViewInsets
        if collectionInitConfig.installTopConstraint {
            collectionView.topAnchor.constraint(equalTo: superView.topAnchor, constant: insets.top).isActive = true
        }
        if collectionInitConfig.installBottomConstraint {
            collectionView.bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -insets.bottom).isActive = true
        }
        if collectionInitConfig.installLeadingConstraint {
            collectionView.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: insets.left).isActive = true
        }
        if collectionInitConfig.installTrailingConstraint {
            collectionView.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: -insets.right).isActive = true
        }
    }
    
    func reloadData() {
        collectionView.reloadData()
        collectionView.layoutIfNeeded()
    }
    
//    func set(dataSource: [AnyObject]) {
//        self.dataSource = dataSource as! [CellViewModelType]
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionInitConfig.cellId, for: indexPath) as! Cell
        let _: CellManagerType = cell.configure(with: dataSource[indexOfDataSourceItem(forCellAt: indexPath)])
        return cell as! UICollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCellAtIndexPath?(indexPath)
    }
    
    func indexOfDataSourceItem(forCellAt indexPath: IndexPath, useBlockIfExist: Bool = true) -> Int {
        if useBlockIfExist, indexOfDataSourceItemForCellAtIndexPath != nil {
            return indexOfDataSourceItemForCellAtIndexPath!(indexPath)
        }
        return indexPath.item
    }
    
    func indexPathOfCell(forDataSourceItemAt index: Int, useBlockIfExist: Bool = true) -> IndexPath {
        if useBlockIfExist, indexPathOfCellForDataSourceItemAtIndex != nil {
            return indexPathOfCellForDataSourceItemAtIndex!(index)
        }
        return IndexPath(item: index, section: 0)
    }
}


final class TableInitConfiguration {
    
    enum CustomHeaderViewLocation {
        case beforeSearchBar, afterSearchBar
    }
    
    //var shouldSetupTableView = true
    var tableViewInsets: UIEdgeInsets = .zero
    var installTopConstraint = true
    var installBottomConstraint = true
    var installLeadingConstraint = true
    var installTrailingConstraint = true
    var isCellHaveNib = true
    var cellId: String! //
    var cellNibName: String! //
    var cellHeight = UITableViewAutomaticDimension
    var shouldSetupSearchBar = true
    var searchBarInsets = UIEdgeInsets.zero
    var separatorHeight: CGFloat = 0
    var shouldSetupRefreshControl = false
    var onRefresh: (()->())?
    var emptyListLabelText: String?
    var customHeaderView: UIView?
    var customHeaderViewLocation = CustomHeaderViewLocation.afterSearchBar
    var customHeaderViewInsets = UIEdgeInsets.zero
    var showCityTitle = false
}


enum LocationOfCellWithLoadingIndicatorInTableView {
    case none, top, bottom
}


class TableViewManager<TableViewType: UITableView, CellManagerType: ViewConfigurator>: NSObject, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, TableViewManaging {
    
    typealias CellViewModelType = CellManagerType.ViewModelType
    typealias Cell = CellManagerType.ViewType

    class SeparatorCell: UITableViewCell {
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            selectionStyle = .none
        }
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class CellWithLoadingIndicator: UITableViewCell {
        
        weak var spinner: UIActivityIndicatorView!
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            selectionStyle = .none
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            addSubview(spinner)
            self.spinner = spinner
            spinner.translatesAutoresizingMaskIntoConstraints = false
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
            spinner.startAnimating()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func prepareForReuse() {
            super.prepareForReuse()
            spinner.startAnimating()
        }
    }
    
    weak var tableView: UITableView!
    weak var searchBar: UISearchBar?
    weak var refreshControl: UIRefreshControl?
    final let tableInitConfig = TableInitConfiguration()
    var dataSource: [CellViewModelType] = []
    var indexPathOfCellForDataSourceItemAtIndex: ((Int) -> (IndexPath))?
    var indexOfDataSourceItemForCellAtIndexPath: ((IndexPath) -> (Int))?
    private weak var superView: UIView?
    weak var tableViewHeightConstraint: NSLayoutConstraint?
    var didSelectCellAtIndexPath: ((IndexPath)->())?
    var didSearch: ((String?) -> ())?
    var locationOfCellWithLoadingIndicator = LocationOfCellWithLoadingIndicatorInTableView.bottom
    var onShowCellWithLoadingIndicator: (()->Void)?
    var autoUpdateTableViewHeightToFit: Bool = false {
        didSet {
            if autoUpdateTableViewHeightToFit {
                setTableViewHeightToFit()
                tableView.isScrollEnabled = false
            }
        }
    }
    private let separatorCellId = String(describing: SeparatorCell.self)
    private let cellWithLoadingIndicatorId = String(describing: CellWithLoadingIndicator.self)
    private let cellWithLoadingIndicatorHeight = CGFloat(44)
    private weak var emptyListLabel: UILabel?
    
    required init(with superView: UIView?) {
        self.superView = superView
        tableInitConfig.cellId = String(describing: Cell.self)
        tableInitConfig.cellNibName = Cell.xibName() ?? String(describing: Cell.self)
    }
    
    private func setupHeaderViewsIfNeed(into tableView: UITableView) {
        if tableInitConfig.shouldSetupSearchBar {
            if let headerView = tableInitConfig.customHeaderView {
                searchBar = tableView.setupSearchBar(delegate: self, insets: tableInitConfig.searchBarInsets, withCustomView: headerView, customViewLocation: tableInitConfig.customHeaderViewLocation, customViewInsets: tableInitConfig.customHeaderViewInsets)
            }
            else {
                searchBar = tableView.setupSearchBar(delegate: self, insets: tableInitConfig.searchBarInsets)
            }
            searchBar?.addDoneButton()
        }
    }
    
    func reloadHeaderViews() {
        removeHeaderViews()
        setupHeaderViewsIfNeed(into: tableView)
    }
    
    private func removeHeaderViews() {
        if let searchBar = searchBar, let contentView = searchBar.superview {
            contentView.removeFromSuperview()
        }
    }
    
    @discardableResult
    func setup() -> UITableView {
        let tableView = setupTableView()
        setupHeaderViewsIfNeed(into: tableView)
        if tableInitConfig.shouldSetupRefreshControl {
            refreshControl = tableView.setupRefreshControl()
            refreshControl!.tintColor = .gray
            refreshControl!.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        }
        registerCells()
        addEmptyListLabel()
        return tableView
    }
    
    @objc private func refreshAction() {
        tableInitConfig.onRefresh?()
    }
    
    func reloadData() {
        if let label = emptyListLabel { label.isHidden = !dataSource.isEmpty }
        tableView.reloadData()
        if autoUpdateTableViewHeightToFit {
            setTableViewHeightToFit()
        }
        refreshControl?.endRefreshing()
    }
    
    private func setupTableView() -> TableViewType {
        let tableView = TableViewType(frame: .zero, style: .plain)
        tableView.separatorColor = UIColor(red: 230.0/255.0, green: 233.0/255.0, blue: 237.0/255.0, alpha: 1.0)
        tableView.separatorInset = UIEdgeInsets.zero
        if tableInitConfig.separatorHeight > 0 { tableView.separatorStyle = .none }
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        tableView.tableFooterView = UIView()
        self.tableView = tableView
        if let superView = superView { affixTableView(into: superView) }
        return tableView
    }
    
    private func addEmptyListLabel() {
        if let view = superView, let text = tableInitConfig.emptyListLabelText {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 18)
            label.text = text
            label.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            label.sizeToFit()
            view.addSubview(label)
            label.isHidden = true
            label.translatesAutoresizingMaskIntoConstraints = false
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
            emptyListLabel = label
        }
    }
    
    private func affixTableView(into superView: UIView) {
        superView.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let insets = tableInitConfig.tableViewInsets
        if tableInitConfig.installTopConstraint {
            tableView.topAnchor.constraint(equalTo: superView.topAnchor, constant: insets.top).isActive = true
        }
        if tableInitConfig.installBottomConstraint {
            tableView.bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -insets.bottom).isActive = true
        }
        if tableInitConfig.installLeadingConstraint {
            tableView.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: insets.left).isActive = true
        }
        if tableInitConfig.installTrailingConstraint {
            tableView.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: -insets.right).isActive = true
        }
    }
    
    private func registerCells() {
        if tableInitConfig.isCellHaveNib {
            let nib = UINib(nibName: tableInitConfig.cellNibName, bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: tableInitConfig.cellId)
        }
        else {
            tableView.register(Cell.self, forCellReuseIdentifier: tableInitConfig.cellId)
        }
        tableView.register(SeparatorCell.self, forCellReuseIdentifier: separatorCellId)
        tableView.register(CellWithLoadingIndicator.self, forCellReuseIdentifier: cellWithLoadingIndicatorId)
    }
    
    func setTableViewHeightToFit() {
        tableView.layoutIfNeeded()
        tableView.updateConstraints()
        if tableViewHeightConstraint == nil {
            tableViewHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: tableView.contentSize.height)
            tableViewHeightConstraint!.priority = UILayoutPriority(999)
            tableViewHeightConstraint!.isActive = true
        }
        else {
            tableViewHeightConstraint!.constant = tableView.contentSize.height
        }
    }
    
//    func set(dataSource: [AnyObject]) {
//        self.dataSource = dataSource as! [CellViewModelType]
//    }
    
    func scrollTableViewToBottom(animated: Bool) {
        guard !dataSource.isEmpty else { return }
        let indexPath = IndexPath(row: dataSource.count - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
    }
    
    //Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number: Int
        if tableInitConfig.separatorHeight > 0 {
            if dataSource.isEmpty { number = 0 }
            else { number = dataSource.count * 2 - 1 }
        }
        else { number = dataSource.count }
        if locationOfCellWithLoadingIndicator != .none { number += 1 }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = cellIdAndHeight(forIndexPath: indexPath).id
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.selectionStyle = .none
        if let cell = cell as? Cell {
            let index = indexOfDataSourceItem(forCellAt: indexPath)
            let cellViewModel = dataSource[index]
            let cellManager: CellManagerType = cell.configure(with: cellViewModel)
            configureCellManager(cellManager)
        }
        
        if let cell = cell as? EventTableCell {
            cell.eventView.locationLabel.isHidden = !tableInitConfig.showCityTitle
        }
        return cell
    }
    
    func configureCellManager(_ cellManager: CellManagerType) { }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = cellIdAndHeight(forIndexPath: indexPath).height
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectCellAtIndexPath?(indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell is CellWithLoadingIndicator {
            onShowCellWithLoadingIndicator?() }
    }
    
    func indexOfDataSourceItem(forCellAt indexPath: IndexPath, useBlockIfExist: Bool = true) -> Int {
        if useBlockIfExist, indexOfDataSourceItemForCellAtIndexPath != nil {
            return indexOfDataSourceItemForCellAtIndexPath!(indexPath)
        }
        var i = indexPath.row
        if tableInitConfig.separatorHeight > 0 {
            i = i / 2
        }
        if locationOfCellWithLoadingIndicator == .top {
            i -= 1
        }
        return i
    }
    
    func indexPathOfCell(forDataSourceItemAt index: Int, useBlockIfExist: Bool = true) -> IndexPath {
        if useBlockIfExist, indexPathOfCellForDataSourceItemAtIndex != nil {
            return indexPathOfCellForDataSourceItemAtIndex!(index)
        }
        var indexPath = IndexPath(row: index, section: 0)
        if tableInitConfig.separatorHeight > 0 {
            indexPath.row *= 2
        }
        if locationOfCellWithLoadingIndicator == .top {
            indexPath.row += 1
        }
        return indexPath
    }
    
    func cellIdAndHeight(forIndexPath indexPath: IndexPath) -> (id: String, height: CGFloat) {
        let info: (id: String, height: CGFloat)
        if locationOfCellWithLoadingIndicator == .top, indexPath.row == 0 {
            info.id = cellWithLoadingIndicatorId
            info.height = cellWithLoadingIndicatorHeight
        }
        else if tableInitConfig.separatorHeight > 0 {
            if (indexPath.row == dataSource.count * 2 - 1 || dataSource.isEmpty) && locationOfCellWithLoadingIndicator == .bottom {
                info.id = cellWithLoadingIndicatorId
                info.height = cellWithLoadingIndicatorHeight
            }
            else if indexPath.row % 2 == 0 {
                info.id = tableInitConfig.cellId!
                info.height = tableInitConfig.cellHeight
            }
            else {
                info.id = separatorCellId
                info.height = tableInitConfig.separatorHeight
            }
        }
        else {
            if indexPath.row == dataSource.count && locationOfCellWithLoadingIndicator == .bottom {
                info.id = cellWithLoadingIndicatorId
                info.height = cellWithLoadingIndicatorHeight
            }
            else {
                info.id = tableInitConfig.cellId!
                info.height = tableInitConfig.cellHeight
            }
        }
        return info
    }
    
    func deleteRow(at indexPath: IndexPath, withRowAnimation animation: UITableViewRowAnimation = .automatic) {
        var indexPathsToDelete = [indexPath]
        if dataSource.count > 0, tableInitConfig.separatorHeight > 0 {
            var indexPath = indexPath
            if indexPath.row > 0 {
                indexPath.row -= 1
            }
            else {
                indexPath.row += 1
            }
            indexPathsToDelete.append(indexPath)
        }
        tableView.deleteRows(at: indexPathsToDelete, with: animation)
    }
    
    func deleteRows(at indexPaths: [IndexPath], withRowAnimation animation: UITableViewRowAnimation = .automatic) {
        tableView.beginUpdates()
        indexPaths.forEach({ [weak self] in self?.deleteRow(at: $0) })
        tableView.endUpdates()
    }

    // Search Bar TextField
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar === self.searchBar {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) { [weak self] in
                self?.didSearch?(searchText.isEmpty ? nil : searchText)
            }
        }
    }
}

protocol SwipeTableViewManagerDelegate: class {
    func tableViewManager(_ manager: AnyObject, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]?
}

extension SwipeTableViewManagerDelegate {
    
    func tableViewManager(_ manager: AnyObject, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        return SwipeTableOptions()
    }
}

class SwipeTableViewManager<TableViewType: UITableView, CellManagerType: ViewConfigurator>: TableViewManager<TableViewType, CellManagerType>, SwipeTableViewCellDelegate {
    
    weak var swipeDelegate: SwipeTableViewManagerDelegate?
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if let cell = cell as? SwipeTableViewCell {
            cell.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard let cell = tableView.cellForRow(at: indexPath), cell is Cell else { return nil }
        return swipeDelegate?.tableViewManager(self, editActionsForRowAt: indexPath, for: orientation)
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        return swipeDelegate?.tableViewManager(self, editActionsOptionsForRowAt: indexPath, for: orientation) ?? SwipeTableOptions()
    }
}
