//
//  MembersController.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ObjectMapper
import PKHUD


protocol EventMembersManagingInfo: class {
    func getEventId() -> Int
    func getMemberId(withIndex i: Int) -> Int?
}

protocol EventMembersManagingErrorHandler: class {
    func onReceive(urlError: MainURLEngine.Error?)
}


protocol MembersTableViewManaging: TableViewManaging {
    var delegate: EventMembersManagingInfo? { get set }
    var errorHandler: EventMembersManagingErrorHandler? { get set }
}


class UnconfirmedMembersTableManager: TableViewManager<UITableView, UserRequestCellManager>, MembersTableViewManaging {

    weak var delegate: EventMembersManagingInfo?
    weak var errorHandler: EventMembersManagingErrorHandler?

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _cell = super.tableView(tableView, cellForRowAt: indexPath)
        guard let cell = _cell as? UserRequestCell else { return _cell }
        cell.onTapButtonWithAnswer = { [weak self] accept in
            guard let `self` = self else { return }
            guard let delegate = self.delegate, let memberId = delegate.getMemberId(withIndex: indexPath.row) else { return }
            guard let indexPath = self.tableView.indexPath(for: cell) else { return }
            self.dataSource[indexPath.row].state = .loading
            let successCompletion: () -> Void = { [weak self] in
                guard let `self` = self else { return }
                HUD.flash(.success)
                self.dataSource.remove(at: indexPath.row)
                if let indexPaths = tableView.indexPathsForVisibleRows, indexPaths.contains(indexPath) {
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
            let errorCompletion: (MainURLEngine.Error?, HTTPURLResponse?) -> Void = { [weak self] (error, _) in
                guard let `self` = self else { return }
                self.dataSource[indexPath.row].state = .loading
                self.errorHandler?.onReceive(urlError: error)
            }
            if accept {
                HUD.show(.progress)
                ServerApi.shared.acceptUser(withId: memberId, toJoinToEventWithId: delegate.getEventId(), completion: successCompletion, errorBlock: errorCompletion)
            } else {
                HUD.show(.progress)
                ServerApi.shared.removeMember(withId: memberId, fromEventWithId: delegate.getEventId(), completion: successCompletion, errorBlock: errorCompletion)
            }
        }
        return cell
    }
}


class ConfirmedMembersTableManager: TableViewManager<UITableView, ConfirmedMemberCellManager>, MembersTableViewManaging {

    weak var delegate: EventMembersManagingInfo?
    weak var errorHandler: EventMembersManagingErrorHandler?

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _cell = super.tableView(tableView, cellForRowAt: indexPath)
        guard let cell = _cell as? ConfirmedMemberCell else { return _cell }
        cell.onTapRemoveButton = { [weak self] in
            guard let `self` = self else { return }
            guard let delegate = self.delegate, let memberId = delegate.getMemberId(withIndex: indexPath.row) else { return }
            guard let indexPath = self.tableView.indexPath(for: cell) else { return }
            self.dataSource[indexPath.row].state = .loading
            let successCompletion: () -> Void = { [weak self] in
                guard let `self` = self else { return }
                HUD.flash(.success)
                self.dataSource.remove(at: indexPath.row)
                if let indexPaths = tableView.indexPathsForVisibleRows, indexPaths.contains(indexPath) {
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
            let errorCompletion: (MainURLEngine.Error?, HTTPURLResponse?) -> Void = { [weak self] (error, _) in
                guard let `self` = self else { return }
                self.dataSource[indexPath.row].state = .loading
                self.errorHandler?.onReceive(urlError: error)
            }
            HUD.show(.progress)
            ServerApi.shared.removeMember(withId: memberId, fromEventWithId: delegate.getEventId(), completion: successCompletion, errorBlock: errorCompletion)
        }
        return cell
    }
}


class MembersController<TableManagerT: MembersTableViewManaging>: BaseConfigurableListController<BaseUserCellViewModelsPage>, EventMembersManagingInfo, EventMembersManagingErrorHandler {
    
    var tableManager: TableManagerT!
    var loader: PageDataLoadingManager<GetEventMembersRequestModel, MembersController>!
    let eventId: Int
    var loaded = false
    
    init(confirmedMembers: Bool, eventId: Int) {
        self.eventId = eventId
        super.init(nibName: nil, bundle: nil)
        loader = PageDataLoadingManager(withController: self, reloadDataFunc: ServerApi.shared.getEventMembers as AnyObject)
        loader.requestModel = GetEventMembersRequestModel(eventId: eventId, confirmedMembers: confirmedMembers)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func createTableManager() {
        tableManager = TableManagerT(with: view)
        tableManager.delegate = self
        tableManager.errorHandler = self
        tableManager.tableInitConfig.isCellHaveNib = false
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            guard let `self` = self else { return }
            if self.loaded { self.loader.loadNextPage(animated: false) }
        }
        tableManager.didSearch = { [weak self] text in
            self?.loader.requestModel.submodel.searchString = text
            self?.loader.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        createTableManager()
        tableManager.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.reloadData()
    }
    
    override func refresh() {
        loaded = true
        tableManager.dataSource = viewModel.viewModels as! [TableManagerT.DataSourceItem]
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .bottom : .none
        tableManager.reloadData()
    }
    
    func getEventId() -> Int {
        return eventId
    }
    
    func getMemberId(withIndex i: Int) -> Int? {
        return loader.model?.models?[i].id
    }
    
    func onReceive(urlError: MainURLEngine.Error?) {
        showError(urlError)
    }
}


class ConfirmedMembersController: MembersController<ConfirmedMembersTableManager> {
    
    init(eventId: Int) {
        super.init(confirmedMembers: true, eventId: eventId)
        title = NSLoc("participants")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class UnconfirmedMembersController: MembersController<UnconfirmedMembersTableManager> {
    
    init(eventId: Int) {
        super.init(confirmedMembers: false, eventId: eventId)
        title = NSLoc("inquiries")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
