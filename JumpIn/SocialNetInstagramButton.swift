//
//  SocialNetInstagramButton.swift
//  JumpIn
//
//  Created by Admin on 31.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class SocialNetInstagramButton: SocialNetButton {

    override func setup() {
        super.setup()
        backgroundColor = #colorLiteral(red: 0.8862745098, green: 0.3647058824, blue: 0.5450980392, alpha: 1)
        leftIcon = #imageLiteral(resourceName: "ic_instagram2")
        leftViewHorizontalOffset = -2
    }

}
