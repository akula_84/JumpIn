//
//  SimplePhotoController.swift
//  JumpIn
//
//  Created by Admin on 20.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import ImageSlideshow
import PKHUD


class SimplePhotoController: BaseViewController {

    @IBOutlet weak var slideshowView: ImageSlideshow!
    var image: UIImage?
    var imageLink: String?
    var photoId: Int?
    
    
    private init() {
        super.init(nibName: "SimplePhotoController", bundle: nil)
        title = NSLoc("photo")
    }
    
    
    convenience init(image: UIImage?, imageLink: String?, photoId: Int?) {
        self.init()
        self.image = image
        self.imageLink = imageLink
        self.photoId = photoId
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = self.image {
            let imageSource = ImageSource(image: image)
            slideshowView.setImageInputs([imageSource])
        }
        else if let imageLink = self.imageLink {
            let imageSource = SDWebImageSource(urlString: imageLink)!
            slideshowView.setImageInputs([imageSource])
        }
        setupNavBarButtons()
    }
    
    private func setupNavBarButtons() {
        let more = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_complain"), style: .plain, target: self, action: #selector(moreAction))
        navigationItem.rightBarButtonItem = more
    }
    
    @objc private func moreAction() {
        AlertsPresentator.presentActionsForPhoto(in: self) { [weak self] action in
            guard let action = action, let `self` = self, let id = self.photoId else { return }
            switch action {
            case .complain: self.complain(photoId: id)
            }
        }
    }
    
    private func complain(photoId: Int) {
        HUD.show(.progress)
        let model = ComplainRequestModel(withObjectType: .photo, objectId: photoId)
        ServerApi.shared.complain(withModel: model, completion: { HUD.flash(.success) }, errorBlock: { [weak self] error, _ in
            self?.showError(error)
        })
    }
}
