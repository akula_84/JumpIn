//
//  GetEventMembersRequestModel.swift
//  JumpIn
//
//  Created by Admin on 29.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetEventMembersRequestModel: RequestModelWithPageProtocol {
    
    enum MemberConfirmationType: String {
        case confirmed = "members", unconfirmed = "requests"
    }
    
    class SubModel: BaseRequestModelWithPagination {
        
        var membersType: MemberConfirmationType!
        var searchString: String?
        
        required init?(map: Map) {
            super.init(map: map)
        }
        
        init(confirmedMembers: Bool, searchString: String?) {
            super.init()
            self.membersType = confirmedMembers ? .confirmed : .unconfirmed
            self.searchString = searchString
        }
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            membersType <- map["t"]
            searchString <- map["q"]
        }
    }
    
    var eventId: Int
    var submodel: SubModel
    var page: Int { get { return submodel.page } set { submodel.page = newValue } }
    
    init(eventId: Int, confirmedMembers: Bool, searchString: String? = nil) {
        self.eventId = eventId
        self.submodel = SubModel(confirmedMembers: confirmedMembers, searchString: searchString)
    }
}
