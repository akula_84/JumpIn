//
//  ProfileServerModel.swift
//  JumpIn
//
//  Created by Admin on 10.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class BaseProfileModel: ModelProtocol {
    
    var id: Int!
    var nickname: String!
    var first_name: String!
    var last_name: String!
    var sex: SexModel!
    var birthday: String!
    
    required init?(map: Map) {}
    required init() {}
    
    func mapping(map: Map) {
        id <- map["id"]
        nickname <- map["nickname"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        sex <- map["sex"]
        birthday <- map["birthday"]
    }
}


class ProfileModel: BaseProfileModel {
    
    var show_phone: Bool!
    //var videos_events: Bool!
    var hide_age: Bool!
    var avatar: PhotoWithLikesModel?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        show_phone <- map["show_phone"]
        //videos_events <- map["videos_events"]
        hide_age <- map["hide_age"]
        avatar <- map["avatar"]
    }
}
