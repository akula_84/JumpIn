//
//  GetPhotosRequestModel.swift
//  JumpIn
//
//  Created by Admin on 12.10.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class GetPhotosRequestModel: RequestModelWithPageProtocol {
    
    enum OwnerType: String { case users, events }
    
    var page: Int = 1
    let type: OwnerType
    let id: Int
    
    init(type: OwnerType, id: Int) {
        self.type = type
        self.id = id
    }
}
