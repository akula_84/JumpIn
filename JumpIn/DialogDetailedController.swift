//
//  DialogDetailedController.swift
//  JumpIn
//
//  Created by Admin on 02.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import Starscream
import SideMenu
import ObjectMapper
import PKHUD


class DialogMessagesPage: PageModel<DialogMessageModel> {
    
    init(models: [DialogMessageModel], currentPage: Int) {
        super.init()
        self.models = models
        self.currentPage = currentPage
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
}

class DialogDetailedControllerViewModel: PageViewModel<DialogMessagesPage, DialogCellViewModel> {
    
    var messages: [DialogCellViewModel]
    
    required init(withModel model: DialogMessagesPage) {
        messages = model.models!.map({ DialogCellViewModel(withModel: $0) })
        super.init(withModel: model)
    }
}

class DialogDetailedLoader: PageDataLoadingManager<GetDialogMessagesRequestModel, DialogDetailedController> {
    
    var allMessagesModel: DialogMessagesPage {
        var allMessages = pendingMessages
        if let loadedMessages = model.models {
            allMessages.insert(contentsOf: loadedMessages, at: 0)
        }
        return DialogMessagesPage(models: allMessages, currentPage: model.currentPage)
    }
    //
    var pendingMessages: [DialogMessageModel] = []
    
    
    required init(withController controller: UIViewController, reloadDataFunc `func`: AnyObject) {
        super.init(withController: controller, reloadDataFunc: `func`)
        additionMode = .insertAtFirst
    }
    
    override func updateControllerViewModel() {
        controller.viewModel = DialogDetailedControllerViewModel(withModel: allMessagesModel)
    }
    
    func setModel(_ model: DialogMessagesPage) {
        self.setValue(model, forKey: "model")
    }
    
    func addMessage(model: DialogMessageModel) {
        if let pageModel = self.model {
            pageModel.models!.append(model)
        }
        else {
            setModel(DialogMessagesPage(models: [model], currentPage: 0))
        }
        updateControllerViewModel()
    }
    
    func removeMessage(at index: Int) {
        guard let pageModel = self.model, !pageModel.models.isEmpty else { return }
        let i = index - pendingMessages.count
        guard i >= 0 else { return }
        pageModel.models.remove(at: i)
        updateControllerViewModel()
    }
    
    func addPendingMessage(model: DialogMessageModel) {
        model.pendingState = .pendingToSend
        pendingMessages.append(model)
        updateControllerViewModel()
    }
}

class DialogDetailedTableManager: TableViewManager<UITableView, DialogCellManager>, DialogCellManagerDelegate {
    
    var onTapAvatarOfCellAtIndexPath: ((IndexPath)->Void)?
    var onLongPressToCellAtIndexPath: ((IndexPath)->Void)?
    var onTapPhoto: ((UIImage?, String?, Int)->Void)?
    private var lastContentSizeHeight: CGFloat = 0
    
    
    override func configureCellManager(_ cellManager: DialogCellManager) {
        super.configureCellManager(cellManager)
        cellManager.delegate = self
    }
    
    func onTapAvatar(manager: DialogCellManager) {
        guard let indexPath = tableView.indexPath(for: manager.view) else { return }
        onTapAvatarOfCellAtIndexPath?(indexPath)
    }
    
    func onLongPress(manager: DialogCellManager) {
        guard let indexPath = tableView.indexPath(for: manager.view) else { return }
        onLongPressToCellAtIndexPath?(indexPath)
    }
    
    func onTapPhoto(manager: DialogCellManager, photo: UIImage?, photoLink: String?) {
        guard let indexPath = tableView.indexPath(for: manager.view) else { return }
        onTapPhoto?(photo, photoLink, indexOfDataSourceItem(forCellAt: indexPath))
    }
    
    func reloadData(withNumberOfNewElements number: Int) {
        var firstVisibleIndexPath: IndexPath?
        firstVisibleIndexPath = tableView.indexPathsForVisibleRows?.first
        firstVisibleIndexPath?.row += number
        reloadData()
        guard let indexPath = firstVisibleIndexPath, tableView.numberOfRows(inSection: 0) - 1 >= indexPath.row else {
            scrollTableViewToBottom(animated: false)
            return
        }
        tableView.scrollToRow(at: indexPath, at: .top, animated: false)
    }
}

class DialogDetailedController: BaseConfigurableListController<DialogDetailedControllerViewModel>, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private let dialogInputViewController: DialogInputTextViewController
    private var loader: DialogDetailedLoader!
    private var tableManager: DialogDetailedTableManager!
    private let userId: Int
    private let userAvatarLink: String?
    private var bottomConstraint: NSLayoutConstraint!
    private let messageEventsObserver = DialogMessageEventsObserver()
    private var loaded = false
    
    
    init(userId: Int, userAvatarLink: String?) {
        self.userId = userId
        self.userAvatarLink = userAvatarLink
        dialogInputViewController = DialogInputTextViewController()
        super.init(nibName: nil, bundle: nil)
        title = NSLoc("dialog")
        loader = DialogDetailedLoader(withController: self, reloadDataFunc: ServerApi.shared.getDialogMessages as AnyObject)
        loader.requestModel = GetDialogMessagesRequestModel(userId: userId)
        setupMessageEventsObserver()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
        setupBgImageView()
        setupTableManager()
        setupDialogInputViewController()
        setupBackFromBgWatcher()
        setupKeyboardWatchers()
        setUnreadMessages()
    }
    
    private func setUnreadMessages() {
        ServerApi.shared.markAsReadMessagesOf(userWithId: userId)
    }
    
    private func setupNavBarButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadData))
    }
    
    @objc private func reloadData() {
        loader.reloadData()
//        loader.reloadData() { [weak self] success, _ in
//            if success { self?.tableManager.scrollTableViewToBottom(animated: true) }
//        }
    }
    
    private func setupBackFromBgWatcher() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(reloadData),
                                       name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    private func setupKeyboardWatchers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        let keyboardHeight = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        bottomConstraint.constant = -keyboardHeight
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        tableManager.scrollTableViewToBottom(animated: true)
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func setupTableManager() {
        tableManager = DialogDetailedTableManager(with: view)
        tableManager.tableInitConfig.shouldSetupSearchBar = false
        tableManager.tableInitConfig.installBottomConstraint = false
        tableManager.setup()
        tableManager.tableView.keyboardDismissMode = .onDrag
        tableManager.locationOfCellWithLoadingIndicator = .none
        tableManager.onShowCellWithLoadingIndicator = { [weak self] in
            guard let `self` = self else { return }
            if self.loaded { self.loader.loadNextPage(animated: false) }
            self.loaded = false
        }
        tableManager.tableView.separatorStyle = .none
        tableManager.tableView.backgroundColor = .clear
        tableManager.onTapAvatarOfCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let i = self.tableManager.indexOfDataSourceItem(forCellAt: indexPath)
            let model = self.loader.allMessagesModel.models![i]
            let profileVC = ProfileController(userId: model.senderId)
            self.navigationController!.pushViewController(profileVC, animated: true)
        }
        tableManager.onLongPressToCellAtIndexPath = { [weak self] indexPath in
            guard let `self` = self else { return }
            let i = self.tableManager.indexOfDataSourceItem(forCellAt: indexPath)
            let message = self.loader.allMessagesModel.models[i]
            AlertsPresentator.presentAlertMessageActions(in: self, model: message, completion: { [weak self] action in
                switch action {
                case .copy: UIPasteboard.general.string = message.text
                case .delete: self?.deleteMessage(withCellAt: indexPath)
                case .complain: self?.complain(messageId: message.id)
                }
            })
        }
        tableManager.onTapPhoto = { [weak self] (image, imageLink, indexOfAssignedDSItem) in
            guard let `self` = self else { return }
            let id = self.loader.allMessagesModel.models[indexOfAssignedDSItem].id!
            self.navigationController!.pushViewController(SimplePhotoController(image: image, imageLink: imageLink, photoId: id), animated: true)
        }
    }
    
    private func complain(messageId: Int) {
        HUD.show(.progress)
        let model = ComplainRequestModel(withObjectType: .message, objectId: messageId)
        ServerApi.shared.complain(withModel: model, completion: { HUD.flash(.success) }, errorBlock: { [weak self] error, _ in
            self?.showError(error)
        })
    }
    
    private func setupMessageEventsObserver() {
        messageEventsObserver.onReceiveNewMessage = { [weak self] message in
            self?.loader.addMessage(model: DialogMessageModel(model: message))
            self?.refresh()
        }
        messageEventsObserver.onDeleteMessage = { [weak self] model in
            guard let `self` = self else { return }
            let messageId = model.messageId
            guard let allMessages = self.loader.allMessagesModel.models else { return }
            guard let i = allMessages.index(where: { $0.id == messageId }) else { return }
            self.onDeleteMessage(withCellAt: self.tableManager.indexPathOfCell(forDataSourceItemAt: i))
        }
    }
    
    private func setupDialogInputViewController() {
        addChildViewController(dialogInputViewController)
        let inputView = dialogInputViewController.view!
        view.addSubview(inputView)
        inputView.translatesAutoresizingMaskIntoConstraints = false
        inputView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        inputView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        bottomConstraint = inputView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: 0)
        bottomConstraint.isActive = true
        inputView.layoutIfNeeded()
        dialogInputViewController.didMove(toParentViewController: self)
        dialogInputViewController.onSendMessage = { [weak self] (msg: String) -> Void in self?.send(text: msg) }
        dialogInputViewController.view.topAnchor.constraint(equalTo: tableManager.tableView.bottomAnchor, constant: 0).isActive = true
        dialogInputViewController.onTapPlus = { [weak self] in
            guard let `self` = self else { return }
            AlertsPresentator.presentAlertChoosePhotoSource(in: self) { photoSource in
                guard UIImagePickerController.isSourceTypeAvailable(photoSource) else { return }
                let picker = UIImagePickerController()
                picker.sourceType = photoSource
                picker.delegate = self
                self.present(picker, animated: true)
            }
        }
    }
    
    private func deleteMessage(withCellAt indexPath: IndexPath) {
        let model = loader.allMessagesModel.models[tableManager.indexOfDataSourceItem(forCellAt: indexPath)]
        guard model.senderId != UserInfoService.shared.userId else { return }
        model.pendingState = .pendingToDelete
        loader.updateControllerViewModel()
        refresh()
        JsonRpcDeleteMessageRequest(messageId: model.id).execute(with: { [weak self] _ in
            self?.onDeleteMessage(withCellAt: indexPath)
        }) { [weak self] errorModel in
            guard let `self` = self else { return }
            AlertsPresentator.presentAlertCantDeleteMessage(in: self, withError: errorModel.error)
            model.pendingState = .noPending
            self.loader.updateControllerViewModel()
            self.refresh()
        }
    }
    
    private func onDeleteMessage(withCellAt indexPath: IndexPath) {
        let indexOfModel = tableManager.indexOfDataSourceItem(forCellAt: indexPath)
        loader.removeMessage(at: indexOfModel)
        tableManager.dataSource = self.viewModel.messages
        tableManager.tableView.deleteRows(at: [indexPath], with: .right)
    }
    
    private func send(text: String) {
        let info = UserInfoService.shared
        let model = DialogMessageModel(senderId: info.userId, recipientId: self.userId, text: text, senderAvatarLink: info.avatarLink)
        addToPendingList(model: model)
        sendMessage(model: model)
    }
    
    private func send(photo: UIImage) {
        let info = UserInfoService.shared
        let model = DialogMessageModel(senderId: info.userId, recipientId: self.userId, photo: photo, senderAvatarLink: info.avatarLink)
        addToPendingList(model: model)
        sendMessage(model: model)
    }
    
    private func addToPendingList(model: DialogMessageModel) {
        self.loader.addPendingMessage(model: model)
        self.refresh(scrollToBottom: true)
    }
    
    private func sendMessage(model: DialogMessageModel) {
        let requestModel = JsonRpcSendMessageRequestModel(targetUserId: userId, photo: model.photo, text: model.text)
        JsonRpcSendMessageRequest(requestModel: requestModel).execute(with: { [weak self] response in
            guard let `self` = self else { return }
            model.id = response.result.messageId
            model.pendingState = .noPending
            model.timestampDate = Date()
            self.loader.pendingMessages = self.loader.pendingMessages.filter({ $0 !== model })
            self.loader.addMessage(model: model)
            self.refresh()
            }, errorHandler: { [weak self] errorModel in
                guard let `self` = self else { return }
                self.loader.pendingMessages = self.loader.pendingMessages.filter({ $0 !== model })
                self.loader.updateControllerViewModel()
                self.refresh()
                AlertsPresentator.presentAlertCantSendMessage(in: self, withError: errorModel.error)
        })
    }
    
    private func setupBgImageView() {
        let imageView = UIImageView(frame: view.bounds)
        imageView.image = #imageLiteral(resourceName: "ic_dialogBg")
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.insertSubview(imageView, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
        SocketManager.shared.beginUsing()
        SideMenuManager.removeAllPanGestureRecognizers()
        reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SocketManager.shared.endUsing()
        SideMenuManager.repairGestureRecognizers()
    }
    
    override func refresh() {
        tableManager.dataSource = viewModel.messages
        tableManager.locationOfCellWithLoadingIndicator = loader.model.nextPageLink != nil ? .top : .none
        tableManager.reloadData(withNumberOfNewElements: loader.numberOfElementsInLastLoadedPage)// + loader.pendingMessages.count)
        loaded = true
    }
    
    private func refresh(scrollToBottom: Bool) {
        refresh()
        if scrollToBottom { tableManager.scrollTableViewToBottom(animated: true) }
    }
    
    deinit {
        messageEventsObserver.dispose()
    }
    
    //MARK: UIImagePickerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        send(photo: image)
        picker.dismiss(animated: true)
    }
}
