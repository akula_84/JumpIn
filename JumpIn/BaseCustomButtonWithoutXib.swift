//
//  BaseCustomButtonWithoutXib.swift
//  JumpIn
//
//  Created by Admin on 21.06.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class BaseCustomButtonWithoutXib: BaseCustomButton {

    override var xibName: String { return "" }
    
    override func setup() {
        //
        super.setup()
        internalView = viewWithTag(1)
    }
}
