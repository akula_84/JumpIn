//
//  StackViewWithBackgroundColor.swift
//  JumpIn
//
//  Created by Admin on 20.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class StackViewWithBackgroundColor: UIStackView {
    
    @IBInspectable var bgColor: UIColor?
    override var backgroundColor: UIColor? {
        get { return bgColor }
        set {
            bgColor = newValue
            self.setNeedsLayout()
        }
    }
    private lazy var backgroundLayer: CAShapeLayer = {
        //
        let layer = CAShapeLayer()
        self.layer.insertSublayer(layer, at: 0)
        return layer
    }()
    
    override func layoutSubviews() {
        //
        super.layoutSubviews()
        backgroundLayer.path = UIBezierPath(rect: self.bounds).cgPath
        backgroundLayer.fillColor = self.bgColor?.cgColor
    }
}
