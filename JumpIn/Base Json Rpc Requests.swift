//
//  JsonRpcRequests.swift
//  JumpIn
//
//  Created by Admin on 29.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


protocol JsonRpcRequestProtocol {
    
    associatedtype RequestModelT: JsonRpcRequestModelProtocol
    associatedtype ResponseModelT: JsonRpcResponseModelProtocol
    
    typealias CompletionT = (ResponseModelT)->Void
    typealias ErrorHandlerT = (JsonRpcErrorModel)->Void
    
    var requestModel: RequestModelT { get set }
    var responseModel: ResponseModelT! { get }
    var completion: CompletionT? { get }
    
    func execute(with completion: CompletionT?, errorHandler: ErrorHandlerT?)
}


class JsonRpcRequest<RequestModel: JsonRpcRequestModelProtocol, ResponseModel: JsonRpcResponseModelProtocol>: JsonRpcRequestProtocol, JsonRpcSocketMessageReceiverObserver {
    
    typealias Completion = (ResponseModel)->Void
    typealias ErrorHandler = (JsonRpcErrorModel)->Void
    
    var requestModel: RequestModel
    private(set) var responseModel: ResponseModel!
    private(set) var completion: Completion?
    private(set) var errorHandler: ErrorHandler?
    
    init(requestModel: RequestModel) {
        self.requestModel = requestModel
    }

    func execute(with completion: Completion?, errorHandler: ErrorHandler?) {
        self.completion = completion
        self.errorHandler = errorHandler
        let id = SocketManager.shared.nextMessageId()
        requestModel.id = id
        JsonRpcSocketMessageReceiver.shared.add(observer: self, forMessageWithId: id)
        JsonRpcSocketMessageSendingQueue.shared.add(model: requestModel)
    }
    
    func onReceive(rawResponse: String) {
        guard let data = rawResponse.data(using: .utf8) else {
            print("OnReceiveRawResponse Warning: CANT GET DATA from raw message string")
            return
        }
        guard let _dict = try? JSONSerialization.jsonObject(with: data), let dict = _dict as? [String : Any] else {
            print("OnReceiveRawResponse Warning: CANT GET DICT from message data")
            return
        }
        if dict["error"] != nil {
            guard let errorModel = JsonRpcErrorModel(JSONString: rawResponse) else {
                print("OnReceiveRawResponse Warning: CANT CREATE ERROR MODEL")
                return
            }
            DispatchQueue.main.async {
                self.errorHandler?(errorModel)
            }
        }
        else if let model = ResponseModel(JSONString: rawResponse) {
            DispatchQueue.main.async {
                self.completion?(model)
            }
        }
        else { print("OnReceiveRawResponse Warning: CANT CREATE RESPONSE MODEL") }
        JsonRpcSocketMessageReceiver.shared.remove(observer: self)
    }
}


class JsonRpcSendMessageRequest: JsonRpcRequest<JsonRpcSendMessageRequestModel, JsonRpcSendMessageResponseModel> {
    
    override func execute(with completion: ((JsonRpcSendMessageResponseModel) -> Void)?, errorHandler: ErrorHandler?) {
        requestModel.prepareToSend {
            super.execute(with: completion, errorHandler: errorHandler)
        }
    }
}

class JsonRpcDeleteMessageRequest: JsonRpcRequest<JsonRpcDeleteMessageRequestModel, JsonRpcDeleteMessageResponse> {
    
    init(messageId: Int) {
        super.init(requestModel: JsonRpcDeleteMessageRequestModel(messageId: messageId))
    }
}
