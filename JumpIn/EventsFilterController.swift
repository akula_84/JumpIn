//
//  EventsFilterController.swift
//  JumpIn
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class FilterStackViewButton: BaseCustomButtonWithoutXib {
    weak var titleLabel: UILabel!
    weak var checkImageView: UIImageView!
    //
    override func setup() {
        super.setup()
        titleLabel = viewWithTag(1) as! UILabel
        checkImageView = viewWithTag(2) as! UIImageView
    }
}


class EventsFilterController: BaseConfigurableController<EventsFilterViewModel>, UITextFieldDelegate {

    @IBOutlet var sortButtons: [FilterStackViewButton]!
    let popularityTypes: [GetEventsRequestModel.PopularityType?] = [.new, nil, .unpopular]

    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var stackView: StackViewWithBackgroundColor!
    //
    private lazy var startDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(startDatePickerChangedAction), for: .valueChanged)
        return datePicker
    }()
    private lazy var endDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(endDatePickerChangedAction), for: .valueChanged)
        return datePicker
    }()
    var selectedIndex: Int?
    var model: GetEventsRequestModel
    var didApply: (() -> ())?
    var filterButton: UIBarButtonItem?
    private var oldModel: GetEventsRequestModel
    private var lastActiveTextField: UITextField!
    

    init(withModel model: GetEventsRequestModel, filterButton: UIBarButtonItem?) {
        self.model = model
        self.oldModel = GetEventsRequestModel(JSON: model.toJSON())!
        self.filterButton = filterButton
        super.init(nibName: String(describing: type(of: self).self), bundle: nil)
        self.viewModel = EventsFilterViewModel(withModel: model)
        title = NSLoc("filter")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyFilter(to model: GetEventsRequestModel) {
        model.popularityType = viewModel.sortedType
        model.dateStart = viewModel.startDate
        model.dateEnd = viewModel.endDate
    }
    
    override func backAction() {
        super.backAction()
        restoreModel()
    }
    
    private func restoreModel() {
        model.dateEnd = oldModel.dateEnd
        model.dateStart = oldModel.dateStart
        model.popularityType = oldModel.popularityType
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        startDateTextField.inputView = startDatePicker
        endDateTextField.inputView = endDatePicker
        addAccessoryViewForDateTextField(startDateTextField)
        addAccessoryViewForDateTextField(endDateTextField)
        setupNavBarButtons()
        configureStackView()
        refresh()
        endDatePicker.minimumDate = startDatePicker.date
    }
    
    private func addAccessoryViewForDateTextField(_ textfield: UITextField) {
        textfield.addDoneButton()
        guard let toolbar = textfield.inputAccessoryView as? UIToolbar else { return }
        let cancel = UIBarButtonItem(title: NSLoc("clear"), style: .done, target: self, action: #selector(cancelDateAction))
        toolbar.items?.insert(cancel, at: 0)
    }
    
    @objc private func cancelDateAction() {
        lastActiveTextField.resignFirstResponder()
        if lastActiveTextField === startDateTextField {
            model.dateStart = nil
        }
        else if lastActiveTextField === endDateTextField {
            model.dateEnd = nil
        }
        updateViewModel()
        refresh()
    }

    override func refresh() {
        sortButtons.enumerated().forEach { index, view in
            let popularityType = popularityTypes[index]
            view.checkImageView.isHidden = viewModel.sortedType != popularityType
        }
        startDateTextField.text = viewModel.startDate
        endDateTextField.text = viewModel.endDate
        if let startDate = viewModel.startDate {
            startDatePicker.date = DateConverter.ForInterface.EventsFilter.date(fromServerDateString: startDate)
        }
        else {
            startDatePicker.date = Date()
        }
        endDatePicker.minimumDate = startDatePicker.date
        if let endDate = viewModel.endDate {
            endDatePicker.date = DateConverter.ForInterface.EventsFilter.date(fromServerDateString: endDate)
            startDatePicker.maximumDate = endDatePicker.date
        }
        else {
            endDatePicker.date = startDatePicker.date
            startDatePicker.maximumDate = nil
        }
    }

    private func configureStackView() {
        sortButtons.enumerated().forEach { index, view in
            view.onClick = { _ in
                self.model.popularityType = self.popularityTypes[index]
                self.updateViewModel()
                self.refresh()
            }
        }
    }
    
    private func updateViewModel() {
        viewModel = EventsFilterViewModel(withModel: model)
    }
    
    private func setupNavBarButtons() {
        let resetButton = UIBarButtonItem(title: NSLoc("reset"), style: .plain, target: self, action: #selector(resetButtonAction))
        resetButton.tintColor = #colorLiteral(red: 0.9980296493, green: 0.3800280094, blue: 0.4532628655, alpha: 1)
        navigationItem.rightBarButtonItem = resetButton
    }
    
    @objc private func resetButtonAction() {
        viewModel = EventsFilterViewModel()
        applyFilter(to: model)
        refresh()
    }

    @objc private func startDatePickerChangedAction(_ sender: UIDatePicker) {
        model.dateStart = DateConverter.ForServer.EventsFilter.string(from: sender.date)
        updateViewModel()
        refresh()
    }
    
    @objc private func endDatePickerChangedAction(_ sender: UIDatePicker) {
        model.dateEnd = DateConverter.ForServer.EventsFilter.string(from: sender.date)
        updateViewModel()
        refresh()
    }

    @IBAction func showResultsButtonPressed(_ sender: Any) {
        if let filterButton = self.filterButton {
            if model.dateEnd != nil || model.dateStart != nil || (model.popularityType != nil && model.popularityType != .popular) {
                filterButton.tintColor = #colorLiteral(red: 0.431372549, green: 0.7411764706, blue: 0.9098039216, alpha: 1)
            }
            else {
                filterButton.tintColor = nil
            }
        }
        navigationController?.popViewController(animated: true)
        didApply?()
    }
    
    //MARK UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lastActiveTextField = textField
        if textField === startDateTextField {
            guard model.dateStart == nil else { return }
            model.dateStart = DateConverter.ForServer.EventsFilter.string(from: startDatePicker.date)
        }
        else if textField === endDateTextField {
            if model.dateStart == nil {
                model.dateStart = DateConverter.ForServer.EventsFilter.string(from: startDatePicker.date)
            }
            guard model.dateEnd == nil else { return }
            model.dateEnd = model.dateStart
        }
        updateViewModel()
        refresh()
    }
}
