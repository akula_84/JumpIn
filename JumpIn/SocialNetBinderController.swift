//
//  SocialNetBinder.swift
//  JumpIn
//
//  Created by Admin on 01.09.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import PKHUD
import FBSDKLoginKit


protocol SocNetBinderDelegate: class {
    func binder(_ binder: SocNetBinder, didBindWithResult result: SocNetBinder.Result)
}


class SocNetBinder {
    
    struct Result {
        let success: Bool
        let socialNet: SocialNetType
        let error: String?
    }
    
    weak var delegate: (SocNetBinderDelegate & BaseViewController)?
    
    init(delegate: SocNetBinderDelegate & BaseViewController) {
        self.delegate = delegate
    }
    
    func bindFb() {
        guard let delegate = delegate else { return }
        let manager = FBSDKLoginManager()
        manager.logIn(withReadPermissions: ["user_birthday", "email", "user_location"], from: delegate, handler: { [weak self] (result, error) in
            if result != nil && result!.token != nil {
                self?.bind(socialNet: .Fb, withAccessToken: result!.token.tokenString)
            }
            else if error != nil {
                print(error!)
            }
        })
    }
    
    func bindInstagram() {
        let vc = InstagramOAuthController.createIntoNavController(withCompletion: { [weak self] token in
            self?.bind(socialNet: .Instagram, withAccessToken: token)
        })
        self.delegate?.present(vc, animated: true)
    }
    
    func bindVk() {
        let vc = VKOAuthController.createIntoNavController { [weak self] token in
            self?.bind(socialNet: .Vk, withAccessToken: token)
        }
        delegate?.present(vc, animated: true)
    }
    
    fileprivate func bind(socialNet type: SocialNetType, withAccessToken token:String) {
        let model = BindSocialNetRequestModel(accessToken: token, socialNet: type)
        HUD.show(.progress, onView: delegate?.view)
        ServerApi.shared.bindSocialNet(withModel: model, completion: { [weak self] in
            guard let `self` = self else { return }
            HUD.flash(.success)
            self.delegate?.binder(self, didBindWithResult: Result(success: true, socialNet: type, error: nil))
        }) { [weak self] (error, _) in
            guard let `self` = self else { return }
            self.delegate?.showError(error)
            self.delegate?.binder(self, didBindWithResult: Result(success: false, socialNet: type, error: error?.description))
        }
    }
}
