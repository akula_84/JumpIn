//
//  TabController.swift
//  JumpIn
//
//  Created by Admin on 27.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class TabController: BaseViewController {
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var scopeControl: UISegmentedControl!
    //
    private(set) var viewControllers: [UIViewController] = []
    private(set) var selectedControllerIndex: Int!
    private(set) weak var selectedController: UIViewController!
    var showScopeControl: Bool = true { didSet { self.scopeControl?.isHidden = !showScopeControl } }
    var onChangeSelectedController: ((_ disAppearingController: UIViewController?, _ appearingController: UIViewController)->())?
    
    
    init() {
        super.init(nibName: String(describing: TabController.self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupScopeControl()
        setupSelectedControllerIfNeed()
    }
    
    private func setupScopeControl() {
        scopeControl.isHidden = !showScopeControl
        scopeControl.removeAllSegments()
        viewControllers.enumerated().forEach { (index, controller) in
            self.scopeControl.insertSegment(withTitle: controller.title, at: index, animated: false)
        }
    }
    
    private func setupSelectedControllerIfNeed() {
        if let index = selectedControllerIndex {
            setSelected(controllerIndex: index)
        }
        else if !viewControllers.isEmpty {
            setSelected(controllerIndex: 0)
        }
    }
    
    @IBAction private func selectedScopeDidChange() {
        let i = scopeControl.selectedSegmentIndex
        onChangeSelectedController?(selectedController, viewControllers[i])
        setSelected(controllerIndex: i, animated: true)
    }
    
    func setSelected(controllerIndex i: Int, animated: Bool = false, completion:(()->())? = nil) {
        setSelected(viewController: viewControllers[i], animated: animated)
    }
    
    func setSelected(viewController: UIViewController, animated: Bool = false, completion:(()->())? = nil) {
        if let index = viewControllers.index(where: { $0 === viewController }) {
            guard self.isViewLoaded else {
                selectedControllerIndex = index
                return
            }
            show(subcontroller: viewController, animated: animated, completion: { [weak self] in
                guard let `self` = self else { return }
                self.selectedController = viewController
                self.selectedControllerIndex = index
                if self.scopeControl.selectedSegmentIndex != index { self.scopeControl.selectedSegmentIndex = index }
            })
        }
        else { fatalError("you should add controller before set it selected") }
    }
    
    private func show(subcontroller: UIViewController, animated: Bool, completion: (()->())?) {
        if animated { subcontroller.view.alpha = 0 }
        self.contentView.addSubview(subcontroller.view)
        subcontroller.view.frame = contentView.bounds
        let _completion = { [weak self] in
            self?.selectedController?.view?.removeFromSuperview()
            self?.selectedController?.removeFromParentViewController()
            self?.addChildViewController(subcontroller)
            subcontroller.didMove(toParentViewController: self)
            completion?()
        }
        if animated {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.selectedController?.view?.alpha = 0
                subcontroller.view.alpha = 1
            }) { finished in
                guard finished else { return }
                _completion()
            }
        }
        else { _completion() }
    }
    
    func add(subcontroller: UIViewController) {
        viewControllers.append(subcontroller)
        if isViewLoaded {
            scopeControl.insertSegment(withTitle: subcontroller.title, at: scopeControl.numberOfSegments, animated: false)
        }
    }
    
    func add(subcontrollers: [UIViewController]) {
        subcontrollers.forEach({ self.add(subcontroller: $0) })
    }
}
