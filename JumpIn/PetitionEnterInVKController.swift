//
//  PetitionEnterInVKController.swift
//  JumpIn
//
//  Created by Admin on 20.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class PetitionEnterInVKController: BaseViewController, SocNetBinderDelegate {
    
    
    @IBOutlet weak var enterVKButton: SocialNetVkButton!
    var binder: SocNetBinder!
    

    init() {
        super.init(nibName: "PetitionEnterInVKController", bundle: nil)
        title = NSLoc("invite")
        binder = SocNetBinder(delegate: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterVKButton.onClick = { [weak self] _ in
            self?.binder.bindVk()
        }
    }
    
    func binder(_ binder: SocNetBinder, didBindWithResult result: SocNetBinder.Result) {
        if result.success || result.error == NSLoc("the_social_network_is_already_tied") {
            var viewControllers = navigationController!.viewControllers
            viewControllers.removeLast()
            viewControllers.append(InviteFriendsToAppController())
            navigationController?.setViewControllers(viewControllers, animated: true)
        }
        else { showError(withDescription: result.error) }
    }
    
}
