//
//  ImageCacheService.swift
//  JumpIn
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class ImageCacheService {

    private(set) static var shared = ImageCacheService()
    private var cache = ImageCache()
    
    
    private init() {
        cache.name = "ImageCacheService"
    }
    
    func image(forLink link: String) -> UIImage? {
        return cache.object(forKey: link as NSString)
    }
    
    func set(image: UIImage, forLink link: String, dataLength: Int) {
        cache.setObject(image, forKey: link as NSString, cost: dataLength)
    }
    
    func clearCache() {
        cache.removeAllObjects()
    }
}
