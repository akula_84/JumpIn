//
//  MyLastEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class MyLastEventsFeedController: TabController {
    var myEventsController: MyLastEventsFeedTableSubcontroller!
    var confirmedEventsController: MyLastEventsFeedTableSubcontroller!

    override init() {
        //
        super.init()
        self.title = NSLoc("pastEvents")
        myEventsController = MyLastEventsFeedTableSubcontroller(eventsType: .my)
        confirmedEventsController = MyLastEventsFeedTableSubcontroller(eventsType: .confirmed)
        self.add(subcontrollers: [myEventsController, confirmedEventsController])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
    }
    
    private func setupNavBarButtons() {
        let filterBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_filter"), style: .plain, target: self, action: #selector(filterAction))
        navigationItem.rightBarButtonItem = filterBarButton
    }
    
    @objc private func filterAction(sender: UIBarButtonItem) {
        var model: GetEventsRequestModel!
        if selectedController == myEventsController {
            model = myEventsController.loader.requestModel
        } else {
            model = confirmedEventsController.loader.requestModel
        }
        navigationController?.pushViewController(EventsFilterController(withModel: model, filterButton: sender), animated: true)
    }
}
