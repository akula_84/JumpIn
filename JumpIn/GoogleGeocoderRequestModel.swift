//
//  GoogleGeocoderRequestModel.swift
//  JumpIn
//
//  Created by Admin on 20.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class GoogleGeocoderRequestModel: Mappable {
    
    var address: String!
    var apiKey = "AIzaSyBCZ7qeP1iwIuZzBC3Y2lmxSrOneFjXW-s"
    
    required init?(map: Map) { }
    func mapping(map: Map) {
        address <- map["address"]
        apiKey <- map["key"]
    }
    
    init(with address: String) {
        self.address = address
    }
}
