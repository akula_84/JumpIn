//
//  DetailedEventModel.swift
//  JumpIn
//
//  Created by Admin on 22.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class DetailedEventOwnerModel: SmallUserModel {
    var city: String!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        city <- map["city"]
    }
}


class DetailedEventModel: EventModel {
    var owner: DetailedEventOwnerModel!
    var members: ShortUserModelsPage!
    var date: String!
    var description: String?
    var address: String!
    var photos: [PhotoWithLikesModel]!
    var sex: SexModel?
    var shareText: String?
    var isUserInvited: Bool!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        owner <- map["user"]
        members <- map["users"]
        date <- map["date_time"]
        description <- map["description"]
        address <- map["location"]
        photos <- map["photos"]
        sex <- map["gender"]
        shareText <- map["share"]
        isUserInvited <- map["is_current_user_in"]
    }
}
