//
//  Authorization.swift
//  JumpIn
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class JsonRpcAuthorizationRequestTokenPayloadModel: JsonRpcRequestPayloadModel {
    var token: String!

    required init?(map: Map) {
        super.init(map: map)
    }

    init(token: String) {
        self.token = token
        super.init() 
    }

    override func mapping(map: Map) {
        token <- map["token"]
    }
}

class JsonRpcAuthorizationRequestModel: JsonRpcRequestModel<JsonRpcAuthorizationRequestTokenPayloadModel> {
    
    override var method: String! { get { return "send_token" } set { } }
}

class JsonRpcAuthorizationResponseModel: JsonRpcResponseModel<Int> {}
