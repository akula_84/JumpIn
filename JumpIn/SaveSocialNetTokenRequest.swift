//
//  SaveSocialNetTokenRequest.swift
//  JumpIn
//
//  Created by Admin on 27.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class SaveSocialNetTokenRequest: Mappable {
//
    var token: String!
    
    init(withToken token:String) {
        //
        self.token = token
    }
    
    required init(map:Map) {}
    
    func mapping(map: Map) {
        //
        token <- map["token"]
    }
}
