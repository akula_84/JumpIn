//
//  GetDetailedEventRequestModel.swift
//  JumpIn
//
//  Created by Admin on 18.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class GetDetailedEventRequestModel {
    
    var eventId: Int
    init(withEventId id: Int) {
        eventId = id
    }
}
