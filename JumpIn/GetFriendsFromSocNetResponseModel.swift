//
//  GetFriendsFromSocNetResponseModel.swift
//  JumpIn
//
//  Created by Admin on 23.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class GetFriendsFromSocNetResponseModel: PageModel<GetFriendsFromSocNetResponseModelItem> { }

class GetFriendsFromSocNetResponseModelItem: ModelProtocol {
    
    var socNetUserId: Int?
    var name: String!
    var avatarLink: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        socNetUserId <- map["id"]
        name <- map["name"]
        avatarLink <- map["avatar"]
    }
}
