//
//  MapAddressController.swift
//  JumpIn
//
//  Created by IvanLazarev on 14/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import GoogleMaps
import PKHUD
import GooglePlaces


class MapAddressController: BaseViewController, CLLocationManagerDelegate {

    @IBOutlet var mapView: GMSMapView!

    var locationManager = CLLocationManager()
    var didChangeAddress: ((String) -> ())?
    private(set) var city: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeTheLocationManager()
        setupMapView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.title = NSLoc("enter_the_address_of_the_event")
    }

    func initializeTheLocationManager() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager,      didUpdateLocations locations: [CLLocation]) {
        let location = locationManager.location?.coordinate
        cameraMoveToLocation(toLocation: location)
        manager.stopUpdatingLocation()
    }

    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if toLocation != nil {
            mapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 15)
        }
    }

    func setupMapView() {
        mapView.delegate = self
        let camera = GMSCameraPosition.camera(withLatitude: 55.749792, longitude: 37.632495, zoom: 12.0)
        mapView.isMyLocationEnabled = true
        let update = GMSCameraUpdate.setCamera(camera)
        mapView.moveCamera(update)
    }
}

extension MapAddressController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.map = mapView
        HUD.show(.progress)
        GMSGeocoder().reverseGeocodeCoordinate(position) { response, error in
            if let result = response?.firstResult(), result.thoroughfare?.isEmpty == false {
                HUD.flash(.success)
                self.navigationItem.title = result.thoroughfare
                if let address = result.thoroughfare {
                    self.city = result.locality
                    self.didChangeAddress?(address)
                }
            } else {
                if let error = error {
                    print("Error reverse geocode: \(error)")
                }
                mapView.clear()
                HUD.flash(.error)
            }
        }
    }
}
