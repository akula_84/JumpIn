//
//  MyLastEventsFeedController.swift
//  JumpIn
//
//  Created by Admin on 27.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import PKHUD
import SwipeCellKit


class MyOwnEventsFeedController: EventsFeedController {
    
    init() {
        super.init(withEventsType: .myOwnCurrent)
        self.title = NSLoc("mys")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func tableViewManager(_ manager: AnyObject, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let archive = SwipeAction(style: .default, title: nil, handler: { [weak self] _, _ in
            guard let `self` = self else { return }
            HUD.show(.progress)
            let index = self.tableManager.indexOfDataSourceItem(forCellAt: indexPath)
            ServerApi.shared.archiveEvent(withId: self.loader.model.models[index].id, completion: {
                self.loader.reloadData(animated: false, refreshController: false, completion: { (success, error) in
                    HUD.hide()
                    guard success else { return }
                    self.updateTableManager()
                    self.tableManager.deleteRow(at: indexPath)
                })
                }, errorBlock: { error, _ in
                    self.showError(error)
            })
        })
        archive.image = #imageLiteral(resourceName: "ic_archive")
        archive.backgroundColor = #colorLiteral(red: 0.8862745098, green: 0.7176470588, blue: 0.2470588235, alpha: 1)
        return [archive]
    }
}
