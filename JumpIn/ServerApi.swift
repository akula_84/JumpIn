//
//  ServerApi.swift
//  JumpIn
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import Foundation
import ObjectMapper
import FBSDKLoginKit


class ServerApi: MainURLEngineDelegate {
    
    private class GetEventsJsonParser: JsonObjectParser<EventPageModel> {
        override func parse(_ json: Any, parseEnd: (() -> Void)?) {
            if let jsonDict = json as? [String:Any], let responseModel = GetEventsResponseModel(JSON:jsonDict) {
                DispatchQueue.main.async { [weak self] in
                    self?.completion(responseModel.page)
                    if let parseEnd = parseEnd { parseEnd() }
                }
            }
            else {
                super.parse(json, parseEnd: parseEnd)
            }
        }
    }
    
    static let shared = ServerApi()
    private let urlEngine = MainURLEngine(with: "http://jump.softwarecenter.ru/api")
    private init() { urlEngine.delegate = self }
    
    
    func process(request: URLRequest, method: String)->URLRequest {
        var request = request
        if let token = UserInfoService.shared.token {
            if method != "callback/facebook" && method != "callback/vkontakte" {
                request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }
        return request
    }
    
    
    private func send(request model:Mappable?, method:String, parser:JsonParser? = nil, completionForRawJson:((Any)->Void)? = nil, completionForEmptyResponse:(()->Void)? = nil, shouldUseHttpMethodPost:Bool = false, errorBlock:RequestErrorBlockType?) {
        urlEngine.send(request: model, method: method, parser: parser, completionForRawJson: completionForRawJson, completionForEmptyResponse: completionForEmptyResponse, shouldUseHttpMethodPost: shouldUseHttpMethodPost, errorBlock: errorBlock)
    }
    
    private func send<T: Mappable>(request model: [T]?, method: String, parser: JsonParser? = nil, completionForRawJson: ((Any)->Void)? = nil, completionForEmptyResponse: (()->Void)? = nil, shouldUseHttpMethodPost: Bool = false, errorBlock: RequestErrorBlockType?) {
        urlEngine.send(request: model, method: method, parser: parser, completionForRawJson: completionForRawJson, completionForEmptyResponse: completionForEmptyResponse, shouldUseHttpMethodPost: shouldUseHttpMethodPost, errorBlock: errorBlock)
    }
    
    
    func authorize(withSocialNetToken token:String, type:SocialNetType, completion:((_ token:String)->())?, errorBlock:RequestErrorBlockType?) {
        if UserInfoService.shared.restoreTokenFromLastSession() {
            _logout { [unowned self] in
                self._authorize(withSocialNetToken: token, type: type, completion: completion, errorBlock: errorBlock)
            }
        }
        else { _authorize(withSocialNetToken: token, type: type, completion: completion, errorBlock: errorBlock) }
    }
    
    
    private func _authorize(withSocialNetToken token:String, type:SocialNetType, completion:((_ token:String)->())?, errorBlock:RequestErrorBlockType?) {
        var method: String!
        switch type {
        case .Fb: method = "callback/facebook"
        case .Vk: method = "callback/vkontakte"
        case .Instagram: method = "callback/instagram"
        }
        let parser = JsonObjectParser { [weak self] (responseModel: AuthorizeResponseModel) in
            UserInfoService.shared.token = responseModel.jwtoken
            if let deviceToken = (UIApplication.shared.delegate as? AppDelegate)?.deviceToken { self?.sendPushToken(deviceToken) }
            completion?(responseModel.jwtoken)
        }
        send(request: SaveSocialNetTokenRequest(withToken:token), method: method, parser: parser, errorBlock: errorBlock)
    }
    
    
    func getStatus(completion:((_ status:GetStatusResponseModel.UserAuthorizationStatus)->())?, errorBlock:RequestErrorBlockType?) {
        //
        let parser = JsonObjectParser { (responseModel: GetStatusResponseModel) in
            completion?(responseModel.status)
        }
        send(request: nil, method: "status", parser: parser, errorBlock: errorBlock)
    }
    
    
    func sendSmsCode(to phone:String, completion:(()->())?, errorBlock:RequestErrorBlockType?) {
        //
        send(request: SendSmsCodeRequestModel(withPhone:phone), method: "code/send", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    
    func check(smsCode code: String, completion:(()->())?, errorBlock:RequestErrorBlockType?) {
        //
        send(request: CheckSmsCodeRequestModel(withCode:code), method: "code/check", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func getProfile(completion:((_ profile: GetProfileResponseModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { (responseModel: GetProfileResponseModel) in
            let info = UserInfoService.shared
            info.userId = responseModel.id
            info.city = responseModel.city
            info.avatarLink = responseModel.avatar?.filepath
            info.isPremium = responseModel.premium
            completion?(responseModel)
        }
        send(request: nil, method: "profile", parser: parser, errorBlock: errorBlock)
    }
    
    //interface for data loader
    func getProfile(with _: EmptyModel, completion:((_ profile: GetProfileResponseModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        getProfile(completion: completion, errorBlock: errorBlock)
    }

    func updateProfile(status: String, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: UpdateStatusRequestModel(withStatus: status), method: "profile/update/status", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func updateProfile(with model: UpdateProfileRequestModel, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "profile/update", completionForEmptyResponse: {
            self.getProfile(completion: { _ in completion?() }, errorBlock: errorBlock)
        }, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }

    func getEvents(with model: GetEventsRequestModel, completion: ((EventPageModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = GetEventsJsonParser { (responseModel: EventPageModel) in
            completion?(responseModel)
        }
        send(request: model, method: "events", parser: parser, errorBlock: errorBlock)
    }

    func getEventCategoryList(with model: GetEventCategoriesRequestModel, completion: ((GetEventsCategoriesResponseModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { (responseModel: GetEventsCategoriesResponseModel) in
             completion?(responseModel )
        }
        send(request: model, method: "events/categories", parser: parser, errorBlock: errorBlock)
    }
    
    func getUserInfo(with model: GetUserInfoRequestModel, completion: ((GetUserInfoResponseModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { (responseModel: GetUserInfoResponseModel) in
            if let isPremium = responseModel.userInfo.isPremium {
                UserInfoService.shared.isPremium = isPremium
            }
            completion?(responseModel)
        }
        let userId = model.userId
        let method = userId != nil ? String(describing: userId!) : "me"
        send(request: nil, method: "users/\(method)", parser: parser, errorBlock: errorBlock)
    }
    
    func getFriends(with model: GetFriendsRequestModel, completion: ((ShortUserModelsPage)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { (responseModel: ShortUserModelsPage) in
            completion?(responseModel)
        }
        send(request: model, method: "friends", parser: parser, errorBlock: errorBlock)
    }

    func friendsAdd(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "friends/add/\(id)", completionForEmptyResponse: completion, shouldUseHttpMethodPost: false, errorBlock: errorBlock)
    }

    func friendsRemove(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "friends/remove/\(id)", completionForEmptyResponse: completion, shouldUseHttpMethodPost: false, errorBlock: errorBlock)
    }

    func acceptFriends(withId userId: Int, completion:(()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "friends/request/\(userId)?t=accept", completionForEmptyResponse: completion, errorBlock: errorBlock)
    }

    func denyFriends(withId userId: Int, completion:(()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "friends/request/\(userId)?t=deny", completionForEmptyResponse: completion, errorBlock: errorBlock)
    }

    func getDetailedEvent(with model: GetDetailedEventRequestModel, completion:((DetailedEventModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { (responseModel: DetailedEventModel) in
            completion?(responseModel)
        }
        send(request: nil, method: "events/\(model.eventId)", parser: parser, errorBlock: errorBlock)
    }

    func storeEvent(with model: StoreEventRequestModel, completion: ((Int)->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "events/store", completionForRawJson: {
            completion?(($0 as! [String:Any])["id"] as! Int)
        }, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }

    func joinToEvent(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "events/\(id)/join", completionForEmptyResponse: completion, errorBlock: errorBlock)
    }

    func vote(withModel model: VoteRequestModel, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "votes", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func leaveEvent(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "events/\(id)/leave", completionForEmptyResponse: completion, errorBlock: errorBlock)
    }
    
    func deletePhoto(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "photos/delete/\(id)", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func uploadPhotos(withModel model: PhotoUploadRequestModel, completion: (([Int])->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "photos/upload", completionForRawJson: {
            let responseArray = $0 as! [Int]
            completion?(responseArray)
        }, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }

    func setAvatar(withModel model: SetAvatarRequestModel, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "photos/avatar", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func getEventMembers(withModel model: GetEventMembersRequestModel, completion: ((ShortUserModelsPage)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { model in completion?(model) }
        send(request: model.submodel, method: "events/\(model.eventId)/users", parser: parser, errorBlock: errorBlock)
    }

    func inviteUser(withId userId: Int, toEventWithId eventId: Int, completion:(()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "events/\(eventId)/invite/\(userId)", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }

    func acceptUser(withId userId: Int, toJoinToEventWithId eventId: Int, completion:(()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "events/\(eventId)/accept/\(userId)", completionForEmptyResponse: completion, errorBlock: errorBlock)
    }
    
    func removeMember(withId userId: Int, fromEventWithId eventId: Int, completion:(()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "events/\(eventId)/remove/\(userId)", completionForEmptyResponse: completion, errorBlock: errorBlock)
    }
    
    func bindSocialNet(withModel model: BindSocialNetRequestModel, completion:(()->Void)?, errorBlock: RequestErrorBlockType?) {
        let method: String
        switch model.socialNetType! {
        case .Fb: method = "bind/facebook"; break
        case .Vk: method = "bind/vkontakte"; break
        case .Instagram: method = "bind/instagram"; break
        }
        send(request: model.tokenModel, method: method, completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func getPhotos(withModel model: GetPhotosRequestModel, completion: ((PhotoWithLikesModelsPage) -> Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { completion?($0) }
        let method = "\(model.type.rawValue)/\(model.id)/photos?page=\(model.page)"
        send(request: nil, method: method, parser: parser, errorBlock: errorBlock)
    }
    
    func getDialogList(withModel model: GetDialogListRequestModel, completion: ((DialogListItemModelPage) -> Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { completion?($0) }
        send(request: model, method: "messages/users", parser: parser, errorBlock: errorBlock)
    }
    
    func getDialogMessages(withModel model: GetDialogMessagesRequestModel, completion: ((DialogMessagesPage) -> Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { completion?($0) }
        let method = "messages/users/\(model.userId)?page=\(model.page)"
        send(request: nil, method: method, parser: parser, errorBlock: errorBlock)
    }
    
   static  func logout(completion: EmptyBlock?) {
        shared._logout(completion: completion)
    }
    
    private func _logout(completion: EmptyBlock?) {
        VKApi.shared.logout()
        InstagramApi.shared.logout()
        FBSDKLoginManager().logOut()
        if let token = (UIApplication.shared.delegate as! AppDelegate).deviceToken {
            send(request: LogoutRequestModel(withDeviceToken: token), method: "logout",
                 completionForEmptyResponse: { UserInfoService.shared.clear(saveToken: false); completion?() },
                 shouldUseHttpMethodPost: true,
                 errorBlock: { _,_ in
                    completion?()
            })
        }
    }
    
    static func destroySocials() {
        print("destroySocials")
        if let token = (UIApplication.shared.delegate as! AppDelegate).deviceToken {
            shared.send(request: LogoutRequestModel(withDeviceToken: token), method: "users/destroysocials",
                 completionForEmptyResponse: { },
                 shouldUseHttpMethodPost: true,
                 errorBlock: { _,_ in
                    
            })
        }
    }
    
    func makeProfilePremium(completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "profile/update/premium", completionForEmptyResponse: {
            UserInfoService.shared.isPremium = true
            completion?()
        }, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func getFriendsFromSocNet(with model: GetFriendsFromSocNetRequestModel, completion: ((GetFriendsFromSocNetResponseModel) -> Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { completion?($0) }
        send(request: model, method: "friends/social", parser: parser, errorBlock: errorBlock)
    }
    
    func sendInviteToApp(with model: [SendInviteToAppRequestModelItem], completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "deep_link/send", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func setInviterToApp(with model: SetInviterToAppRequestModel, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "deep_link/referred", shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func makeEventPremium(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "events/\(id)/premium", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func getUnreadMessagesCount(completion: ((GetUnreadMessagesResponseModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { completion?($0) }
        send(request: nil, method: "messages/unread", parser: parser, errorBlock: errorBlock)
    }
    
    func markAsReadMessagesOf(userWithId id: Int) {
        send(request: nil, method: "messages/users/\(id)/read", shouldUseHttpMethodPost: true, errorBlock: nil)
    }
    
    func removeConversation(with userId: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "messages/users/\(userId)/remove", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func sendPushToken(_ token: String) {
        send(request: SendPushTokenRequestModel(token: token), method: "device", shouldUseHttpMethodPost: true, errorBlock: nil)
    }
    
    func archiveEvent(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "events/\(id)/archive", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func getEventVideos(with model: GetEventVideosRequestModel, completion: ((VideoPageModel)->Void)?, errorBlock: RequestErrorBlockType?) {
        let parser = JsonObjectParser { completion?($0) }
        send(request: nil, method: "events/\(model.eventId)/videos?per_page=\(model.numberOfVideosOnSinglePage)&page=\(model.page)", parser: parser, errorBlock: errorBlock)
    }
    
    func uploadEventVideo(with model: UploadEventVideoRequestModel, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model.videoDataModels, method: "events/\(model.eventId)/videos",
            completionForRawJson: { _ in completion?() }, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func removeVideo(withId id: Int, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: nil, method: "videos/\(id)/delete", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
    
    func complain(withModel model: ComplainRequestModel, completion: (()->Void)?, errorBlock: RequestErrorBlockType?) {
        send(request: model, method: "frauds/store", completionForEmptyResponse: completion, shouldUseHttpMethodPost: true, errorBlock: errorBlock)
    }
}



extension Dictionary {
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let stringValue: String
            if let boolValue = value as? Bool { stringValue = boolValue ? "1" : "0" }
            else if let array = value as? [String] { stringValue = array.joined(separator: ",") }
            else { stringValue = String(describing: value) }
            let percentEscapedValue = stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        return parameterArray.joined(separator: "&")
    }
}
