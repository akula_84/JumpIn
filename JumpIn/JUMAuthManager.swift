//
//  JUMAuthManager.swift
//  JumpIn
//
//  Created by Артем Кулагин on 23.04.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import UIKit

class JUMAuthManager {
    
    static func checkStatus(_ failure: EmptyBlock? = nil) {
        guard UserInfoService.isHaveToken else {
            failure?()
            return
        }
        STRouter.showLoader()
        ServerApi.shared.getStatus(completion: {  status in
            ServerApi.shared.getProfile(completion: { (_) in
                STRouter.removeLoader()
                STRouter.gotoNext(withAuthorizationStatus: status)
            }, errorBlock: { (error, _) in
                STRouter.showError(error)
            })
        }, errorBlock: {  (error, _) in
            if error != nil && error!.description != nil && error!.description! == "Профиль не заполнен." {
                STRouter.gotoNext(withAuthorizationStatus: .NeedInfo)
            }
            else {
                STRouter.showError(error)
            }
        })
    }
    
    static func checkStatusInit() {
        checkStatus {
            showLicense()
        }
    }
    
    static func logOut() {
        STRouter.showLoader()
        ServerApi.logout(completion: {
            STRouter.removeLoader()
            STRouter.popToRootMain(false)
            showLicense()
        })
    }
    
    static func showLicense() {
        LicenseAgreementController.initialController?.pushMain(false)
    }
  
}
