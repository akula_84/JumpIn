//
//  SearchBarContentView.swift
//  JumpIn
//
//  Created by Admin on 21.07.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit

class SearchBarContentView: BaseCustomView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bottomSpacingLayout: NSLayoutConstraint!
    
    init(width: CGFloat, searchBar: UISearchBar, searchBarBottomSpacing: CGFloat) {
        //
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: 100))//searchBar.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height))
        searchBar.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bottomSpacingLayout.constant = searchBarBottomSpacing
        contentView.addSubview(searchBar)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
