//
//  Int+Utils.swift
//  JumpIn
//
//  Created by Артем Кулагин on 14.05.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import Foundation

extension Int {
    
    var eventText: String {
        var events_word = String()
        if self > 10, self < 20 {
            events_word = NSLoc("of_events")
        }
        else {
            switch self % 10 {
            case 1: events_word = NSLoc("event")
            case 2...4: events_word = NSLoc("oe_events")
            default: events_word = NSLoc("of_events")
            }
        }
        return self > 0 ? "\(self) \(events_word)" : NSLoc("no_events")
    }
}
