//
//  VoteRequestModel.swift
//  JumpIn
//
//  Created by IvanLazarev on 06/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper

class VoteRequestModel: Mappable {
    enum `Type`: String {
        case event, photo, video
    }

    var type: Type!
    var id: Int!
    var rating: Bool?

    required init?(map: Map) {}
    init(type: Type, id: Int, rating: Bool?) {
        self.type = type
        self.id = id
        self.rating = rating
    }

    func mapping(map: Map) {
        type <- map["type"]
        id <- map["id"]
        rating <- map["rating"]
    }
}

