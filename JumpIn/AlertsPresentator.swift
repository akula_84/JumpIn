//
//  AlertsPresentator.swift
//  JumpIn
//
//  Created by Admin on 16.11.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit
import MobileCoreServices


class AlertsPresentator {
    
    typealias BoolCompletion = (Bool)->()
    
    
    private class func boolAlert(title: String? = "", message: String? = "", okTitle: String = "OK", okStyle: UIAlertActionStyle = .default, cancelTitle: String = NSLoc("cancel"), style: UIAlertControllerStyle = .alert, completion: BoolCompletion?) -> UIAlertController {
        let ok = UIAlertAction(title: okTitle, style: okStyle, handler: { _ in completion?(true) })
        let cancel = UIAlertAction(title: cancelTitle, style: .cancel, handler: { _ in completion?(false) })
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(ok)
        alert.addAction(cancel)
        return alert
    }
    
    private class func simpleAlert(withTitle title: String = "", message: String, completion: (()->Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in completion?() })
        return alert
    }
    
    class func presentAlertExitWithoutSaving(in controller: UIViewController?, withCompletion completion: BoolCompletion?) {
        let alert = boolAlert(message: NSLoc("do_you_want_to_exit_without_saving_the_changes"), completion: completion)
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertAddUserToFriends(in controller: UIViewController?, withUserName name: String, completion: @escaping BoolCompletion) {
        let alert = boolAlert(message: NSLoc("add_User") + " \(name) " + NSLoc("in_friends_list"), completion: completion)
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertBindVK(in controller: UIViewController?, completion: @escaping BoolCompletion) {
        let alert = boolAlert(message: NSLoc("to_download_friends_from_Vkontakte"), completion: completion)
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertChoosePhotoSource(in controller: UIViewController?, completion: @escaping (UIImagePickerControllerSourceType)->Void) {
        let alert = UIAlertController(title: NSLoc("take_photo"), message: nil, preferredStyle: .actionSheet)
        let galery = UIAlertAction(title: NSLoc("take_photo_galary"), style: .default) { _ in
            completion(.photoLibrary)
        }
        let camera = UIAlertAction(title: NSLoc("take_photo_camera"), style: .default) { _ in
            completion(.camera)
        }
        let cancel = UIAlertAction(title: NSLoc("cancel"), style: .cancel, handler: nil)
        alert.addAction(galery)
        alert.addAction(camera)
        alert.addAction(cancel)
        controller?.present(alert, animated: true)
    }
    
    enum MessageAction {
        case delete, copy, complain
    }
    
    class func presentAlertMessageActions(in controller: UIViewController?, model: DialogMessageModel, completion: @escaping (MessageAction)->()) {
        let isMyMessage = model.senderId == UserInfoService.shared.userId
        let alert = UIAlertController(title: "", message: NSLoc("select_the_action_for_the_message") + " \"\(model.text!)\"?", preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: NSLoc("remove"), style: .destructive) { _ in completion(.delete) }
        alert.addAction(delete)
        if model.mediaType == .noMedia || model.text != model.mediaType.localizedString {
            let copy = UIAlertAction(title: NSLoc("copy"), style: .default) { _ in completion(.copy) }
            alert.addAction(copy)
        }
        if (!isMyMessage) {
            let complain = UIAlertAction(title: NSLoc("complain"), style: .default, handler: { _ in completion(.complain) })
            alert.addAction(complain)
        }
        let cancel = UIAlertAction(title: NSLoc("cancel"), style: .cancel, handler: nil)
        alert.addAction(cancel)
        controller?.present(alert, animated: true)
    }
    
    class func makeEventTopForThreeDays(in controller: UIViewController?, completion: @escaping BoolCompletion) {
        let alert = boolAlert(message: NSLoc("make_the_event_top_ranking_for_three_days"), completion: completion)
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertHereWillOpenAppStore(in controller: UIViewController?, completion: @escaping BoolCompletion) {
        let alert = boolAlert(message: NSLoc("here_you_will_open_the_AppStore_with_your_purchase"), completion: completion)
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertDeletingConversation(in controller: UIViewController?, userName: String, completion: @escaping BoolCompletion) {
        let alert = boolAlert(message: NSLoc("are_you_sure_that_you") + " \"\(userName)\" " + NSLoc("without_the_ability_to_restore_messages"), completion: completion)
        controller?.present(alert, animated: true)
    }
    
//    class func presentAlertChangeCityToSeeYourEvent(in controller: UIViewController?) {
//        controller?.present(simpleAlert(message: "Событие вы можете увидеть, сменив город в профиле"), animated: true)
//    }
    
    class func presentAlertCantSendMessage(in controller: UIViewController?, withError error: JsonRpcErrorModel.Error) {
        controller?.present(simpleAlert(message: NSLoc("failed_to_send_message") + " \(error.message!)"), animated: true)
    }
    
    class func presentAlertCantDeleteMessage(in controller: UIViewController?, withError error: JsonRpcErrorModel.Error) {
        controller?.present(simpleAlert(message: NSLoc("could_not_delete_the_message") + " \(error.message!)"), animated: true)
    }
    
    class func presentAlertVideoSource(in controller: UIViewController?, with completion: ((UIImagePickerControllerSourceType?)->Void)?) {
        let alert = UIAlertController(title: "", message: NSLoc("add_video"), preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: NSLoc("take_photo_camera"), style: .default) { _ in completion?(.camera) }
        let gallery = UIAlertAction(title: NSLoc("take_photo_galary"), style: .default) { _ in completion?(.photoLibrary) }
        let cancel = UIAlertAction(title: NSLoc("cancel"), style: .cancel) { _ in completion?(nil) }
        let actions = [camera, gallery, cancel]
        actions.forEach({ alert.addAction($0) })
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertUnavalaibleMediaSource(in controller: UIViewController?, completion: (()->Void)? = nil) {
        let alert = UIAlertController(title: "", message: NSLoc("media_source_is_not_available"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in completion?() }))
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertChooseGender(in controller: UIViewController?, completion: ((SexModel?)->Void)?) {
        let alert = UIAlertController()
        let boys = UIAlertAction(title: NSLoc("guys"), style: .default, handler: { _ in completion?(.Man) })
        let girls = UIAlertAction(title: NSLoc("girls"), style: .default, handler: { _ in completion?(.Woman) })
        let all = UIAlertAction(title: NSLoc("all"), style: .default, handler: { _ in completion?(nil) })
        let cancel = UIAlertAction(title: NSLoc("cancel"), style: .cancel, handler: nil)
        [boys, girls, all, cancel].forEach({ alert.addAction($0) })
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertChooseCategory(in controller: UIViewController?, completion: ((CategoryInfoService.CategoryInfo.Name)->Void)?) {
        let alert = UIAlertController()
        let cancel = UIAlertAction(title: NSLoc("cancel"), style: .cancel, handler: nil)
        alert.addAction(cancel)
        CategoryInfoService.categories.forEach({ arg in
            let (key, value) = arg
            alert.addAction(UIAlertAction(title: value.title, style: .default, handler: { _ in completion?(key) }))
        })
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertCantSaveVideo(in controller: UIViewController?) {
        controller?.present(simpleAlert(message: NSLoc("could_not_save_video")), animated: true)
    }
    
    class func presentAlertErrorWhileProcessingVideo(in controller: UIViewController?) {
        controller?.present(simpleAlert(message: NSLoc("the_video_processing_failed")), animated: true)
    }
    
    class func presentAlertTooSmallVideo(in controller: UIViewController?, completion: (()->Void)?) {
        let alert = UIAlertController(title: "", message: NSLoc("the_video_file_is_too_short"), preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { _ in completion?() }
        alert.addAction(ok)
        controller?.present(alert, animated: true)
    }
    
    class func presentAlertErrorWhileDeletingVideos(in controller: UIViewController?) {
        controller?.present(simpleAlert(message: NSLoc("failed_to_delete_one_or_more_video_files")), animated: true)
    }
    
    class func presentVideoPickerWithPreChooseSource(in controller: UIViewController?, presentationPresetBlock: ((UIImagePickerController?)->Void)?) {
        presentAlertVideoSource(in: controller) { source in
            guard let source = source else { presentationPresetBlock?(nil); return }
            guard UIImagePickerController.isSourceTypeAvailable(source) else {
                presentAlertUnavalaibleMediaSource(in: controller) {
                    presentVideoPickerWithPreChooseSource(in: controller, presentationPresetBlock: presentationPresetBlock)
                }
                return
            }
            let picker = UIImagePickerController()
            picker.sourceType = source
            picker.allowsEditing = false
            picker.mediaTypes = [kUTTypeMovie as String]
            presentationPresetBlock?(picker)
            controller?.present(picker, animated: true)
        }
    }
    
    class func presentAlertBindMailToSendMessage(in controller: UIViewController?) {
        controller?.present(simpleAlert(message: NSLoc("configure_the_mail_application")), animated: true)
    }
    
    class func presentAlertChangeCityToSeeEventInFeed(in controller: UIViewController?, withAddress address: String, completion: (()->Void)?) {
        controller?.present(simpleAlert(message: NSLoc("event_at") + " \(address) " + NSLoc("city_in_your_profile"), completion: completion), animated: true)
    }
    
    class func presentAlertChangeCityCanOnlyPremiumUser(in controller: UIViewController?) {
        controller?.present(simpleAlert(message: NSLoc("only_premium_users_can_change_the_city")), animated: true)
    }
    
    
    enum PhotoAction {
        case complain
    }
    
    class func presentActionsForPhoto(in controller: UIViewController?, completion: ((PhotoAction?)->Void)?) {
        let alert = UIAlertController(title: "", message: NSLoc("select_an_action"), preferredStyle: .actionSheet)
        let complain = UIAlertAction(title: NSLoc("complain"), style: .default, handler: nil)
        let cancel = UIAlertAction(title: NSLoc("cancel"), style: .cancel, handler: nil)
        alert.addAction(complain)
        alert.addAction(cancel)
        controller?.present(alert, animated: true)
    }
}
