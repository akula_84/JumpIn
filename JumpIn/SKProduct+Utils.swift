//
//  SKProduct+Utils.swift
//  JumpIn
//
//  Created by Артем Кулагин on 20.07.2018.
//  Copyright © 2018 SWC. All rights reserved.
//

import Foundation
import StoreKit

public extension SKProduct {
    
    public var localizedPrice: String? {
        return priceFormatter(locale: priceLocale).string(from: price)
    }
    
    private func priceFormatter(locale: Locale) -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.numberStyle = .currency
        return formatter
    }
}
