//
//  UpdateStatusRequestModel.swift
//  JumpIn
//
//  Created by IvanLazarev on 07/09/2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class UpdateStatusRequestModel: Mappable {

    var status: String!

    init(withStatus status:String) {
        self.status = status
    }

    required init(map:Map) {
    }

    func mapping(map: Map) {
        status <- map["status"]
    }
}
