//
//  ImageCacheService.swift
//  JumpIn
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class ImageCache: NSCache <NSString, UIImage> {

    var maxSizeForItem: Int = 2 * 1024 * 1024
    

    override init() {
        super.init()
        self.totalCostLimit = 30 * maxSizeForItem
    }
    
    override func setObject(_ obj: UIImage, forKey key: NSString, cost g: Int) {
        if (g <= maxSizeForItem) {
            super.setObject(obj, forKey: key, cost: g)
        }
    }
}
