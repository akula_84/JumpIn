//
//  FacebookAuthorizeController.swift
//  JumpIn
//
//  Created by Admin on 31.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import UIKit


class VKOAuthController: OAuthController<VKApi> {
    
    class func createIntoNavController(withCompletion completion: @escaping Completion) -> UINavigationController {
        let navController = BaseNavigationController(rootViewController: VKOAuthController(completion: completion))
        return navController
    }
    
    private init(completion: @escaping Completion) {
        super.init(helper: VKApi.shared, completion: completion)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class InstagramOAuthController: OAuthController<InstagramApi> {
    
    class func createIntoNavController(withCompletion completion: @escaping Completion) -> UINavigationController {
        let navController = BaseNavigationController(rootViewController: InstagramOAuthController(completion: completion))
        return navController
    }
    
    private init(completion: @escaping Completion) {
        super.init(helper: InstagramApi.shared, completion: completion)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class OAuthController<Helper: OAuthHelperProtocol>: BaseViewController, UIWebViewDelegate {
    
    typealias Completion = (String)->()
    
    let helper: Helper
    weak var webView: UIWebView!
    let completion: Completion
    
    init(helper: Helper, completion: @escaping Completion) {
        self.helper = helper
        self.completion = completion
        super.init(nibName: nil, bundle: nil)
        title = NSLoc(helper.oAuthModel.displayName)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
        setupNavBarButtons()
    }
    
    private func setupNavBarButtons() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction))
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshAction))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = refreshButton
    }
    
    @objc private func cancelAction() {
        dismiss(animated: true)
    }
    
    @objc private func refreshAction() {
        webView.reload()
    }
    
    private func setupWebView() {
        let webView = UIWebView()
        view.addSubview(webView)
        self.webView = webView
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        webView.delegate = self
        webView.loadRequest(helper.authorizationRequest())
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request)
        guard let link = request.url?.absoluteString, helper.isValidAuthorizationResult(link: link) else {
            return true
        }
        if helper.parseAuthorizationResult(link: link) {
            dismiss(animated: true) {
                self.completion(self.helper.token!)
            }
        }
        else {
            helper.clearCookieAndCache()
            showError(withDescription: helper.lastError)
        }
        return true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        helper.clearCookieAndCache()
        showError(withDescription: helper.lastError)
    }

}
