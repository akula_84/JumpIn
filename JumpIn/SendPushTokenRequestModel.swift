//
//  SendPushTokenRequestModel.swift
//  JumpIn
//
//  Created by Admin on 28.12.2017.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class SendPushTokenRequestModel: Mappable {
    
    var osType = "ios"
    var token: String!
    
    init(token: String) {
        self.token = token
    }
    
    required init?(map: Map) {  }
    
    func mapping(map: Map) {
        osType <- map["type"]
        token <- map["token"]
    }
}
