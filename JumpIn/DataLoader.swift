//
//  Loader.swift
//  JumpIn
//
//  Created by Admin on 13.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import PKHUD
import ObjectMapper


protocol ModelProtocol: class, Mappable { }

protocol ViewModelProtocol: class {
    associatedtype ModelType: ModelProtocol
    init(withModel model: ModelType)
}

protocol ViewControllerProtocol: class {
    var view: UIView! { get }
}

protocol Configurable: class {
    associatedtype ViewModelType: ViewModelProtocol
    var viewModel: ViewModelType! { get set }
    func refresh()
}

protocol ConfigurableList: Configurable where ViewModelType: PageViewModelProtocol { }


protocol ConfigurableControllerProtocol: Configurable, ViewControllerProtocol { }
protocol ConfigurableListControllerProtocol: ConfigurableControllerProtocol, ConfigurableList { }


class BaseViewModel<ModelT: ModelProtocol>: ViewModelProtocol {
    required init(withModel model: ModelT) {}
    init() {}
}

class BaseConfigurableController<ViewModel: ViewModelProtocol>: BaseViewController, ConfigurableControllerProtocol {
    var viewModel: ViewModel!
    func refresh() {
        fatalError("method not implemented")
    }
}

class BaseConfigurableListController<ViewModel: PageViewModelProtocol>: BaseConfigurableController<ViewModel>, ConfigurableListControllerProtocol { }


enum ProgressViewArea {
    case full, withoutNavigationBar
}


protocol DataLoadingManaging {
    associatedtype ResponseModelT: Mappable
    associatedtype RequestModelT
    var model: ResponseModelT! { get }
    var requestModel: RequestModelT! { get }
    var progressViewArea: ProgressViewArea { get set }
    func set(requestModel: AnyObject?)
    func reloadData(animated: Bool, refreshController: Bool, handleError: Bool, completion:((Bool, MainURLEngine.Error?)->Void)?)
    init(withController controller: UIViewController, reloadDataFunc `func`: AnyObject)
}


class DataLoadingManager<RequestModel, Controller: ConfigurableControllerProtocol>: NSObject, DataLoadingManaging {
    
    typealias ViewModel = Controller.ViewModelType
    typealias ResponseModel = ViewModel.ModelType
    
    private(set) var model: ResponseModel!
    private var reloadDataFunc: (RequestModel, ((ResponseModel)->Void)?, RequestErrorBlockType?)->Void
    unowned var controller: Controller
    var requestModel: RequestModel!
    var progressViewArea: ProgressViewArea = .withoutNavigationBar
    
    
    required init(withController controller: UIViewController, reloadDataFunc `func`: AnyObject) {
        self.reloadDataFunc = `func` as! (RequestModel, ((ResponseModel)->Void)?, RequestErrorBlockType?)->Void
        self.controller = controller as! Controller
    }
    
    func reloadData(animated: Bool = true, refreshController: Bool = true, handleError: Bool = true, completion:((Bool, MainURLEngine.Error?)->Void)? = nil) {
        if animated {
            switch progressViewArea {
                case .full:
                    HUD.show(.progress, onView: controller.view)
                case .withoutNavigationBar:
                    HUD.show(.progress)
            }
        }
        reloadDataFunc(requestModel, { [weak self] model in
            if animated { HUD.hide() }
            self?.onLoad(model: model, refreshController: refreshController, completion: completion)
            }, { [weak self] error, _ in
                if handleError { self?.onError(error, animated: animated, completion: completion) }
                completion?(false, error)
        })
    }
    
    func onLoad(model: ResponseModel, refreshController: Bool, completion: ((Bool, MainURLEngine.Error?)->Void)?) {
        updateModel(withResponse: model)
        updateControllerViewModel()
        if refreshController { controller.refresh() }
        completion?(true, nil)
    }
    
    func onError(_ error: MainURLEngine.Error?, animated: Bool, completion: ((Bool, MainURLEngine.Error?)->Void)?) {
        completion?(false, error)
        if animated {
            if let baseController = controller as? BaseViewController {
                baseController.showError(error)
            }
            else {
                HUD.flash(.error)
            }
        }
    }
    
    func updateModel(withResponse response: ResponseModel) {
        model = response
    }
    
    func updateControllerViewModel() {
        controller.viewModel = ViewModel(withModel: model)
    }
    
    func set(requestModel: AnyObject?) {
        self.requestModel = (requestModel as! RequestModel)
    }
}


class PageDataLoadingManager<RequestModel: RequestModelWithPageProtocol, Controller: ConfigurableListControllerProtocol>: DataLoadingManager<RequestModel, Controller> {
    
    enum LoadingMode {
        case reload, addition
    }
    
    enum AdditionNewDataMode {
        case addToEnd, insertAtFirst
    }
    
    var mode = LoadingMode.reload
    var additionMode = AdditionNewDataMode.addToEnd
    var numberOfElementsInLastLoadedPage: Int = 0
    
    override func reloadData(animated: Bool = true, refreshController: Bool = true, handleError: Bool = true, completion: ((Bool, MainURLEngine.Error?) -> Void)? = nil) {
        mode = .reload
        if let model = requestModel { model.page = 1 }
        super.reloadData(animated: animated, refreshController: refreshController, handleError: handleError, completion: completion)
    }
    
    func loadNextPage(animated: Bool = true, completion:((Bool, MainURLEngine.Error?)->Void)? = nil) {
        mode = .addition
        if let model = requestModel { 
            model.page += 1
        }
        super.reloadData(animated: animated, completion: completion)
    }
    
    override func updateModel(withResponse response: ResponseModel) {
        numberOfElementsInLastLoadedPage = response.models.count
        if mode == .addition, let model = self.model, var models = model.models {
            if additionMode == .addToEnd {
                models.append(contentsOf: response.models)
            }
            else if additionMode == .insertAtFirst {
                models.insert(contentsOf: response.models, at: 0)
            }
            model.models = models
            model.currentPage = response.currentPage
            model.nextPageLink = response.nextPageLink
        }
        else { super.updateModel(withResponse: response) }
    }
}
