//
//  EmptyModel.swift
//  JumpIn
//
//  Created by Admin on 23.08.17.
//  Copyright © 2017 SWC. All rights reserved.
//

import ObjectMapper


class EmptyModel: ModelProtocol {
    init() {}
    required init?(map: Map) {}
    func mapping(map: Map) {}
}
